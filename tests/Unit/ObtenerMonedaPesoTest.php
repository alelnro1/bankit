<?php

namespace Tests\Unit;

use App\Classes\Factories\MonedasFactory;
use Tests\TestCase;

class ObtenerMonedaPesoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNombreTieneQueSerPeso()
    {
        $moneda = (new MonedasFactory())->create("Peso");

        $nombre_moneda = $moneda->getNombre();

        $this->assertTrue($nombre_moneda == "Peso");
    }
}
