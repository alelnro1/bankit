<?php

namespace Tests\Unit\CuentasBancarias;

use App\Classes\Factories\MonedasFactory;
use App\Http\Requests\AltaCuentaBancariaRequest;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Tests\TestCase;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AltaCuentaBancariaPesoCorrectaTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $request = new AltaCuentaBancariaRequest();

        factory(CuentaBancaria::class)->create();

        $this->assertDatabaseHas('cuentas_bancarias', $request->all());
    }
}
