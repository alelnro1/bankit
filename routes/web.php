<?php

Route::group(['prefix' => 'apertura-cuenta', 'namespace' => 'AperturaCuenta'], function () {
    include_once 'apertura-cuenta/auth.php';

    Route::group(['middleware' => 'auth:apertura-cuenta,backend'], function () {
        include_once 'apertura-cuenta/proceso.php';
    });
});


Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], function () {
    
    include_once 'backend/auth.php';

    Route::group(['middleware' => ['auth:backend','verificar-cambio-password-backend']], function () {
                Route::get('/', 'HomeController@index')->name('admin.dashboard');

                    
                    /* Cambiar Contraseña */
                    include_once 'backend/cambiar-password.php';



                    /* Cuentas Bancarias */
                    include_once 'backend/tesoreria/cuentas-bancarias.php';

                    /* Prospectos */
                    include_once 'backend/prospectos.php';

                    /* Comitentes */
                    include_once 'backend/comitentes.php';

                    /* Operaciones Pendientes */
                    include_once 'backend/operaciones.php';

                    /* ABM Fondos */
                    include_once  'backend/abm_fondos.php';

                    /* ABM Usuarios Internos */
                    include_once 'backend/abm_usuarios_internos.php';

    });

});

Route::group(['namespace' => 'Frontend'], function () {
    include_once 'frontend/auth.php';

    include_once 'frontend/autogestion.php';

    Route::group(['middleware' => 'auth'], function () {

        /* Seleccion del comitente con el cual vamos a operar */
        include_once 'frontend/seleccion-comitente.php';

        Route::group(['middleware' => ['comitente-seleccionado', 'verificar-cambio-password']], function () {

            Route::get('/', 'HomeController@index')->name('home');
            Route::get('/home', 'HomeController@index');
            Route::post('logout', 'Auth\LoginController@logout')->name('frontend.logout');

            Route::post('operaciones-pendientes', 'HomeController@getOperacionesPendientesDeFondoParaModal')->name('frontend.operaciones-pendientes-fondo');

            /* Cambiar Contraseña */
            include_once 'frontend/cambiar-password.php';

            /* Cuentas Bancarias */
            include_once 'frontend/tesoreria/cuentas-bancarias.php';

            /* Form Operar */
            include_once 'frontend/form-operar/datos-dinamicos.php';
            include_once 'frontend/form-operar/operaciones.php';

            /* Cotizaciones */
            include_once 'frontend/cotizaciones.php';
        });
    });
});