<?php

Route::post('cambiar-comitente', 'CambiarComitenteController@mostrarSeleccionComitentes')->name('comitentes.mostrar-seleccion');

Route::get('comitente', 'SeleccionComitenteController@seleccionarComitente')->name('comitentes.seleccionar.form');
Route::post('procesar-seleccion-comitente', 'SeleccionComitenteController@procesarSeleccion')->name('comitentes.seleccionar.procesar');