<?php

Route::group(['prefix' => 'cotizaciones'], function () {
    Route::get('fondos', 'CotizacionesController@fondos')->name('frontend.cotizaciones.fondos');

    Route::get('fondo/{fondo}', 'CotizacionesController@singleFondo')->name('frontend.cotizaciones.fondos.single');
});