<?php
Route::group(['namespace' => 'FormOperar'], function () {
    Route::post('operaciones-disponibles-instrumento', 'DatosDinamicosController@getTipoOperacionSegunInstrumento')->name('operaciones.disponibles.instrumentos');

    Route::post('instrumentos-por-tipo-operacion', 'DatosDinamicosController@getInstrumentosPorTipoOperacion')->name('operaciones.instrumento.instrumentos-por-operacion');

    Route::post('campos-por-instrumento', 'DatosDinamicosController@getCamposParaInfoPorInstrumento')->name('operaciones.instrumento.campos-para-ingresar');

    Route::post('plazos-monedas-por-instrumento', 'DatosDinamicosController@getPlazosYMonedasPorInstrumento')->name('operaciones.instrumento.plazos-monedas-por-instrumento');
    
    Route::post('cuentas-bancarias-por-moneda', 'DatosDinamicosController@getCuentasBancarias')->name('operaciones.instrumento.cuentas-bancarias-por-moneda');

    Route::post('procesar-operacion-instrumento', 'OperacionesController@procesarOperacion')->name('operaciones.instrumentos.procesar');
});