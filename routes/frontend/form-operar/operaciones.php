<?php

Route::group(['prefix' => 'operaciones', 'namespace' => 'FormOperar'], function () {
    
    Route::get('{operacion}/adjuntar-comprobante', 'OperacionesController@mostrarFormAdjuntarComprobante')->name('operaciones.form-adjuntar-comprobante');

    Route::post('{operacion}/adjuntar-comprobante', 'OperacionesController@adjuntarComprobante')->name('operaciones.adjuntar-comprobante');

    Route::get('{operacion}/cancelar-operacion', 'OperacionesController@cancelarOperacion')->name('operaciones.cancelar-operacion');

    Route::get('fondos-realizadas/{fecha_desde?}/{fecha_hasta?}','OperacionesController@getOperacionesRealizadas')
            ->name('frontend.fondos.operaciones-realizadas');

});