<?php

Route::group(['prefix' => 'cuentas-bancarias', 'namespace' => 'Tesoreria'], function (){
    
    Route::get('/', 'CuentasBancariasController@index')->name('tesoreria.cuentas-bancarias.index');
    Route::post('alta-cbu-alias', 'CuentasBancariasController@precargarDatosCuenta')->name('tesoreria.cuentas-bancarias.precargar-datos');
    Route::get('alta', 'CuentasBancariasController@alta')->name('tesoreria.cuentas-bancarias.alta');
    Route::post('alta', 'CuentasBancariasController@procesarFormAlta')->name('tesoreria.cuentas-bancarias.procesar');

    Route::get('desvincular-cuenta', 'CuentasBancariasController@desvincularCuenta')->name('tesoreria.cuentas-bancarias-desvincular');
});