<?php

Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('frontend.login');
    $this->post('login', 'LoginController@login')->name('frontend.login');
    //$this->post('logout', 'LoginController@logout')->name('frontend.logout');

    // Registration Routes...
    //$this->get('register', 'AuthController@showRegistrationForm');
    //$this->post('register', 'AuthController@register');

    // Password Reset Routes...
    $this->get('password-forgot', 'ForgotPasswordController@showLinkRequestForm')->name('frontend.password.forgot-form');
    $this->get('password/reset/{token?}', 'ResetPasswordController@showResetForm')->name('frontend.password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('frontend.password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');

});