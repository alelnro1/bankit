<?php

Route::get('password/cambiar', 'Auth\CambiarPasswordController@showChangePasswordForm')->name('frontend.password.cambiar.form');
Route::post('password/cambiar', 'Auth\CambiarPasswordController@cambiar')->name('frontend.password.cambiar.procesar');