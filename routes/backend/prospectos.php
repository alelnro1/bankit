<?php

/*Route::get('prospectos', 'ProspectosController@index')->name('backend.prospectos');*/

Route::get('prospectos',
				[	'middleware'=>'check-back-permission:Administrador|Prospectos',
					'uses'=>'ProspectosController@index'])
		->name('backend.prospectos');



Route::get('prospecto/{prospecto}',
				[	'middleware'=>'check-back-permission:Administrador|Prospectos',
					'uses'=>'ProspectosController@ver'])
		->name('backend.prospecto.ver');



Route::post('generar-comitente',
				[	'middleware'=>'check-back-permission:Administrador|Prospectos',
					'uses'=>'ProspectosController@generarComitente'])
		->name('backend.apertura-cuenta.generar-comitente');


Route::get('generar-pdf/{prospecto}/{comitente}',
				[	'middleware'=>'check-back-permission:Administrador|Prospectos',
					'uses'=>'ProspectosController@pruebaGenerarPDF']);






