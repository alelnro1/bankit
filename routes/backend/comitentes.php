<?php

Route::get('comitentes',
				[	'middleware'=>'check-back-permission:Administrador|Comitentes',
					'uses'=>'ComitentesController@index'])
		->name('backend.comitentes');


Route::get('comitentes/{comitente}',
				[	'middleware'=>'check-back-permission:Administrador|Comitentes',
					'uses'=>'ComitentesController@ver'])
		->name('backend.comitente.ver');