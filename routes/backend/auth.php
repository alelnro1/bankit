<?php

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', 'BackendLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'BackendLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'BackendLoginController@logout')->name('backend.logout');

    // Password Reset Routes...
    $this->get('password/reset/{token?}', 'ResetPasswordController@showLinkRequestForm')->name('backend.password.request');
    $this->post('password/email', 'PasswordController@sendResetLinkEmail')->name('backend.password.reset');
    $this->post('password/reset', 'PasswordController@reset');
});