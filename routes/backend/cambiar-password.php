<?php

Route::get('password/cambiar', 'Auth\CambiarPasswordController@showChangePasswordForm')->name('backend.password.cambiar.form');

Route::post('password/cambiar', 'Auth\CambiarPasswordController@cambiar')->name('backend.password.cambiar.procesar');