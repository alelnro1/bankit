<?php

Route::group(['prefix' => 'cuentas-bancarias', 'namespace' => 'Tesoreria'], function (){
    Route::get('/', 'CuentasBancariasController@index')->name('backend.tesoreria.cuentas-bancarias.index');

    Route::get('/solicitud/{cuenta}', 'CuentasBancariasController@cargarSolicitud')->name('backend.cuentas-bancarias.ver.solicitud');

    Route::post('/aceptar', 'CuentasBancariasController@aceptarSolicitud')->name('backend.cuentas-bancarias.aceptar');
    Route::post('/rechazar', 'CuentasBancariasController@rechazarSolicitud')->name('backend.cuentas-bancarias.rechazar');
});