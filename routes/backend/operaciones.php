<?php

Route::group(['prefix' => 'operaciones', 'namespace' => 'Operaciones'], function () {

    Route::get('fondos-aprobadas/{fecha?}', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@getOperacionesAprobadas'])
            ->name('backend.fondos.operaciones-aprobadas');

    Route::get('fondos-realizadas/{fecha_desde?}/{fecha_hasta?}', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@getOperacionesRealizadas'])
            ->name('backend.fondos.operaciones-realizadas');

    Route::get('fondos-pendientes-comprobante', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@getOperacionesPendientesComprobante'])
            ->name('backend.fondos.operaciones-pendientes-comprobante');    

    Route::get('fondos', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@getOperacionesPendientes'])
            ->name('backend.fondos.operaciones-pendientes');

    Route::post('cargar-operacion', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@cargarOperacionPendiente'])
            ->name('backend.fondos.cargar-operacion-pendiente');

    Route::post('operacion/procesar-ws', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@enviarOperacionAINTL'])
            ->name('backend.fondos.enviar-operacion-intl');

    Route::post('operacion/desaprobar', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@desaprobarOperacion'])
            ->name('backend.fondo-desaprobar-operacion');

    Route::get('exportar-pendientes-xls', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@exportarOperacionesPendientesXLS'])
            ->name('operaciones.exportar-pendientes-xls');

    Route::get('exportar-aprobadas-xls', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@exportarOperacionesAprobadasXLS'])
            ->name('operaciones.exportar-aprobadas-xls');

    Route::get('exportar-realizadas-xls/{fecha_desde?}/{fecha_hasta?}', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@exportarOperacionesRealizadasXLS'])
            ->name('operaciones.exportar-realizadas-xls');

    Route::get('exportar-aprobadas-txt', 
                    [   'middleware'=>'check-back-permission:Administrador|Operaciones',
                    'uses'=>'FondosController@exportarOperacionesAprobadasTXT'])
            ->name('operaciones.exportar-aprobadas-txt');

});