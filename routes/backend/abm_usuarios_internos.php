<?php

Route::group(['prefix' => 'usuarios-internos'], function () {

	Route::get('/',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@index'])
		->name('backend.interno.index');

	Route::get('nuevo',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@nuevo'])
		->name('backend.interno.nuevo');

	Route::post('nuevo',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@procesarAlta'])
		->name('backend.interno.nuevo.submit');
    

	Route::get('ver/{interno}',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@ver'])
		->name('backend.interno.ver');

	Route::get('modificar/{interno}',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@modificar'])
		->name('backend.interno.modificar');
    

	Route::post('modificar/{interno}',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@procesarModificacion'])
		->name('backend.interno.modificar.submit');

	Route::post('eliminar',
				[	'middleware'=>'check-back-permission:Administrador|Usuarios Internos',
					'uses'=>'UsuariosInternosController@eliminar'])
		->name('backend.interno.eliminar');


});