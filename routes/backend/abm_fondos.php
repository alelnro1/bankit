<?php

Route::group(['prefix' => 'fci'], function () {


	Route::get('/',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@index'])
		->name('backend.abm-fondos.index');


	Route::get('/exportar-cotizaciones-txt',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@exportarCotizacionesTXT'])
		->name('backend.fondos.exportar-cotizaciones-txt');


	Route::get('{fondo}',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@show'])
		->name('backend.abm-fondos.show');


	Route::get('{fondo}/edit',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@edit'])
		->name('backend.abm-fondos.edit');

    
	Route::post('{fondo}/edit',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@procesarEdit'])
		->name('backend.abm-fondos.procesar-edit');
    
	Route::post('/{fondo}/editar-composicion',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@cargarModalComposicionEdit'])
		->name('backend.abm-fondos.composicion.editar-item');

	Route::post('/{fondo/procesar-editar-composicion',
				[	'middleware'=>'check-back-permission:Administrador|Fondos',
					'uses'=>'FondosController@procesarComposicionEdit'])
		->name('backend.abm-fondos.composicion.procesar-edit');


});