<?php

Route::get('/', 'ProcesoAperturaController@index')->name('apertura-cuenta.proceso.paso-1');

Route::post('ultimo-paso', 'ProcesoAperturaController@registrarUltimoPaso')->name('apertura-cuenta.registrar-ultimo-paso');

Route::post('finalizar-proceso', 'ProcesoAperturaController@finalizarProceso')->name('apertura-cuenta.finalizar-proceso');
Route::get('finalizar-proceso-logout', 'ProcesoAperturaController@procesoLogout')->name('apertura-cuenta.finalizar-proceso.logout');

Route::group(['prefix' => 'titulares'], function () {
    Route::post('guardar', 'TitularesController@guardar')->name('apertura-cuenta.titulares.guardar');
    Route::post('eliminar', 'TitularesController@eliminar')->name('apertura-cuenta.titulares.eliminar');

    Route::post('consultar-por-documento', 'TitularesController@consultarPorDocumento')->name('apertura-cuenta.titulares.consultar-por-documento');
    Route::post('relacionar-titular-existente', 'TitularesController@relacionarTitularExistente')->name('apertura-cuenta.titulares.existente.relacionar');
});

Route::group(['prefix' => 'cuentas-bancarias'], function () {
    Route::post('buscar-cuenta', 'CuentasBancariasController@buscarCuenta')->name('apertura-cuenta.cuentas-bancarias.buscar');

    Route::post('guardar', 'CuentasBancariasController@guardar')->name('apertura-cuenta.cuentas-bancarias.guardar');
    Route::post('eliminar', 'CuentasBancariasController@eliminar')->name('apertura-cuenta.cuentas-bancarias.eliminar');
});

Route::group(['prefix' => 'datos-societarios'], function () {
    Route::post('guardar', 'DatosSocietariosController@guardar')->name('apertura-cuenta.datos-societarios.guardar');
});

Route::group(['prefix' => 'perfil'], function () {
    Route::post('guardar', 'PerfilController@guardar')->name('apertura-cuenta.perfil.guardar');

    Route::post('generar-pdf-tyc', 'ProcesoAperturaController@generarPDFTyC')->name('apertura-cuenta.generar-pdf');
});
