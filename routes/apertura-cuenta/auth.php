<?php

Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('apertura-cuenta.login');
    Route::post('login', 'LoginController@login')->name('apertura-cuenta.login.procesar');
    Route::post('logout', 'LoginController@logout')->name('apertura-cuenta.logout');

    // Registration Routes...
    Route::get('registro', 'RegisterController@showRegistrationForm')->name('apertura-cuenta.register');
    Route::post('registro', 'RegisterController@register')->name('apertura-cuenta.register.procesar');

    // Password Reset Routes...
    $this->get('password/reset/{token?}', 'ResetPasswordController@showLinkRequestForm')->name('apertura-cuenta.password.request');
    $this->post('password/email', 'PasswordController@sendResetLinkEmail')->name('apertura-cuenta.password.reset');
    $this->post('password/reset', 'PasswordController@reset');

    Route::get('password/cambiar', 'CambiarPasswordController@showChangePasswordForm')->name('apertura-cuenta.password.cambiar.form');
    
    Route::post('password/cambiar', 'CambiarPasswordcontroller@cambiar')->name('apertura-cuenta.password.cambiar.procesar');
});