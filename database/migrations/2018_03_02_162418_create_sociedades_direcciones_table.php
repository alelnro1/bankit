<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSociedadesDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sociedades_direcciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('calle')->nullable();
            $table->integer('altura')->nullable();
            $table->integer('piso')->nullable();
            $table->string('departamento')->nullable();
            $table->string('localidad')->nullable();
            $table->string('provincia')->nullable();
            $table->string('pais')->nullable();

            $table->integer('sociedad_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('sociedades_direcciones', function (Blueprint $table) {
            $table->foreign('sociedad_id')->references('id')->on('sociedades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sociedades_direcciones', function (Blueprint $table) {
            $table->dropForeign('sociedad_id');
        });

        Schema::dropIfExists('sociedades_direcciones');
    }
}
