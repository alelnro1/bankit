<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFuncionPep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropForeign('personas_funcion_pep_foreign');
            
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->string('funcion_pep')->nullable()->change();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->unsignedInteger('funcion_pep')->nullable()->change();
        });
    }
}
