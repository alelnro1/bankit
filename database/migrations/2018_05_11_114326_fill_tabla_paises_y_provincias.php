<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillTablaPaisesYProvincias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            INSERT INTO paises (descripcion,codigo) VALUES ('ARGENTINA','80');
            INSERT INTO paises (descripcion,codigo) VALUES ('ARGELIA','102');
            INSERT INTO paises (descripcion,codigo) VALUES ('REP.CTRO AFRICANA','107');
            INSERT INTO paises (descripcion,codigo) VALUES ('EGIPTO','113');
            INSERT INTO paises (descripcion,codigo) VALUES ('GHANA','117');
            INSERT INTO paises (descripcion,codigo) VALUES ('GUINEA','118');
            INSERT INTO paises (descripcion,codigo) VALUES ('KENYA','120');
            INSERT INTO paises (descripcion,codigo) VALUES ('MARRUECOS','127');
            INSERT INTO paises (descripcion,codigo) VALUES ('MOZAMBIQUE','151');
            INSERT INTO paises (descripcion,codigo) VALUES ('SUDAFRICA','159');
            INSERT INTO paises (descripcion,codigo) VALUES ('BOLIVIA','202');
            INSERT INTO paises (descripcion,codigo) VALUES ('REP. DEL BRASIL','203');
            INSERT INTO paises (descripcion,codigo) VALUES ('CANADA','204');
            INSERT INTO paises (descripcion,codigo) VALUES ('COLOMBIA','205');
            INSERT INTO paises (descripcion,codigo) VALUES ('COSTA RICA','206');
            INSERT INTO paises (descripcion,codigo) VALUES ('CUBA','207');
            INSERT INTO paises (descripcion,codigo) VALUES ('REPUBLICA DE CHILE','208');
            INSERT INTO paises (descripcion,codigo) VALUES ('REPUBLICA DOMINICANA','209');
            INSERT INTO paises (descripcion,codigo) VALUES ('ECUADOR','210');
            INSERT INTO paises (descripcion,codigo) VALUES ('EL SALVADOR','211');
            INSERT INTO paises (descripcion,codigo) VALUES ('ESTADOS UNIDOS','212');
            INSERT INTO paises (descripcion,codigo) VALUES ('GUATEMALA','213');
            INSERT INTO paises (descripcion,codigo) VALUES ('GUYANA','214');
            INSERT INTO paises (descripcion,codigo) VALUES ('HAITI','215');
            INSERT INTO paises (descripcion,codigo) VALUES ('HONDURAS','216');
            INSERT INTO paises (descripcion,codigo) VALUES ('JAMAICA','217');
            INSERT INTO paises (descripcion,codigo) VALUES ('MEXICO','218');
            INSERT INTO paises (descripcion,codigo) VALUES ('NICARAGUA','219');
            INSERT INTO paises (descripcion,codigo) VALUES ('PANAMA','220');
            INSERT INTO paises (descripcion,codigo) VALUES ('PARAGUAY','221');
            INSERT INTO paises (descripcion,codigo) VALUES ('REP. DEL PERU','222');
            INSERT INTO paises (descripcion,codigo) VALUES ('PUERTO RICO E.A','223');
            INSERT INTO paises (descripcion,codigo) VALUES ('TRINIDAD TOBAGO','224');
            INSERT INTO paises (descripcion,codigo) VALUES ('REP. ORIENTAL DEL URUGUAY','225');
            INSERT INTO paises (descripcion,codigo) VALUES ('VENEZUELA','226');
            INSERT INTO paises (descripcion,codigo) VALUES ('DOMINICA','233');
            INSERT INTO paises (descripcion,codigo) VALUES ('COREA DEL NORTE','308');
            INSERT INTO paises (descripcion,codigo) VALUES ('COREA DEL SUR','309');
            INSERT INTO paises (descripcion,codigo) VALUES ('CHINA','310');
            INSERT INTO paises (descripcion,codigo) VALUES ('FILIPINAS','312');
            INSERT INTO paises (descripcion,codigo) VALUES ('TAIWAN R.O.C.','313');
            INSERT INTO paises (descripcion,codigo) VALUES ('INDIA','315');
            INSERT INTO paises (descripcion,codigo) VALUES ('INDONESIA','316');
            INSERT INTO paises (descripcion,codigo) VALUES ('ISRAEL','319');
            INSERT INTO paises (descripcion,codigo) VALUES ('JAPON','320');
            INSERT INTO paises (descripcion,codigo) VALUES ('LIBANO','325');
            INSERT INTO paises (descripcion,codigo) VALUES ('REPUBLICA DE MALASIA','326');
            INSERT INTO paises (descripcion,codigo) VALUES ('REPUBLICA DE SINGAPUR','333');
            INSERT INTO paises (descripcion,codigo) VALUES ('SIRIA','334');
            INSERT INTO paises (descripcion,codigo) VALUES ('TAILANDIA','335');
            INSERT INTO paises (descripcion,codigo) VALUES ('VIETNAM','337');
            INSERT INTO paises (descripcion,codigo) VALUES ('HONG KONG - CHINA','341');
            INSERT INTO paises (descripcion,codigo) VALUES ('ARMENIA','349');
            INSERT INTO paises (descripcion,codigo) VALUES ('AUSTRIA','405');
            INSERT INTO paises (descripcion,codigo) VALUES ('BELGICA','406');
            INSERT INTO paises (descripcion,codigo) VALUES ('BULGARIA','407');
            INSERT INTO paises (descripcion,codigo) VALUES ('DINAMARCA','409');
            INSERT INTO paises (descripcion,codigo) VALUES ('ESPANA','410');
            INSERT INTO paises (descripcion,codigo) VALUES ('FINLANDIA','411');
            INSERT INTO paises (descripcion,codigo) VALUES ('FRANCIA','412');
            INSERT INTO paises (descripcion,codigo) VALUES ('GRECIA','413');
            INSERT INTO paises (descripcion,codigo) VALUES ('HUNGRIA','414');
            INSERT INTO paises (descripcion,codigo) VALUES ('IRLANDA DEL NORTE','415');
            INSERT INTO paises (descripcion,codigo) VALUES ('ITALIA','417');
            INSERT INTO paises (descripcion,codigo) VALUES ('LUXEMBURGO','419');
            INSERT INTO paises (descripcion,codigo) VALUES ('NORUEGA','422');
            INSERT INTO paises (descripcion,codigo) VALUES ('PAISES BAJOS (HOLANDA)','423');
            INSERT INTO paises (descripcion,codigo) VALUES ('POLONIA','424');
            INSERT INTO paises (descripcion,codigo) VALUES ('PORTUGAL','425');
            INSERT INTO paises (descripcion,codigo) VALUES ('REINO UNIDO DE GRAN BRETANA','426');
            INSERT INTO paises (descripcion,codigo) VALUES ('RUMANIA','427');
            INSERT INTO paises (descripcion,codigo) VALUES ('SUECIA','429');
            INSERT INTO paises (descripcion,codigo) VALUES ('SUIZA','430');
            INSERT INTO paises (descripcion,codigo) VALUES ('TERRITORIO VINC. REINO UNIDO','433');
            INSERT INTO paises (descripcion,codigo) VALUES ('TURQUIA','436');
            INSERT INTO paises (descripcion,codigo) VALUES ('ALEMANIA','438');
            INSERT INTO paises (descripcion,codigo) VALUES ('LETONIA','441');
            INSERT INTO paises (descripcion,codigo) VALUES ('RUSIA','444');
            INSERT INTO paises (descripcion,codigo) VALUES ('UCRANIA','445');
            INSERT INTO paises (descripcion,codigo) VALUES ('CROACIA','447');
            INSERT INTO paises (descripcion,codigo) VALUES ('ESLOVAQUIA REPUBLICA DE','448');
            INSERT INTO paises (descripcion,codigo) VALUES ('ESLOVENIA','449');
            INSERT INTO paises (descripcion,codigo) VALUES ('YUGOESLAVIA','452');
            INSERT INTO paises (descripcion,codigo) VALUES ('AUSTRALIA','501');
            INSERT INTO paises (descripcion,codigo) VALUES ('NUEVA ZELANDA','504');
            INSERT INTO paises (descripcion,codigo) VALUES ('ISLAS CAIMAN','903');


            INSERT INTO provincias (descripcion,codigo) VALUES ('CIUDAD AUTONOMA DE BUENOS AIRES','1');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE BUENOS AIRES','2');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE CATAMARCA','3');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE CORDOBA','4');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE CORRIENTES','5');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE CHACO','6');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE CHUBUT','7');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE ENTRE RIOS','8');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE FORMOSA','9');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE JUJUY','10');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE LA PAMPA','11');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE LA RIOJA','12');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE MENDOZA','13');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE MISIONES','14');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE NEUQUEN','15');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE RIO NEGRO','16');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SALTA','17');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SAN JUAN','18');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SAN LUIS','19');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SANTA CRUZ','20');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SANTA FE','21');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE SANTIAGO DEL ESTERO','22');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PROVINCIA DE TUCUMAN','23');
            INSERT INTO provincias (descripcion,codigo) VALUES ('PCIA. DE T. DEL FUEGO, ANTAR. E I.','24');

        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
