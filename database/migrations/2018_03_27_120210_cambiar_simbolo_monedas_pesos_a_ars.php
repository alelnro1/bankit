<?php

use App\Models\Monedas\MonedaBase;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiarSimboloMonedasPesosAArs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $moneda_pesos = MonedaBase::where('descripcion', 'Pesos')->first();

        $moneda_pesos->simbolo = "ARS";
        $moneda_pesos->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
