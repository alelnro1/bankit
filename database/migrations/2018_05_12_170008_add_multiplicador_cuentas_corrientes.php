<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultiplicadorCuentasCorrientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_corrientes_instrumentos', function (Blueprint $table) {
            $table->integer('multiplicador')->default(1);
        });

        Schema::table('cuentas_corrientes_monedas', function (Blueprint $table) {
            $table->integer('multiplicador')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('cuentas_corrientes_instrumentos', function (Blueprint $table) {
            $table->dropColumn('multiplicador');
        });

        Schema::table('cuentas_corrientes_monedas', function (Blueprint $table) {
            $table->dropColumn('multiplicador');
        });
     
    }
}
