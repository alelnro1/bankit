<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePerfilesPep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("update personas set funcion_pep=null");

        Schema::table('personas', function (Blueprint $table) {

            $table->integer('funcion_pep')->change()->nullable()->unsigned();

        });

        Schema::table('personas', function (Blueprint $table) {

            $table->foreign('funcion_pep')->references('id')->on('funciones_pep');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
