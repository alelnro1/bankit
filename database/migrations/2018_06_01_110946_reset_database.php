<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("delete from categorias_preguntas_prospectos;
delete from personas_prospectos;
delete from sociedades_direcciones;
delete from sociedades;
delete from instrumentos_operaciones;
delete from depositos;
delete from transferencias;
delete from cuentas_corrientes_instrumentos;
delete from cuentas_corrientes_monedas;
update cuentas_bancarias set prospecto_id=null;
delete from cuentas_bancarias where comitente_id<>1;
delete from prospectos_parametros;
delete from prospectos;
update categorias_preguntas_perfil set pregunta='Mi horizonte de inversión es menor a un mes' where pregunta='Mi horizonte de inversión es diario' and categoria='Horizonte de Inversión';
delete from comitentes_usuarios;
delete from comitentes_personas;
delete from personas_conyuges;
delete from personas_direcciones;
delete from personas;
delete from comitentes_usuarios where user_id not in (1,2);
delete from comitentes where id<>1;
delete from backend_permisos_users;
delete from feriados;
delete from instrumentos_plazo_liq;
insert into instrumentos_plazo_liq (instrumento_id, plazo_liquidacion_id, tipo_operacion_id) values (1,1,1);
insert into instrumentos_plazo_liq (instrumento_id, plazo_liquidacion_id, tipo_operacion_id) values (1,2,2);
delete from password_resets;
delete from comitentes_usuarios;
delete from users;
insert into estados (descripcion) values ('Cancelado');
DELETE FROM `bancos` WHERE cod_banco='';



ALTER TABLE comitentes AUTO_INCREMENT = 2;
ALTER TABLE comitentes_personas AUTO_INCREMENT = 1;
ALTER TABLE comitentes_usuarios AUTO_INCREMENT = 3;
ALTER TABLE cuentas_bancarias AUTO_INCREMENT = 2;
ALTER TABLE cuentas_corrientes_instrumentos AUTO_INCREMENT = 1;
ALTER TABLE cuentas_corrientes_monedas AUTO_INCREMENT = 1;
ALTER TABLE depositos AUTO_INCREMENT = 1;
ALTER TABLE feriados AUTO_INCREMENT = 1;
ALTER TABLE instrumentos_operaciones AUTO_INCREMENT = 1;
ALTER TABLE instrumentos_plazo_liq AUTO_INCREMENT = 2;
ALTER TABLE password_resets AUTO_INCREMENT = 1;
ALTER TABLE personas AUTO_INCREMENT = 1;
ALTER TABLE personas_conyuges AUTO_INCREMENT = 1;
ALTER TABLE personas_direcciones AUTO_INCREMENT = 1;
ALTER TABLE personas_prospectos AUTO_INCREMENT = 1;
ALTER TABLE prospectos_parametros AUTO_INCREMENT = 1;
ALTER TABLE prospectos AUTO_INCREMENT = 1;
ALTER TABLE sociedades AUTO_INCREMENT = 1;
ALTER TABLE sociedades_direcciones AUTO_INCREMENT = 1;
ALTER TABLE transferencias AUTO_INCREMENT = 1;
ALTER TABLE comitentes_usuarios AUTO_INCREMENT = 1;
ALTER TABLE users AUTO_INCREMENT = 1;



");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
