<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillTablaBancos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("update bancos set deleted_at = CURRENT_TIMESTAMP;insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE GALICIA Y BUENOS AIRES S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA NACION ARGENTINA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA PROVINCIA DE BUENOS AIRES',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('INDUSTRIAL AND COMMERCIAL BANK OF CHINA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('CITIBANK N.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BBVA BANCO FRANCES S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('THE BANK OF TOKYO-MITSUBISHI UFJ, LTD.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA PROVINCIA DE CORDOBA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO SUPERVIELLE S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA CIUDAD DE BUENOS AIRES',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO PATAGONIA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO HIPOTECARIO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE SAN JUAN S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DEL TUCUMAN S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO MUNICIPAL DE ROSARIO',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO SANTANDER RIO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DEL CHUBUT S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE SANTA CRUZ S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA PAMPA SOCIEDAD DE ECONOMÍA M',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE CORRIENTES S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO PROVINCIA DEL NEUQUÉN SOCIEDAD ANÓ',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO INTERFINANZAS S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('HSBC BANK ARGENTINA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('JPMORGAN CHASE BANK, NATIONAL ASSOCIATIO',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO CREDICOOP COOPERATIVO LIMITADO',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE VALORES S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO ROELA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO MARIVA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO ITAU ARGENTINA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANK OF AMERICA, NATIONAL ASSOCIATION',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BNP PARIBAS',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO PROVINCIA DE TIERRA DEL FUEGO',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE LA REPUBLICA ORIENTAL DEL URUGU',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO SAENZ S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO MERIDIAN S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO MACRO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO COMAFI SOCIEDAD ANONIMA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE INVERSION Y COMERCIO EXTERIOR S',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO PIANO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO FINANSUR S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO JULIO SOCIEDAD ANONIMA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO RIOJA SOCIEDAD ANONIMA UNIPERSONAL',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DEL SOL S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('NUEVO BANCO DEL CHACO S. A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO VOII S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE FORMOSA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO CMF S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE SANTIAGO DEL ESTERO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO INDUSTRIAL S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('NUEVO BANCO DE SANTA FE SOCIEDAD ANONIMA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO CETELEM ARGENTINA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE SERVICIOS FINANCIEROS S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO BRADESCO ARGENTINA S.A.U.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE SERVICIOS Y TRANSACCIONES S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('RCI BANQUE S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BACS BANCO DE CREDITO Y SECURITIZACION S',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO MASVENTAS S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('NUEVO BANCO DE ENTRE RÍOS S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO COLUMBIA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO BICA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO COINAG S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('BANCO DE COMERCIO S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('FORD CREDIT COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('COMPAÑIA FINANCIERA ARGENTINA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('VOLKWAGEN FINANCIAL SERVICES CIA.FIN.S.A',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('CORDIAL COMPAÑÍA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('FCA COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('GPAT COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('MERCEDES-BENZ COMPAÑÍA FINANCIERA ARGENT',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('ROMBO COMPAÑÍA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('JOHN DEERE CREDIT COMPAÑÍA FINANCIERA S.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('PSA FINANCE ARGENTINA COMPAÑÍA FINANCIER',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('TOYOTA COMPAÑÍA FINANCIERA DE ARGENTINA',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('FINANDINO COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('MONTEMAR COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('MULTIFINANZAS COMPAÑIA FINANCIERA S.A.',CURRENT_TIMESTAMP,'');
insert into bancos (descripcion, created_at,cod_banco) values ('CAJA DE CREDITO CUENCA COOPERATIVA LIM',CURRENT_TIMESTAMP,'');
");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
