<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosicionesYDisponibilidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/*
        Schema::table('transferencias', function (Blueprint $table) {
            $table->boolean('ingresado_cuenta_corriente')->default(0);
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->boolean('ingresado_cuenta_corriente')->default(0);
        });
*/
        DB::unprepared("
            CREATE PROCEDURE `getPosicionMonedas`(
                IN `pComitente_id` INT, 
                IN `pFecha` DATE) 

            NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER 

            SELECT moneda_id, sum(importe) as importe

            FROM cuentas_corrientes_monedas
            where deleted_at is not null
            and date(fecha_concertacion)<=pFecha
            and comitente_id=pComitente_id
            GROUP BY moneda_id;
        ");

        DB::unprepared("
            CREATE PROCEDURE `getPosicionInstrumentos`(
                    IN `pComitente_id` INT, 
                    IN `pFecha` DATE) 

            NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER 


            SELECT instrumento_id, SUM(cantidad) as cantidad 
            FROM cuentas_corrientes_instrumentos
            WHERE deleted_at is not null
            and date(fecha_concertacion)<=pFecha
            and comitente_id=pComitente_id
            GROUP BY instrumento_id;
        ");

        DB::unprepared("
            CREATE PROCEDURE `getDisponibilidadInstrumentos`(
                IN `pComitente_id` INT, 
                IN `pFecha` DATE) 

            NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER 

            SELECT  instrumento_id, SUM(cantidad) as cantidad
            FROM (
                SELECT instrumento_id, cantidad
                FROM cuentas_corrientes_instrumentos
                WHERE deleted_at is not null
                AND date(fecha_liquidacion)<=pFecha
                AND comitente_id=pComitente_id

                UNION ALL

                SELECT instrumento_id, cantidad from instrumentos_operaciones 
                WHERE deleted_at is not null 
                AND date(fecha_liquidacion)<=pFecha
                AND ingresado_cuenta_corriente<>1
                AND comitente_id=pComitente_id) AS InstrumentosTotal
            GROUP BY instrumento_id
        ");

        DB::unprepared("
            CREATE PROCEDURE `getDisponibilidadMonedas`(
                IN `pComitente_id` INT, 
                IN `pFecha` DATE) 

            NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER 

            SELECT  moneda_id, SUM(importe) as importe
            FROM (
                SELECT instrumento_id, importe
                FROM cuentas_corrientes_monedas
                WHERE deleted_at is not null
                AND date(fecha_liquidacion)<=pFecha
                AND comitente_id=pComitente_id

                UNION ALL

                SELECT moneda_id, importe 
                FROM instrumentos_operaciones 
                WHERE deleted_at is not null 
                AND date(fecha_liquidacion)<=pFecha
                AND ingresado_cuenta_corriente<>1
                AND comitente_id=pComitente_id


                UNION ALL

                SELECT moneda_id, importe 
                FROM transferencias 
                WHERE deleted_at is not null 
                AND date(fecha_liquidacion)<=pFecha
                AND ingresado_cuenta_corriente<>1
                AND comitente_id=pComitente_id) AS MonedasTotal

            GROUP BY moneda_id
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transferencias', function (Blueprint $table) {
            $table->dropColumn([
                'ingresado_cuenta_corriente'
            ]);
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->dropColumn([
                'ingresado_cuenta_corriente'
            ]);
        });
/*
        DB::unprepared("
            DROP PROCEDURE `getPosicionMonedas`;
        ");

        DB::unprepared("
            DROP PROCEDURE `getPosicionInstrumentos`;
        ");

        DB::unprepared("
            DROP PROCEDURE `getDisponibilidadInstrumentos`;
        ");

        DB::unprepared("
            DROP PROCEDURE `getDisponibilidadMonedas`;
        ");
*/
    }
}
