<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_operaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('nombre_display');
            $table->integer('multiplicador_moneda');
            $table->timestamps();
        });

        DB::unprepared("INSERT INTO tipos_operaciones (nombre, nombre_display, multiplicador_moneda) VALUES('Suscripción', 'Suscripción', '-1')");
        DB::unprepared("INSERT INTO tipos_operaciones (nombre, nombre_display, multiplicador_moneda) VALUES('Rescate', 'Rescate', '1')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_operaciones');
    }
}
