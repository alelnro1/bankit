<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArreglarTransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transferencias', function (Blueprint $table) {
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transferencias', function (Blueprint $table) {
            $table->dropForeign([
                'transferencias_moneda_id_foreign',
                'transferencias_comitente_id_foreign'
            ]);
        });
    }
}
