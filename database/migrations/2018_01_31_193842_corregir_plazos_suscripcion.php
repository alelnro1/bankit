<?php

use App\Models\Operaciones\PlazoLiquidacion;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorregirPlazosSuscripcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DELETE FROM tipo_op_plazo_liq WHERE tipo_operacion_id = '1' AND plazo_liquidacion_id = '1'");

        $tipo_operacion_suscripcion_id = TipoOperacion::where('nombre', 'Suscripción')->first()->id;

        $plazo_0hs_id = PlazoLiquidacion::where('dias', '0')->first()->id;
        $plazo_24hs_id = PlazoLiquidacion::where('dias', '1')->first()->id;
        $plazo_48hs_id = PlazoLiquidacion::where('dias', '2')->first()->id;

        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_0hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_24hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_48hs_id')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
