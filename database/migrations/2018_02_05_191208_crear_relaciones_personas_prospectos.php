<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearRelacionesPersonasProspectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('personas_prospectos');

        Schema::create('personas_prospectos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('persona_id')->unsigned();
            $table->integer('prospecto_id')->unsigned();

            $table->boolean('esta_confirmado');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('personas_prospectos', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('prospecto_id')->references('id')->on('prospectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
