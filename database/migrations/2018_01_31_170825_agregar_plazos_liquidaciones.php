<?php

use App\Models\Operaciones\PlazoLiquidacion;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarPlazosLiquidaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazos_liquidaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('dias');
            $table->string('abreviatura');

            $table->timestamps();
        });

        PlazoLiquidacion::create(['nombre' => 'Inmediata', 'dias' => 0, 'abreviatura' => 'Cdo.']);
        PlazoLiquidacion::create(['nombre' => 'A 24HS', 'dias' => 1, 'abreviatura' => '24hs']);
        PlazoLiquidacion::create(['nombre' => 'A 48HS', 'dias' => 2, 'abreviatura' => '48hs']);
        PlazoLiquidacion::create(['nombre' => 'A 72HS', 'dias' => 3, 'abreviatura' => '72hs']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plazos_liquidaciones');
    }
}
