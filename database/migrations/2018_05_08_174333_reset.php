<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           DB::unprepared("DELETE FROM `instrumentos_operaciones`;
                        ALTER TABLE `instrumentos_operaciones` AUTO_INCREMENT = 1;

                        DELETE FROM `cuentas_bancarias`;
                        ALTER TABLE `cuentas_bancarias` AUTO_INCREMENT = 1;

                        DELETE FROM `backend_permisos_users`;
                        ALTER TABLE `backend_permisos_users` AUTO_INCREMENT = 1;
                        DELETE FROM `backend_users`;
                        ALTER TABLE `backend_users` AUTO_INCREMENT = 1;
                        
                        INSERT INTO `backend_users` (`id`, `nombre`, `apellido`, `email`, `password`, `intentos_fallidos`, `cambio_password`, `created_at`, `updated_at`, `deleted_at`, `username`) VALUES (NULL, 'Eugenia', 'Tejerina', 'etejerina.admin@saenz.com.ar', 
                        '$2a$06\$D1xJFWboNJbvCeXb8KxgKO4AoEsr2Xp2IlxAxyIgtKt2ctVyvBn66', NULL, '0', CURRENT_TIMESTAMP, NULL, NULL, 'etejerina.admin@bsaenz.com.ar');
                        
                        INSERT INTO `backend_users` (`id`, `nombre`, `apellido`, `email`, `password`, `intentos_fallidos`, `cambio_password`, `created_at`, `updated_at`, `deleted_at`, `username`) VALUES (NULL, 'Belen', 'Ierace', 'bierace.admin@saenz.com.ar', 
                        '$2a$06\$D1xJFWboNJbvCeXb8KxgKO4AoEsr2Xp2IlxAxyIgtKt2ctVyvBn66', NULL, '0', CURRENT_TIMESTAMP, NULL, NULL, 'bierace.admin@bsaenz.com.ar');

                        DELETE FROM `comitentes_usuarios`;
                        ALTER TABLE `comitentes_usuarios` AUTO_INCREMENT = 1;
                        DELETE FROM `users`;
                        ALTER TABLE `users` AUTO_INCREMENT = 1;
                        DELETE FROM `instrumentos_operaciones`;
                        ALTER TABLE `instrumentos_operaciones` AUTO_INCREMENT = 1;
                        DELETE FROM `transferencias`;
                        ALTER TABLE `transferencias` AUTO_INCREMENT = 1;
                        DELETE FROM `depositos`;
                        ALTER TABLE `depositos` AUTO_INCREMENT = 1;

                        DELETE FROM `personas_prospectos`;
                        ALTER TABLE `personas_prospectos` AUTO_INCREMENT = 1;

                        DELETE FROM `comitentes_personas`;
                        ALTER TABLE `comitentes_personas` AUTO_INCREMENT = 1;



                        DELETE FROM `comitentes`;
                        ALTER TABLE `comitentes` AUTO_INCREMENT = 1;

                        DELETE FROM `monedas` WHERE id IN (3,4);
                        ALTER TABLE `monedas` AUTO_INCREMENT = 3;

                        INSERT INTO `comitentes` (`id`, `descripcion`, `created_at`, `updated_at`, `deleted_at`, `numero_comitente`) VALUES (NULL, 'Comitente de Prueba', CURRENT_TIMESTAMP, NULL, NULL, '8641');

                        INSERT INTO `users` (`nombre`, `apellido`, `email`, `password`, `intentos_fallidos`, `cambio_password`, `created_at`, `updated_at`, `deleted_at`, `username`, `remember_token`, `id`, `tipo_documento_id`, `numero_documento`) VALUES ('Eugenia', 'Tejerina', 'etejerina.front@saenz.com.ar', '$2a$06\$D1xJFWboNJbvCeXb8KxgKO4AoEsr2Xp2IlxAxyIgtKt2ctVyvBn66', '0', '0', CURRENT_TIMESTAMP, NULL, NULL, 'etejerina.front@bsaenz.com.ar', NULL, NULL, '1', '35147771'), ('Belen', 'Ierace', 'bierace.front@bsaenz.com.ar', 
                        '$2a$06\$D1xJFWboNJbvCeXb8KxgKO4AoEsr2Xp2IlxAxyIgtKt2ctVyvBn66', '0', '0', CURRENT_TIMESTAMP, NULL, NULL, 'bierace.front@bsaenz.com.ar', NULL, NULL, '1', '35147772');

                        INSERT INTO `comitentes_usuarios` (`id`, `comitente_id`, `user_id`, `created_at`, `updated_at`) VALUES (NULL, '1', '1', NULL, NULL);
                        INSERT INTO `comitentes_usuarios` (`id`, `comitente_id`, `user_id`, `created_at`, `updated_at`) VALUES (NULL, '1', '2', NULL, NULL);


                        INSERT INTO `cuentas_bancarias` (`id`, `banco_id`, `moneda_id`, `estado_id`, `comitente_id`, `prospecto_id`, `cuenta_bancaria_tipo_id`, `user_id`, `backend_user_id`, `cbu`, `alias`, `comprobante`, `numero_cuenta`, `created_at`, `updated_at`) VALUES (NULL, '145', '1', '2', '1', NULL, '1', '1', '1', '1234567890123456789012', 'ComitentePrueba1', NULL, '123456/7', NULL, NULL);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
