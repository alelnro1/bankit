<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComposicionFondo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("insert into instrumentos_composicion (instrumento_id, descripcion, porcentaje) values (2,'Lebacs', 72.5)");
        DB::unprepared("insert into instrumentos_composicion (instrumento_id, descripcion, porcentaje) values (2,'Fideicomisos Financieros', 11.5)");
        DB::unprepared("insert into instrumentos_composicion (instrumento_id, descripcion, porcentaje) values (2,'Obligaciones Negociables', 16)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
