<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFondoMoneda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("INSERT INTO `instrumentos_monedas` (`id`, `instrumento_id`, `moneda_id`, `created_at`, `updated_at`) VALUES (NULL, '2', '2', NULL, NULL)");


        DB::unprepared("INSERT INTO `instrumentos_plazo_liq` (`id`, `instrumento_id`, `plazo_liquidacion_id`, `tipo_operacion_id`) VALUES (NULL, '2', '1', '1')");

        DB::unprepared("INSERT INTO `instrumentos_plazo_liq` (`id`, `instrumento_id`, `plazo_liquidacion_id`, `tipo_operacion_id`) VALUES (NULL, '2', '1', '2')");


        DB::unprepared("INSERT INTO `instrumentos_plazo_liq` (`id`, `instrumento_id`, `plazo_liquidacion_id`, `tipo_operacion_id`) VALUES (NULL, '2', '2', '2')");

        DB::unprepared("INSERT INTO `instrumentos_plazo_liq` (`id`, `instrumento_id`, `plazo_liquidacion_id`, `tipo_operacion_id`) VALUES (NULL, '2', '3', '2')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
