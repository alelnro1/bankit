<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAllUnusedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('instrumento_moneda'); 
        Schema::dropIfExists('instrumento_cotizaciones');
        Schema::dropIfExists('depositos');
        Schema::dropIfExists('transferencias');
        Schema::dropIfExists('mercados');
        Schema::dropIfExists('operaciones');
        Schema::dropIfExists('instrumentos');
        Schema::dropIfExists('tipos_instrumentos');
        Schema::dropIfExists('instrumentos_operaciones');
        Schema::dropIfExists('cuentas_corrientes_monedas');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
