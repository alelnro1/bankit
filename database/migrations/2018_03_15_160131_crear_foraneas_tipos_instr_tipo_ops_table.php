<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearForaneasTiposInstrTipoOpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipos_instrumentos_tipos_operaciones', function (Blueprint $table) {
            $table->foreign('tipo_instrumento_id')->references('id')->on('tipos_instrumentos');
            $table->foreign('tipo_operacion_id')->references('id')->on('tipos_operaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
