<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTipoDocumentoNroDocumentoUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('tipo_documento_id')->nullable()->unsigned();
            $table->integer('numero_documento');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_tipo_documento_id_foreign');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'tipo_documento_id',
                'numero_documento'
            ]);
        });
    }
}
