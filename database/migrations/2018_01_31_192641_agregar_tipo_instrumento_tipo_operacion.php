<?php

use App\Models\Operaciones\TipoInstrumento;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTipoInstrumentoTipoOperacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipos_operaciones', function (Blueprint $table) {
            $table->integer('tipo_instrumento_id')->unsigned()->nullable();
        });

        Schema::table('tipos_operaciones', function (Blueprint $table) {
            $table->foreign('tipo_instrumento_id')->references('id')->on('tipos_instrumentos');
        });

        $fondos_id = TipoInstrumento::where('descripcion', 'Fondos')->first()->id;

        $operacion_suscripcion = TipoOperacion::where('nombre', 'Suscripción')->first();
        $operacion_rescate = TipoOperacion::where('nombre', 'Rescate')->first();

        $operacion_suscripcion->update(['tipo_instrumento_id' => $fondos_id]);
        $operacion_rescate->update(['tipo_instrumento_id' => $fondos_id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
