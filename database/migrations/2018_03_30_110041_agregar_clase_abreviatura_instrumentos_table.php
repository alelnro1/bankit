<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarClaseAbreviaturaInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos', function (Blueprint $table) {
            $table->char('clase');
            $table->string('abreviatura');
        });

        DB::unprepared("UPDATE instrumentos SET abreviatura = 'GR' WHERE descripcion = 'Gainvest Regional'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos', function (Blueprint $table) {
            $table->dropColumn([
                'clase',
                'abreviatura'
            ]);
        });
    }
}
