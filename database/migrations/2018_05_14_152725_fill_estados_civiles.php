<?php

use App\Models\Registro\Tipo\EstadoCivil;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillEstadosCiviles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("INSERT INTO estados_civiles (descripcion) VALUES ('Soltero')");
        DB::unprepared("INSERT INTO estados_civiles (descripcion) VALUES ('Casado')");
        DB::unprepared("INSERT INTO estados_civiles (descripcion) VALUES ('Divorciado')");
        DB::unprepared("INSERT INTO estados_civiles (descripcion) VALUES ('Separado')");
        DB::unprepared("INSERT INTO estados_civiles (descripcion) VALUES ('Viudo')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
