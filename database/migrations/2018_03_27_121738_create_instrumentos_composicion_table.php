<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentosComposicionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrumentos_composicion', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('instrumento_id')->unsigned();
            $table->string('descripcion');
            $table->double('porcentaje');
            $table->timestamps();
        });

        Schema::table('instrumentos_composicion', function (Blueprint $table) {
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrumentos_composicion');
    }
}
