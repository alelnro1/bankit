<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarColumnasCotizacionesInstrFondos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cotizaciones_instrumentos', function (Blueprint $table) {
            $table->dropColumn('importe');
            $table->double('valor_compra');
            $table->double('valor_venta')->nullable();
            $table->double('var_diaria')->nullable();
            $table->double('var_mensual')->nullable();
            $table->double('var_anual')->nullable();
            $table->double('cant_cuotapartes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cotizaciones_instrumentos', function (Blueprint $table) {
            $table->dropColumn('importe');
            $table->double('valor_compra');
            $table->double('valor_venta');
            $table->double('var_diaria');
            $table->double('var_mensual');
            $table->double('var_anual');
            $table->double('cant_cuotapartes');
        });
    }
}
