<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTipoOperacionIdInstrumentosOperaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->integer('tipo_operacion_id')->unsigned()->nullable();
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->foreign('tipo_operacion_id')->references('id')->on('tipos_operaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->dropForeign(['tipo_operacion_id']);
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->dropColumn('tipo_operacion_id');
        });
    }
}
