<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoBanco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
                UPDATE bancos SET cod_banco='007' WHERE descripcion='BANCO DE GALICIA Y BUENOS AIRES S.A.';
                UPDATE bancos SET cod_banco='011' WHERE descripcion='BANCO DE LA NACION ARGENTINA';
                UPDATE bancos SET cod_banco='014' WHERE descripcion='BANCO DE LA PROVINCIA DE BUENOS AIRES';
                UPDATE bancos SET cod_banco='015' WHERE descripcion='INDUSTRIAL AND COMMERCIAL BANK OF CHINA';
                UPDATE bancos SET cod_banco='016' WHERE descripcion='CITIBANK N.A.';
                UPDATE bancos SET cod_banco='017' WHERE descripcion='BBVA BANCO FRANCES S.A.';
                UPDATE bancos SET cod_banco='018' WHERE descripcion='THE BANK OF TOKYO-MITSUBISHI UFJ, LTD.';
                UPDATE bancos SET cod_banco='020' WHERE descripcion='BANCO DE LA PROVINCIA DE CORDOBA S.A.';
                UPDATE bancos SET cod_banco='027' WHERE descripcion='BANCO SUPERVIELLE S.A.';
                UPDATE bancos SET cod_banco='029' WHERE descripcion='BANCO DE LA CIUDAD DE BUENOS AIRES';
                UPDATE bancos SET cod_banco='034' WHERE descripcion='BANCO PATAGONIA S.A.';
                UPDATE bancos SET cod_banco='044' WHERE descripcion='BANCO HIPOTECARIO S.A.';
                UPDATE bancos SET cod_banco='045' WHERE descripcion='BANCO DE SAN JUAN S.A.';
                UPDATE bancos SET cod_banco='060' WHERE descripcion='BANCO DEL TUCUMAN S.A.';
                UPDATE bancos SET cod_banco='065' WHERE descripcion='BANCO MUNICIPAL DE ROSARIO';
                UPDATE bancos SET cod_banco='072' WHERE descripcion='BANCO SANTANDER RIO S.A.';
                UPDATE bancos SET cod_banco='083' WHERE descripcion='BANCO DEL CHUBUT S.A.';
                UPDATE bancos SET cod_banco='086' WHERE descripcion='BANCO DE SANTA CRUZ S.A.';
                UPDATE bancos SET cod_banco='093' WHERE descripcion='BANCO DE LA PAMPA SOCIEDAD DE ECONOMÍA M';
                UPDATE bancos SET cod_banco='094' WHERE descripcion='BANCO DE CORRIENTES S.A.';
                UPDATE bancos SET cod_banco='097' WHERE descripcion='BANCO PROVINCIA DEL NEUQUÉN SOCIEDAD ANÓ';
                UPDATE bancos SET cod_banco='147' WHERE descripcion='BANCO INTERFINANZAS S.A.';
                UPDATE bancos SET cod_banco='150' WHERE descripcion='HSBC BANK ARGENTINA S.A.';
                UPDATE bancos SET cod_banco='165' WHERE descripcion='JPMORGAN CHASE BANK, NATIONAL ASSOCIATIO';
                UPDATE bancos SET cod_banco='191' WHERE descripcion='BANCO CREDICOOP COOPERATIVO LIMITADO';
                UPDATE bancos SET cod_banco='198' WHERE descripcion='BANCO DE VALORES S.A.';
                UPDATE bancos SET cod_banco='247' WHERE descripcion='BANCO ROELA S.A.';
                UPDATE bancos SET cod_banco='254' WHERE descripcion='BANCO MARIVA S.A.';
                UPDATE bancos SET cod_banco='259' WHERE descripcion='BANCO ITAU ARGENTINA S.A.';
                UPDATE bancos SET cod_banco='262' WHERE descripcion='BANK OF AMERICA, NATIONAL ASSOCIATION';
                UPDATE bancos SET cod_banco='266' WHERE descripcion='BNP PARIBAS';
                UPDATE bancos SET cod_banco='268' WHERE descripcion='BANCO PROVINCIA DE TIERRA DEL FUEGO';
                UPDATE bancos SET cod_banco='269' WHERE descripcion='BANCO DE LA REPUBLICA ORIENTAL DEL URUGU';
                UPDATE bancos SET cod_banco='277' WHERE descripcion='BANCO SAENZ S.A.';
                UPDATE bancos SET cod_banco='281' WHERE descripcion='BANCO MERIDIAN S.A.';
                UPDATE bancos SET cod_banco='285' WHERE descripcion='BANCO MACRO S.A.';
                UPDATE bancos SET cod_banco='299' WHERE descripcion='BANCO COMAFI SOCIEDAD ANONIMA';
                UPDATE bancos SET cod_banco='300' WHERE descripcion='BANCO DE INVERSION Y COMERCIO EXTERIOR S';
                UPDATE bancos SET cod_banco='301' WHERE descripcion='BANCO PIANO S.A.';
                UPDATE bancos SET cod_banco='303' WHERE descripcion='BANCO FINANSUR S.A.';
                UPDATE bancos SET cod_banco='305' WHERE descripcion='BANCO JULIO SOCIEDAD ANONIMA';
                UPDATE bancos SET cod_banco='309' WHERE descripcion='BANCO RIOJA SOCIEDAD ANONIMA UNIPERSONAL';
                UPDATE bancos SET cod_banco='310' WHERE descripcion='BANCO DEL SOL S.A.';
                UPDATE bancos SET cod_banco='311' WHERE descripcion='NUEVO BANCO DEL CHACO S. A.';
                UPDATE bancos SET cod_banco='312' WHERE descripcion='BANCO VOII S.A.';
                UPDATE bancos SET cod_banco='315' WHERE descripcion='BANCO DE FORMOSA S.A.';
                UPDATE bancos SET cod_banco='319' WHERE descripcion='BANCO CMF S.A.';
                UPDATE bancos SET cod_banco='321' WHERE descripcion='BANCO DE SANTIAGO DEL ESTERO S.A.';
                UPDATE bancos SET cod_banco='322' WHERE descripcion='BANCO INDUSTRIAL S.A.';
                UPDATE bancos SET cod_banco='330' WHERE descripcion='NUEVO BANCO DE SANTA FE SOCIEDAD ANONIMA';
                UPDATE bancos SET cod_banco='331' WHERE descripcion='BANCO CETELEM ARGENTINA S.A.';
                UPDATE bancos SET cod_banco='332' WHERE descripcion='BANCO DE SERVICIOS FINANCIEROS S.A.';
                UPDATE bancos SET cod_banco='336' WHERE descripcion='BANCO BRADESCO ARGENTINA S.A.U.';
                UPDATE bancos SET cod_banco='338' WHERE descripcion='BANCO DE SERVICIOS Y TRANSACCIONES S.A.';
                UPDATE bancos SET cod_banco='339' WHERE descripcion='RCI BANQUE S.A.';
                UPDATE bancos SET cod_banco='340' WHERE descripcion='BACS BANCO DE CREDITO Y SECURITIZACION S';
                UPDATE bancos SET cod_banco='341' WHERE descripcion='BANCO MASVENTAS S.A.';
                UPDATE bancos SET cod_banco='386' WHERE descripcion='NUEVO BANCO DE ENTRE RÍOS S.A.';
                UPDATE bancos SET cod_banco='389' WHERE descripcion='BANCO COLUMBIA S.A.';
                UPDATE bancos SET cod_banco='426' WHERE descripcion='BANCO BICA S.A.';
                UPDATE bancos SET cod_banco='431' WHERE descripcion='BANCO COINAG S.A.';
                UPDATE bancos SET cod_banco='432' WHERE descripcion='BANCO DE COMERCIO S.A.';
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
