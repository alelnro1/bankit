<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreacionInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2018 a las 17:22:37
-- Versión del servidor: 10.1.24-MariaDB
-- Versión de PHP: 7.1.6

SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = \"+00:00\";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bankit`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `intentos_fallidos` int(11) NOT NULL,
  `cambio_password` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `username` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE `bancos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comitentes`
--

CREATE TABLE `comitentes` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comitentes_personas`
--

CREATE TABLE `comitentes_personas` (
  `id` int(11) NOT NULL,
  `comitente_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comitentes_usuarios`
--

CREATE TABLE `comitentes_usuarios` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comitente_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_bancarias`
--

CREATE TABLE `cuentas_bancarias` (
  `id` int(11) NOT NULL,
  `banco_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `comitente_id` int(11) DEFAULT NULL,
  `prospecto_id` int(11) DEFAULT NULL,
  `numero_cuenta` varchar(30) NOT NULL,
  `cbu` varchar(21) NOT NULL,
  `alias` varchar(30) NOT NULL,
  `comprobante` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `es_default` tinyint(1) NOT NULL,
  `cuit` varchar(11) NOT NULL,
  `cuenta_bancaria_tipo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `backend_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_bancarias_propias`
--

CREATE TABLE `cuentas_bancarias_propias` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `banco_id` int(11) NOT NULL,
  `numero_cuenta` varchar(20) NOT NULL,
  `cbu` varchar(21) NOT NULL,
  `alias` varchar(30) NOT NULL,
  `opera_web` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_bancarias_tipo`
--

CREATE TABLE `cuentas_bancarias_tipo` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `abreviatura` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_corrientes_monedas`
--

CREATE TABLE `cuentas_corrientes_monedas` (
  `id` int(11) NOT NULL,
  `origen_tabla` varchar(100) NOT NULL,
  `origen_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `fecha_movimiento` date NOT NULL,
  `fecha_liquidacion` date NOT NULL,
  `importe` decimal(18,4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `comitente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositos`
--

CREATE TABLE `depositos` (
  `id` int(11) NOT NULL,
  `cuenta_propia_id` int(11) NOT NULL,
  `fecha_movimiento` date NOT NULL,
  `fecha_liquidacion` date NOT NULL,
  `estado_id` int(11) NOT NULL,
  `comitente_id` int(11) NOT NULL,
  `importe` decimal(18,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `comprobante` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `backend_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_civiles`
--

CREATE TABLE `estados_civiles` (
  `id` int(11) NOT NULL,
  `descripcion` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monedas`
--

CREATE TABLE `monedas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `simbolo` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametros`
--

CREATE TABLE `parametros` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `tipo_documento_id` int(11) NOT NULL,
  `numero_documento` int(11) NOT NULL,
  `sexo_id` int(11) NOT NULL,
  `estado_civil_id` int(11) NOT NULL,
  `tipo_responsable_iva_id` int(11) NOT NULL,
  `tipo_responsable_ganancias_id` int(11) NOT NULL,
  `telefono_particular` varchar(20) NOT NULL,
  `telefono_movil` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas_prospectos`
--

CREATE TABLE `personas_prospectos` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `prospecto_id` int(11) NOT NULL,
  `esta_confirmado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospectos`
--

CREATE TABLE `prospectos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospectos_parametros`
--

CREATE TABLE `prospectos_parametros` (
  `id` int(11) NOT NULL,
  `prospecto_id` int(11) NOT NULL,
  `parametro_id` int(11) NOT NULL,
  `valor_texto` varchar(50) DEFAULT NULL,
  `valor_numero` decimal(18,4) DEFAULT NULL,
  `valor_fecha` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexos`
--

CREATE TABLE `sexos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_documentos`
--

CREATE TABLE `tipos_documentos` (
  `id` int(11) NOT NULL,
  `descripcion` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_responsables_iva`
--

CREATE TABLE `tipos_responsables_iva` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_responsales_ganancias`
--

CREATE TABLE `tipos_responsales_ganancias` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transferencias`
--

CREATE TABLE `transferencias` (
  `id` int(11) NOT NULL,
  `comitente_id` int(11) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `moneda_id` int(11) NOT NULL,
  `cuenta_bancaria_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `importe` decimal(18,4) NOT NULL,
  `fecha_movimiento` date NOT NULL,
  `fecha_liquidacion` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `backend_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `intentos_fallidos` int(11) NOT NULL DEFAULT '0',
  `cambio_password` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `username` varchar(191) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backend_users_username_unique` (`username`);

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `comitentes`
--
ALTER TABLE `comitentes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `comitentes_personas`
--
ALTER TABLE `comitentes_personas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comitente_id` (`comitente_id`),
  ADD KEY `persona_id` (`persona_id`);

--
-- Indices de la tabla `comitentes_usuarios`
--
ALTER TABLE `comitentes_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`user_id`),
  ADD KEY `comitentes_id` (`comitente_id`);

--
-- Indices de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banco_id` (`banco_id`),
  ADD KEY `moneda_id` (`moneda_id`),
  ADD KEY `comitente_id` (`comitente_id`),
  ADD KEY `cuenta_bancaria_tipo_id` (`cuenta_bancaria_tipo_id`),
  ADD KEY `estado_id` (`estado_id`),
  ADD KEY `usuario_id` (`user_id`),
  ADD KEY `backend_user_id` (`backend_user_id`),
  ADD KEY `prospecto_id` (`prospecto_id`);

--
-- Indices de la tabla `cuentas_bancarias_propias`
--
ALTER TABLE `cuentas_bancarias_propias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`),
  ADD KEY `moneda_id` (`moneda_id`),
  ADD KEY `banco_id` (`banco_id`);

--
-- Indices de la tabla `cuentas_bancarias_tipo`
--
ALTER TABLE `cuentas_bancarias_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas_corrientes_monedas`
--
ALTER TABLE `cuentas_corrientes_monedas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `moneda_id` (`moneda_id`),
  ADD KEY `comitente_id` (`comitente_id`);

--
-- Indices de la tabla `depositos`
--
ALTER TABLE `depositos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cuenta_propia_id` (`cuenta_propia_id`),
  ADD KEY `comitente_id` (`comitente_id`),
  ADD KEY `estado_id` (`estado_id`),
  ADD KEY `usuario_id` (`user_id`),
  ADD KEY `backend_user_id` (`backend_user_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `monedas`
--
ALTER TABLE `monedas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `numero_documento` (`numero_documento`),
  ADD KEY `estado_civil_id` (`estado_civil_id`),
  ADD KEY `sexo_id` (`sexo_id`),
  ADD KEY `tipo_documento_id` (`tipo_documento_id`),
  ADD KEY `tipo_responsable_ganancias_id` (`tipo_responsable_ganancias_id`),
  ADD KEY `tipo_responsable_iva_id` (`tipo_responsable_iva_id`);

--
-- Indices de la tabla `personas_prospectos`
--
ALTER TABLE `personas_prospectos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persona_id` (`persona_id`),
  ADD KEY `prospecto_id` (`prospecto_id`);

--
-- Indices de la tabla `prospectos`
--
ALTER TABLE `prospectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prospectos_parametros`
--
ALTER TABLE `prospectos_parametros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parametro_id` (`parametro_id`),
  ADD KEY `prospecto_id` (`prospecto_id`);

--
-- Indices de la tabla `sexos`
--
ALTER TABLE `sexos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `tipos_documentos`
--
ALTER TABLE `tipos_documentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `tipos_responsables_iva`
--
ALTER TABLE `tipos_responsables_iva`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `tipos_responsales_ganancias`
--
ALTER TABLE `tipos_responsales_ganancias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comitente_id` (`comitente_id`),
  ADD KEY `moneda_id` (`moneda_id`),
  ADD KEY `cuenta_bancaria_id` (`cuenta_bancaria_id`),
  ADD KEY `estado_id` (`estado_id`),
  ADD KEY `usuario_id` (`user_id`),
  ADD KEY `backend_user_id` (`backend_user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `comitentes`
--
ALTER TABLE `comitentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `comitentes_personas`
--
ALTER TABLE `comitentes_personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comitentes_usuarios`
--
ALTER TABLE `comitentes_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `cuentas_bancarias_propias`
--
ALTER TABLE `cuentas_bancarias_propias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cuentas_bancarias_tipo`
--
ALTER TABLE `cuentas_bancarias_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cuentas_corrientes_monedas`
--
ALTER TABLE `cuentas_corrientes_monedas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `depositos`
--
ALTER TABLE `depositos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `estados_civiles`
--
ALTER TABLE `estados_civiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `monedas`
--
ALTER TABLE `monedas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `parametros`
--
ALTER TABLE `parametros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `personas_prospectos`
--
ALTER TABLE `personas_prospectos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prospectos`
--
ALTER TABLE `prospectos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prospectos_parametros`
--
ALTER TABLE `prospectos_parametros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sexos`
--
ALTER TABLE `sexos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipos_documentos`
--
ALTER TABLE `tipos_documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipos_responsables_iva`
--
ALTER TABLE `tipos_responsables_iva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipos_responsales_ganancias`
--
ALTER TABLE `tipos_responsales_ganancias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comitentes_personas`
--
ALTER TABLE `comitentes_personas`
  ADD CONSTRAINT `comitentes_personas_ibfk_1` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`),
  ADD CONSTRAINT `comitentes_personas_ibfk_2` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `comitentes_usuarios`
--
ALTER TABLE `comitentes_usuarios`
  ADD CONSTRAINT `comitentes_usuarios_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comitentes_usuarios_ibfk_2` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`);

--
-- Filtros para la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  ADD CONSTRAINT `cuentas_bancarias_ibfk_1` FOREIGN KEY (`banco_id`) REFERENCES `bancos` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_2` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_3` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_4` FOREIGN KEY (`cuenta_bancaria_tipo_id`) REFERENCES `cuentas_bancarias_tipo` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_5` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_7` FOREIGN KEY (`backend_user_id`) REFERENCES `backend_users` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_ibfk_8` FOREIGN KEY (`prospecto_id`) REFERENCES `prospectos` (`id`);

--
-- Filtros para la tabla `cuentas_bancarias_propias`
--
ALTER TABLE `cuentas_bancarias_propias`
  ADD CONSTRAINT `cuentas_bancarias_propias_ibfk_1` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`),
  ADD CONSTRAINT `cuentas_bancarias_propias_ibfk_2` FOREIGN KEY (`banco_id`) REFERENCES `bancos` (`id`);

--
-- Filtros para la tabla `cuentas_corrientes_monedas`
--
ALTER TABLE `cuentas_corrientes_monedas`
  ADD CONSTRAINT `cuentas_corrientes_monedas_ibfk_1` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`),
  ADD CONSTRAINT `cuentas_corrientes_monedas_ibfk_2` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`);

--
-- Filtros para la tabla `depositos`
--
ALTER TABLE `depositos`
  ADD CONSTRAINT `depositos_ibfk_1` FOREIGN KEY (`cuenta_propia_id`) REFERENCES `cuentas_bancarias_propias` (`id`),
  ADD CONSTRAINT `depositos_ibfk_3` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`),
  ADD CONSTRAINT `depositos_ibfk_4` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  ADD CONSTRAINT `depositos_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `depositos_ibfk_6` FOREIGN KEY (`backend_user_id`) REFERENCES `backend_users` (`id`);

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`estado_civil_id`) REFERENCES `estados_civiles` (`id`),
  ADD CONSTRAINT `personas_ibfk_2` FOREIGN KEY (`sexo_id`) REFERENCES `sexos` (`id`),
  ADD CONSTRAINT `personas_ibfk_3` FOREIGN KEY (`tipo_documento_id`) REFERENCES `tipos_documentos` (`id`),
  ADD CONSTRAINT `personas_ibfk_4` FOREIGN KEY (`tipo_responsable_ganancias_id`) REFERENCES `tipos_responsales_ganancias` (`id`),
  ADD CONSTRAINT `personas_ibfk_5` FOREIGN KEY (`tipo_responsable_iva_id`) REFERENCES `tipos_responsables_iva` (`id`);

--
-- Filtros para la tabla `personas_prospectos`
--
ALTER TABLE `personas_prospectos`
  ADD CONSTRAINT `personas_prospectos_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `personas_prospectos_ibfk_2` FOREIGN KEY (`prospecto_id`) REFERENCES `prospectos` (`id`);

--
-- Filtros para la tabla `prospectos_parametros`
--
ALTER TABLE `prospectos_parametros`
  ADD CONSTRAINT `prospectos_parametros_ibfk_1` FOREIGN KEY (`parametro_id`) REFERENCES `parametros` (`id`),
  ADD CONSTRAINT `prospectos_parametros_ibfk_2` FOREIGN KEY (`prospecto_id`) REFERENCES `prospectos` (`id`);

--
-- Filtros para la tabla `transferencias`
--
ALTER TABLE `transferencias`
  ADD CONSTRAINT `transferencias_ibfk_1` FOREIGN KEY (`comitente_id`) REFERENCES `comitentes` (`id`),
  ADD CONSTRAINT `transferencias_ibfk_2` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`),
  ADD CONSTRAINT `transferencias_ibfk_3` FOREIGN KEY (`cuenta_bancaria_id`) REFERENCES `cuentas_bancarias` (`id`),
  ADD CONSTRAINT `transferencias_ibfk_4` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`),
  ADD CONSTRAINT `transferencias_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `transferencias_ibfk_6` FOREIGN KEY (`backend_user_id`) REFERENCES `backend_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
