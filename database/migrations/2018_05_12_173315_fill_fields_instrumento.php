<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillFieldsInstrumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            UPDATE instrumentos SET familia='Gainvest', categoria='Renta Fija', codigo_bloomberg='GBREGION AR', sociedad_gerente='Gainvest', sociedad_custodia='Banco Comafi', benchmark='Badlar',codigo_caja_valores='1103',permanencia_recomendada='Menor a 6 meses';
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
