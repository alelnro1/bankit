<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorregirComitentesPersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comitentes_personas');

        Schema::create('comitentes_personas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('comitente_id')->unsigned();
            $table->integer('persona_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('comitentes_personas', function (Blueprint $table) {
           $table->foreign('comitente_id')->references('id')->on('comitentes');
           $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
