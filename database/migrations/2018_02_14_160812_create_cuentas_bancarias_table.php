<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('cuentas_bancarias', function (Blueprint $table) {
            $table->dropForeign('banco_id');
            $table->dropForeign('moneda_id');
            $table->dropForeign('comitente_id');
            $table->dropForeign('prospecto_id');
            $table->dropForeign('cuenta_bancaria_tipo_id');
            $table->dropForeign('user_id');
            $table->dropForeign('backend_user_id');
        });*/

        Schema::dropIfExists('cuentas_bancarias');

        Schema::create('cuentas_bancarias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('banco_id')->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('comitente_id')->unsigned()->nullable();
            $table->integer('prospecto_id')->unsigned()->nullable();
            $table->integer('cuenta_bancaria_tipo_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('backend_user_id')->unsigned()->nullable();
            $table->string('cbu');
            $table->string('alias');
            $table->string('comprobante');
            $table->string('numero_cuenta');
            $table->string('cuit');

            $table->timestamps();
        });

        Schema::table('cuentas_bancarias', function (Blueprint $table) {
            $table->foreign('banco_id')->references('id')->on('bancos');
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
            $table->foreign('prospecto_id')->references('id')->on('prospectos');
            $table->foreign('cuenta_bancaria_tipo_id')->references('id')->on('cuentas_bancarias_tipo');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('backend_user_id')->references('id')->on('backend_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_bancarias');
    }
}
