<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearEspeciesPlazoLiq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tipo_op_plazo_liq');

        Schema::create('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('instrumento_id')->unsigned();
            $table->integer('plazo_liquidacion_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrumentos_plazo_liq');
    }
}
