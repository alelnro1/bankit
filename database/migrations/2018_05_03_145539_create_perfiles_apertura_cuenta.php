<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesAperturaCuenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias_preguntas_perfil', function (Blueprint $table) {
            $table->increments('id');

            $table->string('categoria');
            $table->string('pregunta');
            $table->integer('valor');

            $table->timestamps();
        });

        Schema::create('categorias_preguntas_prospectos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('prospecto_id')->unsigned();
            $table->integer('categoria_pregunta_id')->unsigned();
        });

        Schema::table('categorias_preguntas_prospectos', function (Blueprint $table) {
            $table->foreign('prospecto_id')->references('id')->on('prospectos');
            $table->foreign('categoria_pregunta_id')->references('id')->on('categorias_preguntas_perfil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias_preguntas_prospectos', function (Blueprint $table) {
            $table->dropForeign(['prospecto_id']);
            $table->dropForeign(['categoria_pregunta_id']);
        });

        Schema::dropIfExists('categorias_preguntas_prospectos');

        Schema::dropIfExists('categorias_preguntas_perfil');
    }
}
