<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationWithState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('depositos', function (Blueprint $table) {
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('estados');
        });

        Schema::table('transferencias', function (Blueprint $table) {
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('estados');
        });


        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('estados');
        });

        \App\Models\Estado::insert(['descripcion' => 'Enviado']);
        \App\Models\Estado::insert(['descripcion' => 'Liquidado']);
        \App\Models\Estado::insert(['descripcion' => 'Pendiente de Liquidar']);

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->integer('mercado_registro_id')->unsigned()->nullable();
            $table->string('mercado_mensaje');
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('depositos', function (Blueprint $table) {
            $table->dropForeign('depositos_estado_id_foreign');
            $table->dropColumn('estado_id');
        });

        Schema::table('transferencias', function (Blueprint $table) {
            $table->dropForeign('transferencias_estado_id_foreign');
            $table->dropColumn('estado_id');
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->dropForeign('instrumentos_operaciones_estado_id_foreign');
            $table->dropColumn('estado_id');
            $table->dropColumn('mercado_registro_id');
            $table->dropColumn('mercado_mensaje');
        });


        DB::table('estados')->where('descripcion', 'Enviado')->delete();
        DB::table('estados')->where('descripcion', 'Liquidado')->delete();
        DB::table('estados')->where('descripcion', 'Pendiente de Liquidar')->delete();

    }
}
