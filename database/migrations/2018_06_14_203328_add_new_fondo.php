<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFondo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("INSERT INTO `instrumentos` (`id`, `tipo_instrumento_id`, `descripcion`, `nombre_web`, `opera_web`, `created_at`, `updated_at`, `deleted_at`, `limite_minimo`, `calificacion_cnv`, `clase`, `abreviatura`, `cod_interfaz`, `familia`, `categoria`, `codigo_bloomberg`, `sociedad_gerente`, `sociedad_custodia`, `benchmark`, `codigo_caja_valores`, `permanencia_recomendada`, `moneda_id`) VALUES (NULL, '1', 'Gainvest (USD)', 'Gainvest (USD)', '1', NULL, NULL, NULL, '1000', 'A+', 'A', 'GREGI', '6', NULL, NULL, NULL, 'Gainvest', 'Comafi', NULL, NULL, '6 Meses', '2')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
