<?php

use App\Models\Operaciones\PlazoLiquidacion;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoOpPlazoLiq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_op_plazo_liq', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tipo_operacion_id')->unsigned();
            $table->integer('plazo_liquidacion_id')->unsigned();
        });

        Schema::table('tipo_op_plazo_liq', function (Blueprint $table) {
            $table->foreign('tipo_operacion_id')->references('id')->on('tipos_operaciones');
            $table->foreign('plazo_liquidacion_id')->references('id')->on('plazos_liquidaciones');
        });

        $tipo_operacion_suscripcion_id = TipoOperacion::where('nombre', 'Suscripción')->first()->id;
        $tipo_operacion_rescate_id = TipoOperacion::where('nombre', 'Rescate')->first()->id;

        $plazo_0hs_id = PlazoLiquidacion::where('dias', '0')->first()->id;
        $plazo_24hs_id = PlazoLiquidacion::where('dias', '1')->first()->id;
        $plazo_48hs_id = PlazoLiquidacion::where('dias', '2')->first()->id;

        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_0hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_0hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_suscripcion_id', '$plazo_0hs_id')");

        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_rescate_id', '$plazo_0hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_rescate_id', '$plazo_24hs_id')");
        DB::unprepared("INSERT INTO tipo_op_plazo_liq (tipo_operacion_id, plazo_liquidacion_id) VALUES ('$tipo_operacion_rescate_id', '$plazo_48hs_id')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
