<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonasConyuges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas_conyuges', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->integer('tipo_documento_id')->unsigned()->nullable();
            $table->string('numero_documento')->nullable();
            $table->integer('sexo_id')->unsigned()->nullable();
            $table->integer('persona_id')->unsigned()->nullable();

            $table->timestamps();
        });

        Schema::table('personas_conyuges', function ($table) {
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos');
            $table->foreign('sexo_id')->references('id')->on('sexos');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas_conyuges', function ($table) {
            $table->dropForeign('personas_conyuges_tipo_documento_id_foreign');
            $table->dropForeign('personas_conyuges_sexo_id_foreign');
            $table->dropForeign('personas_conyuges_persona_id_foreign');
        });

        Schema::dropIfExists('personas_conyuges');
    }
}
