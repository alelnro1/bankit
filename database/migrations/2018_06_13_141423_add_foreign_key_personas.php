<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyPersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->integer('pais_nacimiento')->change()->unsigned()->nullable();
            $table->integer('provincia_nacimiento')->change()->unsigned()->nullable();
            $table->foreign('pais_nacimiento')->references('id')->on('paises');
            $table->foreign('provincia_nacimiento')->references('id')->on('provincias');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
