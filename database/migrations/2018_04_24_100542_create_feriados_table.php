<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeriadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feriados', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->timestamps();
        });

        \App\Models\Feriado::create(['fecha' => '2018-04-02']);
        \App\Models\Feriado::create(['fecha' => '2018-04-30']);
        \App\Models\Feriado::create(['fecha' => '2018-05-01']);
        \App\Models\Feriado::create(['fecha' => '2018-05-25']);
        \App\Models\Feriado::create(['fecha' => '2018-07-09']);
        \App\Models\Feriado::create(['fecha' => '2018-08-20']);
        \App\Models\Feriado::create(['fecha' => '2018-10-15']);
        \App\Models\Feriado::create(['fecha' => '2018-11-17']);
        \App\Models\Feriado::create(['fecha' => '2018-12-08']);
        \App\Models\Feriado::create(['fecha' => '2018-12-24']);
        \App\Models\Feriado::create(['fecha' => '2018-12-25']);
        \App\Models\Feriado::create(['fecha' => '2018-12-31']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feriados');
    }
}
