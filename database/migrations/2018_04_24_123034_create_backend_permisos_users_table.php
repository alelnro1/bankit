<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackendPermisosUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backend_permisos_users', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('backend_user_id')->unsigned();
            $table->integer('backend_permiso_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('backend_permisos_users', function (Blueprint $table) {
            $table->foreign('backend_user_id')->references('id')->on('backend_users');
            $table->foreign('backend_permiso_id')->references('id')->on('backend_permisos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backend_permisos_users');
    }
}
