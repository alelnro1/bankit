<?php

use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiarNombreDisplaySuscripcion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $operacion_suscripcion = TipoOperacion::where('nombre_display', 'Suscripción')->first();

        $operacion_suscripcion->nombre = 'Suscripcion';
        $operacion_suscripcion->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
