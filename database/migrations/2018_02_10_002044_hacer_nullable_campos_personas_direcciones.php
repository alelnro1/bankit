<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HacerNullableCamposPersonasDirecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas_direcciones', function (Blueprint $table) {
            $table->string('calle')->nullable(true)->change();
            $table->integer('altura')->nullable(true)->change();
            $table->integer('piso')->nullable(true)->change();
            $table->string('departamento')->nullable(true)->change();

            $table->string('localidad')->nullable(true)->change();
            $table->string('codigo_postal')->nullable(true)->change();
            $table->string('provincia')->nullable(true)->change();
            $table->string('pais')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
