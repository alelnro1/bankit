<?php

use App\Models\Prospectos\PersonasFisicas\TipoResponsableIVA;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrarTiposResponsablesIva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TipoResponsableIVA::create(['descripcion' => 'Consumidor final']);
        TipoResponsableIVA::create(['descripcion' => 'Responsable Inscripto']);
        TipoResponsableIVA::create(['descripcion' => 'No Responsable']);
        TipoResponsableIVA::create(['descripcion' => 'Exento']);
        TipoResponsableIVA::create(['descripcion' => 'Monotributo']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
