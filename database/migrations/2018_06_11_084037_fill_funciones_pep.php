<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillFuncionesPep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("insert into funciones_pep (descripcion) values ('prueba1')");
        DB::unprepared("insert into funciones_pep (descripcion) values ('prueba2')");
        DB::unprepared("insert into funciones_pep (descripcion) values ('prueba3')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("delete from funciones_pep");   
    }
}
