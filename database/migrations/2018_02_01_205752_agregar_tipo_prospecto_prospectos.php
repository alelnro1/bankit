<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTipoProspectoProspectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipos_prospectos', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('prospectos', function (Blueprint $table) {
            $table->integer('tipo_prospecto_id')->unsigned();
        });

        Schema::table('prospectos', function (Blueprint $table) {
            $table->foreign('tipo_prospecto_id')->references('id')->on('tipos_prospectos');
        });

        DB::unprepared("INSERT INTO tipos_prospectos (descripcion) VALUES ('Persona Física')");
        DB::unprepared("INSERT INTO tipos_prospectos (descripcion) VALUES ('Persona Jurídica')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
