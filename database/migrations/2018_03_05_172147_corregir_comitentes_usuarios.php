<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorregirComitentesUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comitentes_usuarios');

        Schema::create('comitentes_usuarios', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('comitente_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('comitentes_usuarios', function (Blueprint $table) {
            $table->foreign('comitente_id')->references('id')->on('comitentes');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
