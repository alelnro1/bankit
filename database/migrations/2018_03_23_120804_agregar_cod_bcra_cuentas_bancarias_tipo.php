<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCodBcraCuentasBancariasTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_bancarias_tipo', function (Blueprint $table) {
            $table->string('cod_bcra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuentas_bancarias_tipo', function (Blueprint $table) {
            $table->dropColumn('cod_bcra');
        });
    }
}
