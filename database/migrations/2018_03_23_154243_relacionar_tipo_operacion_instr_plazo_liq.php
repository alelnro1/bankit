<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelacionarTipoOperacionInstrPlazoLiq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->foreign('tipo_operacion_id')->references('id')->on('tipos_operaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->dropForeign('tipo_operacion_id');
        });
    }
}
