<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackendPermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backend_permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        \App\Models\BackendPermiso::create(['nombre' => 'Gerencia']);
        \App\Models\BackendPermiso::create(['nombre' => 'Usuarios Internos']);
        \App\Models\BackendPermiso::create(['nombre' => 'Tesorería']);
        \App\Models\BackendPermiso::create(['nombre' => 'Operaciones FCI']);
        \App\Models\BackendPermiso::create(['nombre' => 'ABM FCI']);
        \App\Models\BackendPermiso::create(['nombre' => 'Prospectos']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backend_permisos');
    }
}
