<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('tipos_instrumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('instrumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_instrumento_id')->unsigned();
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('tipo_instrumento_id')->references('id')->on('tipos_instrumentos');
        });

        Schema::create('instrumento_cotizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrumento_id')->unsigned();
            $table->dateTime('fecha_cotizacion');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
        });

        Schema::create('instrumento_moneda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrumento_id')->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('moneda_id')->references('id')->on('monedas');
        });

        Schema::create('mercados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('depositos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moneda_id')->unsigned();
            $table->integer('comitente_id')->unsigned();
            $table->integer('cuenta_bancaria_id')->unsigned();
            $table->dateTime('fecha_concertacion');
            $table->dateTime('fecha_aprobacion')->nullable();
            $table->dateTime('fecha_liquidacion')->nullable();
            $table->decimal('importe',18,4);
            $table->string('comprobante');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
        });

        Schema::create('transferencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moneda_id')->unsigned();
            $table->integer('comitente_id')->unsigned();
            $table->integer('cuenta_bancaria_id')->unsigned();
            $table->dateTime('fecha_concertacion');
            $table->dateTime('fecha_aprobacion')->nullable();
            $table->dateTime('fecha_liquidacion')->nullable();
            $table->decimal('importe',18,4);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cuenta_bancaria_id')->references('id')->on('cuentas_bancarias');
        });

        Schema::create('instrumentos_operaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrumento_id')->unsigned();
            $table->integer('moneda_id')->unsigned();
            $table->integer('comitente_id')->unsigned();
            $table->dateTime('fecha_concertacion');
            $table->dateTime('fecha_aprobacion')->nullable();
            $table->dateTime('fecha_envio_mercado')->nullable();
            $table->dateTime('fecha_liquidacion')->nullable();
            $table->integer('deposito_id')->unsigned()->nullable();
            $table->integer('transferencia_id')->unsigned()->nullable();
            $table->decimal('cantidad',18,7);
            $table->decimal('importe',18,2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('deposito_id')->references('id')->on('depositos');
            $table->foreign('transferencia_id')->references('id')->on('transferencias');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
            $table->foreign('moneda_id')->references('id')->on('monedas');
        });

        Schema::create('cuentas_corrientes_monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('moneda_id')->unsigned();
            $table->integer('comitente_id')->unsigned();
            $table->dateTime('fecha_concertacion');
            $table->dateTime('fecha_liquidacion');
            $table->string('registro_tabla');
            $table->integer('registro_id');
            $table->decimal('cantidad',18,7);
            $table->decimal('importe',18,2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('comitente_id')->references('id')->on('comitentes');
            $table->foreign('moneda_id')->references('id')->on('monedas');
        });

        Schema::create('cuentas_corrientes_instrumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrumento_id')->unsigned();
            $table->integer('comitente_id')->unsigned();
            $table->dateTime('fecha_concertacion');
            $table->dateTime('fecha_liquidacion');
            $table->string('registro_tabla');
            $table->integer('registro_id');
            $table->decimal('cantidad',18,7);
            $table->decimal('importe',18,2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('cuentas_corrientes_monedas');
        Schema::dropIfExists('cuentas_corrientes_instrumentos');
        Schema::dropIfExists('instrumento_moneda');
        Schema::dropIfExists('instrumento_cotizaciones');
        Schema::dropIfExists('depositos');
        Schema::dropIfExists('transferencias');
        Schema::dropIfExists('mercados');
        Schema::dropIfExists('instrumentos_operaciones');
        Schema::dropIfExists('instrumentos');
        Schema::dropIfExists('tipos_instrumentos');
        Schema::enableForeignKeyConstraints();
    }
}
