<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableNumeroCuentaCuentasBancarias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_bancarias', function (Blueprint $table) {
            $table->string('numero_cuenta')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuentas_bancarias', function (Blueprint $table) {
            $table->string('numero_cuenta')->nullable(false)->change();
        });
    }
}
