<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldMoneda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('instrumentos', function (Blueprint $table) {
            $table->unsignedInteger('moneda_id')->nullable();
             $table->foreign('moneda_id')->references('id')->on('monedas');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos', function (Blueprint $table) {
            $table->dropForeign('instrumentos_moneda_id_foreign');
            $table->dropColumn('moneda_id');
        });
    }
}
