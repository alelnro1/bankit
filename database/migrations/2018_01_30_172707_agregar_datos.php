<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarDatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2018 a las 17:47:10
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = \"+00:00\";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bankit`
--

--
-- Volcado de datos para la tabla `backend_users`
--

INSERT INTO `backend_users` (`id`, `nombre`, `apellido`, `email`, `password`, `intentos_fallidos`, `cambio_password`, `created_at`, `updated_at`, `deleted_at`, `username`) VALUES
(1, 'Alejandro', 'Ponzo', 'aleponzo1@gmail.com', '$2y$10\$JU2P.IhJpKImsiYLpBTXQ.wCGqa9.9sVbkx84jR/hQpW9VkCkynNm', 0, 0, '2018-01-10 10:17:53', '2018-01-24 11:04:45', NULL, NULL);

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id`, `descripcion`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HSBC Bank', '2018-01-09 21:33:56', NULL, NULL),
(2, 'Standard Bank', '2018-01-09 21:33:56', NULL, NULL),
(4, 'Megane Roob', '2018-01-10 15:43:24', '2018-01-10 15:43:24', NULL),
(5, 'Ezequiel Rosenbaum I', '2018-01-10 15:44:06', '2018-01-10 15:44:06', NULL),
(6, 'Elijah Schoen', '2018-01-30 14:34:39', '2018-01-30 14:34:39', NULL);

--
-- Volcado de datos para la tabla `comitentes`
--

INSERT INTO `comitentes` (`id`, `descripcion`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AVILA, LEONEL', '2018-01-09 21:33:36', NULL, NULL),
(3, 'Dr. Torrance Klocko IV', '2018-01-10 15:43:24', '2018-01-10 15:43:24', NULL),
(4, 'Alexandra Rath', '2018-01-10 15:44:06', '2018-01-10 15:44:06', NULL),
(5, 'Trevion Simonis', '2018-01-30 14:34:39', '2018-01-30 14:34:39', NULL);

--
-- Volcado de datos para la tabla `comitentes_usuarios`
--

INSERT INTO `comitentes_usuarios` (`id`, `user_id`, `comitente_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-01-10 10:02:04', NULL, NULL);

--
-- Volcado de datos para la tabla `cuentas_bancarias`
--

INSERT INTO `cuentas_bancarias` (`id`, `banco_id`, `moneda_id`, `estado_id`, `comitente_id`, `numero_cuenta`, `cbu`, `alias`, `comprobante`, `created_at`, `updated_at`, `deleted_at`, `es_default`, `cuit`, `cuenta_bancaria_tipo_id`, `user_id`, `backend_user_id`) VALUES
(1, 1, 1, 3, 1, '123456/7', '12345689012345678901', 'LeonelGastonAvilaP', '', '2018-01-10 09:04:07', '2018-01-29 15:17:14', NULL, 1, '20351477718', 2, 1, 1),
(2, 5, 5, 2, 4, '57', '387262', 'hahn', '', '2018-01-10 15:44:06', '2018-01-10 15:44:06', NULL, 1, '82', 1, 1, NULL),
(3, 1, 1, 2, 1, '123123', '123123', 'ALEPONZOSANTA', '', '2018-01-12 14:58:55', '2018-01-29 15:19:32', NULL, 1, '20373561690', 1, 2, NULL),
(4, 1, 1, 2, 1, '123123', '123123', 'ALEPONZOSANTA', '', '2018-01-12 15:02:36', '2018-01-29 18:39:27', NULL, 1, '20373561690', 1, 2, 1),
(5, 1, 2, 1, 1, '12312312', '123123', 'TESTDOLAR', '', '2018-01-29 16:02:15', '2018-01-29 16:02:15', NULL, 1, '123123', 1, 2, NULL);

--
-- Volcado de datos para la tabla `cuentas_bancarias_propias`
--

INSERT INTO `cuentas_bancarias_propias` (`id`, `descripcion`, `moneda_id`, `banco_id`, `numero_cuenta`, `cbu`, `alias`, `opera_web`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HSBC Pesos', 1, 2, '1234567/8', '123456789012345678901', 'PruebaCuentaDepositante', 1, '2018-01-09 21:40:32', NULL, NULL);

--
-- Volcado de datos para la tabla `cuentas_bancarias_tipo`
--

INSERT INTO `cuentas_bancarias_tipo` (`id`, `descripcion`, `abreviatura`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Caja de Ahorro', 'C/A', '2018-01-10 08:58:52', NULL, NULL),
(2, 'Cuenta Corriente', 'CC', '2018-01-10 08:58:52', NULL, NULL);

--
-- Volcado de datos para la tabla `cuentas_corrientes_monedas`
--

INSERT INTO `cuentas_corrientes_monedas` (`id`, `origen_tabla`, `origen_id`, `moneda_id`, `fecha_movimiento`, `fecha_liquidacion`, `importe`, `created_at`, `updated_at`, `deleted_at`, `comitente_id`) VALUES
(1, 'depositos', 1, 1, '2018-01-10', '2018-01-12', '100000.0000', '2018-01-10 09:10:49', NULL, NULL, 1),
(2, 'transferencias', 1, 1, '2018-01-10', '2018-01-12', '75000.0000', '2018-01-10 09:11:19', NULL, NULL, 1);

--
-- Volcado de datos para la tabla `depositos`
--

INSERT INTO `depositos` (`id`, `cuenta_propia_id`, `fecha_movimiento`, `fecha_liquidacion`, `estado_id`, `comitente_id`, `importe`, `created_at`, `updated_at`, `deleted_at`, `comprobante`, `user_id`, `backend_user_id`) VALUES
(4, 1, '2018-01-10', '2018-01-11', 1, 1, '100000.00', '2018-01-10 09:05:26', '2018-01-10 10:19:44', NULL, NULL, 1, 1);

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `descripcion`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pendiente', '2018-01-10 09:02:51', NULL, NULL),
(2, 'Aprobado', '2018-01-10 09:03:11', NULL, NULL),
(3, 'Desaprobado', '2018-01-10 09:03:11', NULL, NULL);

--
-- Volcado de datos para la tabla `monedas`
--

INSERT INTO `monedas` (`id`, `descripcion`, `simbolo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pesos', '$', '2018-01-09 21:34:28', NULL, NULL),
(2, 'Dolares', 'USD', '2018-01-09 21:34:40', NULL, NULL),
(3, 'Mr. Pete Raynor', 'SSP', '2018-01-30 14:34:39', '2018-01-30 14:34:39', NULL);

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('aleponzo1@gmail.com', '$2y$10\$tfpgptMFvi2QeNU7agVVX.qow.dLMTk6fjsGG9n0zvVU6kCIcLxuu', '2018-01-29 21:26:22');

--
-- Volcado de datos para la tabla `transferencias`
--

INSERT INTO `transferencias` (`id`, `comitente_id`, `estado_id`, `moneda_id`, `cuenta_bancaria_id`, `created_at`, `updated_at`, `deleted_at`, `importe`, `fecha_movimiento`, `fecha_liquidacion`, `user_id`, `backend_user_id`) VALUES
(1, 1, 1, 1, 1, '2018-01-10 09:06:27', '2018-01-10 10:20:21', NULL, '75000.0000', '2018-01-10', '2018-01-10', 1, 1);

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `apellido`, `email`, `password`, `intentos_fallidos`, `cambio_password`, `created_at`, `updated_at`, `deleted_at`, `username`, `remember_token`) VALUES
(1, 'Leonel', 'Avila', 'lavila@wf-systems.com', 'Argentina1', 0, 0, '2018-01-10 10:01:14', NULL, NULL, NULL, NULL),
(2, 'Alejandro', 'Ponzo', 'aleponzo1@gmail.com', '$2y$10\$YjwHIvsJ2SK6RHPR6BatueKL7rvyvzzCOr97EzI5HqsA4v6ErKRgW', 0, 1, '2018-01-10 16:37:01', '2018-01-29 15:51:45', NULL, NULL, 'BFPDiWkbefW8g4BrUPGDfKa2wIZn5RVNpUA91ofgNluDVwH6l62XDYmyZwpp');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
