<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCampoPersonasProspectosAgregadoPorDni extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas_prospectos', function (Blueprint $table) {
            $table->boolean('agregado_por_dni')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas_prospectos', function (Blueprint $table) {
            $table->dropColumn('agregado_por_dni');
        });
    }
}
