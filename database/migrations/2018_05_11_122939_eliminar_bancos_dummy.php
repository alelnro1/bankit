<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EliminarBancosDummy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DELETE FROM transferencias WHERE cuenta_bancaria_id = 3");

        DB::unprepared("DELETE FROM depositos WHERE cuenta_bancaria_id = 3");

        DB::unprepared("DELETE FROM cuentas_bancarias WHERE banco_id <= 8");

        DB::unprepared("DELETE FROM bancos WHERE id <= 8");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
