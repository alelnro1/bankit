<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForaneasCotizacionesInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cotizaciones_instrumentos', function (Blueprint $table) {
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('moneda_id')->references('id')->on('monedas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cotizaciones_instrumentos', function (Blueprint $table) {
            $table->dropForeign([
                'moneda_id'
            ]);
        });
    }
}
