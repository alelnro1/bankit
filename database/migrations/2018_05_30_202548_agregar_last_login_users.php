<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarLastLoginUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('last_login')->nullable();
        });

        Schema::table('backend_users', function (Blueprint $table) {
            $table->dateTime('last_login')->nullable();
        });

        Schema::table('prospectos', function (Blueprint $table) {
            $table->dateTime('last_login')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_login');
        });

        Schema::table('backend_users', function (Blueprint $table) {
            $table->dropColumn('last_login');
        });

        Schema::table('prospectos', function (Blueprint $table) {
            $table->dropColumn('last_login');
        });
    }
}
