<?php

use App\Models\Operaciones\TipoInstrumento;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgrgarTiposOperacionesFondos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $operacion_suscripcion_id = TipoOperacion::where('nombre', 'Suscripción')->first()->id;
        $operacion_rescate_id = TipoOperacion::where('nombre', 'Rescate')->first()->id;
        $instrumento_fondos = TipoInstrumento::where('descripcion', 'Fondos')->first()->id;

        DB::unprepared("INSERT INTO tipos_instrumentos_tipos_operaciones (tipo_instrumento_id, tipo_operacion_id) VALUES ($instrumento_fondos, $operacion_suscripcion_id)");
        DB::unprepared("INSERT INTO tipos_instrumentos_tipos_operaciones (tipo_instrumento_id, tipo_operacion_id) VALUES ($instrumento_fondos, $operacion_rescate_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
