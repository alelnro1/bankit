<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetPermisosBackend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            "delete from backend_permisos_users;"
        );

        DB::unprepared(
            "delete from backend_permisos;
             ALTER TABLE backend_permisos AUTO_INCREMENT = 1;"
        );

        DB::unprepared(
            "
            insert into backend_permisos (nombre) values ('Administrador');
            insert into backend_permisos (nombre) values ('Usuarios Internos');
            insert into backend_permisos (nombre) values ('Fondos');
            insert into backend_permisos (nombre) values ('Operaciones');
            insert into backend_permisos (nombre) values ('Prospectos');
            insert into backend_permisos (nombre) values ('Comitentes');
            "
        );

        DB::unprepared(
            "insert into backend_permisos_users (backend_user_id, backend_permiso_id)
             (select id,1 from backend_users);"
        );


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
