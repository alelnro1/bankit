<?php

use App\Models\Operaciones\TipoInstrumento;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTipoInstrumentoFondos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TipoInstrumento::create(['descripcion' => 'Fondos']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TipoInstrumento::where('descripcion', 'Fondos')->first()->delete();
    }
}
