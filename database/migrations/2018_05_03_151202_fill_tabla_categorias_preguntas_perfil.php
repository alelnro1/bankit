<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillTablaCategoriasPreguntasPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias_preguntas_perfil', function (Blueprint $table) {
            $table->string('slug');
        });

        DB::unprepared("
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Riesgo', 'Busco rentabilidad y estoy dispuesto a asumir riesgos en ello', '3', 'riesgo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Riesgo', 'Busco un equilibrio entre seguridad y rentabilidad', '2', 'riesgo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Riesgo', 'Prefiero seguridad a rentabilidad', '1', 'riesgo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Riesgo', 'La seguridad es tan importante que quiero tomar el menor riesgo posible', '0', 'riesgo');
            
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Tiempo', 'Tengo suficiente tiempo disponible para el manejo de mis inversiones', '3', 'tiempo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Tiempo', 'Tengo algún tiempo disponible para el manejo de mis inversiones', '2', 'tiempo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Tiempo', 'Tengo poco tiempo disponible para el manejo de mis inversiones', '1', 'tiempo');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Tiempo', 'Realmente no tengo tiempo para el manejo de mis inversiones', '0', 'tiempo');
            
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Horizonte de Inversión', 'Mi horizonte de inversión es mayor a 3 años', '3', 'horizonte-inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Horizonte de Inversión', 'Mi horizonte de inversión está entre 1 y 3 años', '2', 'horizonte-inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Horizonte de Inversión', 'Mi horizonte de inversión es menor a 1 año', '1', 'horizonte-inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Horizonte de Inversión', 'Mi horizonte de inversión es diario', '0', 'horizonte-inversion');
            
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Inversión', 'Llevo invirtiendo en bolsa más de 3 años', '3', 'inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Inversión', 'Llevo invirtiendo en bolsa entre 1 y 3 años', '2', 'inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Inversión', 'Llevo invirtiendo en bolsa menos de 1 año', '1', 'inversion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Inversión', 'No he invertido en bolsa aún', '0', 'inversion');
            
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Información', 'Estoy muy informado de los temas financieros y económicos', '3', 'informacion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Información', 'Estoy informado de temas financieros y económicos', '2', 'informacion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Información', 'Manejo poca información de temas financieros o económicos', '1', 'informacion');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Información', 'Realmente no estoy informado de temas financieros o económicos', '0', 'informacion');
            
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Experiencia', 'Llevo invirtiendo en bolsa más de 3 años', '3', 'experiencia');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Experiencia', 'Llevo invirtiendo en bolsa entre 1 y 3 años', '2', 'experiencia');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Experiencia', 'Llevo invirtiendo en bolsa menos de 1 año', '1', 'experiencia');
            INSERT INTO categorias_preguntas_perfil (categoria, pregunta, valor, slug) VALUES ('Experiencia', 'No he invertido en bolsa aún', '0', 'experiencia');
            
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias_preguntas_perfil', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
