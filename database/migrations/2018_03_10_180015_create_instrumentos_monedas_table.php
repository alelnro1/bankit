<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentosMonedasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrumentos_monedas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('instrumento_id')->unsigned();
            $table->integer('moneda_id')->unsigned();

            $table->timestamps();
        });

        // NOTAR LA DIFERENCIA DEL METODO "table", SE USA SOBRE UNA TABLA YA EXISTENTE
        Schema::table('instrumentos_monedas', function (Blueprint $table) {
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('moneda_id')->references('id')->on('monedas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos_monedas', function (Blueprint $table) {
            $table->dropForeign('instrumentos_monedas_instrumento_id_foreign');
            $table->dropForeign('instrumentos_monedas_moneda_id_foreign');
        });

        Schema::dropIfExists('instrumentos_monedas');
    }
}
