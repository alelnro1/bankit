<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('instrumento_cotizaciones');

        Schema::create('cotizaciones_instrumentos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('instrumento_id')->unsigned();
            $table->double('importe');
            $table->double('patrimonio')->nullable();
            $table->double('tasa_mensual')->nullable();
            $table->integer('moneda_id')->unsigned();
            $table->date('fecha_cotizacion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones_instrumentos');
    }
}
