<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCodigoPostalSociedadesDirecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sociedades_direcciones', function (Blueprint $table) {
            $table->string('codigo_postal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sociedades_direcciones', function (Blueprint $table) {
            $table->dropColumn('codigo_postal');
        });
    }
}
