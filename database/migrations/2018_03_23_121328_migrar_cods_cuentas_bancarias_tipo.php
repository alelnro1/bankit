<?php

use App\Models\Tesoreria\CuentasBancarias\TipoCuentaBancaria;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrarCodsCuentasBancariasTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $caja_ahorro = TipoCuentaBancaria::where('descripcion', 'Caja de Ahorro')->first();
        $cuenta_corriente = TipoCuentaBancaria::where('descripcion', 'Cuenta Corriente')->first();

        $caja_ahorro->cod_bcra = 10;
        $cuenta_corriente->cod_bcra = 20;

        $caja_ahorro->save();
        $cuenta_corriente->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
