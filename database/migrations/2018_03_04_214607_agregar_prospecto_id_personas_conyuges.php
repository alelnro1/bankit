<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarProspectoIdPersonasConyuges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas_conyuges', function (Blueprint $table) {
            $table->integer('prospecto_id')->unsigned()->nullable();
        });

        Schema::table('personas_conyuges', function (Blueprint $table) {
            $table->foreign('prospecto_id')->references('id')->on('prospectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('personas_conyuges', function (Blueprint $table) {
            $table->dropForeign('personas_conyuges_prospecto_id_foreign');
        });

        Schema::table('personas_conyuges', function (Blueprint $table) {
            $table->dropColumn('prospecto_id');
        });
    }
}
