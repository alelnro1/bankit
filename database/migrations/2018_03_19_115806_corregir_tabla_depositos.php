<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorregirTablaDepositos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('depositos', function (Blueprint $table) {
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
        });

        Schema::table('depositos', function (Blueprint $table) {
            $table->string('comprobante')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
