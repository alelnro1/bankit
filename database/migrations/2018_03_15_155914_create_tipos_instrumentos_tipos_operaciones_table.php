<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposInstrumentosTiposOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_instrumentos_tipos_operaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tipo_instrumento_id')->unsigned();
            $table->integer('tipo_operacion_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_instrumentos_tipos_operaciones');
    }
}
