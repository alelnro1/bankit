<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasDirecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('personas');

        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('apellido');
            $table->integer('tipo_documento_id')->unsigned();
            $table->string('numero_documento');
            $table->integer('sexo_id')->unsigned();
            $table->integer('estado_civil_id')->unsigned();
            $table->integer('tipo_responsable_iva_id')->unsigned();
            $table->integer('tipo_responsable_ganancias_id')->unsigned();
            $table->string('telefono_particular');
            $table->string('telefono_movil');
            $table->string('email');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('personas', function (Blueprint $table) {
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_documentos');
            $table->foreign('sexo_id')->references('id')->on('sexos');
            $table->foreign('estado_civil_id')->references('id')->on('estados_civiles');
            $table->foreign('tipo_responsable_iva_id')->references('id')->on('tipos_responsables_iva');
            $table->foreign('tipo_responsable_ganancias_id')->references('id')->on('tipos_responsales_ganancias');
        });

        Schema::create('personas_direcciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('calle');
            $table->integer('altura');
            $table->integer('piso');
            $table->string('departamento');

            $table->string('localidad');
            $table->string('codigo_postal');
            $table->string('provincia');
            $table->string('pais');

            $table->integer('persona_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('personas_direcciones', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
