<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarOperacionIdInstrumentosPlazoLiq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->integer('tipo_operacion_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->dropColumn('tipo_operacion_id');
        });
    }
}
