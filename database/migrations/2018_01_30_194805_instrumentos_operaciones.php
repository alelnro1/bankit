<?php

use App\Models\Operaciones\TipoInstrumento;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstrumentosOperaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrumentos_operaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_instrumento_id')->unsigned();
            $table->integer('tipo_operacion_id')->unsigned();
        });

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->foreign('tipo_instrumento_id')->references('id')->on('tipos_instrumentos');
            $table->foreign('tipo_operacion_id')->references('id')->on('tipos_operaciones');
        });

        $instrumento_fondos_id = TipoInstrumento::where('descripcion', 'Fondos')->first()->id;

        $operacion_suscripcion_id = TipoOperacion::where('nombre', 'Suscripción')->first()->id;
        $operacion_rescate_id = TipoOperacion::where('nombre', 'Rescate')->first()->id;

        DB::unprepared("INSERT INTO instrumentos_operaciones (tipo_instrumento_id, tipo_operacion_id) VALUES ('$instrumento_fondos_id', '$operacion_suscripcion_id')");
        DB::unprepared("INSERT INTO instrumentos_operaciones (tipo_instrumento_id, tipo_operacion_id) VALUES ('$instrumento_fondos_id', '$operacion_rescate_id')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
