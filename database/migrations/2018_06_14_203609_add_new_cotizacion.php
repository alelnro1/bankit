<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCotizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::unprepared("INSERT INTO `cotizaciones_instrumentos` (`id`, `instrumento_id`, `patrimonio`, `moneda_id`, `fecha_cotizacion`, `created_at`, `updated_at`, `valor_compra`, `valor_venta`, `var_diaria`, `var_mensual`, `var_anual`, `cant_cuotapartes`) VALUES (NULL, '2', '55434371', '2', '2018-06-01', NULL, NULL, '6.50', '6.50', '0.17', '0.35', '0.47', '134375431')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
