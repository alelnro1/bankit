<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInstrumentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos', function (Blueprint $table) {
            $table->string('familia')->nullable();
            $table->string('categoria')->nullable();
            $table->string('codigo_bloomberg')->nullable();
            $table->string('sociedad_gerente')->nullable();
            $table->string('sociedad_custodia')->nullable();
            $table->string('benchmark')->nullable();
            $table->string('codigo_caja_valores')->nullable();
            $table->string('permanencia_recomendada')->nullable();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos', function (Blueprint $table) {
            $table->dropColumn('familia');
            $table->dropColumn('categoria');
            $table->dropColumn('sociedad_gerente');
            $table->dropColumn('sociedad_custodia');
            $table->dropColumn('codigo_bloomberg');
            $table->dropColumn('benchmark');
            $table->dropColumn('codigo_caja_valores');
            $table->dropColumn('permanencia_recomendada');
        });

    }
}
