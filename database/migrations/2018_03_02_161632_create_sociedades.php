<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSociedades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sociedades', function (Blueprint $table) {
            $table->increments('id');

            $table->string('razon_social')->nullable();
            $table->date('fecha_constitucion')->nullable();
            $table->string('cuit')->nullable();
            $table->integer('cod_area_celular')->nullable();
            $table->integer('prefijo_celular')->nullable();
            $table->integer('telefono_celular')->nullable();
            $table->string('email')->nullable()->unique();

            $table->integer('prospecto_id')->unsigned();
            $table->integer('comitente_id')->unsigned()->nullable();

            $table->timestamps();
        });

        Schema::table('sociedades', function (Blueprint $table) {
            $table->foreign('prospecto_id')->references('id')->on('prospectos');
            $table->foreign('comitente_id')->references('id')->on('comitentes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sociedades', function (Blueprint $table) {
            $table->dropForeign('prospecto_id');
            $table->dropForeign('comitente_id');
        });

        Schema::dropIfExists('sociedades');
    }
}
