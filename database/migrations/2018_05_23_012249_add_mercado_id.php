<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMercadoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::unprepared("INSERT INTO mercados (descripcion) VALUES ('Gainvest');");

        Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->unsignedInteger('mercado_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('instrumentos_operaciones', function (Blueprint $table) {
            $table->dropColumn('mercado_id');
        });
    }
}
