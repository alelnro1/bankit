<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearForaneasInstrumentosPlazoLiq extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->foreign('instrumento_id')->references('id')->on('instrumentos');
            $table->foreign('plazo_liquidacion_id')->references('id')->on('plazos_liquidaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumentos_plazo_liq', function (Blueprint $table) {
            $table->dropForeign([
                'instrumentos_plazo_liq_instrumento_id_foreign',
                'instrumentos_plazo_liq_plazo_liquidacion_id_foreign'
            ]);
        });
    }
}
