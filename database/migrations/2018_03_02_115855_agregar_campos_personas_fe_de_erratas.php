<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCamposPersonasFeDeErratas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->integer('cod_area_celular')->nullable();
            $table->integer('prefijo_celular')->nullable();
            $table->integer('cod_area')->nullable();
            $table->string('pais_nacimiento')->nullable();
            $table->string('provincia_nacimiento')->nullable();
            $table->string('fecha_nacimiento')->nullable();
            $table->boolean('es_funcionario_publico')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function (Blueprint $table) {
            $table->dropColumn([
                'cod_area_celular',
                'prefijo_celular',
                'cod_area',
                'pais_nacimiento',
                'provincia_nacimiento',
                'fecha_nacimiento',
                'es_funcionario_publico'
            ]);
        });
    }
}
