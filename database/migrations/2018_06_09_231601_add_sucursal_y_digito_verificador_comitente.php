<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSucursalYDigitoVerificadorComitente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comitentes', function (Blueprint $table) {
            $table->string('sucursal', 10);
            $table->string('digito_verificador', 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comitentes', function (Blueprint $table) {
            $table->dropColumn('sucursal');
            $table->dropColumn('digito_verificador');
        });
    }
}
