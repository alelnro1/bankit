<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarTiposDocumentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\TipoDocumento::insert(['descripcion' => 'DNI']);
        \App\Models\TipoDocumento::insert(['descripcion' => 'CUIT']);
        \App\Models\TipoDocumento::insert(['descripcion' => 'CUIL']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tipo_documento_dni = \App\Models\TipoDocumento::where('descripcion', 'DNI');
        $tipo_documento_cuit = \App\Models\TipoDocumento::where('descripcion', 'CUIT');
        $tipo_documento_cuil = \App\Models\TipoDocumento::where('descripcion', 'CUIL');

        $tipo_documento_dni->remove();
        $tipo_documento_cuit->remove();
        $tipo_documento_cuil->remove();
    }
}
