<?php

use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Faker\Generator as Faker;

$factory->define(CuentaBancaria::class, function (Faker $faker) {
    return [
        'banco_id' => function() {
            return factory(App\Models\Banco::class)->create()->id;
        },
        'moneda_id' => function() {
            return factory(App\Models\Monedas\MonedaBase::class)->create()->id;
        },
        'estado_id' => rand(1, 3),
        'comitente_id' => function() {
            return factory(App\Models\Comitente::class)->create()->id;
        },
        'numero_cuenta' => $faker->randomNumber,
        'cbu' => $faker->randomNumber,
        'alias' => $faker->domainWord,
        'comprobante' => '',
        'es_default' => 1,
        'cuit' => $faker->randomNumber,
        'cuenta_bancaria_tipo_id' => rand(1, 2),
        'user_id' => 1,
        'backend_user_id' => null
    ];
});
