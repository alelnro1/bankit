<?php

use App\Models\Monedas\MonedaBase;
use Faker\Generator as Faker;

$factory->define(MonedaBase::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name,
        'simbolo' => $faker->currencyCode
    ];
});
