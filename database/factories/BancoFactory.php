<?php

use App\Models\Banco;
use Faker\Generator as Faker;


$factory->define(Banco::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name
    ];
});
