<?php

use App\Models\Comitente;
use Faker\Generator as Faker;

$factory->define(Comitente::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name
    ];
});
