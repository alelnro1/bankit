let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
        'resources/assets/js/lib/jquery.mask.min.js',
        'resources/assets/js/lib/jqueryui.js',
        'resources/assets/js/lib/izimodal.js',
        'resources/assets/js/lib/datatables.min.js',
        'resources/assets/js/lib/tiny-draggable.js',
        'resources/assets/js/lib/selectpicker.js',
        'resources/assets/js/lib/jquery.confirm.js',
        'resources/assets/js/lib/bootstrap-multiselect.js',
        'resources/assets/js/lib/notify.js',
        'resources/assets/js/lib/number_format.js'
    ], 'public/js/lib.js')
    .scripts([
        'resources/assets/js/lib/datetimepicker/moment.js',
        'resources/assets/js/lib/datetimepicker/datetimepicker.js',
        'resources/assets/js/ajaxSetup.js',

        'resources/assets/js/frontend/tesoreria/cuentas-bancarias/*.js',

        'resources/assets/js/frontend/form-operar/eventos.js',
        'resources/assets/js/frontend/form-operar/commons/*.js',
        'resources/assets/js/frontend/form-operar/previsualizacion/*.js',
        'resources/assets/js/frontend/form-operar/operar/*.js',

        'resources/assets/js/frontend/cotizaciones/*.js',

        'resources/assets/js/frontend/cambiar-comitente.js',
        'resources/assets/js/frontend/home.js',

        'resources/assets/js/preloader.js'
    ], 'public/js/scripts.js')
    .scripts([
        'resources/assets/js/lib/datetimepicker/moment.js',
        'resources/assets/js/lib/datetimepicker/datetimepicker.js',
        'resources/assets/js/ajaxSetup.js',

        'resources/assets/js/lib/jqueryui.js',
        'resources/assets/js/lib/jquery.confirm.js',

        'resources/assets/js/backend/tesoreria/cuentas-bancarias/*.js',
        'resources/assets/js/backend/prospectos/*.js',
        'resources/assets/js/backend/operaciones/*.js',
        'resources/assets/js/backend/fci/*.js',
        'resources/assets/js/backend/internos/*.js',

        'resources/assets/js/preloader.js'

    ], 'public/js/backend.js')
    .scripts([
        'resources/assets/js/ajaxSetup.js',
        'resources/assets/js/lib/jqueryui.js',
        'resources/assets/js/preloader.js',

        'resources/assets/js/lib/jquery.confirm.js',
        'resources/assets/js/lib/datetimepicker/moment.js',
        'resources/assets/js/lib/datetimepicker/bootstrap.js',
        'resources/assets/js/lib/datetimepicker/datetimepicker.js',

        'resources/assets/js/apertura-cuenta/*.js'
    ], 'public/js/apertura-cuenta.js')
    .scripts([
        'resources/assets/js/lib/datetimepicker/moment.js',
        'resources/assets/js/lib/datetimepicker/bootstrap.js',
        'resources/assets/js/lib/datetimepicker/datetimepicker.js'
    ], 'public/js/datetimepicker.js')
    .styles([
        'resources/assets/sass/preloader/_css/Icomoon/style.css',
        'resources/assets/sass/preloader/_css/draw1.css',
        'resources/assets/sass/lib/jquery.confirm.css',

        'resources/assets/sass/apertura-cuenta/animate.css',
        'resources/assets/sass/apertura-cuenta/fontawesome.css',
        'resources/assets/sass/apertura-cuenta/multistep.css',

        'resources/assets/sass/lib/datetimepicker.css'
    ], 'public/css/apertura-cuenta.css')

    .styles([
        'resources/assets/sass/preloader/_css/Icomoon/style.css',
        'resources/assets/sass/preloader/_css/draw1.css',
        'resources/assets/sass/lib/jqueryui.css',
        'resources/assets/sass/lib/izimodal.css',
        'resources/assets/sass/lib/datatables.css',
        'resources/assets/sass/lib/jquery.confirm.css',
        'resources/assets/sass/lib/bootstrap-multiselect.css'
    ], 'public/css/styles.css');
