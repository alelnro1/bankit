<?php

namespace App\Console\Commands;

use App\User;
use App\BackendUser;
use Illuminate\Console\Command;

class ForzarCambiosdePassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seguridad:forzar-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Forzamos a cambiar la contraseña luego de 90 dias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dias = env("DIAS_CADUCIDAD_PASSWORD");
        $fecha=date('Y-m-d', strtotime("-".$dias." days"));

        /*Usuarios del Front */
        $users = User::where("cambio_password_fecha","<=", $fecha)->orWhere("cambio_password_fecha",null)->get();
        
        foreach ($users as $user) {
            $user->cambio_password=1;
            $user->save();
        }

        /*Usuarios del Back */
        $users = BackendUser::where("cambio_password_fecha","<=", $fecha)->orWhere("cambio_password_fecha",null)->get();
        
        foreach ($users as $user) {
            $user->cambio_password=1;
            $user->save();
        }

    }
}
