<?php

namespace App\Console\Commands;

use App\Models\Instrumentos\CotizacionInstrumento;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\TipoInstrumento;
use Illuminate\Console\Command;

class ActualizarCotizacionesFondos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cotizaciones:fondos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizamos las cotizaciones de los fondos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();

        $res = $client->request('POST', env('INTL_WS_URL') . 'fondos', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ],
            'json' => [
                'token' => env('INTL_WS_TOKEN')
            ]
        ]);

        $fondos_api = collect(json_decode($res->getBody()))->keyBy(function ($item){
            return strtoupper($item->DescFondo);
        });

        // Buscamos los fondos ya creados
        $tipo_instr_fondos_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        $instrumentos_fondos = Instrumento::getInstrumentosDeTipo($tipo_instr_fondos_id)->keyBy(function ($item){
            return strtoupper($item->descripcion);
        });

        // Si existe el fondo, creamos la cotizacion
        foreach ($fondos_api as $key => $fondo_api) {
            if (isset($instrumentos_fondos[$key])) {
                $fondo_existente = $instrumentos_fondos[$key];

                //$moneda_por_simbolo = MonedaBase::where('simbolo', $fondo_api->CodInterfazMoneda)->first();


                CotizacionInstrumento::create([
                    'instrumento_id' => $fondo_existente->id,
                    'patrimonio' => $fondo_api->PatrimonioNeto,
                    'moneda_id' => $fondo_existente->moneda_id,
                    'fecha_cotizacion' => date("Ymd"),
                    'valor_compra' => $fondo_api->ValorCuotaparte,
                    'valor_venta' => $fondo_api->ValorCuotaparte,
                    'var_diaria' => $fondo_api->Variacion,
                    'var_mensual' => $fondo_api->VariacionMensual,
                    'var_anual' => $fondo_api->VariacionAnual,
                    'cant_cuotapartes' => $fondo_api->CuotapartesCirculacion
                ]);
            }
        }
    }
}
