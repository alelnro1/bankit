<?php

namespace App\Console\Commands;

use App\Classes\WebServices\INTLWebService;
use App\Mail\MontosOperacionActualizados;
use App\Models\Comitente;
use App\Models\Estado;
use App\Models\CuentasCorrientes\CuentaCorrienteInstrumento;
use App\Models\CuentasCorrientes\CuentaCorrienteMoneda;
use App\Models\Instrumentos\CotizacionInstrumento;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\Operacion;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class ActualizarMontosOperaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operaciones:actualizar-montos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizamos los montos de las operaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //TODO: HARCODE FONDO
        $fondos = Instrumento::all();
        foreach ($fondos as $fondo) {

            // 1) Buscamos la ultima cotizacion
            $intl_ws = new INTLWebService();
            $ultimos_datos_fondo = $intl_ws->getUltimosDatosFondo($fondo);

          
            // 2) Verificamos que la cotizacion sea de hoy y no esté en instrumentos_cotizaciones
            if (!$this->cotizacionExiste($ultimos_datos_fondo, $fondo)) {

                // 3) Insertamos en instrumentos_cotizaciones
                $this->insertarCotizacionInstrumento($fondo, $ultimos_datos_fondo);

                // 4) Buscamos las operaciones con fecha de concertacion hoy y cuentas_corrientes inst/mone de esas operaciones que esten aprobadas
                $operaciones = $this->actualizarOperacionesDeHoy($ultimos_datos_fondo, $fondo);

                // 5) Obtenemos todas las cuentas corrientes de las operaciones
                $this->actualizarCtasCtesDeHoy($operaciones, $ultimos_datos_fondo);

                // 6) Enviamos los mails a todos los comitentes de las operaciones que fueron afectadas
                $this->enviarEmails($operaciones);
            }
            
        }


    }

    /**
     * Verifico si la cotizacion del fondo es del dia de hoy
     *
     * @param $ultimos_datos_fondo
     * @return bool
     */
    private function cotizacionEsActual($ultimos_datos_fondo, $fondo)
    {

        $fondo->load([
            'Cotizaciones' => function ($query) {
                $query
                    ->orderBy('fecha_cotizacion', 'DESC')
                    ->take(1)
                    ->with('Moneda');
            }
        ]);
        
        $fecha_cotiz = $this->getFechaCotizacion($ultimos_datos_fondo);
        
        $cotizacion = CotizacionInstrumento::where("fecha_cotizacion",date("Y-m-d",strtotime($fecha_cotiz)))
                                ->where("instrumento_id",$fondo->id)
                                ->count();

        


        return !$cotizacion;
    }

    /**
     * Verifico si la cotizacion ya existe en la tabla de cotizaciones
     *
     * @param $ultimos_datos_fondo
     */
    private function cotizacionExiste($ultimos_datos_fondo, $fondo)
    {

        $fecha_cotiz = $this->getFechaCotizacion($ultimos_datos_fondo)->format("Y-m-d");

        $cotizacion =
            CotizacionInstrumento::where('fecha_cotizacion', $fecha_cotiz)
            ->where('instrumento_id', $fondo->id)->count();


        return $cotizacion;
    }

    /**
     * Obtengo la fecha de la cotizacion en formato Carbon
     *
     * @param $datos_fondo
     * @return static
     */
    private function getFechaCotizacion($datos_fondo)
    {
        
        return
            Carbon::createFromFormat("Y-m-d H:i:s",
                $datos_fondo->Fecha);
    }

    /**
     * Insertamos la cotizacion en cotizaciones_instrumentos
     *
     * @param $ultimos_datos_fondo
     */
    private function insertarCotizacionInstrumento($fondo, $datos_fondo)
    {
        // Busco el ID de la moneda
        $moneda_id = MonedaBase::getIdByNombre($datos_fondo->DescMoneda);

        // Obtenemos la fecha de la cotizacion
        $fecha_cotiz = $this->getFechaCotizacion($datos_fondo);

        CotizacionInstrumento::create([
            'instrumento_id' => $fondo->id,
            'patrimonio' => $datos_fondo->PatrimonioNeto,
            'moneda_id' => $moneda_id,
            'fecha_cotizacion' => $fecha_cotiz,
            'valor_compra' => $datos_fondo->ValorCuotaparte,
            'valor_venta' => $datos_fondo->ValorCuotaparte,
            'var_diaria' => $datos_fondo->Variacion,
            'var_mensual' => $datos_fondo->VariacionMensual,
            'var_anual' => $datos_fondo->VariacionAnual,
            'cant_cuotapartes' => $datos_fondo->CuotapartesCirculacion
        ]);
    }

    /**
     * Obtenemos todas las operaciones que finalizan hoy
     *
     * @return mixed
     */
    private function getOperacionesQuefinalizanHoy($fondo)
    {
        $hoy = Carbon::create()->format('Y-m-d');

        $operaciones = Operacion::whereDate('fecha_concertacion', '<=' , $hoy)
                            ->where("estado_id","2")
                            ->where("instrumento_id",$fondo->id)->get();

        return $operaciones;
    }

    /**
     * Actualizamos los valores de la tabla de instrumetos_operaciones segun el tipo de operacion
     */
    private function actualizarOperacionesDeHoy($datos_fondo,$fondo)
    {
        // Traigo las operaciones cuya fecha de concertacion es hoy
        $operaciones = $this->getOperacionesQuefinalizanHoy($fondo);
        $estado_liquidado = Estado::where("descripcion", "Liquidado")->first();

        foreach ($operaciones as $operacion) {
            $this->actualizarValores($operacion, $datos_fondo);
            
            $operacion->estado_id = $estado_liquidado->id;
            $operacion->save();
        }

        return $operaciones;
    }

    /**
     * Actualizamos los valores de las cuentas corrientes segun el tipo de operacion
     *
     * @param $operaciones
     */
    private function actualizarCtasCtesDeHoy($operaciones, $datos_fondo)
    {

        $ctas_ctes = $this->getCuentasCorrientesDeOperaciones($operaciones);

        foreach ($ctas_ctes as $cta_cte) {
            $this->actualizarValores($cta_cte, $datos_fondo);
        }
    }

    /**
     * Obtenemos un array con todas cuentas corrientes (instrumentos y monedas)
     * de las operaciones recibidas por parametro
     *
     * @param $operaciones
     * @return array
     */
    private function getCuentasCorrientesDeOperaciones($operaciones)
    {
        $operaciones_ids = [];

        // Obtenemos los ids de todas las operaciones que se concertan hoy
        foreach ($operaciones as $operacion) {
            array_push($operaciones_ids, $operacion->id);
        }

        $ctas_ctes = [];

        $cuentas_ctes_monedas = CuentaCorrienteMoneda::whereIn('registro_id', $operaciones_ids)->with('Operacion')->get();
        $cuentas_ctes_instrumentos = CuentaCorrienteInstrumento::whereIn('registro_id', $operaciones_ids)->with('Operacion')->get();

        foreach ($cuentas_ctes_monedas as $cta_cte) {
            array_push($ctas_ctes, $cta_cte);
        }

        foreach ($cuentas_ctes_instrumentos as $cta_cte) {
            array_push($ctas_ctes, $cta_cte);
        }

        return $ctas_ctes;
    }

    /**
     * Actualizamos los valores de la cuenta corriente ó de la operación, segun lo recibamos
     *
     * @param $cta_cte_op
     * @param $datos_fondo
     */
    private function actualizarValores($registro, $datos_fondo)
    {
        if ($registro->esPorMonto()) {
            $registro->cantidad = $registro->importe / $datos_fondo->ValorCuotaparte;
        } else {
            $registro->importe = $registro->importe * $datos_fondo->ValorCuotaparte;
        }

        $registro->save();
    }

    /**
     * Enviamos los emails a todas las personas de los comitentes cuyas operaciones fueron afectadas por este proceso
     *
     * @param $operaciones
     */
    private function enviarEmails($operaciones)
    {
        
        foreach ($operaciones as $operacion) {
            $comitente = Comitente::where('id', $operacion->comitente_id)->first();
            $personas = $comitente->getTitulares();

            // Vamos a buscar los mails de cada persona
            foreach ($personas as $persona) {
                
                $emails = explode(";", $persona->email);
                // Enviamos el mail a cada uno de los mails
                foreach ($emails as $email) {
                    $email=ltrim(rtrim(trim($email)));
                    Mail::to($email)->send(new MontosOperacionActualizados($operacion,$persona));
                }
            }
        }
    }
}
