<?php

namespace App;

use App\Models\Comitente;
use App\Models\Prospectos\PersonasFisicas\Titular;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'email', 'password', 'numero_documento', 'tipo_documento_id','cambio_password_fecha','cambio_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Comitentes()
    {
        return $this->belongsToMany(Comitente::class, 'comitentes_usuarios');
    }

    public function Titulares()
    {
        return $this->belongsToMany(Titular::class, 'comitentes_personas');
    }

    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class);
    }

    public function getNombre()
    {
        return $this->nombre . " " . $this->apellido;
    }

    public function getNumeroComitente()
    {
        return session('NRO_COMITENTE');
    }

    public function getIdComitente()
    {
        return session('ID_COMITENTE');
    }

    public function getNombreComitente()
    {
        return session('NOMBRE_COMITENTE');
    }
}
