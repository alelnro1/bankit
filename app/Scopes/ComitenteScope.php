<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class ComitenteScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Si es un admin, no hace falta validar que el comitente esté logueado
        if (!Auth::guard('backend')->check() && !Auth::guard('apertura-cuenta')->check()) {
            $builder->where('comitente_id', Auth::user()->getIdComitente());
        }
    }
}