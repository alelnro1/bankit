<?php

namespace App\Http\Requests;

use App\Models\Comitente;
use App\Rules\CBUCuentaBancariaCorrespondeATitular;
use App\Rules\CBUUnicoParaComitente;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AltaCuentaBancariaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $comitente_actual_id = Auth::user()->getIdComitente();

        $comitente_actual = Comitente::where('id', $comitente_actual_id)->first();

        return [
            'cuenta_bancaria_tipo_id' => 'required|exists:cuentas_bancarias_tipo,id',
            'banco_id' => 'required|exists:bancos,id',
            'moneda_id' => 'required|exists:monedas,id',
            'cbu' => [
                'required',
                new CBUUnicoParaComitente($comitente_actual_id),
                new CBUCuentaBancariaCorrespondeATitular($comitente_actual, $this->cbu),
            ],
            //'cuit' => 'required',
            'alias' => 'required'
        ];
    }
}
