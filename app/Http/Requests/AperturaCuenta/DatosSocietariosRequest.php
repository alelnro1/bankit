<?php

namespace App\Http\Requests\AperturaCuenta;

use Illuminate\Foundation\Http\FormRequest;

class DatosSocietariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razon_social' => 'required|string',
            'fecha_constitucion' => 'required|string',
            'cuit' => 'required',
            'codigo_area' => 'string|nullable',
            'prefijo_celular' => 'string|nullable',
            'telefono_celular' => 'string|nullable',

            'direccion.calle' => 'string|nullable',
            'direccion.altura' => 'numeric|nullable',
            'direccion.piso' => 'numeric|nullable',
            'direccion.departamento' => 'string|size:1|nullable',
            'direccion.localidad' => 'string|nullable',
            'direcicon.codigo_postal' => 'string|nullable',
            'direccion.provincia' => 'string|nullable',
            'direccion.pais' => 'string|nullable',
        ];
    }

    
}
