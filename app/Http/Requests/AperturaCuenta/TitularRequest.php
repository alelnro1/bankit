<?php

namespace App\Http\Requests\AperturaCuenta;

use App\Rules\CuitContieneDNI;
use App\Rules\EmailsSeparadosPuntoComa;
use App\Rules\PersonaUnicaDocumento;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TitularRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Auth::guard('backend')->user()) {
            $es_admin = true;
            $prospecto_id = null; // Es el admin => no hace falta mandarle el prospecto_id porque lo puede modificar
        } else {
            $es_admin = false;
            $prospecto_id = Auth::guard('apertura-cuenta')->user()->id;
        }

        // Obtenemos el numero de documento del request
        $numero_documento = $this->numero_documento;

        return [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'estado_civil_id' => 'required',
            'cuit_cuil' => 'required',
            'sexo_id' => 'required',
            'tipo_cliente' => 'required',
            'nacionalidad' => 'string|nullable',
            'doble_nacionalidad' => 'string|nullable',
            'email' => ['required', 'string', 'nullable', new EmailsSeparadosPuntoComa],
            'direccion.calle' => 'required|string',
            'direccion.altura' => 'required|numeric',
            'direccion.piso' => 'string|nullable',
            'direccion.departamento' => 'string|nullable',
            'direccion.localidad' => 'required|string',
            'direccion.codigo_postal' => 'required|string',
            'direccion.provincia' => 'required|string',
            'direccion.pais' => 'required|string',
            'direccion.telefono_particular' => 'string',
            'cod_area' => 'required|string',
            'funcion_pep' => 'required_with:pep',
            'prefijo_celular' => 'required|string',
            'telefono_movil' => 'required|string',
            'fecha_nacimiento' => 'date_format:"d/m/Y"',
            'tipo_responsable_iva_id' => 'required|string',
            'conyuge.nombre_conyuge' => 'required_if:estado_civil_id,2|string|nullable',
            'conyuge.apellido_conyuge' => 'required_if:estado_civil_id,2|string|nullable',
            'conyuge.tipo_documento' => 'required_if:estado_civil_id,2|string|nullable',
            'conyuge.numero_documento' => 'required_if:estado_civil_id,2|string|nullable',
            'conyuge.sexo' => 'required_if:estado_civil_id,2|string|nullable',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es obligatorio',
            'apellido.required' => 'El apellido es obligatorio',
            'estado_civil_id.required' => 'El estado civil es obligatorio',
            'cuit_cuil.required' => 'El CUIT es obligatorio',
            'cuit_cuil.required' => 'El CUIT/CUIL es obligatorio',
            'sexo_id.required' => 'Seleccione una opcion',
            'tipo_cliente.required' => 'Seleccione una opcion',
            'direccion.calle.required' => 'La calle es obligatoria',
            'direccion.altura.required' => 'La altura es obligatoria',
            'direccion.localidad.required' => 'La localidad es obligatoria',
            'direccion.codigo_postal.required' => 'El codigo postal es obligatorio',
            'cod_area.required' => 'El codigo de area es obligatorio',
            'prefijo_celular.required' => 'El prefijo es obligatorio',
            'telefono_movil.required' => 'El telefono movil es obligatorio',
            'fecha_nacimiento.date_format' => 'La fecha de nacimiento es invalida, el formato debe ser DD/MM/AAAA',
            'tipo_responsable_iva_id.required' => 'Seleccione una opcion',
            'funcion_pep.required_with' => 'Funcion PEP desconocida',
            
            'conyuge.nombre_conyuge.required_if' => 'El nombre es obligatorio',
            'conyuge.apellido_conyuge.required_if' => 'El apellido es obligatorio',
            'conyuge.tipo_documento.required_if' => 'El tipo de documento es obligatorio',
            'conyuge.numero_documento.required_if' => 'El numero de documento es obligatorio',
            'conyuge.sexo.required_if' => 'Debe seleccionar una opcion',
        ];
    }
}
