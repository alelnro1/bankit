<?php

namespace App\Http\Requests\AperturaCuenta;

use Illuminate\Foundation\Http\FormRequest;

class CuentaBancariaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cuenta_bancaria_tipo_id' => 'required',
            'banco_id' => 'required|string',
            'cbu' => 'required|string',
            'alias' => 'string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cuenta_bancaria_tipo_id.required' => 'El Campo tipo de cuenta es obligatorio',
            'banco_id.required' => 'debe seleccionar un banco',
            'cbu.required' => 'El CBU es obligatorio',
        ];
    }
}
