<?php

namespace App\Http\Requests\AperturaCuenta;

use Illuminate\Foundation\Http\FormRequest;

class PerfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'riesgo' => 'required|numeric',
            'experiencia' => 'required|numeric',
            'informacion' => 'required|numeric',
            'horizonte_inversion' => 'required|numeric',
            'tiempo' => 'required|numeric',
        ];
    }
}
