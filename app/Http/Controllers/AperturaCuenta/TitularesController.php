<?php

namespace App\Http\Controllers\AperturaCuenta;

use App\Http\Requests\AperturaCuenta\TitularRequest;
use App\Models\Prospectos\PersonasFisicas\Conyuge;
use App\Models\Prospectos\PersonasFisicas\Direccion;
use App\Models\Prospectos\PersonasFisicas\Titular;
use App\Models\Prospectos\Prospecto;
use App\Models\TipoDocumento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TitularesController extends ProcesoAperturaController
{
    /**
     * Guardarmos un titular para el prospecto
     *
     * @param TitularRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function guardar(TitularRequest $request)
    {
        /*if (!empty(trim($request->titular_id))) {
            $titular = Titular::where('id', $request->titular_id)->first();
        } else {*/
        // Lo creamos, es nuevo
        $titular = Titular::updateOrCreate(['id' => $request->titular_id], $request->all());



        if ($request->has('pep')) {
            $titular->pep = true;
        } else {
            $titular->pep = false;
        }

        if ($request->has('funcionario_publico')) {
            $titular->funcionario_publico = true;
        } else {
            $titular->funcionario_publico = false;
        }

        if ($request->has('vinculado')) {
            $titular->vinculado = true;
        } else {
            $titular->vinculado = false;
        }
        
        $titular->save();
        // Le agrego el tipo de documento
        $tipo_documento_dni_id = TipoDocumento::where('descripcion', 'DNI')->first()->id;
        $titular->tipo_documento_id = $tipo_documento_dni_id;

        $dni = substr($request->cuit_cuil, 2, -1);
        $titular->numero_documento = $dni;

        $titular->save();

        // Creo la relacion por primera vez
        if (empty(trim($request->titular_id))) {

            // Vinculamos al titular con el prospecto
            Auth::guard('apertura-cuenta')->user()->Titulares()->attach($titular, [
                'esta_confirmado' => false
            ]);
        }

        // Si hay datos de direccion, la guardamos
        if ($this->hayDatosDeDireccion($request)) {
            $this->guardarDatosDireccion($request, $titular);
        }


        // Si hay datos del conyuge, los guardamos
        if ($this->hayDatosDeCampo($request, 'conyuge')) {
            $this->guardarDatosDeConyuge($request, $titular);
        }

        return response()->json([
            'titular_id' => $titular->id
        ]);
    }

    /**
     * Verificamos si el usuario escribió alguno de los datos de direccion
     *
     * @param $request
     * @return bool
     */
    private function hayDatosDeDireccion($request)
    {
        foreach ($request->direccion as $dato_direccion) {
            if (!empty($dato_direccion) && trim($dato_direccion) != "") {
                return true;
            }
        }

        return false;
    }

    /**
     * Eliminamos un titular del prospecto
     *
     * @param Request $request
     * @return bool|\Exception
     */
    public function eliminar(Request $request)
    {
        $titular_id = $request->titular_id;
        $prospecto_id = $request->prospecto_id;

        try {
            // Buscamos al titular, si ya esta guardado, vemos si lo borramos
            $titular = Titular::findOrFail($titular_id);

            // Si el titular está vinculado solamente a un prospecto, puedo eliminar la relacion con el prospecto y al titular en sí
            if ($titular->Prospectos()->count() <= 1) {
 /*
                $prospecto_id = $prospecto_id;

                $titular->Prospectos()->delete();
                $titular->Conyuge()->delete();
                $titular->Direccion()->delete();
                DB::unprepared("DELETE FROM personas_prospectos WHERE persona_id = ".$titular_id." AND prospecto_id = '$prospecto_id'");
                DB::unprepared("DELETE FROM personas_conyuges WHERE persona_id = ".$titular_id);
                DB::unprepared("DELETE FROM personas_direcciones WHERE persona_id = ".$titular_id);

                $titular->delete();
                */
            } else { 
                // El titular existe en más de un prospecto => elimino sólo la relación
                DB::table('personas_prospectos')->where('persona_id', $titular_id)->where('prospecto_id', $prospecto_id)->delete();
            }
        } catch (\Exception $e) {
            return $e;
        }

        return response()->json(['eliminado' => true]);
    }

    /**
     * Guardamos, creamos o actualizamos los datos de la direccion
     *
     * @param $request
     */
    private function guardarDatosDireccion($request, $titular)
    {
        $direccion = $request->direccion;

        $direccion_id = Direccion::updateOrCreate(['persona_id' => $titular->id], [
            'calle' => $direccion['calle'],
            'altura' => $direccion['altura'],
            'piso' => $direccion['piso'],
            'departamento' => $direccion['departamento'],
            'localidad' => $direccion['localidad'],
            'codigo_postal' => $direccion['codigo_postal'],
            'provincia' => $direccion['provincia'],
            'pais' => $direccion['pais'],
            'persona_id' => $titular->id
        ]);

        return $direccion_id;
    }

    /**
     * Guardamos, creamos o actualizamos los datos del conyuge
     *
     * @param $request
     * @param $titular
     * @return mixed
     */
    private function guardarDatosDeConyuge($request, $titular)
    {
        $conyuge = $request->conyuge;

        $conyuge_id = Conyuge::updateOrCreate(['persona_id' => $titular->id], [
            'nombre' => $conyuge['nombre_conyuge'],
            'apellido' => $conyuge['apellido_conyuge'],
            'tipo_documento_id' => $conyuge['tipo_documento'],
            'numero_documento' => $conyuge['numero_documento'],
            'sexo_id' => $conyuge['sexo'],
            'persona_id' => $titular->id,
            'prospecto_id' => $request->prospecto_id
        ]);

        return $conyuge_id;
    }

    /**
     * Consultamos si un titular existe a partir de un DNI
     *
     * @param Request $request
     */
    public function consultarPorDocumento(Request $request)
    {
        $titular =
            Titular::where('cuit_cuil', $request->numero_documento)
                ->first();
        if ($titular) {
            return response()->json([
                'titular_id' => $titular->id,
                'div_titular' => view('apertura-cuenta.pasos.titulares.titular-existente', ['titular' => $titular])->render()
            ]);
        }

        return response()->json([
            'titular_id' => null
        ]);
    }

    /**
     * Relacionamos una persona existente con el prospecto actual
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function relacionarTitularExistente(Request $request)
    {
        $titular = Titular::findOrFail($request->titular_id);
        $prospecto = Prospecto::findOrFail($request->prospecto_id);

        // Relacionamos el titular con el prospecto
        $prospecto->Titulares()->attach($titular, [
            'esta_confirmado' => false,
            'agregado_por_dni' => true
        ]);

        return response()->json([
            'relacionado' => true
        ]);
    }
}
