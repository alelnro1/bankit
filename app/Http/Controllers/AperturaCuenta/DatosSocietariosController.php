<?php

namespace App\Http\Controllers\AperturaCuenta;

use App\Http\Requests\AperturaCuenta\DatosSocietariosRequest;
use App\Models\Prospectos\PersonasJuridicas\DireccionSociedad;
use App\Models\Sociedad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DatosSocietariosController extends ProcesoAperturaController
{
    public function guardar(DatosSocietariosRequest $request)
    {
        $prospecto = Auth::guard('apertura-cuenta')->user();

        $fecha_constitucion = Carbon::createFromFormat('Y-m-d', $request->fecha_constitucion);

        $sociedad =
            Sociedad::updateOrCreate(['prospecto_id' => $prospecto->id], [
                'razon_social' => $request->razon_social,
                'fecha_constitucion' => $fecha_constitucion,
                'cuit' => $request->cuit,
                'cod_area_celular' => $request->cod_area_celular,
                'prefijo_celular' => $request->prefijo_celular,
                'telefono_celular' => $request->telefono_celular,
                'email' => $request->email,
                'prospecto_id' => $prospecto->id,
                'comitente_id' => null,
            ]);

        if ($this->hayDatosDeCampo($request, 'direccion')) {
            $this->guardarDatosDireccion($request, $sociedad);
        }

        return response()->json([
            'sociedad_id' => $sociedad->id
        ]);
    }

    /**
     * Guardamos, creamos o actualizamos los datos de la direccion
     *
     * @param $request
     */
    private function guardarDatosDireccion($request, $sociedad)
    {
        $direccion = $request->direccion;

        $direccion_id = DireccionSociedad::updateOrCreate(['sociedad_id' => $sociedad->id], [
            'calle' => $direccion['calle'],
            'altura' => $direccion['altura'],
            'piso' => $direccion['piso'],
            'departamento' => $direccion['departamento'],
            'localidad' => $direccion['localidad'],
            'codigo_postal' => $direccion['codigo_postal'],
            'provincia' => $direccion['provincia'],
            'pais' => $direccion['pais'],
            'sociedad_id' => $sociedad->id
        ]);

        return $direccion_id;
    }
}
