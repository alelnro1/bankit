<?php

namespace App\Http\Controllers\AperturaCuenta;

use App\Http\Controllers\Controller;
use App\Http\Controllers\WSSaenzController;
use App\Mail\InformarBackCreacionProspecto;
use App\Mail\InformarProspectoEnProceso;
use App\Models\Banco;
use App\Models\EstadoCivil;
use App\Models\Pais;
use App\Models\Prospectos\PerfilRiesgo\PerfilRiesgoCategoria;
use App\Models\Prospectos\PerfilRiesgo\PerfilRiesgoUser;
use App\Models\Prospectos\PersonasFisicas\TipoResponsableIVA;
use App\Models\Prospectos\Prospecto;
use App\Models\Provincia;
use App\Models\Sexo;
use App\Models\Tesoreria\CuentasBancarias\TipoCuentaBancaria;
use App\Models\TipoDocumento;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ProcesoAperturaController extends WSSaenzController
{

    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    public function index()
    {
        $prospecto = $this->getUsuarioActual();

        $prospecto_id = $prospecto->id;

        $es_persona_fisica = $prospecto->esFisico();

        if ($prospecto->esFisico()) {
            $sociedad = null;
        } else {
            $sociedad = $prospecto->Sociedad;
        }

        $titulares = $prospecto->getTitulares();

        $cuentas_bancarias = $prospecto->getCuentasBancarias();
        $cuenta_bancaria_pesos = isset($cuentas_bancarias['pesos']) ? $cuentas_bancarias['pesos'] : null;
        $cuenta_bancaria_dolares = isset($cuentas_bancarias['dolares']) ? $cuentas_bancarias['dolares'] : null;

        $bancos = Banco::all();
        $tipos_de_cuentas = TipoCuentaBancaria::all();
        $tipos_documentos = TipoDocumento::all();
        $tipos_responsables_iva = TipoResponsableIVA::all();
        $estados_civiles = EstadoCivil::all();
        $sexos = Sexo::all();
        $paises = Pais::all();
        $provincias = Provincia::all();

        $perfil = $prospecto->perfil;
        $categorias_preguntas_perfil = PerfilRiesgoCategoria::datosParaFormulario();
        $respuestas_preguntas_perfil = PerfilRiesgoUser::getIDRespuestasSeleccionadas($prospecto_id);

        return view('apertura-cuenta.index', [
            'es_persona_fisica' => $es_persona_fisica,
            'bancos' => $bancos,
            'sexos' => $sexos,
            'paises' => $paises,
            'provincias' => $provincias,
            'sociedad' => $sociedad,
            'estados_civiles' => $estados_civiles,
            'tipos_de_cuentas' => $tipos_de_cuentas,
            'titulares' => $titulares,
            'tipos_documentos' => $tipos_documentos,
            'tipos_responsables_iva' => $tipos_responsables_iva,
            'cuenta_bancaria_pesos' => $cuenta_bancaria_pesos,
            'cuenta_bancaria_dolares' => $cuenta_bancaria_dolares,
            'prospecto_id' => $prospecto_id,
            'perfil' => $perfil,
            'categorias_preguntas_perfil' => $categorias_preguntas_perfil,
            'respuestas_preguntas_perfil' => $respuestas_preguntas_perfil
        ]);
    }

    /**
     * Registramos el ultimo paso en el que se quedó el usuario par apoder volver cuando reinicie sesión
     *
     * @param Request $request
     */
    public function registrarUltimoPaso(Request $request)
    {

        Auth::guard('apertura-cuenta')->user()->update([
            'step_actual' => $request->step
        ]);
    }

    /**
     * Finalizamos un prospecto del lado del cliente para poder enviarlo al back
     *
     * @return mixed
     */
    public function finalizarProceso(Request $request)
    {


        $cbu_pesos = $request->cbu_pesos;
        $cbu_dolar = $request->cbu_dolar;

        $cbu_pesos_valido = $cbu_dolar_valido = true;

        
        if (!empty($cbu_pesos))
            $cbu_pesos_valido = $this->validarCBUDeCuentas($cbu_pesos);

        if (!empty($cbu_dolar))
            $cbu_dolar_valido = $this->validarCBUDeCuentas($cbu_dolar);
        
        if ($cbu_pesos_valido && $cbu_dolar_valido) {
            $prospecto = Auth::guard('apertura-cuenta')->user();
            $prospecto->actualizarEstado('Finalizado');
            $prospecto->fecha_finalizacion = Carbon::now();
            $prospecto->save();
            $result = [
                'cbus_validos' => true
            ];
        } else {
            $result = [
                'cbus_validos' => false,
                'cbu_pesos' => $cbu_pesos_valido,
                'cbu_dolar' => $cbu_dolar_valido
            ];
        }

        $this->enviarEmailBackCreacionProspecto($prospecto);
        $this->enviarEmailCreacionEnProceso($prospecto);

        Session::flash('proceso_finalizado', true);

        return response()->json($result);
    }

    /**
     * Podemos terminar el prospecto. Lo deslogueamos y lo redirijmos al front.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function procesoLogout(Request $request)
    {
        Auth::guard('apertura-cuenta')->logout();

        return redirect(route('frontend.login'))->with(['proceso_finalizado' => true]);
    }

    /**
     * Verificamos si hay datos de un campo array con datos (por ejemplo direccion[datos])
     *
     * @param $request
     * @param $campo
     * @return bool
     */
    protected function hayDatosDeCampo($request, $campo)
    {
        foreach ($request->{$campo} as $dato) {
            if (!empty($dato) && trim($dato) != "") {
                return true;
            }
        }

        return false;
    }

    /**
     * Enviamos el email al back informándo sobre la creacion de un nuevo prospecto
     *
     * @param $prospecto
     */
    private function enviarEmailBackCreacionProspecto($prospecto)
    {
        $ruta_pdf = $this->armarRutaPDF($prospecto);
        $this->generarPDFTyC($ruta_pdf);
        // Enviamos el email al back informándo sobre la creacion de un nuevo prospecto
        Mail::to(env('EMAIL_ALTA_CUENTA'))
                ->send(new InformarBackCreacionProspecto($ruta_pdf));
    }

    private function enviarEmailCreacionEnProceso($prospecto)
    {
        $ruta_pdf = $this->armarRutaPDF($prospecto);
        $this->generarPDFTyC($ruta_pdf);
        $titulares = $prospecto->getTitulares();

        foreach ($titulares as $titular) {
            // Enviamos el mail a todos los mail del titular
            $emails_titular = explode(";", $titular->email);

            foreach ($emails_titular as $email){
                $email=ltrim(rtrim(trim($email)));
                
                Mail::to($email)
                ->send(new InformarProspectoEnProceso($ruta_pdf,$titular));
            }
        }

        
    }



    /**
     * Armamos la ruta donde se va a almacenar el pdf temporal
     *
     * @param $prospecto
     * @return string
     */
    private function armarRutaPDF($prospecto)
    {
        return 'comitentes/' . $prospecto->email . '-' . time() . '.pdf';
    }

    /**
     * Generamos el PDF para que acepte los terminos y condiciones de uso
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generarPDFTyC($ruta_pdf = null)
    {

        $prospecto = $this->getUsuarioActual();
        $titulares = $prospecto->getTitulares();
        
        if (!$ruta_pdf) {
            $ruta_pdf = $this->armarRutaPDF($prospecto);
        }

        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        $mes_actual = $meses[(int)date('m') - 1];

        $pdf = \PDF::loadView('apertura-cuenta.pdf.index', [
            'prospecto' => $prospecto,
            'titulares' => $titulares,
            'mes_actual' => $mes_actual
        ]);

        $pdf->save($ruta_pdf);

        return response()->json([
            'pdf' => $ruta_pdf
        ]);
    }
}
