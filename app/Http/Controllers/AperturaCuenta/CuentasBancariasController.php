<?php

namespace App\Http\Controllers\AperturaCuenta;

use App\Http\Controllers\WSSaenzController;
use App\Http\Requests\AperturaCuenta\CuentaBancariaRequest;
use App\Models\Monedas\Dolar;
use App\Models\Monedas\Peso;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use App\Models\Monedas\MonedaBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CuentasBancariasController extends WSSaenzController
{
    /**
     * Buscamos los datos de la cuenta por cbu o alias
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function buscarCuenta(Request $request)
    {
        if (!empty($request->dato)) {
            $datos = $this->WSDatos($request->dato, $request->tipo);

            if ((boolean)$datos->CtaActiva !== true) {
                $datos = null;
            }

            return response()->json($datos);
        } else {
            return response()->json([
                'success' => false,
                'error' => 'Ingrese el CBU o el Alias'
            ]);
        }
    }

    /**
     * Guardamos la cuenta bancaria
     *
     * @param CuentaBancariaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function guardar(CuentaBancariaRequest $request)
    {
        $cuenta_bancaria = $this->crearOActualizarCuentaBancaria($request);

        return response()->json([
            'cuenta_bancaria' => $cuenta_bancaria->id
        ]);
    }

    /**
     * Obtenemos el ID de la moneda de la cuenta bancaria (pesos o dolares)
     *
     * @param $request
     * @return mixed
     */
    private function getMonedaDeRequest($request)
    {
        if ($request->cuenta_pesos) {
            $moneda_id = Peso::first()->id;
        } else {
            $moneda_id = Dolar::first()->id;
        }

        return $moneda_id;
    }

    /**
     * Creamos la cuenta bancaria o la actualizamos si ya exite
     *
     * @param $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function crearOActualizarCuentaBancaria($request)
    {
        $moneda_id = $this->getMonedaDeRequest($request);

        // Lo creamos, es nuevo
        
        $moneda = MonedaBase::where("simbolo","ARS")->first();
        $cuenta = CuentaBancaria::where("prospecto_id",$request->prospecto_id)->where("moneda_id",$moneda->id)->first();


        if(!isset($cuenta)) {
            $cuenta = CuentaBancaria::create([
                'cuenta_bancaria_tipo_id' => $request->cuenta_bancaria_tipo_id,
                'banco_id' => $request->banco_id,
                'numero_cuenta' => $request->numero_cuenta,
                'moneda_id' => $moneda_id,
                'cbu' => $request->cbu,
                'alias' => $request->alias,
                'prospecto_id' => $request->prospecto_id
            ]);
        } else {
            $cuenta->cuenta_bancaria_tipo_id = $request->cuenta_bancaria_tipo_id;
            $cuenta->banco_id = $request->banco_id;
            $cuenta->numero_cuenta = $request->numero_cuenta;
            $cuenta->moneda_id = $moneda_id;
            $cuenta->cbu = $request->cbu;
            $cuenta->alias = $request->alias;
            $cuenta->prospecto_id = $request->prospecto_id;
            $cuenta->save();
        }

        return $cuenta;
    }
}
