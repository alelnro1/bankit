<?php

namespace App\Http\Controllers\AperturaCuenta\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/lalala';

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('apertura-cuenta.auth.login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:apertura-cuenta')->except('apertura-cuenta.logout');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('apertura-cuenta');
    }

    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        $this->intended = parse_url(\Session::get('url.intended'), PHP_URL_PATH);

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::guard('apertura-cuenta')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ], $request->remember)) {

            if (Auth::guard('apertura-cuenta')->user()->estaFinalizadoOAprobado()) {
                Auth::guard('apertura-cuenta')->logout();

                return redirect()->back()
                    ->with(['email_error' => 'Su proceso de apertura de cuenta ha sido finalizado. En breve obtendrá una respuesta.']);
            }

            // if successful, then redirect to their intended location
            return (starts_with($this->intended, '/apertura-cuenta'.'/')) ?
                redirect()->to($this->intended) :
                redirect()->route('apertura-cuenta.proceso.paso-1');
        }

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->with(['email_error' => __('auth.failed')]);
    }

    public function logout(Request $request)
    {
        Auth::guard('apertura-cuenta')->logout();

        return redirect(route('frontend.login'));
    }
}