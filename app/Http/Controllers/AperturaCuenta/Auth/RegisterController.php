<?php

namespace App\Http\Controllers\AperturaCuenta\Auth;

use App\Models\Estado;
use App\Models\Prospectos\Prospecto;
use App\Models\Prospectos\TipoProspecto;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/apertura-cuenta';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:apertura-cuenta');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $tipos_prospectos = TipoProspecto::all();

        return view('apertura-cuenta.auth.apertura-cuenta-register', [
            'tipos_prospectos' => $tipos_prospectos
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:prospectos',
            'password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])[\w]{8,20}$/|',
        ], [
            'password.regex' => 'La contraseña debe tener entre 8 y 20 caracteres, una mayúscula, una minuscula y un número.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $estado_pendiente_id = Estado::where('descripcion', 'Pendiente')->first()->id;

        return Prospecto::create([
            'tipo_prospecto_id' => $data['tipo_prospecto'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'estado_id' => $estado_pendiente_id
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        Auth::guard('apertura-cuenta')->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
