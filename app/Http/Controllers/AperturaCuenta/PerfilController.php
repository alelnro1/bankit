<?php

namespace App\Http\Controllers\AperturaCuenta;

use App\Http\Requests\AperturaCuenta\PerfilRequest;
use App\Models\Prospectos\PerfilRiesgo\PerfilRiesgoCategoria;
use App\Models\Prospectos\PerfilRiesgo\PerfilRiesgoUser;
use App\Models\Prospectos\Prospecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    /**
     * Guardamos la cuenta bancaria
     *
     * @param CuentaBancariaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function guardar(PerfilRequest $request)
    {
        $perfil = $this->crearOActualizarPerfil($request);

        return response()->json([
            'success' => true
        ]);
    }

    private function crearOActualizarPerfil($request)
    {
        if (Auth::guard('apertura-cuenta')->user()) {
            $prospecto_id = Auth::guard('apertura-cuenta')->user()->id;
        } else if (Auth::guard('backend')->user()) {
            $prospecto_id = $request->prospecto_id;
        }

        // Buscamos todos los registros de las categorias seleccionadas para crear las relaciones
        $categorias_elegidas =
            PerfilRiesgoCategoria::whereIn('id', [
                $request->riesgo,
                $request->experiencia,
                $request->informacion,
                $request->horizonte_inversion,
                $request->tiempo
            ])->get();

        // Eliminamos todas las relaciones creadas para el perfil
        PerfilRiesgoUser::where('prospecto_id', $prospecto_id)->delete();

        // Creamos las relaciones
        foreach ($categorias_elegidas as $categoria) {
            PerfilRiesgoUser::create([
                'prospecto_id' => $prospecto_id,
                'categoria_pregunta_id' => $categoria->id
            ]);
        }

        // Actualizamos el perfil del prospecto
        $prospecto = Prospecto::where('id', $prospecto_id)->first();
        $prospecto->perfil = $request->perfil_calculado;
        $prospecto->save();
    }
}
