<?php

namespace App\Http\Controllers\Backend\Operaciones;

use App\Classes\WebServices\INTLWebService;
use App\Exports\OperacionesPendientesExport;
use App\Exports\OperacionesAprobadasExport;
use App\Exports\OperacionesRealizadasExport;
use App\Exports\OperacionesAprobadasExportTxt;
use App\Mail\OperacionRechazada;
use App\Models\CuentasCorrientes\CuentaCorriente;
use App\Models\Comitente;
use App\Models\Estado;
use Illuminate\Support\Facades\Mail;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class FondosController extends Controller
{
    public $excel;

    public function __construct(\Maatwebsite\Excel\Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Obtenemos un listado con las operaciones pendientes sobre fondos
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOperacionesPendientes()
    {
        $operaciones = Operacion::getOperacionesPendientesDeTipoInstrumento();
        $operaciones = Operacion::getPendientesConComprobante($operaciones);

        $operaciones->load('Comitente');

        return view('backend.operaciones.fondos', [
            'operaciones' => $operaciones
        ]);
    }

    public function getOperacionesAprobadas(Request $request)
    {
        if ($request->fecha != "") {
            $fecha = date("Ymd", DateTime::createFromFormat("d/m/Y", $request->fecha)->getTimestamp());
            $fecha_datetimepicker = $request->fecha;
        } else {
            $fecha = date("Ymd");
            $fecha_datetimepicker = date("d/m/Y");
        }

        session(['FCI_APROBADAS_FECHA' => $fecha]);

        $operaciones = Operacion::getOperacionesAprobadasDeTipoInstrumento(null, null, $fecha);

        $operaciones->load('Comitente');

        return view('backend.operaciones.fondos-aprobadas', [
            'fecha' => $fecha_datetimepicker,
            'operaciones' => $operaciones
        ]);
    }

    public function getOperacionesRealizadas(Request $request)
    {
        if ($request->fecha_desde != "") {
            $fecha_desde = date("Y-m-d", DateTime::createFromFormat("d/m/Y",$request->fecha_desde)->getTimestamp());
            $fecha_desde_datetimepicker = $request->fecha_desde;
        } else {
            $fecha_desde = date("Y-m-d");
            $fecha_desde_datetimepicker = date("d/m/Y");
        }

        if ($request->fecha_hasta != "") {
            $fecha_hasta = date("Y-m-d", DateTime::createFromFormat("d/m/Y",$request->fecha_hasta)->getTimestamp());
            $fecha_hasta_datetimepicker = $request->fecha_hasta;
        } else {
            $fecha_hasta = date("Y-m-d");
            $fecha_hasta_datetimepicker = date("d/m/Y");
        }

        $fecha_hasta = date("Y-m-d",strtotime($fecha_hasta . " + 1 DAY"));
        
        
        $operaciones = Operacion::where("created_at",">=",$fecha_desde)
                                    ->where("created_at","<=", $fecha_hasta)
                                    ->get();

        $operaciones->load(['Comitente', 'Instrumento', 'Deposito', 'Transferencia', 'Moneda', 'TipoOperacion']);
        

        return view('backend.operaciones.fondos-reporte-operaciones-realizadas', [
            'fecha_desde' => $fecha_desde_datetimepicker,
            'fecha_hasta' => $fecha_hasta_datetimepicker,
            'operaciones' => $operaciones
        ]);
    }


    public function getOperacionesPendientesComprobante(Request $request)
    {

        $operaciones = Operacion::getOperacionesPendientesDeTipoInstrumento();      
        $operaciones = Operacion::getPendientesComprobantePendiente($operaciones);
        $operaciones->load('Comitente');

        return view('backend.operaciones.fondos-pendientes-comprobante', [
            'operaciones' => $operaciones
        ]);
    }


    public static function getOperacionesPorComitente($comitente_id)
    {
        
        $operaciones = Operacion::getOperacionesHistoricasPorComitente($comitente_id);



        return $operaciones;
    }

    /**
     * Cargo la operacion pendiente sobre un fondo y la devuelvo como vista para que pueda ser cargada como un modal
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cargarOperacionPendiente(Request $request)
    {
        $operacion = Operacion::findOrFail($request->operacion_id);
        $operacion->load(['Comitente', 'Instrumento', 'Deposito', 'Transferencia', 'Moneda']);

        // Traemos los datos del comprobante
        if (isset($operacion->Deposito->comprobante)) {
            $dir_comprobante = $operacion->Deposito->comprobante;
            $extension = explode('.', $dir_comprobante)[1];
        } else {
            $dir_comprobante = $extension = null;
        }

        $modal = view('backend.operaciones.vista-operacion-fondo', [
            'operacion' => $operacion,
            'dir_comprobante' => $dir_comprobante,
            'extension' => $extension
        ])->render();

        return response()->json([
            'modal' => $modal
        ]);
    }

    /**
     * Enviamos la operacion a INTL
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enviarOperacionAINTL(Request $request)
    {
        $operacion = Operacion::findOrFail($request->operacion_id);

        $intl_webservice = new INTLWebService();
        $procesar_ws = $intl_webservice->postOperacionFondo($operacion);
        
        // Enviamos a INTL la operación del fondo
        if ($procesar_ws !== true) {
            return response()->json([
                'procesado' => false,
                'errores' => $procesar_ws
            ]);
        }

        // Una vez aprobada la operación, insertamos el registro en las cuentas corrientes de instrumentos y de moneda
        $cuenta_corriente = new CuentaCorriente();
        $cuenta_corriente->insertar($operacion);

        $operacion->aprobar();

        return response()->json([
            'procesado' => true
        ]);
    }

    /**
     * @param Request $request
     */
    public function desaprobarOperacion(Request $request)
    {
        // Buscamos la operacion
        $operacion = Operacion::findOrFail($request->operacion_id);

        $operacion->desaprobar();

        $comitente = Comitente::findOrFail($operacion->comitente->id);
        $personas=$comitente->getTitulares();

        foreach ($personas as $persona) {
            $email = strtolower(ltrim(rtrim(trim(explode(";", $persona->email)[0]))));
            if (isset($email)){
                //Mail::to($email)->send(new OperacionRechazada($operacion, $comitente,$persona));        
            }            
        }
       
        
        return response()->json();
    }

    /**
     * Generamos el XLS con las operaciones pendientes de fondos
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportarOperacionesPendientesXLS()
    {
        return (new OperacionesPendientesExport())->download('operaciones.xlsx');
    }

    public function exportarOperacionesAprobadasXLS()
    {
        return (new OperacionesAprobadasExport())->download('operaciones.xlsx');
    }

    public function exportarOperacionesRealizadasXLS()
    {

        return (new OperacionesRealizadasExport())->download('operaciones.xlsx');
    }

    public function exportarOperacionesAprobadasTXT()
    {
        $fecha = session('FCI_APROBADAS_FECHA');

        $operaciones_aprobadas_txt = new OperacionesAprobadasExportTxt();
        
        return $operaciones_aprobadas_txt->view($fecha);
    }


}
