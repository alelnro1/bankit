<?php

namespace App\Http\Controllers\Backend;

use App\BackendUser;
use App\Models\BackendPermiso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsuariosInternosController extends Controller
{
    /**
     * Validaciones comunes para alta y modificacion del usuario interno
     * @return array
     */

    private $validaciones = [
        'nombre' => 'required|string|max:191',
        'apellido' => 'required|string|max:191',
        'email' => 'required|string|email|max:191'
    ];

    public function index()
    {
        $internos = BackendUser::getUsuariosInternos();

        return view('backend.usuarios-internos.index', [
            'internos' => $internos
        ]);
    }

    public function ver(BackendUser $interno)
    {
        return view('backend.usuarios-internos.ver', [
            'interno' => $interno
        ]);
    }

    public function modificar(BackendUser $interno)
    {
        $permisos = BackendPermiso::all();

        $permisos = $this->marcarPermisosQueYaTieneElInterno($interno, $permisos);

        return view('backend.usuarios-internos.modificar', [
            'interno' => $interno,
            'permisos' => $permisos
        ]);
    }

    public function nuevo()
    {
        $permisos = BackendPermiso::all();

        return view('backend.usuarios-internos.nuevo', [
            'permisos' => $permisos
        ]);
    }

    public function procesarAlta(Request $request)
    {
        // Agrego la validacion del usuario
        $this->validaciones['username'] = 'required|string|max:191|unique:backend_users';
        $this->validaciones['password'] = 'required|confirmed|min:6';


        // Valido al interno
        Validator::make($request->all(), $this->validaciones)->validate();

        // Creo el usuario
        $interno = BackendUser::create([
            'nombre' => $request->nombre,
            'apellido' => $request->apellido,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password)
        ]);

        // Agrego permisos al usuario interno
        $interno->Permisos()->sync($request->permisos);

        return redirect(route('backend.interno.index'))->with('interno_creado', 'El usuario ha sido creado.');
    }

    /**
     * Se procesa la modificación de un usuario interno
     *
     * @param Request $request
     * @param BackendUser $interno
     * @return \Illuminate\Http\RedirectResponse
     */
    public function procesarModificacion(Request $request, BackendUser $interno)
    {
        $this->agregarValidacionesParaModificacion($request, $interno);

        // Valido al interno
        Validator::make($request->all(), $this->validaciones)->validate();

        if ($request->password != null) {
            $request->request->set('password', bcrypt($request->password));
        } else {
            $request->request->add([
                'password' => $interno->password
            ]);
        }

        $interno->update($request->all());

        // Agrego permisos al usuario interno
        $interno->Permisos()->sync($request->permisos);

        return redirect(route('backend.interno.index'))->with('interno_actualizado', 'Interno actualizado');
    }

    public function eliminar(Request $request)
    {
        $interno = BackendUser::findOrFail($request->interno_id);

        $interno->delete();

        return redirect(route('backend.interno.index'))->with('interno_eliminado', 'Interno eliminado');
    }

    /**
     * Recibo todos los permisos, y marco los que estan asignados al usuario interno
     * @param $interno
     * @param $permisos
     * @return mixed
     */
    private function marcarPermisosQueYaTieneElInterno($interno, $permisos)
    {
        foreach ($permisos as $permiso) {
            if ($interno->tienePermiso($permiso)) {
                $permiso->asignado = true;
            } else {
                $permiso->asignado = false;
            }
        }

        return $permisos;
    }

    private function agregarValidacionesParaModificacion($request, $interno)
    {
        // Si escribió la contraseña, es porque la está modificando
        if ($request->password != null) {
            $this->validaciones['password'] = 'required|string|confirmed';
        }

        // Agrego la validacion del usuario modificable
        $this->validaciones['username'] = 'required|string|max:191|' . Rule::unique('backend_users')->ignore($interno->id);
    }
}
