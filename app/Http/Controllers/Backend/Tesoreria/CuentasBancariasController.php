<?php

namespace App\Http\Controllers\Backend\Tesoreria;

use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use App\Mail\CuentaBancariaAprobada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class CuentasBancariasController extends Controller
{
    public function index()
    {
        $cuentas_bancarias_pendientes_pesos =
            CuentaBancaria::getCuentasConEstado('Pendiente', 'Pesos');

        $cuentas_bancarias_pendientes_dolares =
            CuentaBancaria::getCuentasConEstado('Pendiente', 'Dolares');

        $cant_cuentas_pendientes_pesos = count($cuentas_bancarias_pendientes_pesos);
        $cant_cuentas_pendientes_dolares = count($cuentas_bancarias_pendientes_dolares);

        return view(
            'backend.tesoreria.cuentas-bancarias.index',
            [
                'cuentas_bancarias_pendientes_pesos' => $cuentas_bancarias_pendientes_pesos,
                'cuentas_bancarias_pendientes_dolares' => $cuentas_bancarias_pendientes_dolares,
                'cant_cuentas_pendientes_pesos' => $cant_cuentas_pendientes_pesos,
                'cant_cuentas_pendientes_dolares' => $cant_cuentas_pendientes_dolares
            ]
        );
    }

    /**
     * Cargamos la solicitud para mostrar
     *
     * @param CuentaBancaria $cuenta
     * @return string
     */
    public function cargarSolicitud(CuentaBancaria $cuenta)
    {
        $vista = view('backend.tesoreria.cuentas-bancarias.ver', ['cuenta' => $cuenta])->render();

        return $vista;
    }

    public function aceptarSolicitud(Request $request)
    {
        $cuenta_bancaria = CuentaBancaria::where('id', $request->cuenta)->first();

        //$cuenta_bancaria->aceptar();

        $comitente = $cuenta_bancaria->comitente;
        $titulares = $comitente->getTitulares();

        // Recorremos todos los titulares
        foreach ($titulares as $titular) {
            // Enviamos el mail a todos los mail del titular
           $emails_titular = explode(";", $titular->email);

           foreach ($emails_titular as $email){
                $email=ltrim(rtrim(trim($email)));
                
                Mail::to($email)->send(new CuentaBancariaAprobada($comitente, $titular, $cuenta_bancaria));
                
            }
        }

        return json_encode(['aceptado' => true]);
    }

    public function rechazarSolicitud(Request $request)
    {
        $cuenta_bancaria = CuentaBancaria::find($request->cuenta);

        $cuenta_bancaria->rechazar();

        return json_encode(['rechazado' => true]);
    }
}
