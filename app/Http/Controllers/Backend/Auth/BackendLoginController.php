<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\BackendUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\BackendPermisosUsuario;

class BackendLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $intended;

    public function __construct()
    {
        $this->middleware('guest:backend', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }

    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        $this->intended = parse_url(\Session::get('url.intended'), PHP_URL_PATH);

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required',
            'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::guard('backend')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ], $request->remember)) {

            //$this->afterLogin();

            // if successful, then redirect to their intended location
            $user = BackendUser::where("email",$request->email)->first();
            if (isset($user)) {
                $user->intentos_fallidos=0;
                $user->save();
            }
            
            return (starts_with($this->intended, '/admin'.'/')) ?
                redirect()->to($this->intended) :
                redirect()->route('admin.dashboard');
        }

        $user = BackendUser::where("email",$request->email)->first();
        if (isset($user)) {

            /* Bloquear Usuario */
            /*
            if (isset($user->intentos_fallidos) && $user->intentos_fallidos>=5) {
                $user->password=bcrypt("ComitenteBloqueado1234!!");
                $user->cambio_password=true;
                $user->save();                

                return redirect()->back()
                    ->withInput($request->only('email', 'remember'))
                    ->with(['email_error' => __('auth.locked')]);
            }
            */
            /* Actualizo los intengos Fallidos */
            if (isset($user->intentos_fallidos)) {
                $intentosFallidos = $user->intentos_fallidos;                
            } else {
                $intentosFallidos = 0;
            }

            $user->intentos_fallidos=$intentosFallidos+1;
            $user->save();
        };

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->with(['email_error' => __('auth.failed')]);
    }

    public function logout(Request $request)
    {
        Auth::guard('backend')->logout();

        return redirect(route('admin.login'));
    }

    private function afterLogin(){
        $this->asignarPermisos();
    }

    private function asignarPermisos(){

        $users_permisos = BackendPermisosUsuario::where('usuario_id',Auth::guard('backend')->user()->id)->with('Permisos')->get();
        $permisos       = [];

        if($users_permisos){
            foreach ($users_permisos as $user_permiso) {             
                $permisos[] = $user_permiso->Permisos->backend_permisos_nombre;
            }
        }

        session(['permisos' => $permisos]);
    }
}