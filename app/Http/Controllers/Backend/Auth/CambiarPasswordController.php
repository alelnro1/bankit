<?php

namespace App\Http\Controllers\Backend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CambiarPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:backend');
    }

    public function showChangePasswordForm()
    {
        return view('backend.auth.passwords.cambiar');
    }

    public function cambiar(Request $request)
    {
        
        if (!(Hash::check($request->get('current-password'), Auth::guard('backend')->user()->password))) {

            // The passwords matches
            return redirect()->back()->with("error", "La contraseña actual es incorrecta.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {

            //Current password and new password are same
            return redirect()->back()->with("error", "La nueva contraseña no puede ser igual a la anterior.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])[\w]{8,20}$/|',
        ], [
            'new-password.regex' => 'La contraseña debe tener entre 8 y 20 caracteres, una mayúscula, una minuscula y un número.'
        ]);

        //Change Password
        $user = Auth::guard('backend')->user();

        $user->password = bcrypt($request->get('new-password'));
        $user->cambio_password_fecha = date("Ymd");
        $user->cambio_password = 0;
        $user->intentos_fallidos = 0;
        $user->save();

        Session::flash('CAMBIO_PASSWORD',true);
        return redirect(route('admin.dashboard'));
    }

}
