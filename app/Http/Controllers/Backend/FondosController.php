<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Instrumentos\ComposicionInstrumento;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Exports\FondosCotizacionesExportTxt;
use Illuminate\Http\Request;


class FondosController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $fondos = Instrumento::getFondos();

        return view('backend.fci.index', [
            'fondos' => $fondos
        ]);
    }

    /**
     * @param Instrumento $fondo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Instrumento $fondo)
    {
        $fondo->load('Composicion');
        $monedas = MonedaBase::all();

        return view('backend.fci.show', [
            'fondo' => $fondo,
            'monedas' => $monedas
        ]);
    }

    /**
     * Se confirma la edición del fondo. Actualizamos todos los campos
     *
     * @param Instrumento $fondo
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function procesarEdit(Instrumento $fondo, Request $request)
    {
        if ($request->opera_web == "on") {
            $request->merge(['opera_web' => true]);
        }

        $fondo->update($request->all());

        return redirect(route('backend.abm-fondos.index'))->with('fci_actualizado', true);
    }


    /**
     * Se carga el modal con las composiciones del FCI para poder editar
     *
     * @param Request $request
     * @return string
     */
    public function cargarModalComposicionEdit(Request $request)
    {
        $instrumento = Instrumento::findOrFail($request->instrumento_id);

        $vista_modal = view('backend.fci.composicion-edit', [
            'composiciones' => $instrumento->Composicion
        ])->render();

        return $vista_modal;
    }

    public function procesarComposicionEdit(Request $request)
    {
        $composiciones = $request->composiciones;

        foreach ($composiciones as $key => $composicion) {
            $composicion_instr = ComposicionInstrumento::findOrFail($composicion['composicion_id']);

            $composicion_instr->porcentaje = $composicion['valor'];

            $composicion_instr->save();
        }

        return response()->json([
            'procesado' => true
        ]);
    }

    public function exportarCotizacionesTXT()
    {
        $cotizaciones_txt = new FondosCotizacionesExportTxt();

        return $cotizaciones_txt->view();  
    }

}
