<?php

namespace App\Http\Controllers\Backend;
use App\Models\Comitente;
use App\BackendUser;
use App\Models\Prospectos\Prospecto;
use App\Models\Operaciones\Operacion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:backend', ['except' => 'logout']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (env("DIAS_NOVEDADES_BACK")!=null){
            $tiempoNovedades="-" . env("DIAS_NOVEDADES_BACK") . " days";    
        } else {
            $tiempoNovedades="-10 days";
        }
        

        $fecha = date("Y-m-d",strtotime($tiempoNovedades));
        $res = [];


        //Muestro todas las operaciones que estan pendientes de liquidacion en el dia de la fecha
        if(Auth::user()->tienePermisos(["Administrador","Operaciones"]))
        {

            $operaciones = Operacion::where("updated_at",">=", $fecha)
                                        ->where("estado_id",1)
                                        ->get();

            $operaciones = Operacion::getPendientesConComprobante($operaciones);
            

            foreach ($operaciones as $operacion) {
                $operacion->load(["Comitente","TipoOperacion","Moneda","Instrumento"]);
                $res[] = [  "operacionesPendientesLiquidacion",
                            date("Y-m-d",strtotime($operacion->created_at)),
                            $operacion];
            }
        }

        //Muestro todos los comitentes creados desde la fecha 
        if(Auth::user()->tienePermisos(["Administrador","Comitentes"]))
        {
            $comitentes = Comitente::where("created_at",">=", $fecha)->get();

            foreach ($comitentes as $comitente) {
                $res[] = [  "comitentesCreados",
                            date("Y-m-d",strtotime($comitente->created_at)),
                            $comitente];
            }
        }

        //Muestro todos los prospectos finalizados desde la fecha 
        if(Auth::user()->tienePermisos(["Administrador","Prospectos"]))
        {
            $prospectos = Prospecto::where("fecha_finalizacion",">=", $fecha)
                                    ->whereHas('Estado', function ($query) {
                                        $query->where('descripcion', 'Finalizado');
                                        })
                                ->get();

            foreach ($prospectos as $prospecto) {
                $res[] = [  "prospectosFinalizados",
                            date("Y-m-d",strtotime($prospecto->created_at)),
                            $prospecto];
            }
        }
        
        //Muestro los usuarios creados desde la fecha
        if(Auth::user()->tienePermisos(["Administrador","Usuarios Internos"]))
        {
            $usuarios = BackendUser::where("created_at",">=", $fecha)->get();

            foreach ($usuarios as $usuario) {
                $res[] = [  "usuariosCreados",
                            date("Y-m-d",strtotime($usuario->created_at)),
                            $usuario];
            }
        }
        

        //Muestro los usuarios modificados desde la fecha
        if(Auth::user()->tienePermisos(["Administrador","Usuarios Internos"]))
        {
            $usuarios = BackendUser::where("updated_at",">=", $fecha)->get();

            foreach ($usuarios as $usuario) {
                $res[] = [  "usuariosModificados",
                            date("Y-m-d",strtotime($usuario->updated_at)),
                            $usuario];
            }
        }
        
                

        usort($res,function($first,$second){
            return strtolower($first[1]) < strtolower($second[1]);
        });
        
        
        return view('backend.home', [
            'novedades' => $res
        ]);
    }


}
