<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\AperturaCuenta\ProcesoAperturaController;
use App\Http\Controllers\Backend\Operaciones\FondosController;
use App\Models\Comitente;
use App\Models\Prospectos\Prospecto;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class ComitentesController
{
	 public function index()
    {
        $comitentes = Comitente::getComitentes();




        foreach ($comitentes as $comitente) {
            $comitente->titulares=$comitente->getTitulares();
            $comitente->cuentas_bancarias=$comitente->getCuentasBancarias();

        };


        return view('backend.comitentes.index', [
            'comitentes' => $comitentes
        ]);
    }

     public function ver(Comitente $comitente, Request $request)
    {
        
        $comitente->titulares=$comitente->getTitulares();
        $comitente->cuentas_bancarias=$comitente->getCuentasBancarias();
        $comitente->operaciones = FondosController::getOperacionesPorComitente($comitente->id);
        
        

        return view('backend.comitentes.ver', [
            'comitente' => $comitente
        ]);
    }


}