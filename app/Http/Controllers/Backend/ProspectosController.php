<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\AperturaCuenta\ProcesoAperturaController;
use App\Mail\AperturaCuentaFinalizada;
use App\Mail\InformarBackCreacionProspecto;
use App\Mail\CreacionDeUsuario;
use App\Models\Comitente;
use App\Models\Prospectos\Prospecto;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Knp\Snappy\Pdf;
use Illuminate\Support\Facades\Log;

class ProspectosController extends ProcesoAperturaController
{
    public function index()
    {
        $prospectos = Prospecto::getFinalizados();

        return view('backend.prospectos.index', [
            'prospectos' => $prospectos
        ]);
    }

    /**
     * Mostramos el formulario de un prospecto, permitiendole al administrador editarlo
     *
     * @param Prospecto $prospecto
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ver(Prospecto $prospecto)
    {
        $this->setProspecto($prospecto);

        return parent::index();
    }

    /**
     * Confirmamos la apertura de la cuenta, creando al usuario y cambiando el estado del prospecto
     *
     * @param Request $request
     */
    public function generarComitente(Request $request)
    {
        // Buscamos si ya existe un comitente con esa descripcion o numero

        $comitente_existe =
            Comitente::where('numero_comitente', $request->numero_comitente)
                ->get();

        if (count($comitente_existe) > 0) {
            return response()->json([
                'success' => false,
                'error' => 'Ya existe un comitente con ese numero o descripción.'
            ]);
        }

        $prospecto = Prospecto::findOrFail($request->prospecto_id);
        $titulares = $prospecto->getTitulares();
        $cuentas_bancarias = $prospecto->getCuentasBancarias();
        
        $descripcion = "";

        foreach ($titulares as $key => $titular) {
            if($key!=0) {
                $descripcion .= " Y/O ";
            }

            $descripcion .= strtoupper($titular->apellido) ." " . strtoupper($titular->nombre);

        }

        $descripcion .= " (" . $request->numero_comitente . ")";

        $comitente =
            Comitente::create([
                'descripcion' => $descripcion,
                'numero_comitente' => $request->numero_comitente,
                'sucursal' => $request->sucursal,
                'digito_verificador' => $request->digito_verificador
            ]);


        
        DB::unprepared("UPDATE cuentas_bancarias SET comitente_id=" . $comitente->id . ", estado_id=2 where prospecto_id=" . $request->prospecto_id);


        //TODO: Arreglar esta query, no la estaba haciendo
        foreach ($titulares as $key => $persona) {

            DB::unprepared("INSERT INTO comitentes_personas (comitente_id, persona_id, created_at) 
                            VALUES (".$comitente->id.",".$persona->id.",CURRENT_TIMESTAMP);");
        }


        $this->generarUsuarios($comitente, $titulares);

        // Actualizamos el estado del prospecto a aprobado
        $prospecto->actualizarEstado('Aprobado');

        $prospecto->email = mt_rand(152, 953) . "||" . $prospecto->email;
        $prospecto->save();
        
        $pdf = $this->generarPDFComitente($prospecto, $comitente, $titulares, $cuentas_bancarias);


        // Enviamos el mail a todos los titulares
        $this->enviarEmailPDF($prospecto, $comitente, $pdf);
        
        return response()->json([
            'success' => true
        ]);
    }

    private function generarUsuarios($comitente, $titulares)
    {

        foreach($titulares as $persona) {
            
            $user_id = $persona->user_id;

            if ($user_id) {
                Log::info("Asigna Permisos a usuario :".$user_id." para el comitente" . $comitente);
                Comitente::vincular($user_id, $comitente);
            } else {
                $email = strtolower(ltrim(rtrim(trim(explode(";", $persona->email)[0]))));
                Log::info("Crea usuario :".$email." para el comitente" . $comitente." y la persona " . $persona);

                Comitente::crearUserYVincular($email, $comitente, $persona);
            }
        }
    }

    /**
     * Generamos el PDF de apertura de cuenta del comitente
     *
     * @param $prospecto
     * @param $comitente
     * @param $titulares
     * @param $cuentas_bancarias
     * @return mixed
     */
    private function generarPDFComitente($prospecto, $comitente, $titulares, $cuentas_bancarias)
    {
        setlocale(LC_ALL,"es_ES");

        $ruta_pdf = 'comitentes/' . $comitente->numero_comitente . '_' . date("YmdHis") . '.pdf';

        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        $mes_actual = $meses[(int)date('m') - 1];

        $pdf = \PDF::loadView('apertura-cuenta.pdf.index', [
            'prospecto' => $prospecto,
            'comitente' => $comitente,
            'titulares' => $titulares,
            'cuentas_bancarias' => $cuentas_bancarias,
            'mes_actual' => $mes_actual
        ]);

        $pdf->save($ruta_pdf);

        return $ruta_pdf;
    }

    public function pruebaGenerarPDF($prospecto, $comitente)
    {
        $comitente = Comitente::where('id', $comitente)->first();
        $prospecto = Prospecto::findOrFail($prospecto);
        $titulares = $prospecto->getTitulares();
        $cuentas_bancarias = $prospecto->getCuentasBancarias();

        $pdf = \PDF::loadView('apertura-cuenta.pdf.index', [
            'prospecto' => $prospecto,
            'comitente' => $comitente,
            'titulares' => $titulares,
            'cuentas_bancarias' => $cuentas_bancarias
        ]);

        return $pdf->inline();
    }

    /**
     * Enviamos un email a todos los titulares registrados, con el pdf adjunto
     *
     * @param $prospecto
     * @param $comitente
     * @param $pdf
     */
    private function enviarEmailPDF($prospecto, $comitente, $pdf)
    {
        $titulares = $prospecto->getTitulares();

        // Recorremos todos los titulares
        foreach ($titulares as $titular) {
            // Enviamos el mail a todos los mail del titular
            $emails_titular = explode(";", $titular->email);

            foreach ($emails_titular as $email){
                $email=ltrim(rtrim(trim($email)));
                $user = User::where('id', $titular->user_id)->first();
                
                Mail::to($email)->send(new AperturaCuentaFinalizada($comitente, $titular, $pdf,$user));
            }
        }
    }
}
