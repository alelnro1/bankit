<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comitente;
use App\Models\ComitenteUsuario;
use App\Models\Prospectos\PersonasFisicas\Titular;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AutogestionController extends Controller
{
    public function nuevo()
    {
        return view('frontend.autogestion.nuevo');
    }

    /**
     * Formulario para vincular un nuevo comitente
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function agregar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'comitente' => 'required',
            'dni' => 'required',
            'email' => 'required|email'
        ]);

        // Buscamos el numero de comitente
        $comitente = Comitente::getComitentePorNumero($request->comitente);

        $validator->after(function ($validator) use ($comitente) {
            if (!$comitente) {
                $validator->errors()->add('field', 'Los datos ingresados son inválidos.');
            }
        });

        if ($validator->fails()) {
            return redirect()->route('frontend.autogestion')->withErrors($validator)->withInput();
        }

        // Cargamos las personas del comitente
        $comitente->load('Personas');


        if (count($comitente->Personas) > 0) {
            $existe_persona = false;

            // Busco dentro de las personas, la que tenga el mismo dni
            foreach ($comitente->Personas as $persona_comitente) {
                
                
                if ($persona_comitente->numero_documento == $request->dni) { 
                    
                        
                    $mails = explode(";", strtolower(ltrim(rtrim(trim($persona_comitente->email))))); 
                    if (in_array(strtolower($request->email), $mails)) {
                        
                        $user_id = $persona_comitente->getUserIdSiCorresponde($request->dni, strtolower($request->mail));

                        if ($user_id) {
                            Comitente::vincular($user_id, $comitente);
                        } else {
                            Comitente::crearUserYVincular(strtolower($request->email), $comitente, $persona_comitente);
                        }

                        $existe_persona = true;
                        return redirect()->route('frontend.login');
                                
                    }

                }
            }

            if (!$existe_persona) {
                return redirect()->route('frontend.autogestion')->with('datos_invalidos', true)->withInput();
            }

        } else {
            return redirect()->route('frontend.autogestion')->with('datos_invalidos', true)->withInput();
        }
    }
}
