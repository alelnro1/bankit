<?php

namespace App\Http\Controllers\Frontend;

use App\Classes\PosicionComitente;
use App\Exports\OperacionesAprobadasExportTxt;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Http\Controllers\Controller;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->guardarTiposInstrumentos();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comitente_id = Auth::user()->getIdComitente();
        $operaciones_pendientes = Operacion::getOperacionesPendientesDeTipoInstrumento(null, $comitente_id);
        $operaciones_pendientes->load(['TipoOperacion', 'Deposito']);

        $op_pendientes_sin_comprobante = Operacion::getPendientesComprobantePendiente($operaciones_pendientes);
        $op_pendientes_con_comprobante = Operacion::getPendientesConComprobante($operaciones_pendientes);


        $posicion_comitente = new PosicionComitente();
        $vista_posiciones = $posicion_comitente->getVistaPosicionesHome();
        $posicion_instrumentos= $posicion_comitente->getPosicionInstrumentos();


        foreach ($posicion_instrumentos as  $posicion_instrumento) {
            $comitente_id = Auth::user()->getIdComitente();
            $fondo = $posicion_instrumento->Instrumento;
            $operaciones_pendientes = Operacion::where("instrumento_id",$fondo->id)
                                                    ->where("comitente_id",$comitente_id)
                                                    ->select();

            $operaciones_pendientes = Operacion::getOperacionesPendientesDeTipoInstrumento(null, $comitente_id);
            
            $operaciones_pendientes->load(['TipoOperacion', 'Deposito']);
            
            $total_pendientes_con_comprobante = Operacion::getPendientesConComprobante($operaciones_pendientes)
                                                            ->sum("importe");
            
            $posicion_instrumento->operaciones_pendientes=$op_pendientes_con_comprobante;
            $posicion_instrumento->total=$posicion_instrumento->valor_estimado + $total_pendientes_con_comprobante;

            
        }




        $totales["pesos"]=0;
        $totales["dolares"]=0;

        $posicion_instrumentos = $posicion_comitente->getPosicionInstrumentosTotal();

        foreach ($posicion_instrumentos as $posicion) {
            $moneda = MonedaBase::where("id",$posicion->moneda_id)->first();
            
            if ($moneda->simbolo=="USD") {
                $totales["dolares"] += $posicion->pendiente_liquidar * $posicion->Cotizaciones[0]->valor_compra;
                $totales["dolares"] += $posicion->pendiente_comprobante * $posicion->Cotizaciones[0]->valor_compra;
                $totales["dolares"] += $posicion->disponible * $posicion->Cotizaciones[0]->valor_compra;
            } else {
                $totales["pesos"] += $posicion->pendiente_liquidar * $posicion->Cotizaciones[0]->valor_compra;
                $totales["pesos"] += $posicion->pendiente_comprobante * $posicion->Cotizaciones[0]->valor_compra;
                $totales["pesos"] += $posicion->disponible * $posicion->Cotizaciones[0]->valor_compra;
            }

          

        }



        return view('frontend.home', [
            'operaciones_sin_comprobante' => $op_pendientes_sin_comprobante,
            'operaciones_con_comprobante' => $op_pendientes_con_comprobante,
            'vista_posiciones' => $vista_posiciones,
            'totales' => $totales
        ]);
    }

    /**
     * Generamos una sesion con todos los tipos de instrumentos para no tener que consultar muchas veces a la BD
     */
    private function guardarTiposInstrumentos()
    {
        $tipos_instrumentos = TipoInstrumento::getTipos();

        session(['TIPOS_INSTRUMENTOS' => $tipos_instrumentos]);
    }

    /**
     * Cargo las operaciones pendientes de un FCI
     *
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function getOperacionesPendientesDeFondoParaModal(Request $request)
    {
        $fondo = Instrumento::where('id', $request->fondo)->first();

        $operaciones_pendientes = Operacion::all();

        $comitente_id = Auth::user()->getIdComitente();
        $operaciones_pendientes = Operacion::getOperacionesPendientesDeTipoInstrumento(null, $comitente_id);
        $operaciones_pendientes->load(['TipoOperacion', 'Deposito']);
        $op_pendientes_con_comprobante = Operacion::getPendientesConComprobante($operaciones_pendientes);

        return
            view('frontend.operaciones.modal-operaciones-pendientes-fondo', [
                'fondo' => $fondo,
                'operaciones' => $op_pendientes_con_comprobante
            ])->render();
    }


}
