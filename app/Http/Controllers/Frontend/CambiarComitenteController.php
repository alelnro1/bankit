<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CambiarComitenteController extends Controller
{
    public function mostrarSeleccionComitentes()
    {
        $comitentes = Auth::user()->Comitentes;

        return view('frontend.cambiar-comitente', [
            'comitentes' => $comitentes
        ])->render();
    }
}
