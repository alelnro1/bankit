<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comitente;
use App\Models\ComitenteUsuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SeleccionComitenteController extends Controller
{
    /**
     * Si el cliente tiene 1 solo comitente, se lo autoasigno, sino redirijo al formulario para que
     * seleccione un comitente con el cual operar
     */
    public function seleccionarComitente()
    {
        // Vamos a guardar una sesion para el modal de operacion
        //$this->crearSesionConDatosParaOperar();

        // Busco los comitentes que tiene asignado el usuario

        $comitentes_usuario = Auth::user()->Comitentes;


        // Si tiene mas de uno, redirijo para que pueda seleccionar el comitente con que el desea operar
        if (count($comitentes_usuario) > 1) {
            return $this->mostrarSeleccionDeComitente($comitentes_usuario);
        } else {
            // Obtengo el numero de comitente del unico comitente que tiene el usuario
            $comitente = $comitentes_usuario->first();

            $this->asignarComitente($comitente->numero_comitente);
        }

        return redirect('/home');
    }

    /**
     * Muestro el formulario para seleccionar el comitente con el que desea operar
     * @param $comitentes_usuario
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mostrarSeleccionDeComitente($comitentes_usuario)
    {
        //$comitentes_usuario = $this->agregarNombreDeEscoAComitentes($comitentes_usuario);

        return view('frontend.auth.seleccionar-comitente', ['comitentes' => $comitentes_usuario]);
    }

    /**
     * Proceso el formulario de seleccion de cliente
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function procesarSeleccion(Request $request)
    {
        if ($request->nro_comitente && $this->comitentePerteneceAUsuario($request->nro_comitente)) {
            $this->asignarComitente($request->nro_comitente);

            return redirect('/home');
        }
    }

    /**
     * Asigno el comitente que seleccionó el usuario.
     * En caso de haber un solo comitente, se autoselecciona y se genera la sesion con
     * el comitente seleccionado
     *
     * @param $nro_comitente
     * @return bool
     */
    private function asignarComitente($nro_comitente)
    {   
        $comitente = Comitente::where('numero_comitente', $nro_comitente)->first();
        //$comitente = Comitente::where('numero_comitente', "654321")->first();

        session([
            'ID_COMITENTE' => $comitente->id,
            'NRO_COMITENTE' => $comitente->numero_comitente,
            'NOMBRE_COMITENTE' => $comitente->descripcion
        ]);

        return true;
    }

    /**
     * Valido que el comitente al que se quiere cambiar, realmente pertenezca al usuario
     *
     * @param $nro_comitente
     * @return bool
     */
    private function comitentePerteneceAUsuario($nro_comitente)
    {
        // Obtengo el ID del comitente
        $comitente = Comitente::where('numero_comitente', $nro_comitente)->first();

        $comitente_usuario =
            ComitenteUsuario::where('comitente_id', $comitente->id)
                ->where('user_id', Auth::user()->id)
                ->count();

        return $comitente_usuario > 0;
    }
}
