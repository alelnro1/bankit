<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Instrumentos\Instrumento;
use App\Models\Operaciones\TipoInstrumento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class CotizacionesController extends Controller
{
    /**
     * Obtenemos el listado de fondos con sus cotizaciones
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fondos()
    {
        $tipo_instrumento_fondo_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        $fondos = Instrumento::getInstrumentosDeTipo($tipo_instrumento_fondo_id);

        // Cargamos las cotizaciones con las monedas
        $fondos->load([
            'Cotizaciones' => function ($query) {
                $query
                    ->orderBy('fecha_cotizacion', 'DESC')
                    ->with('Moneda');
            }
        ]);
        
        return view('frontend.cotizaciones.fondos.listado', [
            'fondos' => $fondos
        ]);
    }

    /**
     * Obtenemos la ultima cotizacion de un fondo
     *
     * @param Instrumento $fondo
     * @return mixed
     */
    public function getCotizacionDeFondo($fondo_id)
    {
        $fondo = Instrumento::findOrFail($fondo_id);

        // Cargamos las cotizaciones con las monedas
        $fondo->load([
            'Cotizaciones' => function ($query) {
                $query
                    ->orderBy('fecha_cotizacion', 'DESC')
                    ->take(1)
                    ->with('Moneda');
            }
        ]);

        return $fondo->Cotizaciones->first()->valor_compra;
    }

    /**
     * Mostramos la vista de un fondo en especifico
     *
     * @param Instrumento $fondo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function singleFondo(Instrumento $fondo)
    {
        $lava = new \Khill\Lavacharts\Lavacharts;

        $grafico_composicion = $this->armarComposicionFondoSingle($fondo, $lava);

        $grafico_historico = $this->armarHistoricoFondoSingle($fondo, $lava);

        // Cargamos las cotizaciones con las monedas
        $fondo->load([
            'Cotizaciones' => function ($query) {
                $query
                    ->orderBy('fecha_cotizacion', 'DESC')
                    ->take(1)
                    ->with('Moneda');
            }
        ]);

        return view('frontend.cotizaciones.fondos.single', [
            'fondo' => $fondo,
            'grafico_composicion' => $grafico_composicion,
            'grafico_historico' => $grafico_historico
        ]);
    }

    /**
     * Armamos el pie chart de composicion del fondo
     *
     * @param $fondo
     * @return \Khill\Lavacharts\Lavacharts
     */
    private function armarComposicionFondoSingle($fondo, $lava)
    {
        $composicion = $lava->Datatable();
        $composicion->addStringColumn('Tipo')
            ->addStringColumn('Cantidad');

        // Cargamos las composiciones del fondo
        $fondo->load('Composicion');

        foreach ($fondo->Composicion as $fon_composicion) {
            $composicion->addRow([$fon_composicion->descripcion, $fon_composicion->porcentaje]);
        }

        $lava->PieChart('Composicion', $composicion);

        return $lava;
    }

    /**
     * Armamos el area chart del grafico historico
     *
     * @param $fondo
     */
    private function armarHistoricoFondoSingle($fondo, $lava)
    {
        /* Obtengo los datos historicos de la API */
        $client = new \GuzzleHttp\Client();

        $res = $client->request('POST', env('INTL_WS_URL') . 'fondos-cotizaciones', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ],
            'json' => [
                'token' => env('INTL_WS_TOKEN'),
                'cod_accion' => 'D',
                'cod_fondo' => $fondo->cod_interfaz,
                'abreviatura' => $fondo->clase,
                'fecha_desde' => date("Ymd", strtotime("-24 month")),
                'fecha_hasta' => date("Ymd", time())
            ]
        ]);

        $cotizaciones = collect(json_decode($res->getBody()));
        

        /* Armo el grafico area */
        $historico = $lava->Datatable();
        $historico->addDateColumn('Tiempo')
            ->addNumberColumn('Valor Cuotaparte');

        foreach ($cotizaciones as $cotizacion) {
            $fecha = Carbon::createFromFormat("Y-m-d H:i:s", $cotizacion->Fecha)->format("d-m-Y");

            $historico->addRow([$fecha, $cotizacion->ValorCuotaparte]);
        }

        $lava->AreaChart('Historico', $historico, [
            'width' => '1000'
        ]);

        return $lava;
    }
}
