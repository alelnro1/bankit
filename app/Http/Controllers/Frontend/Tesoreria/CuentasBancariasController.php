<?php

namespace App\Http\Controllers\Frontend\Tesoreria;

use App\Http\Controllers\WSSaenzController;
use App\Http\Requests\AltaCuentaBancariaRequest;
use App\Models\Banco;
use App\Models\Operaciones\Operacion;
use App\Models\Monedas\MonedaBase;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use App\Models\Tesoreria\CuentasBancarias\TipoCuentaBancaria;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CuentasBancariasController extends WSSaenzController
{
    public function index()
    {
        $cuentas_dolar = CuentaBancaria::getCuentasDeMoneda('Dolares');
        $cuentas_peso = CuentaBancaria::getCuentasDeMoneda('Pesos');

        return view('frontend.tesoreria.cuentas-bancarias.index', [
            'cuentas_dolar' => $cuentas_dolar,
            'cuentas_peso' => $cuentas_peso
        ]);
    }

    /**
     * Vamos a precargar los datos del formulario de alta de cuenta bancaria con los datos recibidos por
     * el WS de Saenz
     *
     * @param Request $request
     */
    public function precargarDatosCuenta(Request $request)
    {
        if ($request->cbu) {
            $datos = $this->WSDatos($request->cbu, 'cbu');
        } else if ($request->alias) {
            $datos = $this->WSDatos($request->alias, 'alias');
        }

        if ((boolean)$datos->CtaActiva !== true) {
            $datos = null;
        }

        return $this->alta($datos);
    }

    /**
     * Mostramos el formulario de alta de cuenta bancaria
     *
     * @param $datos_cbu Array recibimos los datos de una cuenta bancaria por cbu
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function alta($datos_cbu = null)
    {
        $tipos_cuentas = TipoCuentaBancaria::all();
        $monedas = MonedaBase::all();
        $bancos = Banco::all();

        return view('frontend.tesoreria.cuentas-bancarias.alta', [
            'tipos_cuentas' => $tipos_cuentas,
            'monedas' => $monedas,
            'bancos' => $bancos,
            'datos_por_cbu' => $datos_cbu
        ]);
    }

    /**
     * Procesamos el formulario de alta de cuenta bancaria
     *
     * @param AltaCuentaBancariaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function procesarFormAlta(AltaCuentaBancariaRequest $request)
    {
        $request->request->add([
            'comitente_id' => Auth::user()->getIdComitente(),
            'comprobante' => '',
            'user_id' => Auth::user()->id
        ]);

        // Vamos a crear la cuenta
        $cuenta_creada = $this->crearCuentaBancaria($request);

        return response()->json([
            'success' => $cuenta_creada,
            'redirect_url' => route('tesoreria.cuentas-bancarias.index')
        ]);
    }

    public function desvincularCuenta(CuentaBancaria $cuenta, Request $request)
    {

        $cuenta = CuentaBancaria::where("id",$request->cuenta)->first();

        $cantidad_cuentas_por_moneda = CuentaBancaria::where("id","<>",$cuenta->id)
                                        ->where("comitente_id",Auth::user()->getIdComitente())
                                        ->where("moneda_id",$cuenta->moneda_id)
                                        ->where("deleted_at",null)
                                        ->count();
        

        if ($cantidad_cuentas_por_moneda>=0){
            return response()->json([
                'procesado' => false,
                'mensaje' => 'Tiene que tener al menos una cuenta bancaria para operar'
            ]);
        }

        $operaciones = Operacion::where("comitente_id",Auth::user()->getIdComitente())
                                        ->where("moneda_id",$cuenta->moneda_id)
                                        ->where("estado_id",1)
                                        ->where("deleted_at",null)
                                        ->get();

        
        foreach ($operaciones as $operacion) {
            $operacion->load(['Deposito']);
                             
            if (isset($operacion->Deposito)) {
                $operacion->Deposito->load(['CuentaBancaria']);

                if ($operacion->Deposito->CuentaBancaria->id==$cuenta->id){
                    return response()->json([
                        'procesado' => false,
                        'mensaje' => 'Esta cuenta bancaria tiene suscripciones pendientes, no la puede desvincular'
                    ]);
                }
            }

        }

        


        
        $cuenta->deleted_at= date("Y-m-d H:i");
        $cuenta->delete();

        return response()->json([
            'procesado' => true
        ]);
    }

    /**
     * Creamos la cuenta bancaria
     *
     * @param $request
     * @return bool
     */
    private function crearCuentaBancaria($request)
    {
        try {
            $cuenta = CuentaBancaria::firstOrCreate($request->except('_token'));
            $cuenta->aceptar();


            if ($cuenta->wasRecentlyCreated) {
                Log::info('nueva');
            } else {
                Log::info('existe');
            }

            Log::info(print_r($cuenta, true));

            if (!$cuenta) {
                throw new Exception();
            } else {
                $success = true;
            }

            session()->flash('success', true);
        } catch (Exception $e) {
            Log::warning('EXCEPTION CAUGHT: ' . $e->getMessage());

            $success = false;
        }

        return $success;
    }
}