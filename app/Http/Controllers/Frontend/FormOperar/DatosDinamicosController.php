<?php

namespace App\Http\Controllers\Frontend\FormOperar;

use App\Models\CuentasCorrientes\CuentaCorriente;
use App\Models\CuentasCorrientes\CuentaCorrienteInstrumento;
use App\Models\Operaciones\Operacion;
use App\Classes\PosicionComitente;
use App\Classes\Factories\OperacionesFactory;
use App\Models\Estado;
use App\Models\Instrumentos\CotizacionInstrumento;
use App\Models\Instrumentos\Instrumento;
use App\Models\Operaciones\PlazoLiquidacion;
use App\Models\Operaciones\TipoInstrumento;
use App\Models\Operaciones\TipoOperacion;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DatosDinamicosController extends Controller
{
    /**
     * Obtenemos todos los instrumentos que se pueden operar a través del sitio
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getInstrumentos()
    {
        $instrumentos = TipoInstrumento::all();

        return response()->json([
            'instrumentos' => $instrumentos
        ]);
    }

    /**
     * Obtengo los tipos de operaciones disponibles para cada instrumento
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTipoOperacionSegunInstrumento(Request $request)
    {
        $tipo_instrumento = TipoInstrumento::where('id', $request->id)->first();

        $operaciones_de_instrumento = $tipo_instrumento->getOperacionesPorInstrumento();

        $vista_operaciones =
            view('frontend.form-operar.tipos-operacion', ['operaciones' => $operaciones_de_instrumento])->render();

        return response()->json([
            'tipos_operaciones' => $vista_operaciones
        ]);
    }

    public function getInstrumentosPorTipoOperacion(Request $request)
    {
        $tipo_instrumento = TipoInstrumento::getTipoInstrumentoPorDescripcion($request->tipo_instrumento);
        $tipo_operacion = TipoOperacion::getTipoOperacionPorNombre($request->tipo_operacion);

        $operacion = (new OperacionesFactory())->create($tipo_instrumento, $tipo_operacion);

        $vista_instrumentos = $operacion->getSelectInstrumentosDisponibles();
        //$vista_especies = $tipo_operacion->getSelectEspeciesPorTipo($tipo_instrumento);
        
        return response()->json([
            'instrumentos' => $vista_instrumentos
        ]);
    }

    /**
     * Obtengo los campos especificos para cada instrumento
     * para que el usuario pueda ingresar informacion (ej: unidad, cantidad, plazo validez, etc)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCamposParaInfoPorInstrumento(Request $request)
    {
        $tipo_instrumento = TipoInstrumento::getTipoInstrumentoPorDescripcion($request->tipo_instrumento);
        $tipo_operacion = TipoOperacion::getTipoOperacionPorNombre($request->tipo_operacion);

        $operacion = (new OperacionesFactory())->create($tipo_instrumento, $tipo_operacion);

        $campos_de_instrumento = $operacion->getCamposPorInstrumento();

        return response()->json([
            'campos' => $campos_de_instrumento
        ]);
    }

    /**
     * Obtengo los plazos disponibles segun el instrumento
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlazosYMonedasPorInstrumento(Request $request)
    {
        $instrumento = Instrumento::getInstrumentoPorNombreWeb($request->instrumento);
        $tipo_operacion = TipoOperacion::getTipoOperacionPorNombre($request->tipo_operacion);

        $plazos = ($instrumento->load(['PlazosLiquidaciones' => function ($query) use ($tipo_operacion) {
            $query->where('tipo_operacion_id', $tipo_operacion->id);
        }]))->PlazosLiquidaciones;

        $monedas = $instrumento->Monedas()->get();

        $vista_plazos = view('frontend.form-operar.plazos-liquidacion', ['plazos' => $plazos])->render();
        $vista_monedas = view('frontend.form-operar.monedas', ['monedas' => $monedas])->render();

        $datos_instrumento['limite_minimo'] = $instrumento['limite_minimo'];
        $datos_instrumento['cotizacion'] = CotizacionInstrumento::getUltimaCotizacionDeInstrumento($instrumento->id)->valor_compra;



        $instrumento->disponible=CuentaCorrienteInstrumento::
                                                    where("instrumento_id",$instrumento->id)
                                                    ->where("comitente_id",Auth::user()->getIdComitente())
                                                    ->get();
                                                  
        if (isset($instrumento->disponible)) {
            foreach ($instrumento->disponible   as $value) {
                $value->cantidad=$value->cantidad * $value->multiplicador;

            }
            
            $instrumento->disponible = $instrumento->disponible->sum("cantidad");

            $rescates_efectuados = Operacion::where("comitente_id",Auth::user()->getIdComitente())
                                        ->where("instrumento_id",$instrumento->id)
                                        ->where("subtipo_operacion","<>","SUS")
                                        ->where("estado_id","1")
                                        ->sum("cantidad");
            
            $instrumento->disponible -=$rescates_efectuados;
            $datos_instrumento['cant_cuotapartes'] = $instrumento->disponible;

        } else {
            $datos_instrumento['cant_cuotapartes'] = 0;
        }

        return response()->json([
            'plazos' => $vista_plazos,
            'monedas' => $vista_monedas,
            'datos_instrumento' => $datos_instrumento
        ]);
    }

    /**
     * Obtengo las cuentas bancarias de la moneda seleccionada
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCuentasBancarias(Request $request)
    {
        $cuentas = CuentaBancaria::getCuentasDeMoneda($request->moneda);

        // Buscamos el estado aprobado id para poder filtrar
        $estado_aprobado_id = Estado::getEstadoPorNombre('Aprobado')->id;

        // Filtramos solo las cuentas aprobadas
        $cuentas =
            $cuentas->filter(function ($cuenta) use ($estado_aprobado_id) {
                return $cuenta->estado_id == $estado_aprobado_id;
            });

        $vista_cuentas = view('frontend.form-operar.cuentas-bancarias', [
            'cuentas' => $cuentas
        ])->render();

        return response()->json([
            'cuentas' => $vista_cuentas
        ]);
    }
}
