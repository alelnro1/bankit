<?php

namespace App\Http\Controllers\Frontend\FormOperar;

use App\Classes\Operaciones\Fondos\Fondos;
use App\Classes\WebServices\INTLWebService;
use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use App\Models\Operaciones\TipoOperacion;
use App\Models\Estado;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class OperacionesController extends Controller
{
    /**
     * Vamos a procesar la operacion. Inicializamos fondos si corresponde y le delegamos procesar la operacion.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function procesarOperacion(Request $request)
    {

        // Buscamos el tipo de instrumento
        $tipo_instrumento = TipoInstrumento::findOrFail($request->tipo_instrumento);

        // Buscamos el tipo de operacion
        $tipo_operacion = TipoOperacion::findOrFail($request->tipo_operacion);

        if ($tipo_instrumento->esFondo()) {
            $fondos = new Fondos();

            $fondos->setTipoInstrumento($tipo_instrumento);
            $fondos->setTipoOperacion($tipo_operacion);

            if ($fondos->puedeOperar($request)) {
                // Le pedimos al objeto fondo operar

                
                $fondos->operar($request);
            } else {
                if (!$tipo_operacion->esSuscripcion()) {
                    return response()->json([
                        'procesado' => false,
                        'error' => 'Cuotapartes disponibles insuficientes.'
                    ]);
                }
            }
        }

        return response()->json([
            'procesado' => true
        ]);
    }

    /**
     * Mostramos el formulario de la operación para adjuntar el comprobante
     *
     * @param Operacion $operacion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mostrarFormAdjuntarComprobante(Operacion $operacion)
    {
        // Validamos que la operación corresponda al comitente actualmente logueado
        if (!$this->puedeAdjuntarComprobante($operacion)) abort(404);

        return view('frontend.operaciones.cargar-comprobante', [
            'operacion' => $operacion
        ]);
    }

    /**
     * Procesamos y adjuntamos el comprobante al depósito y redirijimos al home
     *
     * @param Request $request
     */
    public function adjuntarComprobante(Operacion $operacion, Request $request)
    {
        // Validamos que la operación corresponda al comitente actualmente logueado
        if (!$this->puedeAdjuntarComprobante($operacion)) abort(404);

        $this->validate($request, [
            //'comprobante' => 'required|size:4048|mimes:pdf,jpg,jpeg,png'
        ]);

        $nombre = $request->comprobante->hashName();

        $request->file('comprobante')->move('uploads/comprobantes-depositos', $nombre);

        $operacion->Deposito->agregarComprobante($nombre);

        return redirect(route('home'))->with([
            'comprobante_adjunto' => true
        ]);
    }

    public function cancelarOperacion (Operacion $operacion, Request $request)
    {
        // Verificamos que la operacion que se quiere cancelar, sea del comitente actual
        if (Auth::user()->getIdComitente() === $operacion->comitente_id) {
            $estado_cancelado_id = Estado::getIdEstado('Cancelado');

            $operacion->estado_id=$estado_cancelado_id;
            $operacion->save();

            return response()->json([
                'procesado' => true
            ]);
        } else {
            return response()->json([
                'procesado' => false
            ]);
        }
    }

    public function getOperacionesRealizadas (Request $request)
    {

          if ($request->fecha_desde != "") {
                $fecha_desde = date("Ymd", DateTime::createFromFormat("d/m/Y", $request->fecha_desde)->getTimestamp());
                $fecha_desde_datetimepicker = $request->fecha_desde;
            } else {
                $fecha_desde = date("Ymd");
                $fecha_desde_datetimepicker = date("d/m/Y");
            }

          if ($request->fecha_hasta != "") {
                $fecha_hasta = date("Ymd", DateTime::createFromFormat("d/m/Y", $request->fecha_hasta)->getTimestamp());
                $fecha_hasta_datetimepicker = $request->fecha_hasta;
            } else {
                $fecha_hasta = date("Ymd");
                $fecha_hasta_datetimepicker = date("d/m/Y");
            }

            $operaciones = Operacion::where("comitente_id",Auth::user()->getIdComitente())
                                        ->where("fecha_concertacion",">=",$fecha_desde)
                                        ->where("fecha_concertacion","<=",$fecha_hasta)
                                        ->get();


            foreach ($operaciones as $operacion) {
                $operacion->load('Comitente','Moneda','TipoOperacion','Estado');
            }
            

            return view('frontend.operaciones.fondos-realizadas', [
                'fecha_desde' => $fecha_desde_datetimepicker,
                'fecha_hasta' => $fecha_hasta_datetimepicker,
                'operaciones' => $operaciones
            ]);
    }

    /**
     * Un comprobante se puede adjuntar si la operacion necesita un comprobante (ej: suscripcion) y
     * es del comitente actual
     *
     * @param $operacion
     * @return bool
     */
    private function puedeAdjuntarComprobante($operacion)
    {
        return
            ($operacion->getComitenteId() != Auth::user()->getIdComitente() ||
                $operacion->necesitaComprobante());
    }
}
