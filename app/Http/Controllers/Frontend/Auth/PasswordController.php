<?php

namespace App\Http\Controllers\Frontend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function __construct()
    {
    }

    public function showResetForm()
    {
        return view('frontend.auth.passwords.reset');
    }

    public function cambiar(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {

            // The passwords matches
            return redirect()->back()->with("error", "La contraseña actual es incorrecta.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {

            //Current password and new password are same
            return redirect()->back()->with("error", "La nueva contraseña no puede ser igual a la anterior.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])[\w]{8,20}$/|',
        ], [
            'new-password.regex' => 'La contraseña debe tener entre 8 y 20 caracteres, una mayúscula y un número.'
        ]);

        //Change Password
        $user = Auth::user();

        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        Auth::logout();

        return redirect(route('frontend.login'))->with("success", "La contraseña se modificó correctamente.");
    }

}
