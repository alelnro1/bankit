<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Models\Prospectos\Prospecto;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('frontend.auth.passwords.email');
    }

    public function sendResetLinkEmail(ForgotPasswordRequest $request)
    {
        // Buscamos el usuario
        $user = User::where('email', $request->email)->first();

        // Buscamos el prospecto
        $prospecto = Prospecto::where('email', $request->email)->first();
        
        // Generamos la nueva contraseña
        $new_password = str_random(10);

        // Hasheamos la nueva contraseña
        $new_password_hashed = Hash::make($new_password);


        if ($prospecto) {
            $prospecto->password         = $new_password_hashed;
            $prospecto->intentos_fallidos = 0;
            $prospecto->cambio_password = 1;
            $prospecto->save();

            $data = [
                'nombre'   => $prospecto->email,
                'apellido'   => '',
                'password' => $new_password,
                'email' => $prospecto->email,
            ];
        }


        if ($user) {
            $user->password         = $new_password_hashed;
            $user->intentos_fallidos=0;
            $user->cambio_password = 1;
            $user->save();

            $data = [
                'nombre'   => $user->nombre,
                'apellido'   => $user->apellido,
                'password' => $new_password,
                'email' => $user->email
            ];
        }


        if (isset($prospecto) || isset($user)) {
            Mail::send('emails.reset-password', $data, function ($message) use ($data) {
                $message->to($data['email'])->subject('Reinicio de contraseña');
            });
        }

        //return redirect()->back()->with(['success' => true]);
        Session::flash('recuperar-password', true);
        return redirect(route('frontend.login'))->with(['recuperar-password' => true]);;
    }

}
