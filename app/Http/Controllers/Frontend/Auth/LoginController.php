<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\User;
use App\Models\Prospectos\Prospecto;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function login(Request $request)
    {

        // Buscamos el tipo de prospecto Finalizado
        $estado_finalizado_id = Estado::getIdEstado('Finalizado');

        // Buscamos un prospecto con los datos recien ingresados
        $prospecto = Prospecto::where('email', $request->email)->where('estado_id', '<>', $estado_finalizado_id)->first();

        // Si el usuario tiene un prospecto creado, que no está finalizado => redirijo al apertura-cuenta
        if ($prospecto) {
            if (Auth::guard('apertura-cuenta')->attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {
                return redirect()->route('apertura-cuenta.proceso.paso-1');
            }
        } else {
            if (Auth::attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {
                $user = User::where("email",$request->email)->first();
                if (isset($user)) {
                    $user->intentos_fallidos=0;
                    $user->save();
                }
                return redirect()->route('comitentes.seleccionar.form');
            }
        }

        // if unsuccessful, then redirect back to the login with the form data
        $user = User::where("email",$request->email)->first();
        if (isset($user)) {

            /* Bloquear Usuario */
            if (isset($user->intentos_fallidos) && $user->intentos_fallidos>=5) {
                $user->password=bcrypt("ComitenteBloqueado1234!!");
                $user->cambio_password=true;
                $user->save();                

            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->with(['email_error' => __('auth.locked')]);
            }

            /* Actualizo los intengos Fallidos */
            if (isset($user->intentos_fallidos)) {
                $intentosFallidos = $user->intentos_fallidos;                
            } else {
                $intentosFallidos = 0;
            }

            $user->intentos_fallidos=$intentosFallidos+1;
            $user->save();
        };


        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->with(['email_error' => __('auth.failed')]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
