<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WSSaenzController extends Controller
{
    protected $usuario;

    public function setProspecto($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Obtengo el prospecto actual, sea seteado, o el de la sesion de apertura de cuenta
     *
     * @return mixed
     */
    protected function getUsuarioActual()
    {
        if ($this->usuario) {
            $usuario = $this->usuario;
        } else {
            $usuario = Auth::guard('apertura-cuenta')->user();
        }

        return $usuario;
    }

    /**
     * Validamos que los CBU de los titulares matcheen con los cbus de la cuenta bancaria (obtenidos por el servicio)
     *
     * @param $cbu
     * @return bool
     */
    public function validarCBUDeCuentas($cbu)
    {
        if (env("VALIDAR_TITULARIDAD_CBU")==false){
            return true;
        }


        $ws = new WSSaenzController();
        $xml = $ws->WSDatos($cbu,"cbu");

        // Obtenemos los cuits del servicio y los almacenamos en un array
        $cuits_servicio = $cuits_titulares = [];

        foreach ($xml->Titulares as $titular) {
            array_push($cuits_servicio, (string)$titular->Titular->CUIT);
        }

        // Obtengo los CUITS de los titulares cargados
        $prospecto = $this->getUsuarioActual();

        $titulares = $prospecto->getTitulares();

        foreach ($titulares as $titular) {
            array_push($cuits_titulares, $titular->cuit_cuil);
        }

        // Ordenamos los dos arrays de cuits para poder compararlos
        sort($cuits_servicio);
        sort($cuits_titulares);

        return ($cuits_servicio == $cuits_titulares);
    }

    public function WSDatos($dato, $tipo)
    {
        if ($tipo == "cbu") {
            return $this->WSDatosPorCBU($dato);
        } else {
            return $this->WSDatosPorAlias($dato);
        }
    }

    /**
     * Buscamos los datos de una cuenta bancaria por CBU
     *
     * @param $cbu
     * @return \SimpleXMLElement
     */
    private function WSDatosPorCBU($cbu)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'http://10.2.73.11:8610/WsAliasCBUconsulta.asmx/ConsultaPorCBU', [
            'form_params' => [
                'strCBU' => $cbu
            ]
        ]);

        $xml = new \SimpleXMLElement($res->getBody()->getContents());

        return $xml;

    }

    /**
     * Buscamos los datos de una cuenta bancaria por Alias
     *
     * @param $alias
     * @return \SimpleXMLElement
     */
    private function WSDatosPorAlias($alias)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'http://10.2.73.11:8610/WsAliasCBUconsulta.asmx/ConsultaPorAlias', [
            'form_params' => [
                'strAlias' => $alias
            ]
        ]);

        $xml = new \SimpleXMLElement($res->getBody()->getContents());

        return $xml;
    }
}
