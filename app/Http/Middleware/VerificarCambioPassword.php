<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerificarCambioPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->cambio_password == true) {
            if ($request->getPathInfo() != "/password/cambiar") {
                return redirect(route('frontend.password.cambiar.form'));
            }
        }

        return $next($request);
    }
}
