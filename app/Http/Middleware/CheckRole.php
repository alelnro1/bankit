<?php 

namespace App\Http\Middleware;

use Closure;
use App\Permiso;
use Illuminate\Support\Facades\Auth;

class CheckRole{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role,$role2 = '',$role3 = '')
    {
       
        $modules = Permiso::whereIn('backend_permisos_nombre',[$role3,$role2,$role])->get();
        

        if($modules){
            foreach ($modules as $module) {
                $permiso = $module ? Auth::user()->tienePermiso($module->id) : false;
                if($permiso){
                    break;
                }
            }
        }else{
            $permiso = false;
        }

        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.

        if($permiso)
        {
            return $next($request);
        }else{
            abort(403, 'Unauthorized action.');
        }
    }
}