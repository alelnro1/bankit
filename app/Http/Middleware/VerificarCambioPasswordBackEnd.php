<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerificarCambioPasswordBackEnd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->cambio_password == true) {
            if ($request->getPathInfo() != "/admin/password/cambiar") {

                return redirect(route('backend.password.cambiar.form'));
            }
        }

        return $next($request);
    }
}
