<?php

namespace App\Mail;

use App\Models\Operaciones\Operacion;
use App\Models\Comitente;
use App\Models\Prospectos\PersonasFisicas\Titular;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MontosOperacionActualizados extends Mailable
{
    use Queueable, SerializesModels;

    private $operacion;
    private $titular;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($operacion, $persona)
    {
        $this->operacion = $operacion;
        $this->titular = $persona;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('emails.operaciones-actualizadas')
                    ->with([
                        'operacion' => $this->operacion,
                        'titular' => $this->titular
                    ]);
    }
}
