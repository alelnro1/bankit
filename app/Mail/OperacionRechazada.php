<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class OperacionRechazada extends Mailable
{
    use Queueable, SerializesModels;

    private $operacion;

    private $comitente;

    private $titular;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($operacion, $comitente, $titular)
    {
        $this->operacion = $operacion;

        $this->comitente = $comitente;

        $this->titular = $titular;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        
        return $this->view('emails.operaciones-rechazadas')
            ->with([
                'comitente' => $this->comitente,
                'titular' => $this->titular,
                'operacion' => $this->operacion
            ]);
    }
}
