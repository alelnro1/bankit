<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AperturaCuentaFinalizada extends Mailable
{
    use Queueable, SerializesModels;

    private $comitente;

    private $titular;

    private $user;

    private $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comitente, $titular, $pdf, $user = null)
    {
        $this->comitente = $comitente;

        $this->titular = $titular;

        $this->pdf = $pdf;

        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::info($this->comitente);
        Log::info($this->titular);
        Log::info($this->user);

        return $this->view('emails.apertura-cuenta-finalizada')
            ->with([
                'comitente' => $this->comitente,
                'titular' => $this->titular,
                'user' => $this->user
            ])
            ->attach($this->pdf);
    }
}
