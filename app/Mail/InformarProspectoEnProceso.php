<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InformarProspectoEnProceso extends Mailable
{
    use Queueable, SerializesModels;

    private $pdf;

    private $titular;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf,$titular)
    {
        $this->pdf = $pdf;

        $this->titular = $titular;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.apertura-cuenta-en-proceso')
            ->attach($this->pdf)
            ->with([
                        'titular' => $this->titular
                    ]);
    }
}
