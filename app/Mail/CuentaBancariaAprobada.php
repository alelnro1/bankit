<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class CuentaBancariaAprobada extends Mailable
{
    use Queueable, SerializesModels;

    private $comitente;

    private $titular;

    private $cuenta_bancaria;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comitente, $titular, $cuenta_bancaria)
    {

        $this->comitente = $comitente;

        $this->titular = $titular;

        $this->cuenta_bancaria = $cuenta_bancaria;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        
        return $this->view('emails.cuenta-bancaria-aprobada')
            ->with([
                'comitente' => $this->comitente,
                'titular' => $this->titular,
                'cuenta_bancaria' => $this->cuenta_bancaria
            ]);
    }
}
