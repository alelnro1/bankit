<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CreacionDeUsuario extends Mailable
{
    use Queueable, SerializesModels;


    private $titular;

    private $user;

    private $passwordTemporal;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($titular, $user, $passwordTemporal)
    {

        $this->titular = $titular;

        $this->user = $user;

        $this->passwordTemporal = $passwordTemporal;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.creacion-usuario')
            ->with([
                'titular' => $this->titular,
                'user' => $this->user,
                'passwordTemporal' => $this->passwordTemporal
            ]);
    }
}
