<?php

namespace App;

use App\Models\BackendPermiso;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class BackendUser extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = "backend_users";

    protected $username = 'username';

    protected $guard = 'backend';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'username',
        'password',
        'cambiar_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    public static function getUsuarioInterno($usuario)
    {
        $interno = self::where('backend_users_username', $usuario)->first();

        return $interno;
    }

    public function Permisos()
    {
        return $this->belongsToMany(BackendPermiso::class, 'backend_permisos_users', 'backend_user_id', 'backend_permiso_id');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsuario()
    {
        return $this->email;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getNombreCompleto()
    {
        return $this->getNombre() . " " . $this->getApellido();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFechaDeAlta()
    {
        return date("d/m/Y H:i", strtotime($this->created_at));
    }

    public function getEmpresa(){
        return $this->backend_users_empresa;
    }

    /**
     * Indica si el interno tiene el permiso dentro de su listado de permisos
     * @param $permiso
     * @return bool
     */
    public function tienePermiso($permiso)
    {
        $permisos_del_interno = $this->Permisos;

        return ($permisos_del_interno->contains($permiso));
    }

    public function tienePermisos($permisos)
    {
        //le paso un array de permisos y valido si tiene alguno de esos
        $permisos_del_interno = $this->Permisos;
        foreach($permisos as $permiso)   {
            foreach ($permisos_del_interno as $permiso_del_interno) {
                if ($permiso==$permiso_del_interno->nombre) {
                    return true;
                }
            }
        }

        return false;        

        
    }


    /**
     * Obtengo todos los usuarios internos junto con sus permisos
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getUsuariosInternos()
    {
        $internos = self::with('Permisos')->get();

        foreach ($internos as $interno) {
            $cant_permisos = $interno->Permisos->count();

            $permisos_temp = "";

            foreach ($interno->Permisos as $key => $permiso) {
                $permisos_temp .= $permiso->getNombre();

                if ($key+1 < $cant_permisos) {
                    $permisos_temp .= " | ";
                }
            }

            $interno->Permisos = $permisos_temp;

        }

        return $internos;
    }

    /**
     * Elimino con Soft Delete al usuario interno y le cambio el email y username
     * para que pueda volver a registrarse un usuario interno con esos mismos datos
     * @return bool
     */
    public function eliminate()
    {
        $this->backend_users_email = "@'" . time() . "_" . $this->backend_users_email;
        $this->backend_users_username = "@'" . time() . "_" . $this->backend_users_username;
        $this->save();
        $this->delete();

        return true;
    }

}
