<?php

namespace App\Models;

use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = 'bancos';

    protected $fillable = [
        'descripcion'
    ];

    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class);
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
