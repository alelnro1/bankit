<?php

namespace App\Models;

use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estados';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class);
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Obtenemos el estado a partir de un nombre
     *
     * @param $nombre
     * @return mixed
     */
    public static function getEstadoPorNombre($nombre)
    {
        $estado = self::where('descripcion', $nombre)->first();

        return $estado;
    }

    /**
     * Obtenemos el ID del estado a partir del nombre
     *
     * @param $nombre_estado
     * @return bool
     */
    public static function getIdEstado($nombre_estado)
    {
        $estado = self::getEstadoPorNombre($nombre_estado);

        if ($estado)
            return $estado->id;
        else
            return false;
    }
}
