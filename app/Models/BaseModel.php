<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class BaseModel extends Model
{
    /**
     * Cada vez que se inserta un registro, si el modelo tiene estado => lo ponemos en pendiente
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // Si la tabla del modelo tiene la columna 'estado_id' le vamos a poner el estado Pendiente
            if (Schema::hasColumn($model->getTable(), 'estado_id')) {
                $model = self::agregarEstadoPendiente($model);

            }

            if (Schema::hasColumn($model->getTable(), 'user_id') && Auth::guest()) {
                $model = self::agregarUserId($model);
            }


            if (Schema::hasColumn($model->getTable(), 'prospecto_id') && isset(Auth::guard('apertura-cuenta')->user()->prospecto_id)) {
                $model = self::agregarProspectoId($model);
            }

            return $model;
        });
    }

    /**
     * Agregamos el estado pendiente al modelo
     *
     * @param $model
     * @return mixed
     */
    private static function agregarEstadoPendiente($model)
    {
        // Obtenemos el estado Pendiente
        $estado_pendiente = Estado::getEstadoPorNombre('Pendiente');

        // Le agregamos al modelo el estado pendiente
        $model->estado_id = $estado_pendiente->id;

        return $model;
    }

    /**
     * Agregamos el campo user_id al modelo
     *
     * @param $model
     * @return mixed
     */
    private static function agregarUserId($model)
    {
        $model->user_id = Auth::user()->id;

        return $model;
    }

    private static function agregarProspectoId($model)
    {
        $model->prospecto_id = Auth::guard('apertura-cuenta')->user()->prospecto_id;

        return $model;
    }
}
