<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ComitenteUsuario extends Model
{
    protected $table = "comitentes_usuarios";

    protected $fillable = [
        'comitente_id',
        'user_id'
    ];

    public function Usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function Comitente()
    {
        return $this->belongsTo(Comitente::class, 'comitente_id');
    }
}
