<?php

namespace App\Models\CuentasCorrientes;

use App\Models\Operaciones\Operacion;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class CuentaCorrienteMoneda extends CuentaCorriente
{
    protected $table = "cuentas_corrientes_monedas";

    protected $fillable = [
        'moneda_id',
        'comitente_id',
        'fecha_concertacion',
        'fecha_liquidacion',
        'registro_tabla',
        'registro_id',
        'cantidad',
        'importe',
        'multiplicador'
    ];

    public function Operacion()
    {
        return $this->belongsTo(Operacion::class, 'registro_id');
    }

    public function esPorMonto()
    {
        return $this->Operacion->esPorMonto();
    }

    public function insertar($operacion)
    {
        self::create([
            'moneda_id' => $operacion->moneda_id,
            'comitente_id' => $operacion->comitente_id,
            'fecha_concertacion' => $operacion->fecha_concertacion,
            'fecha_liquidacion' => $operacion->fecha_liquidacion,
            'registro_tabla' => $this->getTable(),
            'registro_id' => $operacion->id,
            'cantidad' => $operacion->cantidad,
            'importe' => $operacion->importe,
            'multiplicador' =>($operacion->tipoOperacion->multiplicador_moneda * -1)
        ]);
    }
}
