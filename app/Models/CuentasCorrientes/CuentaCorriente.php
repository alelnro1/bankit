<?php

namespace App\Models\CuentasCorrientes;


use Illuminate\Database\Eloquent\Model;

class CuentaCorriente extends Model
{
    protected $operacion;

    public function setOperacion($operacion)
    {
        $this->operacion = $operacion;
    }

    public function insertar($operacion)
    {
        $instrumento = new CuentaCorrienteInstrumento();
        $moneda = new CuentaCorrienteMoneda();

        $instrumento->insertar($operacion);
        $moneda->insertar($operacion);
    }
}