<?php

namespace App\Models\CuentasCorrientes;

use App\Models\Operaciones\Operacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CuentaCorrienteInstrumento extends CuentaCorriente
{
    protected $table = "cuentas_corrientes_instrumentos";

    protected $fillable = [
        'instrumento_id',
        'comitente_id',
        'fecha_concertacion',
        'fecha_liquidacion',
        'registro_tabla',
        'registro_id',
        'cantidad',
        'importe',
        'multiplicador'
    ];

    public function Operacion()
    {
        return $this->belongsTo(Operacion::class, 'registro_id');
    }

    public function esPorMonto()
    {
        return $this->Operacion->esPorMonto();
    }

    public function insertar($operacion)
    {
        self::create([
            'instrumento_id' => $operacion->instrumento_id,
            'comitente_id' => $operacion->comitente_id,
            'fecha_concertacion' => $operacion->fecha_concertacion,
            'fecha_liquidacion' => $operacion->fecha_liquidacion,
            'registro_tabla' => $this->getTable(),
            'registro_id' => $operacion->id,
            'cantidad' => $operacion->cantidad,
            'importe' => $operacion->importe,
            'multiplicador' => ($operacion->tipoOperacion->multiplicador_moneda * -1)
        ]);
    }
}
