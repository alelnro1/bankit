<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = "provincias";

    public function Titulares()
    {
        return $this->hasMany(Titular::class,"provincia_nacimiento");
    }
}
