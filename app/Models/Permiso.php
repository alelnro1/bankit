<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = "backend_permisos";

    public function Internos()
    {
        return $this->belongsToMany(User::class, 'backend_permisos_users', 'backend_permiso_id', 'backend_user_id');
    }

    public function getNombre()
    {
        return $this->backend_permisos_nombre;
    }
}
