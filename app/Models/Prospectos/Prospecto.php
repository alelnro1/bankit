<?php

namespace App\Models\Prospectos;

use App\Models\Estado;
use App\Models\Prospectos\PersonasFisicas\Titular;
use App\Models\Sociedad;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Prospecto extends Authenticatable
{
    protected $table = "prospectos";

    protected $guard = "apertura-cuenta";

    protected $fillable = [
        'tipo_prospecto_id',
        'email',
        'password',
        'estado_id',
        'step_actual'
    ];

    public function TipoProspecto()
    {
        return $this->belongsTo(TipoProspecto::class, 'tipo_prospecto_id');
    }

    public function Titulares()
    {
        return $this->belongsToMany(Titular::class, 'personas_prospectos', 'prospecto_id', 'persona_id');
    }

    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class, 'prospecto_id');
    }

    public function Estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    public function Sociedad()
    {
        return $this->hasOne(Sociedad::class);
    }

    /**
     * Obtenemos todos los titulares del prospecto
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getTitulares()
    {
        $titulares = $this->Titulares()->withPivot(['agregado_por_dni'])->get();
        $titulares->load(['Direccion','PaisNacimiento','ProvinciaNacimiento','TipoResponsableIVA']);

        
        

        return $titulares;
    }

    /**
     * Obtenemos todas las cuentas bancarias del prospecto (1 en peso y 1 en dolares maximo)
     *
     * @return mixed
     */
    public function getCuentasBancarias()
    {
        $cuentas_bancarias = $this->CuentasBancarias()->get();

        // Inicializo un array con clave (moneda) => valor (cuenta)
        $cuentas_return = [];

        foreach ($cuentas_bancarias as $cuenta) {
            if ($cuenta->esPesos()) {
                $cuentas_return['pesos'] = $cuenta;
            } else {
                $cuentas_return['dolares'] = $cuenta;
            }
        }

        return $cuentas_return;
    }

    /**
     * Devolvemos si el tipo de prospecto es de una persona fisica
     *
     * @return bool
     */
    public function esFisico()
    {
        $prospecto = $this->TipoProspecto;

        return $prospecto->descripcion == "Persona Física";
    }

    /**
     * Obtenemos los prospectos que hayan sido finalizados por el cliente y puedan mostrarse en el back
     *
     * @return mixed
     */
    public static function getFinalizados()
    {
        $prospectos =
            self::whereHas('Estado', function ($query) {
                $query->where('descripcion', 'Finalizado');
            })
                ->with('Titulares')
                ->get();

        return $prospectos;
    }

    /**
     * Damos por finalizado un prospecto del lado del cliente para que pueda enviarse al back
     */
    public function actualizarEstado($nombre_estado)
    {
        $estado_finalizado = Estado::getEstadoPorNombre($nombre_estado);

        $this->estado_id = $estado_finalizado->id;
        $this->save();
    }

    /**
     * Verificamos si el prospecto está finalizado o aprobado
     *
     * @return bool
     */
    public function estaFinalizadoOAprobado()
    {
        $estado_aprobado = Estado::getEstadoPorNombre('Aprobado');
        $estado_finalizado = Estado::getEstadoPorNombre('Finalizado');

        return ($this->estado_id == $estado_aprobado->id || $this->estado_id == $estado_finalizado->id);
    }
}
