<?php

namespace App\Models\Prospectos;

use Illuminate\Database\Eloquent\Model;

class TipoProspecto extends Model
{
    protected $table = "tipos_prospectos";

    protected $fillable = [
        'descripcion'
    ];

    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
