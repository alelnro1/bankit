<?php

namespace App\Models\Prospectos\PersonasJuridicas;

use App\Models\Sociedad;
use Illuminate\Database\Eloquent\Model;

class DireccionSociedad extends Model
{
    protected $table = "sociedades_direcciones";

    protected $fillable = [
        'calle',
        'altura',
        'piso',
        'departamento',
        'localidad',
        'codigo_postal',
        'provincia',
        'pais',
        'sociedad_id'
    ];

    public function Sociedad()
    {
        return $this->belongsTo(Sociedad::class, 'sociedad_id');
    }
}
