<?php

namespace App\Models\Prospectos\PerfilRiesgo;

use App\Models\Prospectos\Prospecto;
use Illuminate\Database\Eloquent\Model;

class PerfilRiesgoUser extends Model
{
    protected $table = "categorias_preguntas_prospectos";

    public $timestamps = false;

    protected $fillable = [
        'prospecto_id',
        'categoria_pregunta_id'
    ];

    public function Prospecto()
    {
        return $this->belongsTo(Prospecto::class, 'prospecto_id');
    }

    public function Pregunta()
    {
        return $this->belongsTo(PerfilRiesgoCategoria::class, 'categoria_pregunta_id');
    }

    /**
     * Obtengo los IDs de las respuestas seleccionadas del perfil por usuario
     * @param $prospecto_id
     * @return array
     */
    public static function getIDRespuestasSeleccionadas($prospecto_id)
    {
        $respuestas_array = [];

        $respuestas = self::where('prospecto_id', $prospecto_id)->get();

        foreach ($respuestas as $respuesta) {
            array_push($respuestas_array, $respuesta->categoria_pregunta_id);
        }

        return $respuestas_array;
    }
}
