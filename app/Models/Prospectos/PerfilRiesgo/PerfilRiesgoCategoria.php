<?php

namespace App\Models\Prospectos\PerfilRiesgo;

use Illuminate\Database\Eloquent\Model;

class PerfilRiesgoCategoria extends Model
{
    protected $table = "categorias_preguntas_perfil";

    public $timestamps = false;

    /**
     * Obtenemos las categorias con sus preguntas y un id para poder operar con jquery
     *
     * @return static
     */
    public static function datosParaFormulario()
    {
        $categorias_preguntas =
            self::all()
                ->groupBy(function ($item) {
                    return $item->categoria;
                });

        return $categorias_preguntas;
    }
}
