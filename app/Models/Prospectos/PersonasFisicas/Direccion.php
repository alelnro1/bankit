<?php

namespace App\Models\Prospectos\PersonasFisicas;

use App\Models\Pais;
use App\Models\Provincia;
use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = "personas_direcciones";

    protected $fillable = [
        'calle',
        'altura',
        'piso',
        'departamento',
        'localidad',
        'codigo_postal',
        'provincia',
        'pais',
        'persona_id'
    ];

    public function Persona()
    {
        return $this->belongsTo(Titular::class);
    }

    public function Provincia()
    {
        return $this->hasOne(Provincia::class, 'id');
    }

    public function Pais()
    {
        return $this->hasOne(Pais::class, 'id');
    }
}
