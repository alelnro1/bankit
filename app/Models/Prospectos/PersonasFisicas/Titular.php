<?php

namespace App\Models\Prospectos\PersonasFisicas;

use App\Models\EstadoCivil;
use App\Models\Prospectos\Prospecto;
use App\Models\Sexo;
use App\Models\Provincia;
use App\Models\Pais;
use App\Models\TipoDocumento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Titular extends Model
{
    protected $table = "personas";

    protected $fillable = [
        'nombre',
        'apellido',
        'tipo_documento_id',
        'numero_documento',
        'cuit_cuil',
        'sexo_id',
        'estado_civil_id',
        'tipo_responsable_iva_id',
        'tipo_responsable_ganancias_id',
        'telefono_particular',
        'prefijo_celular',
        'cod_area',
        'telefono_movil',
        'email',
        'pep',
        'vinculado',
        'tipo_cliente',
        'user_id',
        'fecha_nacimiento',
        'pep',
        'funcionario_publico',
        'pais_nacimiento',
        'provincia_nacimiento',
        'funcion_pep'
    ];

    public function Prospectos()
    {
        return $this->belongsToMany(Prospecto::class, 'personas_prospectos', 'persona_id', 'prospecto_id');
    }

    public function Direccion()
    {
        return $this->hasOne(Direccion::class, 'persona_id');
    }

    public function Conyuge()
    {
        return $this->hasOne(Conyuge::class, 'persona_id');
    }

    public function Sexo()
    {
        return $this->belongsTo(Sexo::class);
    }

    public function EstadoCivil()
    {
        return $this->belongsTo(EstadoCivil::class);
    }

    public function Provincia()
    {
        return $this->belongsTo(Provincia::class);
    }

    public function Pais()
    {
        return $this->belongsTo(Pais::class);
    }


    public function PaisNacimiento()
    {
        return $this->belongsTo(Pais::class, 'pais_nacimiento','id');
    }

    public function ProvinciaNacimiento()
    {
        return $this->belongsTo(Provincia::class, 'provincia_nacimiento','id');
    }

    public function TipoDocumento()
    {
        return $this->belongsTo(TipoDocumento::class);
    }

    public function TipoResponsableIVA()
    {
        return $this->belongsTo(TipoResponsableIVA::class ,'tipo_responsable_iva_id', 'id');
    }

    public function getProvincia()
    {
        return $this->Direccion->Provincia->descripcion;
    }

    

    /**
     * Obtenemos el user_idd si el dni e email recibidos matchean con lo que estan en la base
     *
     * @param $dni
     * @param $email
     * @return bool|mixed
     */
    public function getUserIdSiCorresponde($dni, $email)
    {
        $mails_persona = explode(";", ltrim(rtrim(trim($this->email))));

        // Recorro todos los mails de la persona, que pueden estar separados por ;
        foreach ($mails_persona as $mail_persona) {
            if ($this->numero_documento == $dni && $mail_persona == $email) {
                return $this->user_id;
            }
        }

        return false;
    }
}
