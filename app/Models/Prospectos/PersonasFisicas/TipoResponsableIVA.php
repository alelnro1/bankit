<?php

namespace App\Models\Prospectos\PersonasFisicas;

use Illuminate\Database\Eloquent\Model;

class TipoResponsableIVA extends Model
{
    protected $table = "tipos_responsables_iva";

    protected $fillable = [
        'descripcion'
    ];

    public function Titulares()
    {
        return $this->hasMany(Titular::class,"tipo_responsable_iva_id");
    }
}
