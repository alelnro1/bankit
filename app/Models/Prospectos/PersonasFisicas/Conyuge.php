<?php

namespace App\Models\Prospectos\PersonasFisicas;

use Illuminate\Database\Eloquent\Model;

class Conyuge extends Model
{
    protected $table = "personas_conyuges";

    protected $fillable = [
        'nombre',
        'apellido',
        'tipo_documento_id',
        'numero_documento',
        'sexo_id',
        'persona_id',
        'prospecto_id'
    ];

    public function Persona()
    {
        return $this->belongsTo(Titular::class);
    }

}
