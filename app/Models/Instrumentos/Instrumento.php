<?php

namespace App\Models\Instrumentos;

use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\PlazoLiquidacion;
use App\Models\Operaciones\TipoInstrumento;
use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    protected $table = 'instrumentos';

    protected $fillable = [
        "descripcion",
        "nombre_web",
        "opera_web",
        "limite_minimo",
        "calificacion_cnv",
        "clase",
        "abreviatura",
        "cod_interfaz",
        "familia",
        "categoria",
        "codigo_bloomberg",
        "sociedad_gerente",
        "sociedad_custodia",
        "benchmark",
        "codigo_caja_valores",
        "permanencia_recomendada",
        "moneda_id"
    ];

    public function PlazosLiquidaciones()
    {
        return $this->belongsToMany(
            PlazoLiquidacion::class,
            'instrumentos_plazo_liq',
            'instrumento_id',
            'plazo_liquidacion_id');
    }

    public function TipoInstrumento()
    {
        return $this->belongsTo(TipoInstrumento::class);
    }

    public function getNombreWeb()
    {
        return $this->nombre_web;
    }

    public function getAbreviatura()
    {
        return $this->abreviatura;
    }

    public function getLimiteMinimo()
    {
        return $this->limite_minimo;
    }

    public function Monedas()
    {
        return $this->belongsToMany(MonedaBase::class, 'instrumentos_monedas', 'moneda_id', 'instrumento_id');
    }

    public function Cotizaciones()
    {
        return $this->hasMany(CotizacionInstrumento::class);
    }

    public function Composicion()
    {
        return $this->hasMany(ComposicionInstrumento::class);
    }

    /**
     * Obtenemos una coleccion de instrumentos por tipo que operan web (o no)
     *
     * @param $tipo_instrumento_id
     * @param bool $opera_web
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getInstrumentosDeTipo($tipo_instrumento_id = null, $opera_web = true)
    {
        $instrumentos = self::where('tipo_instrumento_id', $tipo_instrumento_id)->where('opera_web', $opera_web)->get();

        return $instrumentos;
    }

    /**
     * Traemos todos los fondos
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getFondos()
    {
        $tipo_instrumento_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;

        return self::getInstrumentosDeTipo($tipo_instrumento_id);
    }

    /**
     * Obtenemos un instrumento segun nombre web
     *
     * @param $instrumento
     * @return Model|null|object|static
     */
    public static function getInstrumentoPorNombreWeb($instrumento)
    {
        $instrumento = self::where('nombre_web', $instrumento)->first();

        return $instrumento;
    }
}
