<?php

namespace App\Models\Instrumentos;

use App\Models\Monedas\MonedaBase;
use App\Models\Mercado;
use Illuminate\Database\Eloquent\Model;

class CotizacionInstrumento extends Model
{
    protected $table = "cotizaciones_instrumentos";

    protected $fillable = [
        'instrumento_id',
        'patrimonio',
        'tasa_mensual',
        'moneda_id',
        'fecha_cotizacion',
        'valor_compra',
        'valor_venta',
        'var_diaria',
        'var_mensual',
        'var_anual',
        'cant_cuotapartes'
    ];

    public function Instrumento()
    {
        return $this->belongsTo(Instrumento::class);
    }

    public function Moneda()
    {
        return $this->belongsTo(MonedaBase::class);
    }

    public static function getUltimaCotizacionDeInstrumento($instrumento_id)
    {
        $cotizacion = self::where('instrumento_id', $instrumento_id)->orderBy("fecha_cotizacion","desc")->first();

        return $cotizacion;
    }
}
