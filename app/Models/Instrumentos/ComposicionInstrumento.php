<?php

namespace App\Models\Instrumentos;

use Illuminate\Database\Eloquent\Model;

class ComposicionInstrumento extends Model
{
    protected $table = "instrumentos_composicion";

    protected $fillable = ['porcentaje'];

    public function Instrumento()
    {
        return $this->belongsTo(Instrumento::class);
    }
}
