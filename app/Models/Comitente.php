<?php

namespace App\Models;

use App\Models\Prospectos\PersonasFisicas\Titular;
use App\Persona;
use App\Mail\CreacionDeUsuario;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class Comitente extends Model
{
    protected $table = 'comitentes';

    protected $fillable = [
        'descripcion', 'numero_comitente', 'usuario_id','sucursal','digito_verificador'
    ];

    public function Personas()
    {
        return $this->belongsToMany(Titular::class, 'comitentes_personas', 'comitente_id', 'persona_id');
    }

    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class, 'comitente_id');
    }

    public function getTitulares()
    {
        return $this->Personas()->get();
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Obtenemos el comitente buscando por el numero
     *
     * @param $nro_comitente
     * @return Model|null|static
     */
    public static function getComitentePorNumero($nro_comitente)
    {
        return self::where('numero_comitente', '=', $nro_comitente)->first();
    }

    public static function getComitentes()
    {
        return self::where('deleted_at', null)->get();
    }

    /**
     * Creamos la relacion entre el comitente y el usuario
     *
     * @param $user_id
     * @param $comitente
     */
    public static function vincular($user_id, $comitente)
    {   
        ComitenteUsuario::create([
            'comitente_id' => $comitente->id,
            'user_id' => $user_id
        ]);
    }

    /**
     * @param $email
     * @param $comitente
     */
    public static function crearUserYVincular($email, $comitente, $persona)
    {
        // Creo el usuario

        $password=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
        Log::info("va a crear el usuario");
        $user =
            User::create([
                'nombre' => $persona->nombre,
                'apellido' => $persona->apellido,
                'tipo_documento_id' => $persona->tipo_documento_id,
                'numero_documento' => $persona->numero_documento,
                'email' => $email,
                'password' => bcrypt($password)
            ]);
        Log::info("creo el usuario");
        // Vinculo el comitente con el usuario
        Log::info("va a vincularlo");
        self::vincular($user->id, $comitente);
        Log::info("Lo vinculo");

        // Actualizo el usuario con la persona
        Log::info("Actualizo al tabla de la persona");
        $persona->user_id = $user->id;
        $persona->save();
        
        Log::info("va a enviar el mail");
        Mail::to($email)->send(new CreacionDeUsuario($persona, $user, $password));
        Log::info("envio el mail");
    }

    public function getCuentasBancarias()
    {
        $cuentas_bancarias = $this->CuentasBancarias()->where("deleted_at",null)->get();

        // Inicializo un array con clave (moneda) => valor (cuenta)
        $cuentas_return = [];

        foreach ($cuentas_bancarias as $cuenta) {
            if ($cuenta->esPesos()) {
                $cuentas_return['pesos'] = $cuenta;
            } else {
                $cuentas_return['dolares'] = $cuenta;
            }
        }

        $cuentas_bancarias->load("Moneda");

        return $cuentas_return;
    }
}
