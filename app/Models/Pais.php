<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = "paises";


    public function Titulares()
    {
        return $this->hasMany(Titular::class,"pais_nacimiento");
    }

}
