<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class TipoOperacion extends Model
{
    protected $table = "tipos_operaciones";

    protected $fillable = [
        'nombre',
        'nombre_display',
        'tipo_instrumento_id'
    ];

    /**
     * Obtenemos el tipo de operacion por nombre
     *
     * @param $tipo_operacion
     * @return mixed
     */
    public static function getTipoOperacionPorNombre($tipo_operacion)
    {
        $tipo_operacion =
            self::where('nombre', $tipo_operacion)->first();

        return $tipo_operacion;
    }

    /**** RELACIONES ****/
    public function Instrumentos()
    {
        return $this->belongsToMany(TipoInstrumento::class,
            'instrumentos_operaciones',
            'tipo_operacion_id',
            'tipo_instrumento_id');
    }

    public function PlazosLiquidacion()
    {
        return $this->belongsToMany(PlazoLiquidacion::class, 'tipo_op_plazo_liq');
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getNombreDisplay()
    {
        return $this->nombre_display;
    }

    public function esSuscripcion()
    {
        return $this->getNombre() == "Suscripcion";
    }


    public function getSelectEspeciesPorTipo($tipo_instrumento)
    {
        // Buscamos la clase necesaria para obtener las especies. Ej: App\Models\Operaciones\Fondos\Suscripcion
        $clase = "App\Models\Operaciones\\" . $tipo_instrumento->getDescripcion() . '\\' . $this->getNombre();

        // Instanciamos la clase
        $instrumento = new $clase($tipo_instrumento);

        // Devolvemos las especies disponibles de la clase
        return $instrumento->getSelectInstrumentosDisponibles();
    }

    /**
     * Obtenemos el subtipo de operacion para poder enviar al WS y que se pueda procesar
     *
     * @param $nombre
     * @return string
     */
    public static function getSubtipoOperacionPorNombre($nombre)
    {
        switch ($nombre) {
            case "cuotaparte":
                $subtipo = 'RCU';

                break;

            case "monto":
                $subtipo = 'RMO';

                break;

            case "total":
                $subtipo = 'RTO';

                break;

            default:
                $subtipo = "SUS";
                break;
        }

        return $subtipo;
    }
}
