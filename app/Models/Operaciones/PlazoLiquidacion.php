<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class PlazoLiquidacion extends Model
{
    protected $table = "plazos_liquidaciones";

    protected $fillable = [
        'nombre',
        'dias',
        'abreviatura',
    ];

    public function Instrumentos()
    {
        return $this->belongsToMany(
            PlazoLiquidacion::class,
            'instrumentos_plazo_liq',
            'plazo_liquidacion_id',
            'instrumento_id');
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}
