<?php

namespace App\Models\Operaciones;

use App\Models\Comitente;
use App\Models\Estado;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Models\Tesoreria\Depositos\Deposito;
use App\Models\Tesoreria\Transferencias\Transferencia;
use App\Models\Operaciones\TipoOperacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Operacion extends Model
{
    protected $table = "instrumentos_operaciones";

    protected $fillable = [
        'instrumento_id',
        'moneda_id',
        'comitente_id',
        'tipo_operacion_id',
        'fecha_concertacion',
        'fecha_aprobacion',
        'fecha_envio_mercado',
        'fecha_liquidacion',
        'deposito_id',
        'transferencia_id',
        'cantidad',
        'importe',
        'estado_id',
        'mercado_registro_id',
        'mercado_mensaje',
        'subtipo_operacion'
    ];

    public function Instrumento()
    {
        return $this->belongsTo(Instrumento::class);
    }

    public function Moneda()
    {
        return $this->belongsTo(MonedaBase::class);
    }

    public function Comitente()
    {
        return $this->belongsTo(Comitente::class);
    }

    public function Deposito()
    {
        return $this->belongsTo(Deposito::class);
    }

    public function Transferencia()
    {
        return $this->belongsTo(Transferencia::class);
    }

    public function Estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function TipoOperacion()
    {
        return $this->belongsTo(TipoOperacion::class);
    }

    public function necesitaComprobante()
    {
        return $this->esSuscripcion();
    }

    public function getNombreTipoOperacion()
    {
        return $this->TipoOperacion->getNombreDisplay();
    }

    public function getNombreMoneda()
    {
        return $this->Moneda->simbolo;
    }

    public function esSuscripcion()
    {
        return $this->TipoOperacion->esSuscripcion();
    }

    public function esPorMonto()
    {
        return $this->subtipo_operacion == "SUS" || $this->subtipo_operacion == "RMO";
    }

    public function getNombreInstrumento()
    {
        return $this->Instrumento->getNombreWeb();
    }

    public function getAbreviaturaInstrumento()
    {
        return $this->Instrumento->getAbreviatura();
    }

    public function getComitente()
    {
        return $this->Comitente;
    }

    public function getComitenteId()
    {
        return $this->getComitente()->id;
    }

    public function getNombreComitente()
    {
        return $this->getComitente()->getDescripcion();
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getImporte()
    {
        return $this->importe;
    }

    public function estaAprobada()
    {
        return $this->fecha_aprobacion != null;
    }

    public function tieneComprobante()
    {
        return $this->Deposito->tieneComprobante();
    }

    /**
     * Desaprobamos una operacion
     */
    public function desaprobar()
    {
        // Buscamos el estado desaprobado
        $estado_desaprobado_id = Estado::getIdEstado('Desaprobado');

        $this->estado_id = $estado_desaprobado_id;
        $this->save();
    }

    public function aprobar()
    {
        // Buscamos el estado desaprobado
        $estado_desaprobado_id = Estado::getIdEstado('Aprobado');

        $this->estado_id = $estado_desaprobado_id;
        $this->save();
    }


    /**
     * Verificamos si una operacion tiene el comprobante adjunto para poder realizarse.
     * Por ej: un rescate no necesita comprobante, pero un deposito si
     *
     * @param $operacion
     * @return bool
     */
    /*public static function tieneComprobante($operacion)
    {
        $tiene_comprobante = $operacion->Deposito->tieneComprobante();

        return $tiene_comprobante;
    }*/

    /**
     * Obtenemos las operaciones pendientes del tipo instrumento pedido.
     * Por defecto cargamos las operaciones pendientes de fondos.
     * Desde el front pide el comitente id para filtrar por el mismo.
     *
     * @param null $tipo_instrumento_id
     * @param mixed $comitente_id
     * @return mixed
     */
    public static function getOperacionesPendientesDeTipoInstrumento($tipo_instrumento_id = null, $comitente_id = null, $instrumento_id = null)
    {
        // TODO: Por defecto cargamos las operaciones del instrumento FONDOS
        if (!$tipo_instrumento_id) {
            $tipo_instrumento_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        }

        $operaciones =
            self::with(['Instrumento', 'Moneda'])
                ->whereHas('Instrumento', function ($query) use ($tipo_instrumento_id) {
                    $query->where('tipo_instrumento_id', $tipo_instrumento_id);
                });

        if ($comitente_id) {
            $operaciones = $operaciones->where('comitente_id', $comitente_id);
        }

        if ($instrumento_id) {
            $operaciones = $operaciones->where('instrumento_id', $instrumento_id);
        }

        // Estado pendiente
        $estado_pendiente_id = Estado::getIdEstado('Pendiente');

        $operaciones = $operaciones->where('estado_id', $estado_pendiente_id);

        $operaciones = $operaciones->get();

        return $operaciones;
    }

   public static function getOperacionesAprobadasDeTipoInstrumento($tipo_instrumento_id = null, $comitente_id = null, $fecha = null)
    {
        // TODO: Por defecto cargamos las operaciones del instrumento FONDOS
        if (!$tipo_instrumento_id) {
            $tipo_instrumento_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        }

        $operaciones =
            self::with(['Instrumento', 'Moneda'])
                ->whereHas('Instrumento', function ($query) use ($tipo_instrumento_id) {
                    $query->where('tipo_instrumento_id', $tipo_instrumento_id);
                });

        if ($comitente_id) {
            $operaciones = $operaciones->where('comitente_id', $comitente_id);
        }

        if ($fecha) {
            $operaciones = $operaciones->whereDate('created_at', $fecha);
        }

        // Estado pendiente
        $estado_aprobado_id = Estado::getIdEstado('Aprobado');

        $operaciones = $operaciones->where('estado_id', $estado_aprobado_id);

        $operaciones = $operaciones->get();

        return $operaciones;
    }

   public static function getOperacionesHistoricasPorComitente($comitente_id = null)
    {
        // TODO: Por defecto cargamos las operaciones del instrumento FONDOS
        $tipo_instrumento_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        
        $operaciones =
            self::with(['Instrumento', 'Moneda'])
                ->whereHas('Instrumento', function ($query) use ($tipo_instrumento_id) {
                    $query->where('tipo_instrumento_id', $tipo_instrumento_id);
                });

        if ($comitente_id) {
            $operaciones = $operaciones->where('comitente_id', $comitente_id);
        }

        $operaciones = $operaciones->get();

        $operaciones->load("TipoOperacion");
        $operaciones->load("Estado");

        return $operaciones;
    }

    /**
     * Traemos todas las operaciones que necesitan que se les adjunte un comprobante.
     * Ej: suscripciones, compras
     *
     * @param $operaciones_pendientes
     * @return mixed
     */
    public static function getPendientesComprobantePendiente($operaciones_pendientes)
    {
        $operaciones_pendientes =
            $operaciones_pendientes->filter(function ($operacion) {
                if ($operacion->esSuscripcion()) {
                    return !$operacion->tieneComprobante();
                }
            });

        return $operaciones_pendientes;
    }

    /**
     * Obtenemos las operaciones pendientes de confirmación del back. Son las que necesitan
     * un comprobante (suscripciones) y se les cargó, o las que no necesitan un
     * comprobamte, como los rescate.
     *
     * @param $operaciones_pendientes
     * @param $true
     * @return mixed
     */
    public static function getPendientesConComprobante($operaciones_pendientes)
    {
        $operaciones_pendientes =
            $operaciones_pendientes->filter(function ($operacion) {
                return
                    (($operacion->esSuscripcion() && $operacion->tieneComprobante($operacion) && !$operacion->estaAprobada())
                    ||
                    (!$operacion->esSuscripcion() && !$operacion->estaAprobada()));
            });

        return $operaciones_pendientes;
    }
}
