<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class TipoInstrumento extends Model
{
    protected $table = "tipos_instrumentos";

    protected $fillable = [
        'descripcion'
    ];

    /**
     * Obtenemos un tipo de instrumento por descripcion
     *
     * @param $descripcion
     * @return mixed
     */
    public static function getTipoInstrumentoPorDescripcion($descripcion)
    {
        $tipo_instrumento =
            self::where('descripcion', $descripcion)->first();

        return $tipo_instrumento;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function esFondo()
    {
        return $this->getDescripcion() == "Fondos";
    }

    /**** RELACIONES ****/
    public function TiposOperaciones()
    {
        return $this->belongsToMany(TipoOperacion::class,
            'tipos_instrumentos_tipos_operaciones',
            'tipo_instrumento_id',
            'tipo_operacion_id');
    }

    /**
     * Obtengo los tipos de operaciones validas (compra/venta, suscripcion/rescate)
     * para cada especie (fondos, acciones, etc)
     *
     * @param $instrumento_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOperacionesPorInstrumento()
    {
        // Obtengo los tipos de operaciones validas para esa especie
        $tipos_operaciones = $this->TiposOperaciones()->get();

        return $tipos_operaciones;
    }

    public static function getTipos()
    {
        $tipos = self::all();

        return $tipos;
    }
}
