<?php

namespace App\Models\Tesoreria\Depositos;

use App\Models\BaseModel;
use App\Models\Comitente;
use App\Models\Estado;
use App\Models\Monedas\MonedaBase;
use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Database\Eloquent\Model;

class Deposito extends BaseModel
{
    protected $table = "depositos";

    protected $fillable = [
        'moneda_id',
        'comitente_id',
        'cuenta_bancaria_id',
        'fecha_concertacion',
        'fecha_aprobacion',
        'fecha_liquidacion',
        'importe',
        'comprobante',
        'estado_id'
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function Moneda()
    {
        return $this->hasOne(MonedaBase::class);
    }

    public function Comitente()
    {
        return $this->belongsTo(Comitente::class);
    }

    public function CuentaBancaria()
    {
        return $this->belongsTo(CuentaBancaria::class);
    }

    public function Estado()
    {
        return $this->hasOne(Estado::class);
    }

    public function tieneComprobante()
    {
        return $this->comprobante != null;
    }

    /**
     * Agregamos un comprobante para el depósito
     *
     * @param $comprobante
     */
    public function agregarComprobante($comprobante)
    {
        $this->comprobante = $comprobante;
        $this->save();
    }
}
