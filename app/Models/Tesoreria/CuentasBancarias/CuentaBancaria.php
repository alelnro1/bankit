<?php

namespace App\Models\Tesoreria\CuentasBancarias;

use App\Classes\Factories\MonedasFactory;
use App\Http\Requests\AltaCuentaBancariaRequest;
use App\Models\Banco;
use App\Models\BaseModel;
use App\Models\Comitente;
use App\Models\Estado;
use App\Models\Monedas\MonedaBase;
use App\Scopes\ComitenteScope;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class CuentaBancaria extends BaseModel
{
    protected $table = 'cuentas_bancarias';

    protected $fillable = [
        'banco_id', 'moneda_id', 'estado_id', 'comitente_id', 'numero_cuenta', 'cbu', 'alias', 'comprobante',
        'es_default', 'cuit', 'cuenta_bancaria_tipo_id', 'user_id', 'backend_user_id', 'prospecto_id','deleted_at'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ComitenteScope());
    }

    /*********** RELACIONES ***************/

    public function Tipo()
    {
        return $this->belongsTo(TipoCuentaBancaria::class, 'cuenta_bancaria_tipo_id');
    }

    public function Banco()
    {
        return $this->belongsTo(Banco::class);
    }

    public function Estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function Moneda()
    {
        return $this->belongsTo(MonedaBase::class);
    }

    public function Comitente()
    {
        return $this->belongsTo(Comitente::class);
    }

    public function Usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Prospecto()
    {
        return $this->belongsTo(CuentaBancaria::class, 'prospecto_id');
    }


    /************ GETTERS ************************/

    public static function getCuentasDeMoneda($moneda_nombre)
    {

        
        $cuentas =
            self::whereHas('Moneda', function ($query) use ($moneda_nombre) {
                $query->where('descripcion', $moneda_nombre);
            })
                ->with(['Banco', 'Estado', 'Comitente', 'Usuario'])->where('deleted_at',null)->get();

        return $cuentas;
    }

    public static function getCuentasConEstado($nombre_estado, $moneda_nombre = null)
    {
        // Busco al ID del estado que me pidieron
        $estado_id = Estado::getIdEstado($nombre_estado);

        if ($moneda_nombre) {
            $cuentas = self::whereHas('Moneda', function ($query) use ($moneda_nombre) {
                $query->where('descripcion', $moneda_nombre);
            });
        }

        return $cuentas
            ->where('comitente_id', '<>', null)
            ->where('estado_id', $estado_id)
            ->where('deleted_at', null)
            ->with(['Banco', 'Comitente', 'Usuario'])->get();
    }

    public function getCbu()
    {
        return $this->cbu;
    }

    public function getCuit()
    {
        return $this->cuit;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function getComitente()
    {
        return $this->Comitente->getDescripcion();
    }

    public function getUsuario()
    {
        return $this->Usuario->getNombre();
    }

    public function getNumeroCuenta()
    {
        return $this->numero_cuenta;
    }

    public function getBanco()
    {
        return $this->Banco->getDescripcion();
    }

    public function getTipoCuenta()
    {
        return $this->Tipo->getNombre();
    }

    public function getMoneda()
    {
        return $this->Moneda->getDescripcion();
    }

    public function getTipoCuentaId()
    {
        return $this->Tipo->id;
    }

    public function getBancoId()
    {
        return $this->Banco->id;
    }

    /*************** METODOS *********************/

    public function aceptar()
    {
        $estado_aprobado_id = Estado::getIdEstado('Aprobado');

        $this->estado_id = $estado_aprobado_id;
        

        $this->save();
    }

    public function rechazar()
    {
        $estado_rechazado_id = Estado::getIdEstado('Desaprobado');

        $this->estado_id = $estado_rechazado_id;
        $this->backend_user_id = Auth::user()->id;

        $this->save();
    }

    public function esPesos()
    {
        return $this->Moneda->esPesos();
    }
}
