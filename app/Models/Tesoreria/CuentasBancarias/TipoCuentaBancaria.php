<?php

namespace App\Models\Tesoreria\CuentasBancarias;

use Illuminate\Database\Eloquent\Model;

class TipoCuentaBancaria extends Model
{
    protected $table = 'cuentas_bancarias_tipo';

    public function CuentasBancarias()
    {
        return $this->hasMany(CuentaBancaria::class);
    }

    public function getNombre()
    {
        return $this->descripcion;
    }

    public function getAbreviatura()
    {
        return $this->abreviatura;
    }
}
