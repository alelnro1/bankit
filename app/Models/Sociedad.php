<?php

namespace App\Models;

use App\Models\Prospectos\PersonasJuridicas\DireccionSociedad;
use App\Models\Prospectos\Prospecto;
use Illuminate\Database\Eloquent\Model;

class Sociedad extends Model
{
    protected $table = "sociedades";

    protected $fillable = [
        'razon_social',
        'fecha_constitucion',
        'cuit',
        'cod_area_celular',
        'prefijo_celular',
        'telefono_celular',
        'email',
        'prospecto_id',
        'comitente_id',
    ];

    public function Prospecto()
    {
        return $this->belongsTo(Prospecto::class, 'prospecto_id');
    }

    public function Comitente()
    {
        return $this->belongsTo(Comitente::class, 'comitente_id');
    }

    public function Direccion()
    {
        return $this->hasOne(DireccionSociedad::class);
    }
}
