<?php

namespace App\Models\Monedas;

use App\Models\Instrumentos\Instrumento;
use Illuminate\Database\Eloquent\Model;

class MonedaBase extends Model
{
    protected $table = "monedas";

    protected $nombre;

    public function Instrumentos()
    {
        return $this->belongsToMany(Instrumento::class, 'instrumentos_monedas', 'instrumento_id', 'moneda_id');
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public static function getIdByNombre($nombre)
    {
        $moneda = self::where('descripcion', ucfirst($nombre))->select(['id'])->first()->id;

        return $moneda;
    }

    public static function getIdBySimbolo($simbolo)
    {
        $moneda_id = self::where('simbolo', $simbolo)->select(['id'])->first()->id;

        return $moneda_id;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function esPesos()
    {
        return strtolower($this->getDescripcion()) == "pesos";
    }
}
