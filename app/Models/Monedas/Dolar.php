<?php

namespace App\Models\Monedas;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Dolar extends MonedaBase
{
    public function __construct(array $attributes = [])
    {
        $this->setNombre('Dolar');

        parent::__construct();
    }

    /**
     * Global Scope
     *
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('nombre', '=', 'Dolar');
    }
}
