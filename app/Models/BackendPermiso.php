<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackendPermiso extends Model
{
    protected $table = "backend_permisos";

    protected $fillable = [
        'nombre'
    ];

    public function Permisos()
    {
        return $this->belongsToMany(Permiso::class);
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}
