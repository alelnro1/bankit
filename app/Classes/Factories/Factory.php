<?php

namespace App\Classes\Factories;

use Illuminate\Support\Facades\Log;

class Factory
{
    protected $nombres_validos;

    protected $namespace;

    /**
     * @return mixed
     */
    public function getNombresValidos()
    {
        return $this->nombres_validos;
    }

    /**
     * @param mixed $nombres_validos
     */
    public function setNombresValidos($nombres_validos)
    {
        $this->nombres_validos = $nombres_validos;
    }

    /**
     * @return mixed
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param mixed $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }


    public function create($nombre_requerido)
    {
        if (!in_array($nombre_requerido, $this->nombres_validos)) {
            die(422);
        }

        $clase = $this->armarNombreDeClaseRequerida($nombre_requerido);

        if (class_exists( $clase)) {
            return new $clase();
        }

        Log::critical('MonedasFactory. La moneda ' . $clase . ' no existe');

        return false;
    }

    /**
     * Armamos la ruta completa con la clase de estado
     * @param $estado
     * @return string
     */
    private function armarNombreDeClaseRequerida($estado)
    {
        $clase_requerida = ucfirst(trim(str_replace(' ', '', $estado)));

        $clase = $this->namespace . $clase_requerida;

        return $clase;
    }
}