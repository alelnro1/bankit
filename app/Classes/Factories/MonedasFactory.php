<?php

namespace App\Classes\Factories;

use Illuminate\Support\Facades\Log;

class MonedasFactory extends Factory
{
    protected $nombres_validos = [
        'Peso', 'dolar'
    ];

    protected $namespace = 'App\Models\Monedas\\';

    public function create($nombre_moneda)
    {
        $this->setNombresValidos($this->nombres_validos);

        $this->setNamespace($this->namespace);

        return parent::create($nombre_moneda);
    }
}