<?php

namespace App\Classes\Factories;

use Illuminate\Support\Facades\Log;

class OperacionesFactory
{
    protected $nombres_validos_tipos_instrumentos = [
        'Fondos'
    ];

    protected $nombres_validos_tipos_operaciones = [
        'Rescate', 'Suscripcion'
    ];

    protected $namespace = 'App\Classes\Operaciones\\';

    public function create($tipo_instrumento, $tipo_operacion)
    {

        if (
            !in_array($tipo_instrumento->getDescripcion(), $this->nombres_validos_tipos_instrumentos)
            || !in_array($tipo_operacion->getNombre(), $this->nombres_validos_tipos_operaciones)
        ) {
            die(422);
        }

        $clase = $this->armarNombreDeClaseRequerida($tipo_instrumento->getDescripcion(), $tipo_operacion->getNombre());

        if (class_exists( $clase)) {
            return new $clase($tipo_instrumento);
        }

        Log::critical('OperacionesFactory. El tipo instrumento ' . $tipo_instrumento . ' con operacion ' . $tipo_operacion . ' no existe');

        return false;
    }

    private function armarNombreDeClaseRequerida($tipo_instrumento, $tipo_operacion)
    {
        $nombre = $this->namespace . $tipo_instrumento . '\\' . $tipo_operacion;

        return $nombre;
    }
}