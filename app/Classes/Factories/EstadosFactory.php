<?php

namespace App\Classes\Factories;

use Illuminate\Support\Facades\Log;

class EstadosFactory extends Factory
{
    protected $nombres_validos = [
        'Pendiente', 'Aprobado', 'Rechazado'
    ];

    protected $namespace = 'App\Models\Estados\\';

    public function create($nombre_estado)
    {
        $this->setNombresValidos($this->nombres_validos);

        $this->setNamespace($this->namespace);

        return parent::create($nombre_estado);
    }
}