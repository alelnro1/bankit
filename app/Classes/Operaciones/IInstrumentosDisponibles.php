<?php

namespace App\Classes\Operaciones;

interface IInstrumentosDisponibles {
    public function getInstrumentosDisponibles();
}