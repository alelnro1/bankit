<?php

namespace App\Classes\Operaciones\Fondos;

use App\Classes\Factories\OperacionesFactory;
use App\Http\Controllers\Controller;
use App\Models\Feriado;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\PlazoLiquidacion;
use Illuminate\Support\Carbon;

class Fondos extends Controller
{
    protected $tipo_instrumento;
    protected $tipo_operacion;
    protected $moneda_id;

    /**
     * @return mixed
     */
    public function getTipoInstrumento()
    {
        return $this->tipo_instrumento;
    }

    /**
     * @param mixed $tipo_instrumento
     */
    public function setTipoInstrumento($tipo_instrumento)
    {
        $this->tipo_instrumento = $tipo_instrumento;
    }

    /**
     * @return mixed
     */
    public function getTipoOperacion()
    {
        return $this->tipo_operacion;
    }

    /**
     * @param mixed $tipo_operacion
     */
    public function setTipoOperacion($tipo_operacion)
    {
        $this->tipo_operacion = $tipo_operacion;
    }

    /**
     * @param mixed $moneda_id
     */
    public function setMonedaId($moneda_id)
    {
        $this->moneda_id = $moneda_id;
    }

    public function puedeOperar($request)
    {
        // Busco el fondo que estamos operando
        $fondo = Instrumento::findOrFail($request->instrumento);

        // Buscamos la moneda
        $moneda_id = MonedaBase::getIdByNombre($request->moneda);

        // Calculamos la fecha de liquidacion con el siguiente día hábil
        $fecha_concertacion = $this->calcularFechaConcertacion($request);
        $fecha_liquidacion = $this->calcularFechaLiquidacion($request,$fecha_concertacion);

        $operaciones_factory = new OperacionesFactory();
        $operacion = $operaciones_factory->create($this->getTipoInstrumento(), $this->getTipoOperacion());

        // Agrego el objeto fondo y la momneda id al request
        $request->request->add([
            'Fondo' => $fondo,
            'moneda_id' => $moneda_id,
            'fecha_concertacion' => $fecha_concertacion->toDateTimeString(),
            'fecha_liquidacion' => $fecha_liquidacion->toDateTimeString()
        ]);

        return $operacion->puedeOperar($request);
    }

    /**
     * Operamos el fondo, sea rescate o suscripcion
     *
     * @param $request
     * @return mixed
     */
    public function operar($request)
    {
        // Busco el fondo que estamos operando
        $fondo = Instrumento::findOrFail($request->instrumento);

        // Buscamos la moneda
        $moneda_id = MonedaBase::getIdByNombre($request->moneda);

        // Calculamos la fecha de liquidacion con el siguiente día hábil
        $fecha_concertacion = $this->calcularFechaConcertacion($request);      
        $fecha_liquidacion = $this->calcularFechaLiquidacion($request,$fecha_concertacion);

        // Agrego el objeto fondo y la momneda id al request
        $request->request->add([
            'Fondo' => $fondo,
            'moneda_id' => $moneda_id,
            'fecha_concertacion' => $fecha_concertacion->toDateTimeString(),
            'fecha_liquidacion' => $fecha_liquidacion->toDateTimeString()
        ]);

        $operaciones_factory = new OperacionesFactory();
        $operacion = $operaciones_factory->create($this->getTipoInstrumento(), $this->getTipoOperacion());
        
        $operacion_res = $operacion->operar($request);

        return $operacion_res;
    }

    /**
     * Calculamos la fecha de liquidacion para la operacion teniendo en cuenta la hora y plazo de liquidación
     * Ej: Si opera después de las 15hs, la fecha de liquidación va a ser el siguiente día hábil
     *
     * @param $fondo
     * @param $request
     */

  private function calcularFechaConcertacion($request)
    {
        
        //Calculo la fecha de Hoy, si es mas del horario de suscripcion, pasa para mañana
        $date = date("m/d/Y", time());

        $MyDateCarbon = Carbon::parse($date);
        
        $hora_minuto_actual = (int) date("Hi");
        if ($hora_minuto_actual > env("HORA_TOPE_OPERACION")) {
            $MyDateCarbon->addWeekdays(1);
        }

        
        $holidays = Feriado::all()->toArray();
        

        
        

        // Recorro la tabla hasta encontrar que no es sabado ni domingo ni feriado
        for ($i = 1; $i <= 10; $i++) {
            $feriado = Feriado::where("fecha",Carbon::parse($MyDateCarbon))->count();
            if ($feriado) {

                $MyDateCarbon->addDay();

            } elseif (date("N",strtotime($MyDateCarbon))==6 || date("N",strtotime($MyDateCarbon))==7) {

                $MyDateCarbon->addDay();

            } else {
                break;
            }
        }

        return $MyDateCarbon;
    }

    private function calcularFechaLiquidacion($request,$fecha_concertacion)
    {
        
        $plazo_liquidacion = PlazoLiquidacion::find($request->plazo_liquidacion)->dias;
        
        if($this->getTipoInstrumento()->descripcion=="Fondos" && $this->getTipoOperacion()->nombre=="Suscripcion"){
            $plazo_liquidacion=0;
        }

        

        $holidays = Feriado::all()->toArray();

        $date = $fecha_concertacion;
        $MyDateCarbon = Carbon::parse($date);
        
        

        $l=0;

        while($l<$plazo_liquidacion) {
            
            //le agrego 1 dia por cada plazo de liquidacion
            $MyDateCarbon->addWeekdays(1);  
            
            //si cae en feriado le agrego un dia mas
            for ($i = 1; $i <= 10; $i++) {
                $feriado = Feriado::where("fecha",Carbon::parse($MyDateCarbon))->count();
                if ($feriado) {

                    $MyDateCarbon->addDay();

                } elseif (date("N",strtotime($MyDateCarbon))==6 || date("N",strtotime($MyDateCarbon))==7) {

                    $MyDateCarbon->addDay();

                } else {
                    break;
                }
            }

            $l++;
        }



        return $MyDateCarbon;
    }
}