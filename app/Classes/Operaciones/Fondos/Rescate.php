<?php

namespace App\Classes\Operaciones\Fondos;

use App\Classes\Operaciones\IInstrumentosDisponibles;
use App\Models\Estado;
use App\Models\CuentasCorrientes\CuentaCorrienteInstrumento;
use App\Models\CuentasCorrientes\CuentaCorrienteMoneda;
use App\Models\Comitente;
use App\Models\Instrumentos\Instrumento;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoOperacion;
use App\Models\Tesoreria\Transferencias\Transferencia;
use App\Traits\Fondos\PosicionFondo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Rescate implements IInstrumentosDisponibles
{
    protected $tipo_instrumento;

    private $errores;

    public function __construct($tipo_instrumento)
    {
        $this->tipo_instrumento = $tipo_instrumento;
    }

    /**
     * Obtenemos todos los fondos disponibles para una suscripcion
     *
     * @return mixed
     */
    public function getInstrumentosDisponibles()
    {
        $instrumentos = Instrumento::getInstrumentosDeTipo($this->tipo_instrumento->id);

        // TODO: ACA FILTRAMOS LOS FONDOS QUE PUEDE RESCATAR
        //$instrumentos->filter()...

        return $instrumentos;
    }

    /**
     * Devolvemos un select con la lista de fondos disponibles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSelectInstrumentosDisponibles()
    {
        $fondos_disponibles = $this->getInstrumentosDisponibles();

        return
            view('frontend.form-operar.datos-operacion.fondos.select', [
                'fondos' => $fondos_disponibles
            ])->render();
    }

    public function getCamposPorInstrumento()
    {
        $cuentas_bancarias = Auth::user()->CuentasBancarias()->get();

        return view('frontend.form-operar.datos-operacion.fondos.campos-especiales.rescate', [
            'cuentas' => $cuentas_bancarias
        ])->render();
    }

    /**
     * Vamos a operar el rescate de un fondo
     *
     * @param $request
     */
    public function operar($request)
    {
        try {
            // TODO: Hay que validar que la cuenta bancaria pertenezca al comitente
            $estado_pendiente_id = Estado::getIdEstado('Pendiente');

            // 1) Generamos la transferencia
            $transferencia = $this->generarTransferencia($request, $estado_pendiente_id);

            // Busco el tipo de operacion id de suscripcion
            $tipo_operacion_id = TipoOperacion::getTipoOperacionPorNombre('Rescate')->id;

            // Obtenemos el subtipo de operacion para poder enviar al WS
            $subtipo_operacion = TipoOperacion::getSubtipoOperacionPorNombre($request->tipo_calculo);

            // 2) Insertamos la operacion
            $operacion =
                Operacion::create([
                    'instrumento_id' => $request->instrumento,
                    'moneda_id' => $request->moneda_id,
                    'comitente_id' => Auth::user()->getIdComitente(),
                    'fecha_concertacion' => $request->fecha_concertacion,
                    'fecha_aprobacion' => null,
                    'fecha_envio_mercado' => null,
                    'fecha_liquidacion' => $request->fecha_liquidacion,
                    'deposito_id' => null,
                    'transferencia_id' => $transferencia->id,
                    'cantidad' => $request->cant_instrumentos,
                    'importe' => $request->valor_estimado,
                    'estado_id' => $estado_pendiente_id,
                    'mercado_registro_id' => null,
                    'mercado_mensaje' => null,
                    'tipo_operacion_id' => $tipo_operacion_id,
                    'subtipo_operacion' => $subtipo_operacion
                ]);

            return $operacion;
        } catch (\Exception $e) {
            Log::alert("ERROR: " . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Generamos un deposito con los valores recibidos del formulario
     *
     * @param $request
     * @return mixed
     */
    private function generarTransferencia($request, $estado_pendiente_id)
    {
        $transferencia =
            Transferencia::create([
                'moneda_id' => $request->moneda_id,
                'comitente_id' => Auth::user()->Comitentes()->first()->id,
                'cuenta_bancaria_id' => $request->cuenta_bancaria,
                'fecha_concertacion' => $request->fecha_concertacion,
                'fecha_aprobacion' => null,
                'fecha_liquidacion' => null,
                'importe' => $request->valor_estimado,
                'comprobante' => null,
                'estado_id' => $estado_pendiente_id
            ]);

        return $transferencia;
    }

    /**
     * Se valida que la operacion pueda ser realizada.
     * Se debe tener la cantidad de cuotapartes disponibles para rescatar
     *
     * @return bool
     */
    public function puedeOperar($request)
    {
        $tiene_instrumentos_disponibles = $this->tieneInstrumentosDisponibles($request);

        return $tiene_instrumentos_disponibles;
    }

    /**
     * Verificamos que el usuario tenga disponible la cantidad de instrumentos
     * que desea rescatar.
     *
     * @param $especie
     * @return bool
     */
    private function tieneInstrumentosDisponibles($request)
    {
        // La cantidad de instrumentos que se desean rescatar
        $cuotapartes_a_rescatar = $request->cant_instrumentos;

        // La cantidad de cuotapartes que tiene disponible el comitente
        $cuotapartes_disponibles = $this->getCuotapartesDisponibles($request->Fondo);
        
        return $cuotapartes_disponibles >= $cuotapartes_a_rescatar;
    }

    /**
     * Obtengo la posicion de un fondo por comitente
     *
     * @param $fondo
     * @return null
     */
    public static function getCuotapartesDisponibles($fondo, $nro_comitente = null, $fecha = null)
    {
        // Si no recibimos comitente => usamos el logueado
        if (!$nro_comitente)
            $nro_comitente = Auth::user()->getNumeroComitente();

        // Si no recibimos fecha => usamos hoy
        if (!$fecha)
            $fecha = date("Ymd");

        
        $comitente=Comitente::where("numero_comitente",$nro_comitente)->first();
        
        
        $cuotapartes_cuenta_corriente = CuentaCorrienteInstrumento::where("comitente_id",$comitente->id)
                                                                    ->where("instrumento_id",$fondo->id)
                                                                    ->where("deleted_at",null)
                                                                    ->sum('cantidad');

        
        $cuotapartes_operaciones = Operacion::where("comitente_id",$comitente->id)
                                                    ->where("instrumento_id",$fondo->id)
                                                    ->where("estado_id","1")
                                                    ->where("deleted_at",null)
                                                    ->where("subtipo_operacion","<>","SUS")
                                                    ->sum('cantidad');

        return $cuotapartes_cuenta_corriente - $cuotapartes_operaciones;
    }
}