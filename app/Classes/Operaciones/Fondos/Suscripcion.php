<?php

namespace App\Classes\Operaciones\Fondos;

use App\Classes\Operaciones\IInstrumentosDisponibles;
use App\Models\Estado;
use App\Models\Instrumentos\Instrumento;
use App\Models\Monedas\MonedaBase;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoOperacion;
use App\Models\Tesoreria\Depositos\Deposito;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Suscripcion implements IInstrumentosDisponibles
{
    protected $tipo_instrumento;

    public function __construct($tipo_instrumento)
    {
        $this->tipo_instrumento = $tipo_instrumento;
    }

    /**
     * Obtenemos todos los fondos disponibles para una suscripcion
     *
     * @return mixed
     */
    public function getInstrumentosDisponibles()
    {
        $instrumentos = Instrumento::getInstrumentosDeTipo($this->tipo_instrumento->id);

        return $instrumentos;
    }

    /**
     * Devolvemos un select con la lista de fondos disponibles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSelectInstrumentosDisponibles()
    {
        $fondos_disponibles = $this->getInstrumentosDisponibles();

        return
            view('frontend.form-operar.datos-operacion.fondos.select', [
                'fondos' => $fondos_disponibles
            ])->render();
    }

    public function getCamposPorInstrumento()
    {
        return view('frontend.form-operar.datos-operacion.fondos.campos-especiales.suscripcion')->render();
    }

    /**
     * Vamos a operar la suscripcion de un fondo
     *
     * @param $request
     */
    public function operar($request)
    {
        try {
            // TODO: Hay que validar que la cuenta bancaria pertenezca al comitente

            // 1) Generamos el depósito
            $deposito = $this->generarDeposito($request);

            $estado_pendiente_id = Estado::getIdEstado('Pendiente');

            // Busco el tipo de operacion id de suscripcion
            $tipo_operacion_id = TipoOperacion::getTipoOperacionPorNombre('Suscripcion')->id;

            // 2) Insertamos la operacion
            
            $operacion =
                Operacion::create([
                    'instrumento_id' => $request->instrumento,
                    'moneda_id' => $request->moneda_id,
                    'comitente_id' => Auth::user()->getIdComitente(),
                    'fecha_concertacion' => $request->fecha_concertacion,
                    'fecha_aprobacion' => null,
                    'fecha_envio_mercado' => null,
                    'fecha_liquidacion' => $request->fecha_liquidacion,
                    'deposito_id' => $deposito->id,
                    'transferencia_id' => null,
                    'cantidad' => $request->cant_instrumentos,
                    'importe' => $request->valor_estimado,
                    'estado_id' => $estado_pendiente_id,
                    'mercado_registro_id' => null,
                    'mercado_mensaje' => null,
                    'tipo_operacion_id' => $tipo_operacion_id,
                    'subtipo_operacion' => 'SUS'
                ]);

            return $operacion;
        } catch (\Exception $e) {
            Log::alert("ERROR: " . $e->getMessage());
            abort(500);
        }
    }

    /**
     * Generamos un deposito con los valores recibidos del formulario
     *
     * @param $request
     * @return mixed
     */
    private function generarDeposito($request)
    {
        $deposito =
            Deposito::create([
                'moneda_id' => $request->moneda_id,
                'comitente_id' => Auth::user()->Comitentes()->first()->id,
                'cuenta_bancaria_id' => $request->cuenta_bancaria,
                'fecha_concertacion' => $request->fecha_concertacion,
                'fecha_aprobacion' => null,
                'fecha_liquidacion' => null,
                'importe' => $request->valor_estimado,
                'comprobante' => null,
                'estado_id'
            ]);

        return $deposito;
    }

    /**
     * Se valida que la operacion pueda ser realizada.
     * Se debe tener la cantidad de cuotapartes disponibles para rescatar
     *
     * @return bool
     */
    public function puedeOperar($request)
    {
        return true;
    }
}