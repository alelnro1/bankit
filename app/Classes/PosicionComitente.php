<?php

namespace App\Classes;


use App\Http\Controllers\Frontend\CotizacionesController;
use App\Models\CuentasCorrientes\CuentaCorrienteMoneda;
use App\Models\CuentasCorrientes\CuentaCorrienteInstrumento;
use App\Models\Instrumentos\Instrumento;
use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PosicionComitente
{
    public $fecha;

    public function __construct($fecha = null)
    {
        if (!$fecha) {
            $fecha = Carbon::now();
        }

        $this->fecha = $fecha;
    }

    /**
     * Traemos la posicion del comitente con los fondos
     *
     * @return string
     */
    public function getVistaPosicionesHome()
    {
        //$posicion_instrumentos = $this->getPosicionInstrumentos();
        $posicion_monedas = $this->getPosicionMonedas();
        $posicion_instrumentos = $this->getPosicionInstrumentosTotal();

         
        return view('frontend.posiciones-home.resumen', [
            'posicion_instrumentos' => $posicion_instrumentos,
            'posicion_monedas' => $posicion_monedas
        ])->render();
    }


    public function getPosicionInstrumentosTotal()
    {
        $posicion_instrumentos = Instrumento::all();
        
        foreach ($posicion_instrumentos as $instrumento) {
            $instrumento->pendiente_liquidar=0;          
            $instrumento->pendiente_comprobante=0;
            $instrumento->disponible=CuentaCorrienteInstrumento::
                                                        where("instrumento_id",$instrumento->id)
                                                        ->where("comitente_id",Auth::user()->getIdComitente())
                                                        ->get();

            foreach ($instrumento->disponible   as $instrumento_disponible) {
                $instrumento_disponible->cantidad=$instrumento_disponible->cantidad * $instrumento_disponible->multiplicador;

            }

            $instrumento->disponible = $instrumento->disponible->sum("cantidad");
            
            $operaciones_pendientes = Operacion::where("instrumento_id",$instrumento->id)
                                                    ->where("comitente_id",Auth::user()->getIdComitente())
                                                    ->where("estado_id",1)
                                                    ->get();

            
            
            foreach ($operaciones_pendientes as $operacion_pendiente) {
                $operaciones_pendientes->load(['Deposito','Moneda']);
                if (isset($operacion_pendiente->Deposito) && 
                    !isset($operacion_pendiente->Deposito->comprobante)) {
                    $instrumento->pendiente_comprobante+=$operacion_pendiente->cantidad;    
                    
                } else {
                    if ($operacion_pendiente->TipoOperacion->esSuscripcion()) {
                        $instrumento->pendiente_liquidar+=$operacion_pendiente->cantidad;   
                    } else {
                        $instrumento->pendiente_liquidar-=$operacion_pendiente->cantidad;   
                    }
                    
                }
            }

            $instrumento->load([
                'Cotizaciones' => function ($query) {
                    $query
                        ->orderBy('fecha_cotizacion', 'DESC')
                        ->take(1)
                        ->with('Moneda');
                }
            ]);
            
        }

        return $posicion_instrumentos;
    }
    /**
     * Obtenemos las posiciones de los instrumentos juntos con el instrumento
     *
     * @return \Illuminate\Support\Collection
     */
    public function getPosicionInstrumentos()
    {
        $comitente_id = Auth::user()->getIdComitente();

        $posicion_instrumentos =
            collect(DB::select("call getPosicionInstrumentos ('" . $comitente_id . "', '" . $this->fecha . "')"));

        $cotizaciones = new CotizacionesController();

        $posicion_instrumentos->map(function ($posicion) use ($cotizaciones) {
            $cotizacion = $cotizaciones->getCotizacionDeFondo($posicion->instrumento_id);

            $posicion->Instrumento = Instrumento::find($posicion->instrumento_id)->first();
            
            $posicion->Instrumento->load([
                'Cotizaciones' => function ($query) {
                    $query
                        ->orderBy('fecha_cotizacion', 'DESC')
                        ->take(1)
                        ->with('Moneda');
                }
            ]);
            
            $posicion->valor_estimado = $posicion->cantidad * $cotizacion;
            
            return $posicion;
        });

        return $posicion_instrumentos;
    }

    protected function getPosicionMonedas()
    {
        $comitente_id = Auth::user()->getIdComitente();

        $posicion_monedas = DB::select("call getPosicionMonedas ('" . $comitente_id . "', '" . $this->fecha . "')");

        return $posicion_monedas;
    }
}