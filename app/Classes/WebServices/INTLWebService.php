<?php
/**
 * Created by PhpStorm.
 * User: alejandro.ponzo
 * Date: 13/4/2018
 * Time: 10:27
 */

namespace App\Classes\WebServices;


use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

class INTLWebService
{
    public function postOperacionFondo($operacion)
    {
        $client = new Client();

        try {
            if (in_array($operacion->subtipo_operacion, ['RCU', 'RTO'])) {
                $valor = $operacion->cantidad;
            } else {
                $valor = $operacion->importe;
            }

            $response = $client->request('POST', env('INTL_WS_URL') . 'fondos-ordenes', [
                'form_params' => [
                    'token' => env('INTL_WS_TOKEN'),
                    'cod_accion' => 'A',
                    'fecha_operacion' => date("Ymd"),
                    'cod_tp_operacion' => $operacion->subtipo_operacion,
                    'num_cuotapartista' => env('INTL_WS_NUMERO_CUOTAPARTISTA'),
                    'cod_tp_interfaz_fondo' => $operacion->Instrumento->cod_interfaz,
                    'cod_moneda' => $operacion->moneda_id,
                    'valor' => $valor
                ]
            ]);
            
            
            
            $response = json_decode($response->getBody()->getContents())[0];

            // Si la respuesta es OK => guardamos la id de operacion de trading
            if ($response->Status == "Ok") {
                $operacion->mercado_registro_id = $response->IdOperacion;
                $operacion->mercado_mensaje = $response->Status;
                $operacion->fecha_aprobacion = Carbon::now();
                $operacion->fecha_envio_mercado = Carbon::now();
                $operacion->save();

                return true;
            } else {
                return $response->Mensaje;
            }
        } catch (Exception $e) {
          
            return "Fuera de Servicio. Error al tratar de procesar su transacción. Reintente más tarde.";
        }
    }

    /**
     * Buscamos la ultima cotizacion de un fondo
     *
     * @param $fondo
     */
    public function getUltimosDatosFondo($fondo)
    {
        //$fondo_solicitado = strtoupper($fondo_solicitado);
        
        $client = new Client();

        $res = $client->request('POST', env('INTL_WS_URL') . 'fondos', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ],
            'json' => [
                'token' => env('INTL_WS_TOKEN'),
                'cod_fondo' => $fondo->cod_interfaz,
                'abreviatura' => $fondo->clase
            ]
        ]);

        $fondos_api = collect(json_decode($res->getBody()));
        
        
        if (isset($fondos_api[0])) {
            return $fondos_api[0];
        }


        return false;
    }

}