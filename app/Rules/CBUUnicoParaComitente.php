<?php

namespace App\Rules;

use App\Models\Tesoreria\CuentasBancarias\CuentaBancaria;
use Illuminate\Contracts\Validation\Rule;

class CBUUnicoParaComitente implements Rule
{
    private $comitente_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($comitente_id)
    {
        $this->comitente_id = $comitente_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Buscamos una cuenta con un numero de CBU para el comitente dado
        $cuenta_bancaria =
            CuentaBancaria::where('comitente_id', $this->comitente_id)
                ->where("deleted_at",null)
                ->where('cbu', $value)
                ->first();

        // Si existe => NO se puede agregar otra igual
        if ($cuenta_bancaria) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ya existe una cuenta bancaria con el CBU brindado.';
    }
}
