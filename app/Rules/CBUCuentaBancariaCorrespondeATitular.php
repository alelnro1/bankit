<?php

namespace App\Rules;

use App\Http\Controllers\WSSaenzController;
use Illuminate\Contracts\Validation\Rule;

class CBUCuentaBancariaCorrespondeATitular implements Rule
{
    private $usuario;

    private $cbu;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($usuario, $cbu)
    {
        $this->usuario = $usuario;
        $this->cbu = $cbu;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ws_saenz = new WSSaenzController();
        $ws_saenz->setProspecto($this->usuario);

        return $ws_saenz->validarCBUDeCuentas($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El CBU no corresponde a un titular de la cuenta comitente.';
    }
}
