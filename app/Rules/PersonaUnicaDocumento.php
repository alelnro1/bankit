<?php

namespace App\Rules;

use App\Models\Prospectos\PersonasFisicas\Titular;
use Illuminate\Contracts\Validation\Rule;

class PersonaUnicaDocumento implements Rule
{
    protected $es_admin;

    protected $prospecto_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($es_admin, $prospecto_id)
    {
        $this->es_admin = $es_admin;

        $this->prospecto_id = $prospecto_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = false;

        // Hacemos esta validacion porque el admin no tiene prospecto_id
        if (!$this->es_admin) {
            $titular_con_dni = Titular::where('numero_documento', $value)->with('Prospectos')->first();

            if (count($titular_con_dni) > 0 && $titular_con_dni) {

                // Verificamos si el titular pertenece a alguno de los prospectos
                foreach ($titular_con_dni->Prospectos as $prosp) {
                    // Si el titular actual pertenece al prospecto actual => OK, puede editar
                    if ($prosp->id == $this->prospecto_id) {
                        // Encontramos uno => cortamos la ejecucion
                        $valid = true;

                        break;
                    }
                }
            } else {
                $valid = true;
            }
        } else {
            $valid = true;
        }

        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El titular ya existe.';
    }
}
