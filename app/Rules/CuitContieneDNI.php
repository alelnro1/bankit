<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CuitContieneDNI implements Rule
{
    private $numero_documento;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($numero_documento)
    {

        $this->numero_documento = $numero_documento;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (strpos($value, $this->numero_documento) !== false);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El CUIT no contiene al documento.';
    }
}
