<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailsSeparadosPuntoComa implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $emails = explode(';', trim(ltrim(rtrim($value))));

        $valid = true;

        foreach ($emails as $email) {
            $email=trim(ltrim(rtrim($email)));
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $valid = false;
                break;
            }
        }

        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Hay emails inválidos.';
    }
}
