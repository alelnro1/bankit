<?php

namespace App\Exports;

use App\Models\Operaciones\Operacion;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class OperacionesAprobadasExport implements FromView
{
    use Exportable;
    
    public function view(): View
    {
        $fecha = session('FCI_APROBADAS_FECHA');

        $operaciones = Operacion::getOperacionesAprobadasDeTipoInstrumento(null, null, $fecha);
        
        $operaciones->load('Comitente');

        return view('exports.operaciones-aprobadas-xls', ['operaciones' => $operaciones]);
    }
}