<?php

namespace App\Exports;

use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use App\Models\Instrumentos\Instrumento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;

class FondosCotizacionesExportTxt 
{

    
    public function view()
    {

        
		
        $tipo_instrumento_fondo_id = TipoInstrumento::getTipoInstrumentoPorDescripcion('Fondos')->id;
        $fondos = Instrumento::getInstrumentosDeTipo($tipo_instrumento_fondo_id);

        // Cargamos las cotizaciones con las monedas
        foreach ($fondos as $fondo) {
            $fondos->load([
                'Cotizaciones' => function ($query) {
                    $query
                        ->orderBy('fecha_cotizacion', 'desc')
                        ->with('Moneda');
                }
            ]);
        }
       



        
        $registros = [];
        $espacios_blanco = "                                                            ";
        $espacios_ceros = "00000000000000000000000000000000000000000000000000";
        $content = "";

        foreach ($fondos as $key => $fondo) {
             

        	$content .= substr($espacios_blanco . $fondo->abreviatura, -5) ."|";
            $content .= substr($espacios_blanco . $fondo->descripcion, -50) ."|";
            $content .= substr($espacios_blanco . 1, -5) ."|";
            $content .= substr($espacios_blanco . "Gainvest", -50) ."|";
            $content .= substr($espacios_blanco . date('Ymd',strtotime($fondo->Cotizaciones[0]->fecha_cotizacion)), -8) ."|";
            $content .= substr($espacios_ceros . number_format($fondo->Cotizaciones[0]->valor_compra * 100000000,0,"","") ,-14) ."|";
        	
        	$content .= date("YmdHis");
            $content .= "\n";

        }

        
        // file name that will be used in the download
        $fileName = "cotizaciones.txt";

        // use headers in order to generate the download
        $headers = [
            'Content-type' => 'text/plain',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            'Content-Length' => strlen($content)
        ];

        
        return response()->make($content, 200, $headers);

        
    }
}