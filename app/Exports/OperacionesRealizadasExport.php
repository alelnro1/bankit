<?php

namespace App\Exports;

use App\Models\Operaciones\Operacion;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class OperacionesRealizadasExport implements FromView
{
    use Exportable;
    
    public function view(): View
    {
        $fecha_desde = session('FCI_REALIZADAS_FECHA_DESDE');
        $fecha_hasta = session('FCI_REALIZADAS_FECHA_HASTA');

        $operaciones = Operacion::where("created_at",">=",$fecha_desde)
                                    ->where("created_at","<=",$fecha_hasta)
                                    ->get();

        $operaciones->load(['Comitente', 'Instrumento', 'Deposito', 'Transferencia', 'Moneda', 'TipoOperacion']);
        

        return view('exports.operaciones-realizadas-xls', ['operaciones' => $operaciones]);
    }
}