<?php


namespace App\Exports;

use App\Models\Operaciones\Operacion;
use App\Models\Operaciones\TipoInstrumento;
use App\Models\Instrumentos\Instrumento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;

class OperacionesAprobadasExportTxt
{




    public function view($fecha)
    {
        $operaciones = Operacion::getOperacionesAprobadasDeTipoInstrumento(null, null, $fecha);
        


        $operaciones->load('Comitente');
        $operaciones->load('TipoOperacion');

        $content = "";

        $registros = [];
        $espacios_blanco = "                                                                                                      ";
        $espacios_ceros = "00000000000000000000000000000000000000000000000000000000000000000000000000000";



        foreach ($operaciones as $key => $operacion) {

        	$content .= substr($espacios_blanco . $operacion->Instrumento->abreviatura, -5) ."|";
        	$content .= substr($espacios_blanco . $operacion->Instrumento->descripcion, -50) ."|";
        	$content .= substr($espacios_blanco . $operacion->Moneda->id, -5) ."|";
        	$content .= substr($espacios_blanco . $operacion->Moneda->simbolo, -5) ."|";
        	$content .= substr($espacios_blanco . $operacion->Moneda->descripcion, -20) ."|";
        	$content .= substr($espacios_blanco . $operacion->Comitente->id, -5) ."|";
        	$content .= substr($espacios_blanco . $operacion->Comitente->descripcion, -100) ."|";
        	$content .=    substr($espacios_ceros  . $operacion->Comitente->sucursal, -3)
                        .  substr($espacios_ceros  . $operacion->Comitente->numero_comitente, -6)
                        .  substr($espacios_ceros  . $operacion->Comitente->digito_verificador, -1) ."|";

        	$content .= substr($espacios_blanco . date('Ymd',strtotime($operacion->fecha_concertacion)),-8) ."|";
        	$content .= substr($espacios_blanco . date('Ymd',strtotime($operacion->fecha_liquidacion)),-8) ."|";
        	$content .= substr($espacios_blanco . "",-10) ."|";
        	$content .= substr($espacios_blanco . $operacion->TipoOperacion->id,-10) ."|";
        	$content .= substr($espacios_blanco . $operacion->TipoOperacion->nombre,-20) ."|";
        	$content .= substr($espacios_ceros . $operacion->id,-10) ."|";
        	$content .= substr($espacios_ceros . number_format($operacion->cantidad * 10000,0,"","") ,-14) ."|";
        	$content .= substr($espacios_ceros . number_format($operacion->importe * 100,0,"",""),-14) ."|";
        	$content .= date("YmdHis");
            $content .= "\n";

        }

        // file name that will be used in the download
        $fileName = "operaciones.txt";

        // use headers in order to generate the download
        $headers = [
            'Content-type' => 'text/plain',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            'Content-Length' => strlen($content)
        ];

        return response()->make($content, 200, $headers);

        //return view('exports.operaciones-aprobadas-txt', ['registros' => $registros]);
    }
}