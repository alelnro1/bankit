<?php

namespace App\Exports;

use App\Models\Operaciones\Operacion;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class OperacionesPendientesExport implements FromView
{
    use Exportable;
    
    public function view(): View
    {
        $operaciones = Operacion::getOperacionesPendientesDeTipoInstrumento();
        $operaciones = Operacion::getPendientesConComprobante($operaciones);

        $operaciones->load('Comitente');

        return view('exports.operaciones-pendientes-xls', ['operaciones' => $operaciones]);
    }
}