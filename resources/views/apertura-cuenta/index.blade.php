@extends(Auth::guard('backend')->user() ? 'layouts.backend-app' : 'layouts.apertura-cuenta-app')

@include('_snippets.preloader')

@section('styles')
    <link href="{{ asset('css/apertura-cuenta.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="rms-wizard" class="rms-wizard" style="display: none;">
        <!--Wizard Container-->
        <div class="rms-container">
            <!--Wizard Header-->
            <!--Wizard Header Close-->
        @include('apertura-cuenta.pasos.menu')

        <!--Wizard Navigation Close-->
            <!--Wizard Content Section Start-->
            <div class="rms-content-section">
                @if (!$es_persona_fisica)
                    <div class="rms-content-box rms-current-section" id="step-datos-societarios">
                        @include('apertura-cuenta.pasos.datos-societarios')
                    </div>
                @endif

                <div class="rms-content-box @if ($es_persona_fisica) rms-current-section @endif" id="step-titulares">
                    @include('apertura-cuenta.pasos.titulares')
                </div>

                <div class="rms-content-box" id="step-cuentas-bancarias">
                    @include('apertura-cuenta.pasos.cuentas-bancarias')
                </div>

                <div class="rms-content-box" id="step-perfil">
                    @include('apertura-cuenta.pasos.perfil')
                </div>

                <div class="rms-content-box" id="step-listo">
                    @include('apertura-cuenta.pasos.listo')
                </div>
            </div>

            <!--Wizard Content Section Close-->
            <!--Wizard Footer section Start-->
            <div class="rms-footer-section">
                <div class="button-section">
                    <span class="next">
                                <a id="btn-siguiente" class="btn btn-primary"
                                   data-registrar-ultimo-paso-url="{{ route('apertura-cuenta.registrar-ultimo-paso') }}">
                                    Siguiente
                                <small>Your information</small>
                            </a>
                    </span>
                    <span class="prev">
                        <a href="javascript:void(0)" class="btn btn-danger" id="btn-anterior">Anterior
                             <small>Your information</small>
                        </a>
                    </span>
                    @guest('backend')
                        <span class="submit">
                            <button class="btn btn-success" id="finalizar-apertura-cuenta"
                                    data-finalizar-apertura-cuenta-url="{{ route('apertura-cuenta.finalizar-proceso') }}"
                                    data-finalizar-apertura-cuenta-logout="{{ route('apertura-cuenta.finalizar-proceso.logout') }}">
                                Finalizar
                                <small>Proceso</small>
                            </button>
                        </span>
                    @else
                        <span class="submit">
                            <button class="btn btn-success" id="confirmar-apertura-cuenta"
                                    data-prospecto-id="{{ $prospecto_id }}"
                                    data-generar-comitente-url="{{ route('backend.apertura-cuenta.generar-comitente') }}">
                                Confirmar Apertura
                                <small>de Cuenta</small>
                            </button>
                        </span>
                    @endif
                </div>
            </div>
            <!--Wizard Footer Close-->
        </div>

    </div>

    @include('apertura-cuenta.pasos.titulares.titular-modal-dni')
    @include('apertura-cuenta.pasos.titulares.titular-funciones-pep')
@endsection

@section('javascripts')
    @if (Auth::guard('backend')->user())
        <script src="{{ asset('js/apertura-cuenta.js') }}"></script>
    @endif
    <script type="text/javascript" src="{{ asset('js/datetimepicker.js') }}"></script>
    <script id="titular-nuevo" type="text/template">
        @include('apertura-cuenta.pasos.titulares.titular-nuevo', ['titular' => null])
    </script>
    <script>
        $(document).ready(function () {
            $('#rms-wizard').show();
        });
    </script>
@endsection