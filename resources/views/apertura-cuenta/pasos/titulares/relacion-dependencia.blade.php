<div class="row">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="Email">Empleador</label>
            <div class="inpt-group">
                <input type="text" name="empleador" id="empleador" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="inpt-form-group">
            <label for="Email">Domicilio Laboral</label>
            <div class="inpt-group">
                <input type="text" name="domicilio_actividad" id="domicilio_laboral"
                       class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label for="Email">Piso y Dto</label>
            <div class="inpt-group">
                <input type="text" name="piso_dto_rrdd" id="piso_dto_rrdd" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="inpt-form-group">
            <label for="Email">Localidad</label>
            <div class="inpt-group">
                <input type="text" name="localidad_actividad" id="localidad_actividad"
                       class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label for="Email">Codigo Postal</label>
            <div class="inpt-group">
                <input type="text" name="codigo_postal_actividad" id="codigo_postal_actividad"
                       class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="Email">Cargo</label>
            <div class="inpt-group">
                <input type="text" name="cargo" id="cargo" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="inpt-form-group">
            <label for="Email">C.U.I.T Empleador</label>
            <div class="inpt-group">
                <input type="number" name="cuit_empleador" id="cuit_empleador" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label for="Email">Fecha de ingreso</label>
            <div class="inpt-group">
                <input type="text" name="fecha_actividad" id="fecha_actividad" class="form-control">
            </div>
        </div>
    </div>
</div>