<div class="row">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="Email">Rubro/Profesion</label>
            <div class="inpt-group">
                <input type="text" name="rubro_profesion" id="rubro_profesion" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="inpt-form-group">
            <label for="Email">Domicilio Profesional</label>
            <div class="inpt-group">
                <input type="text" name="domicilio_actividad" id="domicilio_actividad"
                       class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label for="Email">Piso y Dto</label>
            <div class="inpt-group">
                <input type="text" name="piso_dto" id="piso_dto" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="inpt-form-group">
            <label for="Email">Localidad</label>
            <div class="inpt-group">
                <input type="text" name="localidad_actividad" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label for="Email">Codigo Postal</label>
            <div class="inpt-group">
                <input type="text" name="codigo_postal_actividad" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="inpt-form-group">
            <label for="Email">Categoria</label>
            <div class="inpt-group">
                <input type="text" name="categoria" id="categoria" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="inpt-form-group">
            <label for="Email">Fecha de alta</label>
            <div class="inpt-group">
                <input type="text" name="fecha_actividad" id="fecha_actividad" class="form-control">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="Email">Empleados</label>
            <div class="inpt-group">
                <input type="radio" name="empleados"> Posee
                <input type="radio" name="empleados"> No Posee
            </div>
        </div>
    </div>
</div>