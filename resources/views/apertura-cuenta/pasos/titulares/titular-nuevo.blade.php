@if (isset($titular->nombre))
    <h3>
        Titular {{ $titular->apellido }} , {{ $titular->nombre }}
        {{--<span style="float:left;" class="ui-icon ui-icon-closethick eliminar-titular"></span>--}}
    </h3>
@else
    <h3>Nuevo Titular</h3>
@endif

<div style="height: 100% !important;">
    @if (Auth::guard('backend')->user() && $titular)
        @if ($titular->pivot->agregado_por_dni && !$titular->pivot->esta_confirmado)
            <div class="alert alert-warning">
                Titular sin confirmar.
            </div>
        @endif
    @endif
    <form id="form_datos_personales" method="POST">
        <input type="hidden" name="prospecto_id" class="prospecto_id" value="{{ $prospecto_id }}">

        <div class="row accordion" id="datos-personales">
            <h3>Datos personales</h3>
            @include('apertura-cuenta.pasos.titulares.datos-personales', ['titular' => $titular, 'paises' => $paises])
        </div>

        <div class="row" id="datos-conyuge" style="display: none;">
            <div class="col-md-12">
                <h3>Datos del Cónyuge</h3>

                @include('apertura-cuenta.pasos.titulares.datos-conyuge', ['titular' => $titular])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Datos impositivos</h3>

                @include('apertura-cuenta.pasos.titulares.datos-impositivos', ['titular' => $titular])
            </div>
        </div>

        <hr>
        <button class="btn btn-danger eliminar-titular pull-left"
                data-eliminar-titular-url="{{ route('apertura-cuenta.titulares.eliminar') }}"
                @if (isset($titular->id))
                    data-titular-id="{{ $titular->id }}"
                @else
                    data-titular-id=""
                @endif>
            <i class="fas fa-times"></i>
            Eliminar titular
        </button>

        <button class="btn btn-success guardar-titular pull-right"
                data-guardar-titular-url="{{ route('apertura-cuenta.titulares.guardar') }}">
            <i class="far fa-save"></i>
            Guardar datos
        </button>

        
    </form>

    <div style="clear:both;"></div>
</div>