<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="inpt-form-group">
                <input type="hidden" name="titular_id" id="titular_id" value="{{ $titular->id or "" }}">

                <label for="nombre" class="campoObligatorio">Nombre</label>
                <div class="inpt-group">
                    <input type="text" name="nombre" id="nombre" class="inpt-control form-control required"
                           value="{{ $titular->nombre or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="apellido"  class="campoObligatorio">Apellido</label>
                <div class="inpt-group">
                    <input type="text" name="apellido" id="apellido"
                           class="inpt-control form-control required" value="{{ $titular->apellido or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        {{--<div class="col-md-4">
            <div class="inpt-form-group">
                <label for="tipo_documento_id" class="campoObligatorio">Tipo de documento</label>
                <div class="inpt-group">
                    <select class="form-control required" name="tipo_documento_id" id="tipo_documento_id">
                        <option value="">Seleccionar...</option>

                        @foreach ($tipos_documentos as $tipo_documento)
                            <option value="{{ $tipo_documento->id }}"
                                    @if (isset($titular->tipo_documento_id) && $titular->tipo_documento_id == $tipo_documento->id)
                                        selected
                                    @endif>
                                {{ $tipo_documento->descripcion }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>--}}
        {{--<div class="col-md-4">
            <div class="inpt-form-group">
                <label for="numero_documento" class="campoObligatorio">Nº de documento</label>
                <div class="inpt-group">
                    <input type="number" name="numero_documento" id="numero_documento"
                           class="inpt-control form-control required" value="{{ $titular->numero_documento or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>--}}
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="cuit_cuil" class="campoObligatorio">N° de CUIL/CUIT</label>
                <div class="inpt-group">
                    <input type="number"  name="cuit_cuil" id="cuit_cuil" class="inpt-control form-control required"
                           value="{{ $titular->cuit_cuil or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>

        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="estado_civil_id" class="campoObligatorio">Estado civil</label>
                <select class="form-control required" name="estado_civil_id" id="estado_civil_id">
                    <option value="">Seleccionar</option>

                    @foreach ($estados_civiles as $estado_civil)
                        <option value="{{ $estado_civil->id }}"
                                @if (isset($titular->estado_civil_id) && $titular->estado_civil_id == $estado_civil->id)
                                selected
                                @endif>
                            {{ $estado_civil->descripcion }}
                        </option>
                    @endforeach
                </select>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="sexo_id" class="campoObligatorio">Sexo</label>
                <select name="sexo_id" class="form-control required" id="sexo_id">
                    <option value="">Seleccionar</option>
                    @foreach ($sexos as $sexo)
                        <option value="{{ $sexo->id }}"
                                @if (isset($titular->sexo_id) && $titular->sexo_id == $sexo->id)
                                    selected
                                @endif>
                            {{ $sexo->descripcion }}
                        </option>
                    @endforeach
                </select>

                <span class="error-help-block help-block">

                </span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="tipo_cliente_id" class="campoObligatorio">Tipo de Cliente</label>

                <select name="tipo_cliente" class="form-control required" id="tipo_cliente">
                    <option value="">Seleccionar</option>

                    <option value="Residentes en el Pais"
                            @if(isset ($titular->tipo_cliente) && $titular->tipo_cliente == "Residentes en el Pais")
                                selected
                            @endif>
                        Residentes en el Pais
                    </option>
                    <option value="Residentes en el Exterior"
                            @if(isset ($titular->tipo_cliente) && $titular->tipo_cliente == "Residentes en el Exterior")
                                selected
                            @endif>
                        Residentes en el Exterior
                    </option>
                </select>

                <span class="error-help-block help-block">

                </span>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="inpt-form-group">
                <label for="nacionalidad" class="campoObligatorio">Nacionalidad</label>
                <div class="inpt-group">
                    <select name="nacionalidad" id="nacionalidad" class="form-control">
                        @foreach ($paises as $pais)
                            <option value="{{ $pais->descripcion }}">{{ $pais->descripcion }}</option>
                        @endforeach
                    </select>
                </div>

                <span class="error-help-block help-block">

                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="inpt-form-group">
                <br>
                <input type="checkbox" name="funcionario_publico" id="funcionario_publico"
                       class="inpt-control"
                       checked="{{ $titular->funcionario_publico or "" }}"
                       title=" Persona que ocupa un cargo legislativo, ejecutivo, administrativo o judicial de un Estado Parte.">
                <span title=" Persona que ocupa un cargo legislativo, ejecutivo, administrativo o judicial de un Estado Parte.">¿Es Funcionario Público?</span>

                <span class="error-help-block help-block"></span>
            </div>
        </div>

        @if (Auth::guard('backend')->user())
            <div class="col-md-4">
                <div class="inpt-form-group">
                    <br>
                    <input type="checkbox" name="vinculado" id="vinculado"
                           class="inpt-control"
                           checked="{{ $titular->vinculado or "" }}">
                    ¿Vinculado?

                    <span class="error-help-block help-block"></span>
                </div>
            </div>
        @else
            <div class="col-md-4"></div>
        @endif
        <div class="col-md-4"></div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <div class="inpt-form-group">
                <br>

                <input type="checkbox" name="pep" id="pep"
                       class="inpt-control"
                       checked="{{ $titular->pep or "" }}">
                
                <label for="pep" title >
                    <span title="Persona Expuesta Políticamente: Persona que desempeña o haya desempeñado funciones públicas  hasta dos años anteriores a la fecha en que fue realizada la operatoria y los cónyuges, o convivientes reconocidos legalmente, familiares en línea ascendiente o descendiente hasta el primer grado de consanguinidad. Ver Res UIF 52/2012">
                        ¿Es PEP?

                    </span>
                </label>

                <span class="error-help-block help-block"></span>
            </div>
        </div>

        <div class="col-md-8">
            <div class="inpt-form-group" id="div-funcion_pep" style="display: none;">
                <label for="funcion_pep" class="campoObligatorio">Funcion PEP
                    <a class="funciones-pep" style="color:blue;">*ver nomina UIF*</a>
                </label>
                <div class="inpt-group">
                    
                <input type="text" name="funcion_pep" id="funcion_pep" class="inpt-control form-control required"
                       value="{{ $titular->funcion_pep or "" }}">

                <span class="error-help-block help-block"></span>

                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>


    

    <div class="row">
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="calle" class="campoObligatorio">Calle</label>
                <div class="inpt-group">
                    <input type="text" name="direccion[calle]" id="calle" class="inpt-control form-control required"
                           value="{{ $titular->Direccion->calle or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="inpt-form-group">
                <label for="altura" class="campoObligatorio">Altura</label>
                <div class="inpt-group">
                    <input type="number" name="direccion[altura]" id="altura" class="inpt-control form-control required"
                           value="{{ $titular->direccion['altura']  or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="inpt-form-group">
                <label for="piso">Piso</label>
                <div class="inpt-group">
                    <input type="text" name="direccion[piso]" id="piso" class="inpt-control form-control required "
                           value="{{ $titular->direccion['piso'] or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="inpt-form-group">
                <label for="departamento">Departamento</label>
                <div class="inpt-group">
                    <input type="text" name="direccion[departamento]" id="departamento"
                           class="inpt-control form-control required"
                           value="{{ $titular->direccion['departamento'] or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="localidad" class="campoObligatorio">Localidad</label>
                <div class="inpt-group">
                    <input type="text" name="direccion[localidad]" id="localidad" class="form-control required"
                           value="{{ $titular->direccion['localidad'] or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="codigo_postal" class="campoObligatorio">Codigo Postal</label>
                <div class="inpt-group">
                    <input type="text" name="direccion[codigo_postal]" id="codigo_postal" class="form-control required"
                           value="{{ $titular->direccion['codigo_postal'] or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="provincia" class="campoObligatorio">Provincia</label>

                <select name="direccion[provincia]" id="provincia" class="form-control">
                    @foreach ($provincias as $provincia)
                        <option value="{{ $provincia->id }}"
                                @if ( $titular['direccion']['provincia'] == $provincia->id)
                                selected
                                @endif>{{ $provincia->descripcion }}</option>
                    @endforeach
                </select>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="inpt-form-group">
                <label for="pais" class="campoObligatorio">Pais</label>
                <div class="inpt-group">
                    <select name="direccion[pais]" id="pais" class="form-control">
                        @foreach ($paises as $pais)
                            <option value="{{ $pais->id }}"
                                @if ( $titular['direccion']['pais'] == $pais->id)
                                selected
                                @endif>{{ $pais->descripcion }}</option>
                        @endforeach
                    </select>
                    {{--<input type="text" name="direccion[pais]" id="pais" class="form-control required"
                           value="{{ $titular->direccion['pais'] or "" }}">--}}
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">
            <div class="inpt-form-group">
                <label for="cod_area" class="campoObligatorio">Código de Área</label>
                <div class="inpt-group">
                    <input type="text" name="cod_area" id="cod_area"
                           class="inpt-control form-control required"
                           value="{{ $titular->cod_area or "005411" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="inpt-form-group">
                <label for="prefijo_celular" class="campoObligatorio">Prefijo Celular</label>
                <div class="inpt-group">
                    <input type="text" name="prefijo_celular" id="prefijo_celular"
                           class="inpt-control form-control required"
                           value="{{ $titular->prefijo_celular or "15" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="telefono_movil" class="campoObligatorio">Teléfono Celular</label>
                <div class="inpt-group">
                    <input type="text" name="telefono_movil" id="telefono_movil"
                           class="inpt-control form-control required"
                           value="{{ $titular->telefono_movil or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="pais_nacimiento" class="campoObligatorio">País de Nacimiento</label>
                <div class="inpt-group">
                    <select name="pais_nacimiento" id="pais_nacimiento" class="form-control required">
                        @foreach ($paises as $pais)
                            <option value="{{ $pais->id }}"
                                @if ( $titular['pais_nacimiento'] == $pais->id)
                                selected
                                @endif>
                                {{ $pais->descripcion }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>

        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="pais_nacimiento" class="campoObligatorio">Provincia de Nacimiento</label>
                <div class="inpt-group">
                    <select name="provincia_nacimiento" id="provincia_nacimiento" class="form-control required">
                        @foreach ($provincias as $provincia)
                            <option value="{{ $provincia->id }}"
                                @if ($titular['provincia_nacimiento']== $provincia->id)
                                selected
                                @endif>{{ $provincia->descripcion }}</option>
                        @endforeach
                    </select>
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>

        <div class="col-md-4">
            <div class="inpt-form-group">
                <label for="fecha_nacimiento" class="campoObligatorio">Fecha de Nacimiento</label>
                <div class="inpt-group">
                    <input type="text" name="fecha_nacimiento" id="fecha_nacimiento"
                           class="inpt-control form-control required fecha_nacimiento"
                           value="{{ $titular->fecha_nacimiento or "" }}"
                           placeholder="DD/MM/AAAA">

                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="inpt-form-group">
                <label for="email" class="campoObligatorio">Correo electronico</label>
                <div style="padding:3px 5px 3px 5px;margin-bottom:5px" class="alert alert-info">
                    Si desea agregar otro correo electrónico ingréselo a continuación separado por punto y coma (;)
                </div>
                <div class="inpt-group">
                    <input type="text" name="email" id="email" class="inpt-control form-control required"
                           value="{{ $titular->email or "" }}">
                </div>

                <span class="error-help-block help-block"></span>
            </div>
        </div>
    </div>
</div>