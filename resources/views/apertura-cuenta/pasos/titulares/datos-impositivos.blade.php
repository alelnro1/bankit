<!--
<div class="col-md-6">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="Email">Ingresos brutos</label>
            <div class="inpt-group">
                <select class="form-control" name="ingresos_brutos" id="ingresos_brutos">
                    <option value="1">Opciones</option>
                </select>
            </div>
        </div>
    </div>
</div>
-->
<div class="col-md-6">
    <div class="col-md-12">
        <div class="inpt-form-group">
            <label for="tipo_responsable_iva_id" class="campoObligatorio">IVA</label>
            <div class="inpt-group required">
                <select class="form-control" name="tipo_responsable_iva_id" id="tipo_responsable_iva_id">
                    <option value="">Seleccione una opcion...</option>

                    @foreach ($tipos_responsables_iva as $tipo)
                        <option value="{{ $tipo->id }}"
                                @if ($titular['tipo_responsable_iva_id']
                                == $tipo->id)
                                selected
                                @endif>
                            {{ $tipo->descripcion }}
                        </option>
                    @endforeach
                </select>
            </div>
            <span class="error-help-block help-block"></span>
        </div>
    </div>
</div>