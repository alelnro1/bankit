<div id="modal-titular-funciones-pep" title="Funciones PEP" style="display: none;">
    <div class="help-block">
        <strong>
            Nómina de Funciones de Personas Expuestas Políticamente
        </strong>
        <br>
        
    </div>
        <p>
           <strong> Son personas políticamente expuestas las siguientes:</strong>
                <p>El presidente y vicepresidente de la Nación.</p>

                <p>Los senadores y diputados de la Nación.</p>

                <p>Los magistrados del Poder Judicial de la Nación.</p>

                <p>Los magistrados del Ministerio Público de Nación.</p>

                <p>El defensor del pueblo de la Nación y los adjuntos del defensor del pueblo.</p>

                <p>El jefe de gabinete de ministros, los ministros, secretarios y subsecretarios del Poder Ejecutivo Nacional.</p>

                <p>Los interventores federales.</p>

                <p>El síndico general de la Nación y los síndicos generales adjuntos de la Sindicatura General de la Nación, el presidente y los auditores generales de la Auditoria General de la Nación, las autoridades superiores de los entes reguladores y los demás órganos que integran los sistemas de control del sector público nacional, y los miembros de organismos jurisdiccionales administrativos.</p>

                <p>Los miembros del Consejo de la Magistratura y del Jurado de Enjuiciamiento.</p>

                <p>Los embajadores, cónsules y funcionarios destacados en misión oficial permanente en exterior.</p>

                <p>El personal en actividad de las Fuerzas Armadas, de la Policía Federal Argentina, de Gendarmería Nacional, de la Prefectura Naval Argentina y del Servicio Penitenciario Federal, con jerarquía no menor de coronel o grado equivalente según la fuerza.</p>

                <p>Los rectores, decanos y secretarios de las universidades nacionales.</p>

                <p>Los funcionarios o empleados con categoría o función no inferior a la de director o equivalente, que presten servicio en la Administración Pública Nacional, centralizada o descentralizada, las entidades autárquicas, los bancos y entidades financieras del sistema oficial, las obras sociales administradas por el Estado, las empresas del Estado, las sociedades del Estado y el personal con similar categoría o función, designado a propuesta del Estado en las sociedades de economía mixta, en las sociedades anónimas con participación estatal y en otros entes del sector público.</p>

                <p>Los funcionarios colaboradores de interventores federales, con categoría o función no inferior a la de director o equivalente.</p>

                <p> El personal de los organismos indicados en el inciso h) del presente artículo, con categoría no inferior
                a la director o equivalente.</p>

                <p> Todo funcionario o empleado público encargado de otorgar habilitaciones administrativas para el
                ejercicio de cualquier actividad, como también todo funcionario o empleado público encargado de
                controlar el funcionamiento de dichas actividades o de ejercer cualquier otro control en virtud de un poder de policía</p>

                <p> Los funcionarios que integran los organismos de control de los servicios públicos privatizados, con
                categoría no inferior a la de director.</p>
                <p> El personal que se desempeña en el Poder Legislativo, con categoría no inferior a la de director.</p>
                <p> El personal que cumpla servicios en el Poder Judicial de la Nación y en el Ministerio Público de la
                Nación, con categoría no inferior a secretario o equivalente.</p>
                <p> Todo funcionario o empleado público que integre comisiones de adjudicación de licitaciones, de
                compra o de recepción de bienes, o participe en la toma de decisiones de licitaciones o compras.</p>
                <p> Todo funcionario público que tenga por función administrar un patrimonio público o privado, o controlar
                o fiscalizar los ingresos públicos cualquiera fuera su naturaleza.</p>
                <p> Los directores y administradores de las entidades sometidas al control externo del Honorable
                Congreso de la Nación, de conformidad con lo dispuesto en el artículo 120 de la ley 24.156, en los
                casos en que la Comisión Nacional de Ética Pública se las requiera.</p>

        </p>



        <p> <strong>Los funcionarios públicos provinciales, municipales y de la Ciudad Autónoma de Buenos Aires que a
        continuación se señalan, que se desempeñen o hayan desempeñado hasta dos años anteriores a la
        fecha en que fue realizada la operatoria:</strong>

                <p> Gobernadores, intendentes y jefe de gobierno de la Ciudad Autónoma de Buenos Aires.</p>
                <p> Ministros de gobierno, Secretarios y Subsecretarios. Ministros de los Tribunales Superiores de Justicia
                de las provincias y de la Ciudad Autónoma de Buenos Aires.</p>
                <p> Jueces y demás personal que cumpla servicios en los Poderes Judiciales Provinciales y de la Ciudad
                Autónoma de Buenos Aires, con categoría no inferior a Secretario o equivalente.</p>
                <p> Legisladores provinciales, municipales y de la Ciudad Autónoma de Buenos Aires.
                F.906-2</p>

                <p> Máxima autoridad de los Organismos de Control y de los entes autárquicos provinciales, municipales y
                de la Ciudad Autónoma de Buenos Aires.</p>
                <p> Máxima autoridad de las sociedades de propiedad de los estados provinciales, municipales y de la
                Ciudad Autónoma de Buenos Aires.</p>
                <p> Cualquier otra persona que desempeñe o haya desempeñado hasta dos años anteriores a la fecha en
                que fue realizada la operatoria, en las órbitas provinciales, municipales y de la Ciudad Autónoma de
                Buenos Aires, funciones idénticas o similares a las enumeradas en el artículo 5o de la Ley No 25.188.</p>
        </p>

        <p>Las autoridades y apoderados de partidos políticos de nivel nacional, provincial y de la Ciudad
        Autónoma de Buenos Aires, que se desempeñen o hayan desempeñado hasta dos años anteriores a la
        fecha en que fue realizada la operatoria.</p>

        <p>Las Autoridades y representantes legales de organizaciones sindicales y empresariales (cámaras,
        asociaciones y otras formas de agrupación corporativa) y de las obras sociales contempladas en la Ley
        No 23.660, que desempeñen o hayan desempeñado dichas funciones hasta dos años anteriores a la
        fecha en que fue realizada la operatoria.
        El alcance establecido se limita a aquellos rangos, jerarquías o categorías con facultades de decisión
        resolutivas, por lo tanto se excluye a los funcionarios de niveles intermedios o inferiores.
        </p>

        <p><strong>Los funcionarios públicos extranjeros: quedan comprendidas las personas que desempeñen o hayan
        desempeñado dichas funciones hasta dos años anteriores a la fecha en que fue realizada la operación,
        ocupando alguno de los siguientes cargos:</strong>
                <p>Jefes de Estado, jefes de Gobierno, gobernadores, intendentes, ministros, secretarios y subsecretarios de
                Estado y otros cargos gubernamentales equivalentes.</p>
                <p>Miembros del parlamento / poder legislativo.</p>
                <p>Jueces, miembros superiores de tribunales y otras altas instancias judiciales y administrativas de ese
                ámbito del poder judicial.</p>
                <p>Embajadores, cónsules y funcionarios destacados de misiones oficiales permanentes del exterior.</p>
                <p>Oficiales de alto rango de las fuerzas armadas (a partir de coronel o grado equivalente en la fuerza y/ o
                país de que se trate) y de las fuerzas de seguridad pública (a partir de comisario o rango equivalente
                según la fuerza y/ país de que se trate)</p>
                <p>Miembros de los órganos de dirección y control de empresas de propiedad estatal.</p>
                <p>Directores, gobernadores, consejeros, síndicos o autoridades equivalentes de bancos centrales y otros
                organismos estatales de regulación y/ o supervisión.</p>
        </p>

        <p>Allegados o colaboradores, cónyuges o convivientes reconocidos legalmente y familiares en línea
        ascendente, descendente o colateral hasta el tercer grado de consanguinidad o segundo grado de
        afinidad, de las personas a que se refieren los incisos A, B, C, D y E durante los plazos que para ellas se
        indican.
        Allegados o colaboradores cercanos a las personas a que se refieren los incisos A a E, durante los plazos
        que para ellas se indican, entendiéndose por tales a todos aquellos individuos que se benefician directa y
        económicamente en razón de su relación cercana con la persona políticamente expuesta. Entre otros,
        pueden encuadrar en esta categoría los socios personales, los consultores, asesores y colaboradores de
        trabajo de dichas personas.
        </p>
</div>