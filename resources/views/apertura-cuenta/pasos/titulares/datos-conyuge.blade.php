<div class="row">
    <div class="col-md-6">
        <div class="inpt-form-group">
            <label class="campoObligatorio" for="nombre_conyuge">Nombre</label>
            <div class="inpt-group">
                <input type="text" name="conyuge[nombre_conyuge]" id="nombre_conyuge"
                       class="inpt-control form-control required "
                       value="{{ $titular->conyuge['nombre'] or "" }}">
            </div>

            <span class="error-help-block help-block"></span>
        </div>
    </div>

    <div class="col-md-6">
        <div class="inpt-form-group">
            <label class="campoObligatorio" for="apellido_conyuge">Apellido</label>
            <div class="inpt-group">
                <input type="text" name="conyuge[apellido_conyuge]" id="apellido_conyuge"
                       class="inpt-control form-control required "
                       value="{{ $titular->conyuge['apellido'] or "" }}">
            </div>
            <span class="error-help-block help-block"></span>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label class="campoObligatorio" for="tipo_documento">Tipo de documento</label>
            <div class="inpt-group">
                <select class="form-control" name="conyuge[tipo_documento]" id="tipo_documento">
                    <option value="">Seleccionar</option>

                    @foreach ($tipos_documentos as $tipo_documento)
                        <option value="{{ $tipo_documento->id }}"
                                @if ( $titular['conyuge']['tipo_documento_id'] == $tipo_documento->id)
                                selected
                                @endif>
                            {{ $tipo_documento->descripcion }}</option>
                    @endforeach
                </select>
            </div>

            <span class="error-help-block help-block"></span>
        </div>
    </div>
    <div class="col-md-4">
        <div class="inpt-form-group">
            <label class="campoObligatorio" for="numero_documento">Nº de documento</label>
            <div class="inpt-group">
                <input type="number" name="conyuge[numero_documento]" id="numero_documento"
                       class="inpt-control form-control required "
                       value="{{ $titular->conyuge['numero_documento'] or "" }}">
            </div>

            <span class="error-help-block help-block"></span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="inpt-form-group">
            <div class="inpt-form-group">
                <label class="campoObligatorio" for="sexo">Sexo </label>
                <select name="conyuge[sexo]" class="form-control" id="sexo">
                    <option value="">Seleccionar</option>

                    @foreach ($sexos as $sexo)
                        <option value="{{ $sexo->id }}"
                                @if ( $titular['conyuge']['sexo_id'] == $sexo->id)
                                selected
                                @endif>
                            {{ $sexo->descripcion }}
                        </option>
                    @endforeach
                </select>

                <span class="error-help-block help-block">

                </span>
            </div>
        </div>
    </div>
</div>

