<div id="modal-titular-documento" title="Titular por Documento" style="display: none;"
     data-relacionar-titular-existente-url="{{ route('apertura-cuenta.titulares.existente.relacionar') }}">
    <div class="help-block">
        Ingrese el número de documento de la persona que quiera agregar
    </div>

    <div>
        <!--
        <label for="tipo_documento"></label>
        
        
        <select name="tipo_documento" id="tipo-documento-a-consultar" class="form-control">
            @foreach ($tipos_documentos as $tipo_documento)
                <option value="{{ $tipo_documento->id }}">{{ $tipo_documento->descripcion }}</option>
            @endforeach
        </select>
        -->

        <input type="number" class="form-control" name="cuit" id="numero-documento-a-consultar"
            placeholder="CUIT / CUIL">

        <hr>

        <div style="text-align: center;">
            <button type="submit" id="consultar-documento"
                    class="btn btn-primary"
                    data-consultar-documento-url="{{ route('apertura-cuenta.titulares.consultar-por-documento') }}">
                Consultar
            </button>
        </div>
    </div>
</div>