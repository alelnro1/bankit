<h3>{{ $titular->nombre }} {{ $titular->apellido }} (N° Documento: {{ $titular->numero_documento }})</h3>

<div>
    <input type="hidden" name="prospecto_id" class="prospecto_id">

    <input type="hidden" name="titular_id" id="titular_id" value="{{ $titular->id or "" }}">

    <button  class="btn btn-danger eliminar-titular pull-left"
            data-titular-id="{{ $titular->id }}"
            data-eliminar-titular-url="{{ route('apertura-cuenta.titulares.eliminar') }}">
        <i class="fas fa-times"></i>
        Eliminar titular
    </button>
</div>