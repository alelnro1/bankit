<div class="rms-content-area">
    <div class="rms-content-title">
        <div class="panel-heading" style="width:100%; height: 50px;">
            <div class="leftside-title">
                <b> 
                    <span class="title-white">Información de las cuentas bancarias</span>
                </b> 
            </div>
            <div class="step-label">
                @if ($es_persona_fisica)
                    Paso 2
                @else
                    Paso 3
                @endif
            </div>
        </div>
        <div class="alert alert-info">Ingresando el CBU o Alias y presionando en el boton buscar podra precargar la informacion de su cuenta</div>
    </div>

    

    <div class="rms-content-body">
        <br>
        <div id="tabs" data-buscar-cuenta-bancaria-url="{{ route('apertura-cuenta.cuentas-bancarias.buscar') }}">
            <ul>
                <li><a href="#cuenta-pesos">Cuenta Bancaria en Pesos</a></li>
                <li><a href="#cuenta-dolares">Cuenta Bancaria en Dólares</a></li>
            </ul>

            <div class="row" id="cuenta-pesos">
                @include('apertura-cuenta.pasos.cuentas-bancarias.cuenta-bancaria-pesos')
            </div>
            <div class="row" id="cuenta-dolares">
                @include('apertura-cuenta.pasos.cuentas-bancarias.cuenta-bancaria-dolares')
            </div>
        </div>
    </div>
</div>