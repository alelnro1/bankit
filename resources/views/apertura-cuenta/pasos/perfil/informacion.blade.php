<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th  >Información</th>
        <th  >Escala</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Estoy muy informado de los temas financieros y económicos</td>
        <td style="text-align: center;">
            <input type="radio" name="informacion" id="informacion" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Estoy informado de temas financieros y económicos</td>
        <td style="text-align: center;">
            <input type="radio" name="informacion" id="informacion" value="{{ rand(111, 999) . 2 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Manejo poca información de temas financieros o económicos</td>
        <td style="text-align: center;">
            <input type="radio" name="informacion" id="informacion" value="{{ rand(111, 999) . 1 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Realmente no estoy informado de temas financieros o económicos</td>
        <td style="text-align: center;">
            <input type="radio" name="informacion" id="informacion" value="{{ rand(111, 999) . 0 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    </tbody>
</table>