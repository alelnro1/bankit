<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th >Tiempo</th>
        <th >Escala</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Tengo suficiente tiempo disponible para el manejo de mis inversiones</td>
        <td style="text-align: center;">
            <input type="radio" name="tiempo" id="tiempo" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Tengo algún tiempo disponible para el manejo de mis inversiones</td>
        <td style="text-align: center;">
            <input type="radio" name="tiempo" id="tiempo" value="{{ rand(111, 999) . 2 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Tengo poco tiempo disponible para el manejo de mis inversiones</td>
        <td style="text-align: center;">
            <input type="radio" name="tiempo" id="tiempo" value="{{ rand(111, 999) . 1 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Realmente no tengo tiempo para el manejo de mis inversiones</td>
        <td style="text-align: center;">
            <input type="radio" name="tiempo" id="tiempo" value="{{ rand(111, 999) . 0 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    </tbody>
</table>