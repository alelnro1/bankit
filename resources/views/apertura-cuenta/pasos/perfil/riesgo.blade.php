<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th  >Riesgo</th>
        <th  >Escala</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Busco rentabilidad y estoy dispuesto a asumir riesgos en ello </td>
        <td style="text-align: center;"">
            <input type="radio" name="riesgo" id="riesgo" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Busco un equilibrio entre seguridad y rentabilidad</td>
        <td style="text-align: center;"">
            <input type="radio" name="riesgo" id="riesgo" value="{{ rand(111, 999) . 2 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Prefiero seguridad a rentabilidad</td>
        <td style="text-align: center;"">
            <input type="radio" name="riesgo" id="riesgo" value="{{ rand(111, 999) . 1 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>La seguridad es tan importante que quiero tomar el menor riesgo posible</td>
        <td style="text-align: center;"">
            <input type="radio" name="riesgo" id="riesgo" value="{{ rand(111, 999) . 0 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    </tbody>
</table>