<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th  >Horizonte de Inversión</th>
        <th  >Escala</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>Mi horizonte de inversión es mayor a 3 años</td>
        <td style="text-align: center; ">
            <input type="radio" name="horizonte-inversion" id="horizonte-inversion" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Mi horizonte de inversión está entre 1 y 3 años</td>
        <td style="text-align: center; ">
            <input type="radio" name="horizonte-inversion" id="horizonte-inversion" value="{{ rand(111, 999) . 2 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Mi horizonte de inversión es menor a 1 año </td>
        <td style="text-align: center; ">
            <input type="radio" name="horizonte-inversion" id="horizonte-inversion" value="{{ rand(111, 999) . 1 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Mi horizonte de inversión es menor a un mes</td>
        <td style="text-align: center; ">
            <input type="radio" name="horizonte-inversion" id="horizonte-inversion" value="{{ rand(111, 999) . 0 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    </tbody>
</table>