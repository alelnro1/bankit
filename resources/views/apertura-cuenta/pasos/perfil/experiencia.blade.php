<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th  >Experiencia</th>
        <th  >Escala</th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td>{{ dump($titular) }}Llevo invirtiendo en bolsa más de 3 años</td>
        <td style="text-align: center;">
            <input type="radio" name="experiencia" id="experiencia" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Llevo invirtiendo en bolsa entre 1 y 3 años</td>
        <td style="text-align: center; ">
            <input type="radio" name="experiencia" id="experiencia" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>Llevo invirtiendo en bolsa menos de 1 año</td>
        <td style="text-align: center; ">
            <input type="radio" name="experiencia" id="experiencia" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    <tr>
        <td>No he invertido en bolsa aún</td>
        <td style="text-align: center; ">
            <input type="radio" name="experiencia" id="experiencia" value="{{ rand(111, 999) . 3 . rand(1111, 9999) }}" class="item-perfil" style="cursor: pointer !important;">
        </td>
    </tr>
    </tbody>
</table>