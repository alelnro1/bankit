<div class="rms-form-wizard">
    <!--Wizard Step Navigation Start-->
    <div class="rms-step-section" data-step-counter="true" data-step-image="false">
        <ul class="rms-multistep-progressbar">

            @if (!$es_persona_fisica)
                <li class="rms-step" id="datos-societarios">
                    <span class="step-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                    <span class="step-title" style="font-size: 65%;">Datos societarios</span>
                </li>
            @endif

            <li class="rms-step" id="datos-titulares">
                <span class="step-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                <span class="step-title" style="font-size: 65%;">Datos titulares</span>
            </li>

            <li class="rms-step" id="cuentas-bancarias">
                <span class="step-icon ml10"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                <span class="step-title" style="font-size: 65%;">Cuentas bancarias</span>
            </li>


            <li class="rms-step" id="perfil-comitente">
                <span class="step-icon"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                <span class="step-title" style="font-size: 65%;">Perfil de Riesgo del inversor</span>
            </li>

            <li class="rms-step" id="listo">
                <span class="step-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
                <span class="step-title"style="font-size: 65%;">Listo</span>
            </li>
        </ul>
    </div>
</div>