<div class="rms-content-area">
    <div class="rms-content-title">
        <div class="panel-heading" style="width:100%; height: 50px;">
            <div class="leftside-title">
                <b> 
                    <span class="title-white">Perfil de Riesgo del inversor</span>
                </b> 
            </div>
            <div class="step-label">
                @if ($es_persona_fisica)
                    Paso 3
                @else
                    Paso 4
                @endif
            </div>
        </div>
        <div class="alert alert-info">Complete el formulario para poder determinar su perfil de inversor</div>
    </div>
    
    

    <div class="rms-content-body"
         data-guardar-perfil-url="{{ route('apertura-cuenta.perfil.guardar') }}"
         data-generar-pdf-url="{{ route('apertura-cuenta.generar-pdf') }}"
         id="perfil-form">
        <div class="col-xs-12">
            @if (Auth::guard('backend')->user())
                <input type="hidden" name="prospecto_id" id="prospecto_id" value="{{ $prospecto_id }}">
            @endif

            @foreach ($categorias_preguntas_perfil as $categoria => $preguntas)
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th class="col-xs-10">{{ $categoria }}</th>
                        <th class="col-xs-2">Escala</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($preguntas as $key => $pregunta)
                        <tr>
                            <td>{{ $pregunta->pregunta }} </td>
                            <td style="text-align: center;">
                                <input type="radio" name="{{ $pregunta->slug }}"
                                       data-id="{{ $pregunta->id }}"
                                       id="{{ $pregunta->slug }}"
                                       value="{{ rand(111, 999) . $pregunta->valor . rand(1111, 9999) }}"
                                       class="item-perfil"
                                        @if (in_array($pregunta->id, $respuestas_preguntas_perfil))
                                            checked
                                       @endif>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endforeach
            <!-- Riesgo -->
            {{--}}@include('apertura-cuenta.pasos.perfil.riesgo')

            <!-- Tiempo -->
            @include('apertura-cuenta.pasos.perfil.tiempo')

            <!-- Horizonte de Inversión -->
             @include('apertura-cuenta.pasos.perfil.horizonte-de-inversion') 

            <!-- Horizonte de Inversión -->
            @include('apertura-cuenta.pasos.perfil.experiencia')

            <!-- Información -->
            @include('apertura-cuenta.pasos.perfil.informacion')--}}

            <hr>

            <div class="row">
                <div class="col-xs-7"></div>
                <div class="col-xs-5">
                    <div class="alert alert-info">
                        <span id="perfil-riesgo" @if (!$perfil) style="display: none;" @endif>
                            Su perfil de riesgo es <strong><span id="valor-perfil-riesgo">{{ $perfil }}</span></strong>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>