@if (Auth::guard('backend')->user())

    <div class="rms-content-title">
        <div class="panel-heading" style="width:100%; height: 50px;">
            <div class="leftside-title">
                <b> 
                    <span class="title-white">Último Paso</span>
                </b> 
            </div>
        </div>
    </div>

    <div class="rms-content-body">
        <div class="row">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Generación del Comitente</legend>

                                <form action="" type="POST" id="generacion-comitente">
                                    <input type="hidden" name="prospecto_id" value="{{ $prospecto_id }}">
                                    <div class="col-sm-12">
                                        <div class="form-group {{ $errors->has('moneda_id') ? ' has-error' : '' }}">
                                            {{-- 
                                            <label for="descripcion">Nombre del Comitente</label>

                                            <div>
                                                <input type="text" step="1" name="descripcion" 
                                                        class="form-control"

                                                       placeholder="Ingrese el nombre del comitente">
                                            </div>
                                            --}}
                                            <input type="hidden" step="1" name="descripcion">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="sucursal">Sucursal</label>

                                            <div>
                                                <input type="number" step="1" name="sucursal" class="form-control"
                                                       placeholder="Sucursal" value="100">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-7">
                                        <div class="form-group {{ $errors->has('moneda_id') ? ' has-error' : '' }}">
                                            <label for="numero_comitente">Número Comitente</label>

                                            <div>
                                                <input type="number" step="1" name="numero_comitente" class="form-control"
                                                       placeholder="Ingrese el número del comitente">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="digito_verificador">Digito</label>

                                            <div>
                                                <input type="number" step="1" name="digito_verificador" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

    </div>
@else
    <div class="rms-content-title">
        <div class="panel-heading" style="width:100%; height: 50px;">
            <div class="leftside-title">
                <b> 
                    <span class="title-white">Último Paso</span>
                </b> 
            </div>
        </div>

        <div class="alert alert-info">
            Haga click en finalizar proceso para enviar la solicitud de apertura de cuenta
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="rms-content-body">
                <object data="" type="application/pdf" width="100%" height="450" id="pdf-tyc">
                    <p>Alternative text - include a link <a href="">to the PDF!</a></p>
                </object>
            </div>

            <hr>

            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-1" style="margin-right: -30px">
                        <input type="checkbox" class="checkbox" id="confirmar-tyc">
                    </div>
                    <div class="col-xs-11">
                        Para finalizar el proceso debe leer y aceptar los Términos y Condiciones.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif