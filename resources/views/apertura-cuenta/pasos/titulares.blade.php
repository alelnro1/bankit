<div class="rms-content-area">
    <div class="rms-content-title">
        <div class="panel-heading" style="width:100%; height: 50px;">
            <div class="leftside-title">
                <b> 
                    <span class="title-white">Datos de los titulares</span>
                </b> 
            </div>
            <div class="step-label">
                @if ($es_persona_fisica)
                    Paso 1
                @else
                    Paso 2
                @endif
            </div>
        </div>
        <div class="alert alert-info">
           En este apartado deberá informar los datos sobre los titulares de la cuenta.
        </div>
    </div>



    <div class="rms-content-body">
        <input type="hidden" name="prospecto_id" class="prospecto_id" value="{{ $prospecto_id }}">

        @if (count($titulares) > 0)
            <div class="accordion" id="titulares">
                @foreach ($titulares as $titular)
                    @if ($titular->pivot->agregado_por_dni && Auth::guard('apertura-cuenta')->user())
                        @include('apertura-cuenta.pasos.titulares.titular-existente', ['titular' => $titular])
                    @else
                        @include('apertura-cuenta.pasos.titulares.titular-nuevo', ['titular' => $titular])
                    @endif
                @endforeach
            </div>
        @else
            <div class="accordion" id="titulares"></div>
        @endif

        <div class="help-block" id="sin-titulares" style="display: none;">
            Aún no hay titulares. Agregue el primero haciendo <a href="#" class="nuevo-titular">click aquí</a>
        </div>

        <hr>

        <span style="float: right" class="agregar_persona">
                      <button id="btn-agregar-persona" class="btn btn-success nuevo-titular">
                          <i class="fas fa-plus-circle"></i> Agregar persona
                      </button>
                    </span>
    </div>
</div>