<div>
    <form id="form_cuentas_bancarias_pesos" method="POST"
          @if (isset($cuenta_bancaria_pesos))
          data-saved="true"
          @else
          data-saved=""
            @endif>
        <input type="hidden" name="prospecto_id" value="{{ $prospecto_id }}">
        <div class="row">
            <div class="col-xs-12">
                <div class="help-block alert alert-info">
                    Para comenzar a operar ingresá tu cuenta bancaria.

                </div>

                <div class="col-xs-6">
                    <div class="inpt-form-group">
                        <label for="tipo_cuenta_bancaria" class=" campoObligatorio">Tipo de Cuenta</label>

                        <select name="cuenta_bancaria_tipo_id" id="cuenta_bancaria_tipo_id" class="form-control"
                                title="Tipo de Cuenta">
                            @foreach ($tipos_de_cuentas as $tipo_cuenta)
                                <option value="{{ $tipo_cuenta->id }}" data-cod-bcra="{{ $tipo_cuenta->cod_bcra }}"
                                        @if ($cuenta_bancaria_pesos)
                                        @if ($cuenta_bancaria_pesos->getTipoCuentaId() == $tipo_cuenta->id)
                                        selected
                                        @endif
                                        @endif>
                                    {{ $tipo_cuenta->descripcion }}
                                </option>
                            @endforeach
                        </select>
                        
                    </div>

                </div>

                <div class="col-xs-12">
                    <div class="col-xs-6">
                            <div class="inpt-form-group">
                                <label for="numero_cuenta" class=" campoObligatorio">N° de CBU
                                    <br>
                                    <a href="#" id="buscar-cuenta-cbu">
                                        <i class="fas fa-search" tile="Buscar Informacíon de la cuenta"></i>
                                        Click aqui para buscar el alias de la cuenta
                                    </a>

                                </label>
                            
                                <input type="number" class="form-control" name="cbu" id="cbu"
                                       value="{{ $cuenta_bancaria_pesos->cbu or "" }}">

                                <span class="error-help-block help-block"></span>
                            </div>
                        </div>

                    <div class="col-xs-6">
                            <label for="numero_cuenta" class=" campoObligatorio">Alias
                                <br>
                                <a href="#" id="buscar-cuenta-alias">
                                    <i class="fas fa-search" tile="Buscar Informacíon de la cuenta"></i>
                                    Click aqui para buscar el cbu de la cuenta
                                </a>

                            </label>

                            <div class="inpt-form-group">
                                <input type="text" class="form-control" name="alias" id="alias"
                                       value="{{ $cuenta_bancaria_pesos->alias or "" }}">
                                       
                                <span class="error-help-block help-block"></span>
                            </div>
                    </div>
                </div>


                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <div class="inpt-form-group">
                            <label for="banco" class=" campoObligatorio">Banco</label>

                            <select name="banco_id" id="banco_id" class="form-control"
                                    title="Tipo de Cuenta">
                                    <option value=""> Seleccione una opcion...</option>
                                @foreach ($bancos as $banco)
                                    <option value="{{ $banco->id }}" data-cod-banco="{{ $banco->cod_banco }}"
                                            @if ($cuenta_bancaria_pesos)
                                            @if ($cuenta_bancaria_pesos->getBancoId() == $banco->id)
                                            selected
                                            @endif
                                            @endif>
                                        {{ $banco->descripcion }}
                                    </option>
                                @endforeach
                            </select>

                            <span class="error-help-block help-block"></span>
                        </div>
                    </div>
                </div>



                <input type="hidden" name="cuenta_bancaria_id" class="cuenta_bancaria_id"
                       value="{{ $cuenta_bancaria_pesos->id or "" }}">

                <input type="hidden" name="cuenta_pesos" value="true">

            </div>
        </div>

        <hr>

        <button class="btn btn-success guardar-cuenta-bancaria  pull-right"
                data-guardar-titular-url="{{ route('apertura-cuenta.cuentas-bancarias.guardar') }}">
            <i class="far fa-save"></i>
            Guardar cuenta
        </button>
    </form>
</div>