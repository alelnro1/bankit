<div class="rms-content-area">
    <div class="rms-content-title">
        <div class="leftside-title"><b> <i class="fa fa-user" aria-hidden="true"></i> Datos</b> Societarios</div>
        <div class="step-label">Paso 1</div>
    </div>
    <div class="alert alert-info">En este apartado deberá informar los datos sobre los titulares de la cuenta.</div>

    <div class="rms-content-body">
        <form id="datos-societarios-form" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="razon_social">Razón Social</label>
                        <div class="inpt-group">
                            <input type="text" name="razon_social" id="razon_social"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->razon_social or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="fecha_constitucion">Fecha Constitución</label>
                        <div class="inpt-group">
                            <input type="text" name="fecha_constitucion" id="fecha_constitucion"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->fecha_constitucion or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="cuit">CUIT</label>
                        <div class="inpt-group">
                            <input type="number" name="cuit" id="cuit"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->cuit or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="codigo_area">Código de Área</label>
                        <div class="inpt-group">
                            <input type="text" name="codigo_area" id="codigo_area"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->codigo_area or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="prefijo_celular">Prefijo Celular</label>
                        <div class="inpt-group">
                            <input type="text" name="prefijo_celular" id="prefijo_celular"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->prefijo_celular or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="telefono_celular">Teléfono Celular</label>
                        <div class="inpt-group">
                            <input type="text" name="telefono_celular" id="telefono_celular"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->telefono_celular or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="calle">Calle</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[calle]" id="calle"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->Direccion->calle or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="altura">Altura</label>
                        <div class="inpt-group">
                            <input type="number" name="direccion[altura]" id="altura"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->direccion['altura']  or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="piso">Piso</label>
                        <div class="inpt-group">
                            <input type="number" name="direccion[piso]" id="piso"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->direccion['piso'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="inpt-form-group">
                        <label for="departamento">Departamento</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[departamento]" id="departamento"
                                   class="inpt-control form-control required"
                                   value="{{ $sociedad->direccion['departamento'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="localidad">Localidad</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[localidad]" id="localidad" class="form-control"
                                   value="{{ $sociedad->direccion['localidad'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="codigo_postal">Codigo Postal</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[codigo_postal]" id="codigo_postal" class="form-control"
                                   value="{{ $sociedad->direccion['codigo_postal'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="provincia">Provincia</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[provincia]" id="provincia" class="form-control"
                                   value="{{ $sociedad->direccion['provincia'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="inpt-form-group">
                        <label for="pais">Pais</label>
                        <div class="inpt-group">
                            <input type="text" name="direccion[pais]" id="pais" class="form-control"
                                   value="{{ $sociedad->direccion['pais'] or "" }}">
                        </div>

                        <span class="error-help-block help-block"></span>
                    </div>
                </div>
            </div>
        </form>

        <hr>

        <button class="btn btn-success" id="guardar-datos-societarios"
                data-guardar-url="{{ route('apertura-cuenta.datos-societarios.guardar') }}">
            <i class="far fa-save"></i>
            Guardar datos
        </button>
    </div>
</div>