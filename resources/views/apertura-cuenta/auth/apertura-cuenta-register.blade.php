@extends('layouts.apertura-cuenta-app')

@section('content')
    <div class="panel-heading">Apertura de Cuenta</div>

    <div class="panel-body">
        {{--<div class="alert alert-info">
            Si ya ha comenzado el proceso de <strong>apertura de cuenta</strong> inicie sesión con los datos provistos
            haciendo <a href="{{ route('apertura-cuenta.login') }}">click aqui</a>
        </div>--}}

        <form class="form-horizontal" method="POST" action="{{ route('apertura-cuenta.register.procesar') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('tipo_prospecto') ? ' has-error' : '' }}">
                <label for="tipo_prospecto" class="col-md-4 control-label">Tipo Persona</label>

                <div class="col-md-6">
                    <select name="tipo_prospecto" id="" class="form-control">
                        @foreach ($tipos_prospectos as $tipo_prospecto)
                            <option value="{{ $tipo_prospecto->id }}">{{ $tipo_prospecto->getDescripcion() }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('tipo_prospecto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tipo_prospecto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">Correo Electronico</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Contraseña</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Repetir contraseña</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Siguiente
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection