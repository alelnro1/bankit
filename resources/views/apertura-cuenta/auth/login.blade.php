@extends('layouts.apertura-cuenta-app')

@section('content')
    <div class="panel-heading">Apertura de Cuenta</div>
    <script type="text/javascript">
        console.log("1");
        window.location.href="{{ url('/') }}";
    </script>
    <div class="panel-body">
        @if (Session::has('proceso_finalizado'))
            <div class="alert alert-success">
                Proceso de Apertura de Cuenta finalizado. En breve nos estaremos contactando con usted con novedades.
            </div>
        @endif
        
        @if (Session::has('recuperar-password'))
            <div class="alert alert-success">
                Se envió un mail a su casilla de correo con una nueva contraseña temporal para ingresar al sistema.
            </div>
        @endif

        <div class="alert alert-info">
            Si ya ha comenzado el proceso de <strong>apertura de cuenta</strong> inicie sesión con los datos provistos.
            De lo contrario, haga click <a href="{{ route('apertura-cuenta.register') }}">aquí</a> para comenzar dicho proceso.
        </div>

        <form class="form-horizontal" method="POST" action="{{ route('apertura-cuenta.login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ Session::has('email_error') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">Correo Electronico</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                           autofocus>

                    @if (Session::has('email_error'))
                        <span class="help-block">
                                        <strong>{{ Session::get('email_error') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Contraseña</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Ingresa
                    </button>

                    <a class="btn btn-info" href="{{ route('apertura-cuenta.password.request') }}">
                        ¿Olvidaste tu contraseña?
                    </a>
                    
                </div>
            </div>
        </form>
    </div>
@endsection
