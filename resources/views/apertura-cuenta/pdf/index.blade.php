<!DOCTYPE html>
<html>
<head>

    <style>
		.wrapper {
			display:inline-block;
			width: 100%;
		}        

		.subWrapper {
			display:inline-block;
			
		} 

		*{
			font-family: "Verdana",  sans;
		}  

        p {
            text-align: justify;
            text-justify: inter-word;
        }
    
    </style>

    <meta charset="UTF-8">

</head>
<body>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2">
                <span style="color: rgb(132, 133, 135) !important;">
	                BANCO SAENZ S.A. CUIT 30-53467243-4
	                Esmeralda 83 – CABA. C.P. C1035ABA
                </span>
            </div>

        </div>
    </div>

    <h2 style="text-align: center;">
        “Agente de Liquidación y Compensación y Agente de Negociación Propio registrado bajo el N°
        114 de la Comisión Nacional de Valores”
    </h2>

    <h2 style="color: rgb(2, 54, 105); text-align: center;">
    	SOLICITUD DE APERTURA DE CUENTA – PERSONA HUMANA
   	</h2>

    <div class="wrapper">
    	<div class="subWrapper" style="width: 33%; ">
    		Fecha:
    	</div>
    	<div class="subWrapper" style="width: 33%;">
    		Uso de Firma: 
    	</div>
    	<div class="subWrapper" style="width: 33%;">
    		Cuenta Numero:
    	</div>
    </div>

    <fieldset>
        <legend>Titulares</legend>

        @include('apertura-cuenta.pdf.titulares', ['titulares' => $titulares])
    </fieldset>
    <fieldset>
    	<div>
    		<p>
    			En Buenos Aires, a los {{ date("d") }} días del mes de {{ $mes_actual }} de {{ date("Y") }}, entre BANCO SÁENZ S.A. y el/los titular/es indicado/s precedentemente (en forma indistinta o conjunta, el “Cliente”).
    		</p>

    		<p>
				<strong>1. Apertura de Cuenta:</strong>
    		</p>

    		<p>
				El Cliente solicita a BANCO SÁENZ la apertura de una cuenta (la “Cuenta”) y realice la custodia de los fondos y los valores negociables que el Cliente entregue a BANCO SÁENZ o que resulten acreditados en la Cuenta para ser custodiados por este último, en el país y en el exterior, en relación con la operatoria del Cliente y en los términos y condiciones de la ley 26.831, su decreto reglamentario, las Normas de la Comisión Nacional de Valores (“CNV”; - t. o. 2013 -; las “Normas CNV”), la ley 20.643 y sus normas reglamentarias, la ley 25.246 y las resoluciones de la Unidad de Información Financiera (“UIF”), y las disposiciones reglamentarias del/de los mercado/s habilitados por la CNV de los que BANCO SÁENZ sea miembro (en conjunto, la “Normativa Aplicable”). El Cliente podrá realizar la apertura de la cuenta para operar con Fondos Comunes de Inversión de manera online a través de la plataforma y posteriormente se acercará al Banco con los formularios completos.
    		</p>

    		<p>
				<strong>2. Origen de los activos:</strong>
    		</p>

    		<p>
				2.1. BANCO SÁENZ se reserva el derecho de aceptar o rechazar a su solo criterio la recepción y custodia de determinados activos que considere pudieran provenir de fuentes no lícitas, en cumplimiento de las observaciones impuestas por los organismos de control. BANCO SÁENZ queda facultada para solicitar al Cliente toda la información que sea necesaria a fin de cumplir con las disposiciones sobre prevención de los delitos de lavado de dinero proveniente de actividades ilícitas y de financiación del terrorismo, sobre la base de la Normativa Aplicable; así, el Cliente tendrá la obligación de proporcionar a BANCO SÁENZ toda la información y/o documentación que éste le requiera, autorizando expresamente a BANCO SÁENZ a mantener dicha información en una base de datos en los términos requeridos por la Normativa Aplicable y a suministrarla a la CNV, a la UIF o a cualquier otro organismo competente que así lo requiriese o frente al cual BANCO SÁENZ estuviera obligada de conformidad con la Normativa Aplicable.
    		</p>

    		<p>
				2.2. El Cliente se responsabiliza ante BANCO SÁENZ y los futuros titulares de los valores negociables por el origen de los mismos y por toda oposición, adulteración, falsificación, embargo, gravamen o traba de cualquier naturaleza que lo afecte, debiendo en su caso proceder a sólo requerimiento de BANCO SÁENZ al reemplazo de los mismos. Ante cualquier incumplimiento por parte del Cliente de las obligaciones asumidas, BANCO SÁENZ queda facultado para retener la cantidad de valores negociables necesarios para compensar una suma equivalente a la traba u oposición reclamada, sin perjuicio del derecho de exigirle al Cliente el pago de los daños y perjuicios ocasionados.
    		</p>

			<p>
				<strong>3. Ejecucion de las operaciones:</strong>
    		</p>

    		<p>
				3.1. Débitos. Cuando el Cliente impartiera instrucciones a BANCO SÁENZ para realizar operaciones que impliquen débitos (compras y suscripciones), deberá mantener indefectiblemente en su Cuenta los fondos necesarios para abonarlas.
    		</p>

    		<p>
				3.2. Créditos. Las operaciones que impliquen créditos (ventas, percepción de servicios de renta y amortización, dividendos, rescates) serán ejecutadas por BANCO SÁENZ siempre que a la fecha correspondiente los valores negociables se encuentren depositados y a nombre de BANCO SÁENZ en el correspondiente Agente de Depósito Colectivo (tal como Caja de Valores S.A.) o Agente de Custodia, Registro y Pago, o en la depositaria de valores del exterior en su caso.
    		</p>

    		<p>
				3.3. Entrega de fondos. El Cliente deberá entregar a BANCO SÁENZ los fondos necesarios para realizar las operaciones que impliquen débito mediante débito de la/s cuenta/s bancaria/s en BANCO SÁENZ que el mismo indique (la/s “Cuenta/s Bancaria/s”).
    		</p>

    		<p>
				3.4. Depósito en cuenta bancaria. Si el Cliente lo requiriese, BANCO SÁENZ depositará los fondos existentes en la/s Cuenta/s Bancaria/s, sirviendo la constancia de depósito o transferencia de suficiente y eficaz recibo.
    		</p>

    		<p>
    			3.5. Autorización. El Cliente autoriza a BANCO SÁENZ, a que retire por su cuenta y orden de su Cuenta todos los fondos necesarios para cumplir con los pagos que las operaciones requiriesen y conforme a prácticas de mercado, la venta de valores negociables de su Cuenta, en el caso de que esta arrojase saldos deudores exigibles por cualquier concepto o circunstancia, hasta cubrir dichos saldos, sin la necesidad de previa intimación. El Cliente autoriza a BANCO SAENZ a compensar saldos deudores con saldos acreedores de otras cuentas en las que él sea titular en BANCO SÁENZ
    		</p>

    		<p>
    			3.6. Parámetros de precio y plazo. El Cliente entiende y acepta que BANCO SÁENZ ejecutará las instrucciones de adquisición o venta dentro de los parámetros de precio y plazos de ejecución determinados por el Cliente. Si el Cliente no hubiere determinado dichos parámetros, el Cliente entiende y acepta que las instrucciones se ejecutarán al precio y en el plazo disponible en el mercado al momento de ejecutarse la operación por BANCO SÁENZ
    		</p>

    		<p>
    			3.7. Ejecución de las instrucciones. El Cliente entiende y acepta que BANCO SÁENZ ejecutará las instrucciones para operar en los días y horas habilitados por los mercados del
				país o del exterior en los que tales operaciones puedan ejecutarse.
    		</p>

    		<p>
    			3.8. Retiro de valores negociables. El Cliente podrá efectuar retiros de valores negociables de su Cuenta a su solo requerimiento a BANCO SÁENZ, sujeto a la naturaleza y los términos y condiciones de las inversiones y conforme al presente.
    		</p>

    		<p>
    			3.9. Instrucciones del Cliente. Eventual otorgamiento de autorización general. BANCO SÁENZ operará por cuenta del Cliente en base a las instrucciones que éste imparta, sean escritas, verbales o recibidas por medio electrónicos (correo electrónico, fax, etc.), conforme a la Normativa Aplicable. El Cliente puede – pero no será obligado por BANCO SÁENZ – otorgar (mediante la firma de la Cláusula Especial inserta más abajo) una autorización de carácter general para que BANCO SÁENZ actúe en su nombre, que en su caso el Cliente podrá revocar o limitar en cualquier momento. La ausencia de esta autorización hará presumir, salvo prueba en contrario, que las operaciones realizadas a nombre o por cuenta del Cliente no contaron con el consentimiento de éste. La aceptación expresa sin reserva por parte del Cliente de la liquidación correspondiente podrá ser invocada como prueba en contrario a los fines previstos precedentemente. Por el contrario, la aceptación tácita por parte del Cliente de la liquidación correspondiente a una operación que no contó con su autorización previa, no podrá ser invocada por BANCO SÁENZ como prueba de la conformidad del Cliente a la operación efectuada sin su previa autorización.
    		</p>

    		<p>
    			3.10. Contratación con terceros. En el cumplimiento de las instrucciones bajo la presente BANCO SÁENZ podrá contratar con terceros a nombre propio o del Cliente.
    		</p>

    		<p>
    			3.11. Registro de instrucciones. El Cliente autoriza a BANCO SÁENZ a grabar las conversaciones telefónicas y a que éste registre por cualquier medio las instrucciones que el Cliente imparta, otorgando las partes a tales registraciones pleno valor probatorio.
    		</p>
    		
    		<p>
    			3.12. Errores de comunicación y de transmisión. Los riesgos de cualquier tipo, tales como pérdidas, retrasos, malentendidos, errores, alteraciones, expediciones duplicadas, imputables a los corresponsales de BANCO SÁENZ o resultantes de la utilización, interrupción, perturbaciones o fallos de cualquier tipo en los sistemas de comunicación y transmisión, y de cualquier empresa de transporte serán por cuenta exclusiva del Cliente, salvo negligencia grave de BANCO SÁENZ En caso de litigio, recaerá en el Cliente la carga de la prueba.
    		</p>
    		
    		<p>
    			3.13. Depósito de Valores Negociables. BANCO SÁENZ se encuentra autorizado para depositar los valores negociables en un Agente de Depósito Colectivo habilitado (tal como Caja de Valores S.A.) bajo el sistema de depósito colectivo y/o en la cuenta que corresponda en la depositaria de valores del exterior.
    		</p>

			<p>
				<strong>4. Facultades del Banco Saenz:</strong>
    		</p>

    		<p>
    			4.1. Conforme a las instrucciones que imparta el Cliente – salvo lo previsto en la Cláusula Especial - a los efectos de la ejecución del presente mandato el Cliente otorga amplias facultades a BANCO SÁENZ para que éste, actuando en forma directa o a través de terceros, pueda realizar con los valores, otros activos y fondos que integran la cartera del Cliente todo tipo de operaciones por cuenta y orden del Cliente, en moneda argentina o extranjera, quedando expresamente autorizado para firmar en nombre del Cliente la documentación pertinente, y realizar asimismo todos los demás actos que BANCO SÁENZ considere convenientes para la preservación de los intereses del Cliente, siempre conforme a la normativa aplicable. Asimismo, el Cliente expresamente autoriza a BANCO SÁENZ para cobrar y percibir cualquier valor o suma de dinero proveniente del cobro de dividendos, servicios de renta o amortización, rescates, reintegro de capital, o cualquier otro concepto, otorgando a nombre del Cliente los recibos correspondientes. Sin perjuicio de ello, BANCO SÁENZ podrá solicitar al Cliente que éste le extienda recibos, confirmaciones y demás documentación que BANCO SÁENZ le solicite con relación a las operaciones realizadas
    		</p>

    		<p>
    			4.2. BANCO SÁENZ podrá solicitar al Cliente que éste otorgue autorizaciones, permisos y poderes que pueden ser necesarios para representar adecuadamente al Cliente en la realización de cualquier acto bajo el presente mandato.
    		</p>

    		<p>
    			4.3. BANCO SÁENZ estará facultada para realizar cualquier hecho o acto, incluyendo la disposición o adquisición de activos, la constitución y/o ejecución de garantías con cargo a los Clientes, de modo tal de limitar su responsabilidad y/o su exposición personal en el cumplimiento del mandato del Cliente. En particular, BANCO SÁENZ estará facultada para realizar tales actos en las transacciones de derivados y/o aquellas que requieran el cumplimiento del mantenimiento de márgenes de garantías que no sean respetados por el Cliente
    		</p>

    		<p>
    			4.4. BANCO SÁENZ estará facultado – conforme a la normativa aplicable, incluyendo la normativa cambiaria vigente - para efectuar las operaciones de compra y venta de divisas que correspondieran, según las características de las distintas operaciones en diferentes monedas efectuadas bajo la presente carta mandato, estando a cargo del Cliente todos los costos y gastos correspondientes al cambio de moneda y cualquier riesgo financiero inherente a estas operaciones. (REVISAR LEGALES)
    		</p>

    		<p>
    			<strong>
    				5. Obligaciones del Banco Saenz / Derechos del cliente
    			</strong>
    		</p>

    		<p>
    			Sin perjuicio de las obligaciones impuestas por la normativa aplicable, BANCO SÁENZ prestará servicios al Cliente conforme a las siguientes normas: a) Por cada operación entregará al Cliente una liquidación o comunicación que dará cuenta del resultado de la misma, conforme a los requisitos establecidos por las Normas de la CNV. b) Actuará con lealtad y con la diligencia de un buen hombre de negocios y se abstendrá de concertar operaciones que no sean reales. c) A pedido del Cliente expedirá certificación de las operaciones registradas que haya concertado por cuenta de aquél. d) En el ejercicio de sus funciones, BANCO SÁENZ deberá observar una conducta ejemplar, actuando en todo momento en forma leal y diligente con el Cliente. Con ese propósito se encuentra especialmente obligado a: (i) Anotar toda orden que se les encomiende, escrita o verbal, de modo tal que surja en forma adecuada del registro que deben llevar la oportunidad, cantidad, calidad, precio y modalidad en que la orden fue impartida y toda otra circunstancia relacionada con la operación que resulte necesaria para evitar confusión en las negociaciones. Las anotaciones mencionadas deberán ser volcadas diariamente a dicho registro una vez finalizada la jornada de operaciones. (ii) Ejecutar con diligencia las órdenes recibidas en los términos en que fueron impartidas; (iii) Otorgar absoluta prioridad al interés del Cliente, absteniéndose tanto de multiplicar transacciones en forma innecesaria y sin beneficio para BANCO SÁENZ como de incurrir en conflicto de intereses. En caso de existir conflicto de intereses entre distintos clientes, BANCO SÁENZ evitará privilegiar a cualquiera de ellos en particular; (iv) Tener a disposición del Cliente toda información que siendo de su conocimiento y no encontrándose amparada por el deber de reserva pudiera tener influencia directa y objetiva en la toma de decisiones. En los casos en que se le solicite asesoramiento, deberá prestarlo con lealtad; (v) En ningún caso podrá BANCO SÁENZ atribuirse a sí mismo uno o varios valores negociables, índices o activos negociados cuando el Cliente se los hubiera solicitado en idénticas o mejores condiciones, ni anteponer la compra o la venta de aquellos pertenecientes a BANCO SÁENZ a los del Cliente, cuando éste haya ordenado comprar o vender la misma clase de valor en idénticas o mejores condiciones.(vi) Realizar al Cliente, sin cargo, un cuestionario (Anexo C) con la finalidad de determinar su perfil de riesgo o nivel de tolerancia al riesgo, conforme a la Normativa Aplicable (el “Cuestionario”). En caso que BANCO SÁENZ advirtiese como inadecuada alguna inversión, en base al perfil de riesgo determinado para el Cliente, deberá dejar constancia documentada de su opinión adversa, de la comunicación de tal circunstancia al Cliente y de la opinión de este último al respecto. BANCO SÁENZ dejará constancia documentada en caso que el Cliente se rehusare a brindar la información requerida. Ambas circunstancias no representarán impedimento alguno para concretar las operaciones. (vii) Informar al Cliente que en www.bancosaenz.com.ar y en www.cnv.gob.ar puede consultar el Código de Conducta de BANCO SÁENZ, y que sin perjuicio de ello el Cliente podrá solicitar una copia impresa del mismo. 
    		</p>

    		<p>
    			<strong>
    				6. Reclamos del Cliente
    			</strong>
    		</p>

    		<p>
    			6.1. Los reclamos del Cliente serán atendidos por BANCO SÁENZ conforme se explicita en el Código de Conducta.
    		</p>

    		<p>
    			6.2. En el Anexo A se consigna una descripción de las disposiciones que gobiernan el FONDO DE GARANTÍA PARA RECLAMOS DE CLIENTES.
    		</p>

    		<p>
    			<strong>
    				7.Declaraciones del Cliente 
    			</strong>
    		</p>

    		<p>
    			7.1. El Cliente manifiesta tener conocimiento de la naturaleza de las operaciones que BANCO SÁENZ realizará en base a lo aquí estipulado y las instrucciones que el Cliente curse, y de los riesgos inherentes a cada una de esas operaciones. Asimismo, declara y asume que las inversiones a ser realizadas son de carácter especulativo y sujetas a múltiples variaciones, incluyendo condiciones de mercado, riesgo económico-financiero de los emisores de valores, etc. Consecuentemente el Cliente asume los riesgos originados en las inversiones que se realicen quedando expresamente aclarado y convenido que BANCO SÁENZ no garantiza la solvencia ni el rendimiento de las inversiones, ni responde por la existencia, legitimada y/o evicción debida por los enajenantes de tales inversiones.
    		</p>

    		<p>
    			7.2. El Cliente conoce que el incumplimiento de BANCO SÁENZ a sus obligaciones puede provocarle perjuicios, en supuestos tales como frustración de operaciones de inversión o desinversión, adquisición o venta de activos a precios mayores o menores que los de mercado, interpretación errónea de instrucciones impartidas por el cliente, demoras en la ejecución de las instrucciones, o disposición de los activos y fondos del cliente sin su conocimiento o con apartamiento de las instrucciones impartidas. Ello puede provocar pérdidas patrimoniales parciales o totales al cliente por las que será responsable BANCO SÁENZ por su culpa o dolo así declarado por resolución firme de tribunal competente. En tal caso el Cliente podrá reclamar el pago de la indemnización a que tuviera derecho con imputación al Fondo de Garantía para Reclamos de Clientes (ver pto. 6 y Anexo A).
    		</p>

    		<p>
    			7.3. El Cliente está obligado, entiende y acepta que es el único responsable por el conocimiento de las condiciones de emisión de los valores negociables respecto de los cuales efectúa instrucciones para operar, y exime a BANCO SÁENZ de toda responsabilidad que pudiera resultar por la adquisición, venta o mera titularidad de los mismos. En caso que el Cliente deseara obtener una copia del prospecto de los valores negociables de que se trate deberá solicitarlo al emisor o los colocadores autorizados del mismo.
    		</p>

    		<p>
    			7.4. El Cliente manifiesta tener conocimiento y acepta expresamente: a) que otros clientes de BANCO SÁENZ le han otorgado u otorgarán mandato a BANCO SÁENZ en los mismos o similares términos y condiciones expresados en la presente y acepta, con renuncia a reclamar la exclusividad por parte de BANCO SÁENZ, o alguna prioridad o preferencia frente a otros clientes en la ejecución de las operaciones; b) que BANCO SÁENZ, sus directores o empleados pueden estar vinculados con bancos u otras entidades depositarias, emisoras, underwriters, compradoras, vendedoras y/o intermediarias de los valores negociables que puedan integrar la cartera del Cliente; y c) que empresas vinculadas a BANCO SÁENZ, sus directores o empleados, puedan actuar como comprador, vendedor, depositarios, agente o intermediario en operaciones relacionadas con la cartera del Cliente y percibir remuneraciones por sus servicios. El Cliente renuncia expresamente a cualquier reclamo o acción fundada en un supuesto o potencial conflicto de intereses de la naturaleza arriba descripta.
    		</p>

    		<p>
    			7.5. El Cliente se compromete a mantener a BANCO SÁENZ indemne de todo daño y/o perjuicio que BANCO SÁENZ pueda sufrir como consecuencia directa o indirecta del cumplimiento del presente y/o de las instrucciones del cliente y/o de las transacciones que celebre para su beneficio.
    		</p>

    		<p>
    			7.6. El cliente reconoce y acepta que el sitio de Internet www.bancosaenz.com.ar propiedad de BANCO SÁENZ (el “Sitio de Internet”) es un sistema que depende en su funcionamiento de distintos recursos tecnológicos, y que el mismo puede sufrir caídas, cortes y/o interrupciones debido a dificultades técnicas o por cualquier otra circunstancia ajena a la voluntad de BANCO SÁENZ, y que la respuesta del sistema puede variar debido a varios factores incluyendo el rendimiento del sistema. El Cliente asume los riesgos de imposibilidad o demora en la utilización del Sitio de Internet que tales contingencias le pudieran generar. 
    		</p>

    		<p>
    			7.7. Las inversiones en cuotas del fondo no constituyen depósitos en Banco Sáenz S.A., a los fines de la Ley de Entidades Financieras ni cuenta con ninguna de las garantías que tales depósitos a la vista o a plazo puedan gozar de acuerdo a la legislación y reglamentación aplicables en materia de depósitos en entidades financieras. Asimismo, Banco Sáenz S.A. se encuentra impedido por normas de Banco Central de la República Argentina de asumir, tácita o expresamente, compromiso alguno en cuanto al mantenimiento, en cualquier momento, del valor del capital invertido, al rendimiento, al valor de rescate de las cuotapartes o al otorgamiento de liquidez a tal fin.
    		</p>

    		<p>
    			<strong>
    				8. Estado de Cuentas 
    			</strong>
    		</p>

    		<p>
    			Banco Saenz pondrá a disposición del Cliente un detalle de las operaciones realizadas y el saldo de cuenta en un documento impreso a emitir a tal fin y con la frecuencia que éste lo requiera según instrucción que brinde mediante la integración y firma del anexo E al presente. En el caso, que el Cliente lo exprese en forma manifiesta, mediante declaración incluida en anexo E al presente, dicho estado de cuenta/s podrá ser generado (en reemplazo del impreso) en forma electrónica y remitido al/ a los correos electrónicos indicado/s en el presente. Tal resumen de cuenta tendrá el valor de rendición de cuentas, acordando las partes que BANCO SÁENZ no tendrá la obligación de publicar en su Sitio de Internet ni enviar al Cliente los comprobantes de las transacciones que detalle en el mismo, sin perjuicio que dichos comprobantes estarán a disposición del cliente en las oficinas de BANCO SÁENZ Pasados treinta (30) días desde el último acceso del Cliente a su cuenta personal en Internet sin que el Cliente haya observado la información allí contenida por escrito y en forma fundada, se entenderá que el cliente ha prestado conformidad con dicha información, sin perjuicio de lo estipulado en el punto 3.9 in fine. El Cliente podrá solicitar por escrito y a su costo el envío de un resumen de cuenta impreso, el cual será enviado en forma trimestral por correo simple, al domicilio indicado en la presente. Asimismo, el cliente (o la persona que el Cliente indique por escrito), podrá retirar dicho resumen de cuenta trimestral de las oficinas de BANCO SÁENZ En ambos casos, pasados 30 días desde la recepción o entrega del resumen de cuenta sin que el cliente haya observado la información allí contenida por escrito y en forma fundada, se entenderá que el cliente ha prestado conformidad con dicha información, sin perjuicio de lo estipulado en el punto 3.9 in fine.
    		</p>

    		<p>
    			<strong>
    				9. Confidencialidad
    			</strong>
    		</p>

    		<p>
    			Banco Saenz se compromete a mantener con carácter confidencial toda la información relacionada con el Cliente, su cartera, y demás información adquirida como consecuencia de sus funciones bajo el presente mandato, en tanto que tal información no haya tomado estado público o le haya sido requerida a BANCO SÁENZ por alguna autoridad competente con facultades para ello, o deba divulgarse con motivo u ocasión de alguna operación (incluyendo expresamente a la CNV, la UIF el mercado habilitado por la CNV del que BANCO SÁENZ sea miembro).
    		</p>

    		<p>
    			<strong>
    				10. Comisiones y Gastos
    			</strong>
    		</p>

    		<p>
    			10.1 Por sus servicios BANCO SÁENZ tendrá derecho a percibir las comisiones que se detallan en el Anexo B, y que en forma actualizada figuran y figurarán en el Sitio de Internet. BANCO SÁENZ queda expresamente autorizado para debitar las comisiones que se devenguen directamente de la Cuenta, liquidando los activos que sean necesarios a tal efecto, sin necesidad de aviso previo al Cliente. Si por estar afectados a alguna inversión no hubiere fondos disponibles en el momento que BANCO SÁENZ deba realizar el débito, dicho débito se hará efectivo al momento de quedar disponibles los mismos o vendiendo BANCO SÁENZ los valores negociables necesarios para ello, sin perjuicio del derecho de BANCO SÁENZ de exigir el pago directamente al Cliente.
    		</p>

    		<p>
    			10.2. BANCO SÁENZ podrá modificar o ampliar las comisiones que perciba, haciéndolo saber al Cliente mediante comunicación formal y Sitio de Internet en el destacada en el Sitio de Internet. Las modificaciones o ampliaciones, para el caso que importen un aumento de costos para el Cliente, serán de aplicación a partir de los 60 (sesenta) días posteriores a la comunicación. No obstante, toda instrucción de operaciones por parte del Cliente posterior a dicha publicación y anterior al vencimiento del plazo indicado importará su aceptación a las modificaciones.
    		</p>

    		<p>
    			10.3. Quedaran a cargo del Cliente todas las comisiones y honorarios de BANCO SÁENZ y/o terceros, todo timbrado, sellado, impuesto, derecho, costo, honorario, tarifa, penalidades, cargos, y/o gasto actual o futuro de cualquier tipo que sea consecuencia directa o indirecta de las operaciones efectuadas en virtud del presente mandato y de titularidad de los activos adquiridos. BANCO SÁENZ estará facultada para compensar contra los valores negociables y fondos del Cliente todos esos conceptos a cargo del Cliente.
    		</p>

    		<p>
    			10.4 El cliente manifiesta conocer ofertas de otros Bancos del sistema financiero para operaciones similares, pudiendo comparar precios en la web del BCRA www.bcra.gov.ar/Informacion_usuario.
    		</p>

    		<p>
    			<strong>
    				11. Rescision
    			</strong>
    		</p>

    		<p>
    			El Cliente y/o BANCO SÁENZ podrán, cualquiera de ellos y en cualquier momento, dar por terminado unilateralmente este contrato, mediante notificación en forma fehaciente a la otra parte, a los domicilios aquí consignados, produciendo efectos a partir de los sesenta (60) días de recibida la notificación, salvo respecto de las inversiones inmovilizadas, con relación las cuales terminara a las cuarenta y ocho (48) horas de la expiración del plazo de la respectiva inversión. El plazo de antelación se reducirá a dos (2) días si el cierre de la cuenta procediera por (a) aplicación de normativa legal o reglamentaria, u orden judicial, salvo que la norma u orden estableciera un plazo menor, o (b) ante cualquier incumplimiento por parte del Cliente, pudiendo en el ínterin la Sociedad suspender la ejecución de órdenes del Cliente de operaciones de inversión. A partir del momento en que se haga efectiva la terminación de los servicios, BANCO SÁENZ deberá poner a disposición del Cliente, dentro de un plazo de veinte (20) días, los valores negociables y los fondos depositados en la Cuenta, deducidos los impuestos, gastos, comisiones y cualquier otra suma adeudada por el Cliente a BANCO SÁENZ.
    		</p>

    		<p>
    			<strong>
    				12. Modificaciones
    			</strong>
    		</p>

    		<p>
    			BANCO SÁENZ sin perjuicio de lo estipulado para la modificación o ampliaciones de las comisiones, podrá modificar los términos y condiciones establecidas en cualquiera de las cláusulas del presente, previo informe por escrito al Cliente, con una anticipación no menor a sesenta (60) días hábiles anteriores a la fecha de su entrada en vigencia. Si dentro de dicho plazo el Cliente no observa por escrito las modificaciones, éstas se considerarán aceptadas por el Cliente.
    		</p>

    		<p>
    			<strong>
    				13. Autorizados
    			</strong>
    		</p>

    		<p>
    			13.1. La/s persona/s que se menciona/n seguidamente como autorizada/s o apoderado/ s estará/s autorizada/s, en su caso forma individual e indistinta, a efectuar en nombre del Cliente todos los actos que le correspondan a éste (el/los “Apoderado/s”). 
    		</p>

    		<p>
    			13.2. En el caso que el Cliente imponga límites a las facultades de los Apoderados, los mismos se consignarán en el rubro “Observaciones”.
    		</p>

    		<p>
    			13.3. El Cliente podrá revocar en cualquier momento total o parcialmente las facultades atribuidas a los Apoderados.
    		</p>

    		<table id="tableAutorizados">
    			<tr>
    				<th>Fecha </th>
    				<th>Apellido y Nombre </th>
    				<th>N° Documento </th>
    				<th>Firma Autorizado </th>
    				<th>Firma Autorizante </th>
    			</tr>
    			<tr>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    			</tr>
    			<tr>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    			</tr>
				<tr>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    			</tr>
    			<tr>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    				<td></td>
    			</tr>
    		</table>

    		<p>
    			<strong>
    				14. Divisibilidad
    			</strong>
    		</p>

    		<p>
    			La invalidez, ilegalidad o nulidad, total o parcial, de cualquier cláusula del presente mandato no afectara la validez, legalidad o vigencia de ninguna otra cláusula del presente, Si cualquier disposición contenida en esta carta mandato se declara nula o invalida de acuerdo con la legislación aplicable, dicha disposición se considerará sustituida por aquella otra disposición valida que pueda cumplir con mayor rigor el objetivo de la primera. Las restantes disposiciones continuaran en plena vigencia.
    		</p>

    		<p>
    			<strong>
    				15. Jurisdiccion
    			</strong>
    		</p>

    		<p>
    			Las partes solucionarán de buena fe, por medio de consulta mutua, toda cuestión o disputa que surja de o en relación con la presente carta mandato, y tratarán de llegar a un acuerdo satisfactorio sobre dichas cuestiones o disputas. En caso de no arribarse a una solución de acuerdo a lo establecido precedentemente, las partes acuerdan que cualquier controversia, conflicto o reclamo emergente o relacionado a la celebración o cumplimiento del presente mandato será sometida a laudo inapelable del Tribunal de Arbitraje General de la Bolsa de Comercio de Buenos Aires, por las reglas del arbitraje de derecho. Ello sin perjuicio del derecho que al Cliente otorga el artículo 46 de la ley 26.831.
    		</p>

    		<p>
    			<strong>
    				16. Informacion y Normativa relativa al banco Saenz
    			</strong>
    		</p>
    		
    		<p>
    			El Cliente podrá acceder a la información y normativa relativa a la actividad de BANCO SÁENZ como Agente en el Sitio de Internet, en www.cnv.gob.ar, y en las páginas Web de los mercados de los que el agente es miembro.
    		</p>

    		<p>
    			<strong>
    				17. Constitucion de los domicilios
    			</strong>
    		</p>

    		<p>
    			El Cliente constituye como domicilio especial postal y de correo electrónico, cada uno de los indicados en el campo correspondiente de este formulario.Por el presente declaro/declaramos que todos los datos consignados son correctos, ciertos y verdaderos, y que he/hemos leído detenidamente y aceptado la totalidad de los términos y condiciones estipulados en el presente, y que he/hemos recibido una copia.
    		</p>

    		<p>
    			<strong>
    				18. Atencion a los usuarios financieros
    			</strong>
    		</p>

    		<p>
    			Banco Central de la República Argentina - B.C.R.A.
				En virtud de la normativa vigente del Banco Central de la República Argentina sobre “Protección a Usuarios de Servicios Financieros”, tomo conocimiento que por cualquier reclamo / queja o consulta al respecto, podré dirigirme a Responsables Titular y Suplente de Atención a Usuarios Financieros del Banco: personalmente a Bartolomé Mitre 800, Ciudad Autónoma de Buenos Aires, Código Postal C1036AAN; por teléfono al: 5279/4100 - llamando desde el lnterior del País al 011-5279/4100 y desde el Exterior al 0054-11- 5279/4100; por correo electrónico a: usuariosfinancieros@bsaenz.com.ar
				Los datos identificatorios de dichos Responsables, podré obtenerlos en la web del B.C.R.A. 
				www.bcra.gov.ar, ingresando por “Tipo de Entidades”/ “Bancarias y Financieras”, seleccionando BANCO SAENZ S.A./ Responsable de Atención al Usuario de Servicios Financieros y en la web del Banco www.bancosaenz.com.ar.
				Asimismo podré contactar a Responsables de Sucursales del Banco cuyos datos identificatorios y para contacto podré obtener en forma personal en las mismas y/o en la web www.bancosaenz.com.ar, ingresando en la opción “Sucursales”.
    		</p>

    		<p>
    			Por otra parte, me notifico de la existencia del Centro de Atención al Usuario de Servicios Financieros en la órbita del B.C.R.A. el que dispone de un número gratuito de acceso 0800-999-6663. Dicho Centro atenderá en forma telefónica -o por otros medios que en el futuro se establezcan- las consultas del público sobre la normativa emitida por la Institución y la información publicada en su sitio institucional en Internet, dando orientación a los usuarios de servicios financieros sobre la manera de canalizar los reclamos por la actuación del Banco en la atención.
				Asimismo tomo conocimiento que podré acceder al texto actualizado de la normativa del B.C.R.A. sobre “Protección a Usuarios Financieros” ingresando en web www.bcra.gov.ar en el siguiente link http://www.bcra.gov.ar/pdfs/texord/t-pusf.pdf
    		</p>

    		<p>
    			Cualquier cambio en las condiciones pactadas deberán ser notificadas por el Banco (nuevos conceptos y/o valores o reducción de prestaciones del servicio) en forma gratuita para el usuario de servicios financieros, siendo las mismas mediante documento escrito dirigido al domicilio real del usuario de servicios financieros  (en forma separada de cualquier otra información que remita el sujeto obligado(resúmenes de cuenta, boletines informativos, etc.), aun cuando forme parte de la misma remesa - o por vía electrónica en aquellos casos en que ésta fuera la forma de comunicación, con 60 días de anticipación. Asimismo en el cuerpo de estas notificaciones deberán incluirse las siguientes leyendas

    			<p>
    				Usted podrá optar por rescindir el contrato en cualquier momento antes de la entrada en vigencia del cambio y sin cargo alguno, sin perjuicio de que deberá cumplir las obligaciones pendientes a su cargo
    			</p>

    			<p>
    				Usted puede consultar el “Régimen de Transparencia” elaborado por el Banco Central de la República Argentina sobre la base de la información proporcionada por los sujetos obligados a fin de comparar los costos, características y requisitos de los productos y servicios financieros, ingresando a http://www.bcra.gob.ar/BCRAyVos/Regimen_de_transparencia.asp”
    			</p>

    			<p>
    				Asimismo le informamos que el Banco Central de la República Argentina, dispone de un área de Protección al Usuario de Servicios Financieros que podrá contactar ingresando a www.usuariosfinancieros.gob.ar
    			</p>
    		</p>

    		<p>
    			Tomo conocimiento que todo reclamo y/o consulta de respuesta no inmediata que formule el BANCO será resuelta dentro del plazo máximo de 20 días hábiles bancarios, salvo en los supuestos que corresponda reintegro de importes, en los términos del punto 2.3.5. de la comunicación “A” 5.460 del B.C.R.A Para los casos en que el BANCO me solicite documentación y/o información adicional de mi parte al formular el mismo, el plazo quedará suspendido hasta tanto haga entrega de aquella. De no cumplimentar con lo requerido dentro de los 5 días hábiles bancarios contados desde la fecha de la petición del BANCO, el reclamo se tendrá por desistido automáticamente. De persistir con el mismo, deberé iniciar un nuevo trámite en la forma establecida
    		</p>

    		<p>
    			<strong>
    				19. Comisiones
    			</strong>
    		</p>

    		<p>
    			Declaro recibir el Form. 905, Anexo “Detalle de Comisiones y Cargos vinculados con Títulos”, prestando su conformidad. 
    		</p>

    		<p>
    			<strong>
    				CLÁUSULA ESPECIAL. AUTORIZACION GENERAL DEL CLIENTE
    			</strong>
    		</p>

    		<p>
    			Por la presente el Cliente otorga a BANCO SÁENZ una autorización de carácter general en los términos del Anexo II del Capítulo I del Título VII de las Normas de la CNV, a fin de que, por su cuenta y orden, basado en las instrucciones recibidas del Cliente, realice todas las operaciones con valores negociables no prohibidas por la Normativa Aplicable.
				OPERACIONES INCLUIDAS: La presente autorización comprende todas las operaciones que BANCO SÁENZ pueda realizar para el Cliente, salvo las que se indiquen en un anexo, o se indiquen en el futuro por medio fehaciente. Incluye todos los activos o fondos existentes en la Cuenta a la fecha, o los que se reciban en el futuro, salvo los que se indiquen en anexo.
				NIVEL DE RIESGO ACEPTABLE PARA EL CLIENTE: a menos que se indique lo contrario en un anexo, el nivel de riesgo de las operaciones a realizar será el mismo definido para la Cuenta.
				CONDICIONES Y LIMITACIONES3:
				PLAZO DE VIGENCIA: esta autorización estará vigente mientras la Cuenta se encuentre abierta. No obstante, tanto el Cliente como BANCO SÁENZ podrán dejarla sin efecto, previa comunicación a la otra parte con una antelación de tres días hábiles.
				COSTOS DIFERENCIALES: En caso de haberlos, se los explicitan en el Anexo B.
				LAS OPERACIONES REALIZADAS EN VIRTUD DE ESTA AUTORIZACIÒN SE COMUNICARÀN AL CLIENTE CONFORME LO DISPUESTO EN EL PUNTO 8 DE LAS CONDICIONES GENERALES DE LA CUENTA.
				EN SU CASO, TODA INSTRUCCIÓN DEL CLIENTE PARA REALIZAR OPERACIONES NO INCLUIDAS EN ESTA AUTORIZACIÓN, O CON VALORES NEGOCIABLES NO ESPECIFICADOS, NO PRESUMIRÁ AMPLIACIÓN DE LA PRESENTE AUTORIZACIÓN PARA EL FUTURO.
				EN ANEXO A ESTA AUTORIZACIÓN GENERAL SE CONSIGNAN LOS VALORES NEGOCIABLES EXISTENTES A LA FECHA EN LA CUENTA DEL CLIENTE.
				LA PRESENTE AUTORIZACIÓN GENERAL NO ASEGURA RENDIMIENTOS DE NINGÚN TIPO: véase la cláusula 7 precedente.
				BANCO SÁENZ NO PODRÁ IMPONER AL CLIENTE EL OTORGAMIENTO DE LA PRESENTE AUTORIZACIÓN GENERAL, NI UTILIZAR SU NEGATIVA EN PERJUICIO DEL CUMPLIMIENTO DE LAS OBLIGACIONES QUE CORRESPONDEN A BANCO SÁENZ LA PRESENTE AUTORIZACIÓN GENERAL PODRÁ SER DEJADA SIN EFECTO EN CUALQUIER MOMENTO POR EL CLIENTE, MEDIANDO COMUNICACIÓN FEHACIENTE POR ESCRITO.

    		</p>

    		<p>
    			<strong>
    				ANEXO A – FONDO DE GARANTÍA PARA RECLAMOS DE CLIENTES
    			</strong>
    		</p>

    		<p>
    			El artículo 15 del Capítulo I del Título VI de las Normas de la CNV dispone lo siguiente:
				“Los Mercados deberán constituir un Fondo de Garantía, que podrá organizarse bajo la figura fiduciaria o cualquier otra modalidad que resulte aprobada por la Comisión, destinado a hacer frente a los compromisos no cumplidos por los Agentes miembros, originados en operaciones garantizadas, con el CINCUENTA POR CIENTO (50%) como mínimo de las utilidades anuales líquidas y realizadas.
				La Comisión podrá establecer un valor máximo cuando el monto total acumulado en el Fondo de Garantía obligatorio alcance razonable magnitud para cumplir con los objetivos fijados por la Ley N° 26.831.
				En caso que los Mercados utilicen los servicios de una Cámara Compensadora registrada ante la Comisión, que actúe como contraparte central de las operaciones garantizadas registradas, ésta también deberá constituir el Fondo de Garantía impuesto a los Mercados, conforme lo dispuesto por el Decreto N° 1023/13”.

    		</p>

    		<p>
    			<strong>
    				DISPOSICIONES DEL CAPÍTULO III DEL TÍTULO VII DE LAS NORMAS DE LA CNV
    			</strong>
    		</p>

    		<p>
    			FONDO DE GARANTÍA PARA RECLAMOS DE CLIENTES
				SECCIÓN I - DISPOSICIONES GENERALES.
				OBLIGACIÓN DE APORTAR AL FONDO. ARTÍCULO 1°. - Todos los agentes que registren operaciones, deberán aportar a un Fondo de Garantía para Reclamos de Clientes, que será administrado por los Mercados de los que sean miembros.
				REQUISITO. ARTÍCULO 2°. - La realización de aportes al Fondo de Garantía para Reclamos de Clientes será requisito para la actuación de estos agentes.
				CONFORMACIÓN. ARTÍCULO 3°. - El Fondo de Garantía para Reclamos de Clientes se conformará con: a) El valor del importe del Fondo de Garantía Especial que hubiese constituido el respectivo Mercado en funcionamiento con anterioridad a la Ley N° 26.831, y que surja de sus últimos estados contables anuales aprobados.
				b) Los aportes que efectúen los agentes que registran operaciones.
				c) Las rentas derivadas de la inversión que se efectúe del importe del Fondo de Garantía para Reclamos de Clientes.
				d) El recobro a los agentes de las sumas abonadas a clientes por los reclamos efectuados.
				IMPORTE DE APORTE MENSUALMENTE. ARTÍCULO 4°. - Los agentes deberán ingresar al Mercado del que sean miembros, dentro de los primeros DIEZ (10) días de cada mes calendario, en concepto de aporte al Fondo de Garantía para Reclamos de Clientes el importe que surja de aplicar, sobre los derechos de Mercado generados por cada agente el mes inmediato anterior, el porcentaje fijado por la Comisión, que será publicado en www.cnv.gob.ar.
				Conforme lo dispuesto por el Decreto N° 1023/13, hasta tanto el Fondo de Garantía para Reclamos de Clientes alcance el monto mínimo que establezca la Comisión, cada uno de los agentes aportantes deberán contratar un seguro de caución por el monto correspondiente fijado por este Organismo. 
				TOPE MÁXIMO. ARTÍCULO 5°. - La Comisión podrá establecer un valor máximo para el Fondo de Garantía para Reclamos de Clientes cuando el monto total acumulado alcance razonable magnitud para cumplir con sus objetivos.
				OBLIGACIONES DE LOS MERCADOS. ARTÍCULO 6°. - El Fondo de garantía para Reclamos de Clientes no será de propiedad de los Mercados. La actuación de éstos se limitará al cálculo de los aportes mensuales que deberán efectuar los agentes, a la percepción de tales aportes, a la inversión del importe del Fondo y cobro de las acreencias derivadas de ella y al recobro de las sumas aplicadas a reclamos.
				INVERSIONES PERMITIDAS. CUMPLIMIENTO EXIGENCIAS ANEXO I. ARTÍCULO 7°. - Los Mercados deberán observar las exigencias dispuestas en el Anexo I del Capítulo I Mercados del Título VI, en lo que respecta a las inversiones de las sumas acumuladas en el Fondo de Garantía para Reclamos de Clientes, debiendo dar cumplimiento a lo establecido en los Puntos 2, 4, 5 y 6 de dicho Anexo.
				SUPUESTOS. ARTÍCULO 8°. - La Comisión establecerá los supuestos que serán atendidos con el Fondo de Garantía para Reclamos de Clientes.
				El procedimiento a aplicarse para la formulación de reclamos por parte de clientes será el establecido para el trámite de denuncias ante la Comisión y ésta emitirá resolución final, pudiendo en su caso aplicarse el procedimiento específico que a estos efectos disponga el Organismo.
				El reclamo iniciado ante la Comisión no reemplaza la vía judicial, quedando abierto el planteo ante la justicia de aquellas cuestiones que estime hacen a su derecho, tanto para el cliente como para la Comisión. El cliente deberá informar a la Comisión en caso de resolver la presentación de su planteo por la vía judicial.
				RECLAMOS. ARTÍCULO 9°. - En caso de resolver la Comisión favorablemente el reclamo del cliente, hará saber tal decisión al Mercado del que revista la calidad de miembro el Agente de Negociación reclamado, a los fines de la afectación del respectivo Fondo de Garantía para Reclamos de Clientes y efectivo pago.
				 
				PAGO. ARTÍCULO 10.- Efectuado el pago, los Mercados deberán llevar adelante las respectivas medidas en orden al recobro del Agente de Negociación reclamado de las sumas abonadas y reestablecer el nivel del Fondo de Garantía para Reclamos de Clientes.
				AFECTACIÓN POR RECLAMO. ARTÍCULO 11.- La Comisión podrá establecer el máximo a afectar del Fondo de Garantía para Reclamos de Clientes por reclamo y/o por cliente.

    		</p>

    		<p>
    			<strong>
    				ANEXO B – PERFIL DE RIESGO PARA EL INVERSOR
    			</strong>
    		</p>

    		<p>
    			Esta encuesta busca determinar su perfil de riesgo como cliente:
    		</p>

    		<p>
    			<strong>Conservador:</strong>El objetivo principal de la inversión en bolsa de este grupo es mantener el capital seguro asumiendo el menor riesgo crediticio posible
    		</p>

    		<p>
    			<strong>Moderado:</strong>El propósito de este segmento es la generación periódica de ingreso o que permitan complementar o constituir el flujo de caja del inversor titular.
    		</p>

    		<p>
    			<strong>Agresivo:</strong>Inversionistas con amplio conocimiento del mercado y de los riesgos que en él se encuentran. La intención es obtener una rentabilidad muy superior a la del mercado gestionando especialmente el riesgo de mercado.
    		</p>

    		<p>
    			Las variables tiempo y riesgo son necesarias para que el cliente pueda tener el perfil agresivo. Si la calificación en alguna de las preguntas relacionadas con estas variables es menor a 2, no podrá ser calificado con el perfil agresivo.
    		</p>

    		
    	</div>
    </fieldset>
    
	{{-- COMITENTE --}}
	@if (isset($comitente)) 
    <fieldset>
    	<div>
	    	<p>
	    		<strong>
	    			BANCO SÁENZ S.A. ANEXO C de Formulario Interno 303/A (Solicitud de alta cuenta comitente – Persona Humana)
	    		</strong>
	    	</p>

	    	<p>
	    		Estimado/a Cliente/s
	    	</p>

	    	<p>
	    		Por la presente, informamos que su solicitud de alta de cuenta comitente presentada con fecha {{ date("d/m/Y",strtotime($comitente->created_at)) }} por Ud./Uds., ha sido ACEPTADA.
	    	</p>

	    	<p>
	    		Datos identificatorios de la cuenta comitente abierta a su/s titularidad/es:<strong> {{ $comitente->descripcion  }}</strong>
	    	</p>
    	</div>

    	<div>
	    	<p>
	    		<strong>
	    			BANCO SÁENZ S.A. ANEXO C de Formulario Interno 303/A (Solicitud de alta cuenta comitente – Persona Humana)
	    		</strong>
	    	</p>

	    	<p>
	    		Estimado/a Cliente/s
	    	</p>

	    	<p>
	    		Cuenta Comitente Numero <strong> {{ $comitente->sucursal . '-' . $comitente->numero_comitente . '-' . $comitente->digito_verificador  }}</strong>
	    	</p>

	    	<p>
	    		Por la presente, solicito que el estado de saldos y de movimientos registrados en mi cuenta comitente me sea enviado por correo o entregado en forma impresa en la Sucursal donde se encuentra radicada mi cuenta, con frecuencia TRIMESTRAL
	    	</p>
    	</div>
    </fieldset>
	 @endif

</body>
</html>