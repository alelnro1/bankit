@foreach ($titulares as $titular)

	
    <div class="wrapper">
		<div class="subWrapper" style="width: 80%;">
			<p><strong>Apellido y Nombre: </strong>{{ $titular->apellido }} {{ $titular->nombre }}</p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 19%;">
			<p><strong>Sexo:</strong> {{ $titular->Sexo->descripcion }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>Nacionalidad: </strong> {{ $titular->PaisNacimiento->descripcion }}</p>
			<p></p>
		</div>
	</div>

	<div class="wrapper">
		<div class="subWrapper" style="width: 100%;">
			<p><strong>Lugar y Fecha de Nacimiento: {{ $titular->ProvinciaNacimiento->descripcion . ', ' . $titular->fecha_nacimiento }}</strong></p>
			<p></p>
		</div>
	</div>
	
	<div class="wrapper">
		<div class="subWrapper" style="width: 100%;">
			<p><strong>Tipo y N° de Documento: </strong>{{ $titular->TipoDocumento->descripcion }} {{ $titular->numero_documento }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>Domicilio: </strong> {{ $titular->Direccion->calle 
										. ' ' . $titular->Direccion->altura
										. ' ' . $titular->Direccion->piso
										. ' ' . $titular->Direccion->departamento }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 50%; ">
			<p><strong>Localidad: </strong> {{ $titular->Direccion->localidad }}</p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 49%;">
			<p><strong>CP:</strong> {{ $titular->Direccion->codigo_postal }}</p>
			<p></p>
		</div>

		<div class="subWrapper" style="width: 50%;">
			<p><strong>Provincia: </strong> {{ $titular->ProvinciaNacimiento->descripcion }}</p> 
			<p></p>
		</div>
		<div class="subWrapper" style="width: 49%;">
			<p><strong>Pais:  </strong>{{ $titular->PaisNacimiento->descripcion }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 60%; ">
			<p><strong>Telefono Celular:</strong> {{ $titular->cod_area . $titular->prefijo_celular . $titular->telefono_movil }}</p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 39%;">
			<p>
				<strong>Reside en el exterior:</strong>

				@if ($titular->tipo_cliente == "Residentes en el Pais") No @else Sí @endif
			</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 33%; ">
			<p><strong>Estado Civil:</strong> {{ $titular->EstadoCivil->descripcion }}</p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 66%;">
			<p><strong>Email:</strong> {{ $titular->email }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 50%; ">
			<p><strong>CUIL/CUIT:</strong> {{ $titular->cuit_cuil }}</p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 49%;">
			<p><strong>Condicion de IVA:</strong> {{ $titular->TipoResponsableIVA->descripcion }}</p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>DECLARACIÓN JURADA SOBRE ACTIVIDAD:</strong></p>
			<p>DECLARACIÓN JURADA SOBRE ACTIVIDAD: Conforme la Res. 30E/2017 de la Unidad de Información Financiera (UIF) DECLARO BAJO JURAMENTO que la siguiente información es exacta y verdadera:</p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 50%; ">
			<p><strong>Profesion/oficio/actividad principal:</strong></p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 49%;">
			<p><strong>Relacion Laboral:</strong></p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 50%; ">
			<p><strong>Empresa:</strong></p>
			<p></p>
		</div>
		<div class="subWrapper" style="width: 49%;">
			<p><strong>Cargo:</strong></p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>Domicilio Laboral:</strong></p>
			<p></p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>DECLARACION JURADA SOBRE LICITUD Y ORIGEN DE LOS FONDOS</strong></p>
			<p>En cumplimiento de la normativa global relacionada con la Prevención del Lavado de Activos y la Financiación del Terrorismo, DECLARO BAJO JURAMENTO que los fondos y valores provienen y provendrán de ACTIVIDADES LICITAS</p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>DECLARACION JURADA SOBRE LA CONDICION DE PERSONA EXPUESTA POLITICAMENTE</strong></p>
			<p>Declaro bajo juramento que 

			@if($titular->pep)
				SI
			@else
				NO
			@endif

			me encuentro incluido y/o alcanzado dentro de la “Norma de Funciones de Personas Expuestas Políticamente” aprobada por la UIF, que he leído. En caso afirmativo, indicar cargo/función/jerarquía, o relación (con la Persona Expuesta Políticamente): Asumo el compromiso de informar cualquier modificación que se produzca a este respecto, dentro de los treinta días de ocurrida la presentación de una nueva declaración jurada</p>
		</div>
    </div>

    <div class="wrapper">
		<div class="subWrapper" style="width: 100%; ">
			<p><strong>DECLARACION JURADA GENERAL</strong></p>
			<p>DECLARO BAJO JURAMENTO que las informaciones consignadas son exactas y verdaderas, y que tengo conocimiento de la ley 25.246. De ser necesario, me comprometo a aportar documentación que respalde los datos volcados y las declaraciones juradas formuladas</p>
		</div>
    </div>


    <div>
    	<p><strong>PERSONAS EXPUESTAS POLITICAMENTE</strong></p>
    	
		<p>
			En Buenos Aires, a los {{ date("d") }} días del mes de {{ $mes_actual }} de {{ date("Y") }}.
		</p>

		<p>
			Sres. Banco Saenz S.A.
			<br>
			Esmeralda 83
			<br>
			Ciudad Autonoma de Buenos Aires			
		</p>

		<p>
			De mi mayor consideración:
		</p>

    	<p>
    		Por la presente tengo el agrado de dirigirme a Uds. a fin de comunicarles
				que he leído, tomado conocimiento y firmado de conformidad la Declaración Jurada referente a la
				Nómina de Funciones de Personas Expuestas Políticamente, aprobada por la Unidad de
				Información Financiera (UIF), que me fuera entregada el día {{date("d/m/Y")}} en 3 (tres) folios,
				conteniendo, en síntesis:
				A) Declaración Jurada
				B) Detalle: 1) Funcionarios públicos nacionales;
				2) Funcionarios públicos provinciales, municipales y de la Ciudad de Buenos Aires;
				3) Autoridades y apoderados de partidos políticos;
				4) Autoridades y representantes legales de organizaciones sindicales y
				empresariales;
				5) Funcionarios públicos extranjeros;
				6) Allegados o colaboradores, cónyuges o convivientes reconocidos legalmente y
				familiares en línea ascendente, descendente o colateral hasta el tercer grado de
				consanguinidad o segundo grado de afinidad de las personas a las que se
				refieren los puntos 1 a 5.
				7) Allegados o colaboradores cercanos a las personas a que se refieren los puntos 1
				a 5. (Todos aquellos individuos que se benefician directa y económicamente en
				razón de su relación cercana con la persona políticamente expuesta. (Ej.:
				consultores, asesores de trabajo).
				Sin otro particular, saludo a Uds. muy atentamente.
    	</p>

    	<p>
    		El que suscribe {{ $titular->nombre . ' ' . $titular->apellido }}, declara bajo juramento que los datos consignados en la presente son correctos,
			completos y fiel expresión de la verdad y que 
			
			@if($titular->pep)
				SI
			@else
				NO
			@endif
			
			se encuentra incluido y/o alcanzado dentro de la
			"Nómina de Funciones de Personas Expuestas Políticamente" aprobada por la Unidad de Información
			Financiera, que ha leído y suscripto.
			@if($titular->pep)
			En caso de haber respondido en forma afirmativa se deberá detallar el cargo, función, jerarquía o carácter de la
			relación: {{ $titular->funcion_pep }}.
			@endif

			Además, asumo el compromiso de informar cualquier modificación que se produzca a este respecto, dentro de
			los treinta días de ocurrida, mediante la presentación de una nueva declaración jurada.
    	</p>

    	<p>
    		<p>Documento: DNI Número {{$titular->numero_documento}}  </p>
    		<p>CUIT / CUIL / CDI : {{$titular->cuit_cuil}} </p>
    		<p>
Certificamos que la firma que antecede concuerda con la registrada en nuestros registros / fue puesta en
nuestra presencia.</p>
    	</p>
    </div>

    <div>
    	<p>Artículo 1º — Son personas políticamente expuestas las siguientes:
			<br>1) El presidente y vicepresidente de la Nación;
			<br>2) Los senadores y diputados de la Nación;
			<br>3) Los magistrados del Poder Judicial de la Nación;
			<br>4) Los magistrados del Ministerio Público de Nación;
			<br>5) El defensor del pueblo de la Nación y los adjuntos del defensor del pueblo;
			<br>6) El jefe de gabinete de ministros, los ministros, secretarios y subsecretarios del Poder Ejecutivo
			Nacional;
			<br>7) Los interventores federales;
			<br>8) El síndico general de la Nación y los síndicos generales adjuntos de la Sindicatura General de la
			Nación, el presidente y los auditores generales de la Auditoria General de la Nación, las autoridades
			superiores de los entes reguladores y los demás órganos que integran los sistemas de control del
			sector público nacional, y los miembros de organismos jurisdiccionales administrativos;
			<br>9) Los miembros del Consejo de la Magistratura y del Jurado de Enjuiciamiento;
			<br>10) Los embajadores, cónsules y funcionarios destacados en misión oficial permanente en exterior;
			<br>11) El personal en actividad de las Fuerzas Armadas, de la Policía Federal Argentina, de Gendarmería
			Nacional, de la Prefectura Naval Argentina y del Servicio Penitenciario Federal, con jerarquía no
			menor de coronel o grado equivalente según la fuerza;
			<br>12) Los rectores, decanos y secretarios de las universidades nacionales;
			<br>13) Los funcionarios o empleados con categoría o función no inferior a la de director o equivalente, que
			presten servicio en la Administración Pública Nacional, centralizada o descentralizada, las entidades
			autárquicas, los bancos y entidades financieras del sistema oficial, las obras sociales administradas
			por el Estado, las empresas del Estado, las sociedades del Estado y el personal con similar categoría
			o función, designado a propuesta del Estado en las sociedades de economía mixta, en las sociedades
			anónimas con participación estatal y en otros entes del sector público;
			<br>14) Los funcionarios colaboradores de interventores federales, con categoría o función no inferior a la de
			director o equivalente;
			<br>15) El personal de los organismos indicados en el inciso h) del presente artículo, con categoría no inferior
			a la director o equivalente;
			<br>16) Todo funcionario o empleado público encargado de otorgar habilitaciones administrativas para el
			ejercicio de cualquier actividad, como también todo funcionario o empleado público encargado de
			controlar el funcionamiento de dichas actividades o de ejercer cualquier otro control en virtud de un
			poder de policía
			<br>17) Los funcionarios que integran los organismos de control de los servicios públicos privatizados, con
			categoría no inferior a la de director;
			<br>18) El personal que se desempeña en el Poder Legislativo, con categoría no inferior a la de director;
			<br>19) El personal que cumpla servicios en el Poder Judicial de la Nación y en el Ministerio Público de la
			Nación, con categoría no inferior a secretario o equivalente;
			<br>20) Todo funcionario o empleado público que integre comisiones de adjudicación de licitaciones, de
			compra o de recepción de bienes, o participe en la toma de decisiones de licitaciones o compras;
			<br>21) Todo funcionario público que tenga por función administrar un patrimonio público o privado, o controlar
			o fiscalizar los ingresos públicos cualquiera fuera su naturaleza;
			<br>22) Los directores y administradores de las entidades sometidas al control externo del Honorable
			Congreso de la Nación, de conformidad con lo dispuesto en el artículo 120 de la ley 24.156, en los
			casos en que la Comisión Nacional de Ética Pública se las requiera.
			</p><p>B. Los funcionarios públicos provinciales, municipales y de la Ciudad Autónoma de Buenos Aires que a
			continuación se señalan, que se desempeñen o hayan desempeñado hasta dos años anteriores a la
			fecha en que fue realizada la operatoria:
			<br>1) Gobernadores, intendentes y jefe de gobierno de la Ciudad Autónoma de Buenos Aires;
			<br>2) Ministros de gobierno, Secretarios y Subsecretarios; Ministros de los Tribunales Superiores de Justicia
			de las provincias y de la Ciudad Autónoma de Buenos Aires;
			<br>3) Jueces y demás personal que cumpla servicios en los Poderes Judiciales Provinciales y de la Ciudad
			Autónoma de Buenos Aires, con categoría no inferior a Secretario o equivalente;
			<br>4) Legisladores provinciales, municipales y de la Ciudad Autónoma de Buenos Aires;
			F.906-2
			<br>5) Máxima autoridad de los Organismos de Control y de los entes autárquicos provinciales, municipales y
			de la Ciudad Autónoma de Buenos Aires;
			<br>6) Máxima autoridad de las sociedades de propiedad de los estados provinciales, municipales y de la
			Ciudad Autónoma de Buenos Aires;
			<br>7) Cualquier otra persona que desempeñe o haya desempeñado hasta dos años anteriores a la fecha en
			que fue realizada la operatoria, en las órbitas provinciales, municipales y de la Ciudad Autónoma de
			Buenos Aires, funciones idénticas o similares a las enumeradas en el artículo 5º de la Ley Nº 25.188.
			</p><p>C. Las autoridades y apoderados de partidos políticos de nivel nacional, provincial y de la Ciudad
			Autónoma de Buenos Aires, que se desempeñen o hayan desempeñado hasta dos años anteriores a la
			fecha en que fue realizada la operatoria.
			</p><p>D. Las Autoridades y representantes legales de organizaciones sindicales y empresariales (cámaras,
			asociaciones y otras formas de agrupación corporativa) y de las obras sociales contempladas en la Ley
			Nº 23.660, que desempeñen o hayan desempeñado dichas funciones hasta dos años anteriores a la
			fecha en que fue realizada la operatoria.
			El alcance establecido se limita a aquellos rangos, jerarquías o categorías con facultades de decisión
			resolutivas, por lo tanto se excluye a los funcionarios de niveles intermedios o inferiores.
			</p><p>E. Los funcionarios públicos extranjeros: quedan comprendidas las personas que desempeñen o hayan
			desempeñado dichas funciones hasta dos años anteriores a la fecha en que fue realizada la operación,
			ocupando alguno de los siguientes cargos:
			<br>1) Jefes de Estado, jefes de Gobierno, gobernadores, intendentes, ministros, secretarios y subsecretarios de
			Estado y otros cargos gubernamentales equivalentes.
			<br>2) Miembros del parlamento / poder legislativo.
			<br>3) Jueces, miembros superiores de tribunales y otras altas instancias judiciales y administrativas de ese
			ámbito del poder judicial.
			<br>4) Embajadores, cónsules y funcionarios destacados de misiones oficiales permanentes del exterior.
			<br>5) Oficiales de alto rango de las fuerzas armadas (a partir de coronel o grado equivalente en la fuerza y/ o
			país de que se trate) y de las fuerzas de seguridad pública (a partir de comisario o rango equivalente
			según la fuerza y/ país de que se trate)
			<br>6) Miembros de los órganos de dirección y control de empresas de propiedad estatal.
			<br>7) Directores, gobernadores, consejeros, síndicos o autoridades equivalentes de bancos centrales y otros
			organismos estatales de regulación y/ o supervisión.
			</p><p>F. Allegados o colaboradores, cónyuges o convivientes reconocidos legalmente y familiares en línea
			ascendente, descendente o colateral hasta el tercer grado de consanguinidad o segundo grado de
			afinidad, de las personas a que se refieren los incisos A, B, C, D y E durante los plazos que para ellas se
			indican.
			Allegados o colaboradores cercanos a las personas a que se refieren los incisos A a E, durante los plazos
			que para ellas se indican, entendiéndose por tales a todos aquellos individuos que se benefician directa y
			económicamente en razón de su relación cercana con la persona políticamente expuesta. Entre otros,
			pueden encuadrar en esta categoría los socios personales, los consultores, asesores y colaboradores de
			trabajo de dichas personas.
		</p>
    </div>
@endforeach