<!doctype html>
<html lang="es">
<head>
    <style>
        body, p, div {
            color: #444444;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 15px;
        }
        p.chica		{ font-size: 12px; }
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Banco Saenz</title>
</head>

<body>

<div cellpadding="5" cellspacing="0" border="0" width="100%">
        <img src="/public/css/images/logo.png" style="height:60px; float: right;">
</div>

<p>Estimado/a : {{ $titular->nombre . ' ' . $titular->apellido }}</p><br />
<p>Le informamos que ha finalizado con exito el proceso de apertura de cuenta. </p>
<p>Se analizara la informacion brindada y se le notificara ante cualquier novedad. </p>


<br/>
<hr>
<br/>


<table bgcolor="#FFFFFF" cellpadding="10" cellspacing="0" border="0" width="100%">
    <tr><td>
            <p>Este mensaje fue originado automáticamente. Por favor, no responder al mismo.
Banco Sáenz S.A., a los efectos de resguardar su seguridad, no tiene prácticas de solicitar ningún
tipo de información por e-mail.

Si recibe un llamado telefónico o e-mail, solicitando información personal, no lo responda ni ingrese
datos personales ni claves. Ante cualquier consulta, contáctese al 5368-7075 de lunes a viernes de
10.00 a 17.00 hs.
El presente mail no podría haber sido enviado sin que Ud. nos proporcionase su dirección de
correo electrónico. Queda bajo su exclusiva responsabilidad informar al Banco de cualquier cambio
o modificación que dicha dirección sufriese.
El contenido del presente mensaje es privado, confidencial y exclusivo para sus destinatarios,
pudiendo contener información protegida por normas legales y de secreto profesional. Bajo
ninguna circunstancia, su contenido puede ser transmitido o relevado a terceros ni divulgado en
forma alguna. Banco Sáenz S.A. no se responsabilizará por los daños o perjuicios derivados del
incumplimiento de lo aquí establecido.</p>
            <p><small>copyright © 2018 - Banco Saenz</small></p>
        </td></tr>
</table>


<div style="min-height: 39px; clear:both;">
    <div style="float: left; margin-right: 20px;">
        <img src="" height='72'>
    </div>
</div>


</body>
</html>
