<div class="modal fade" id="modal-aceptado" role="dialog">
    <div class="modal-dialog">
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading" id="titulo-mensaje-aceptado"></h4>
            <div>
                <div class="col-xs-1"><i class="fa fa-refresh fa-spin fa-1"></i></div>
                <p id="mensaje-aceptado"></p>
            </div>
        </div>

    </div>
</div>
