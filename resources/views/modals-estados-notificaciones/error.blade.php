<div class="modal fade" id="modal-error" role="dialog">
    <div class="modal-dialog">

        <div class="alert alert-danger" role="alert">
            <div>
                <p id="mensaje-error"></p>
            </div>
        </div>

    </div>
</div>
