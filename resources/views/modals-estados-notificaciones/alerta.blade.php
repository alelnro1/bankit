<div class="modal fade" id="modal-alerta" role="dialog">
    <div class="modal-dialog">

        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading" id="titulo-mensaje-alert">Alerta!</h4>
            <p id="mensaje-alert"></p>
        </div>

    </div>
</div>
