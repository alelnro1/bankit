<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles-saenz.css') }}" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">


@yield('styles')

<!-- Material Design fonts -->
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/FezVrasta/bootstrap-material-design/dist/dist/bootstrap-material-design.min.css">--}}
</head>
<body>
@yield('preloader')
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/admin') }}">
                    <div class="brand-wrapper">

                    </div>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('backend')->check())

                        @if(Auth::user()->tienePermisos(["Administrador","Fondos"]))
                            <li style="margin-top: 10px !important;">
                                
                                <div class="btn btn-primary btn-xs menuButton
                                            @if(strpos(Route::currentRouteName(), 'fondos')!==false && strpos(Route::currentRouteName(), 'operaciones')==false)
                                                {{ 'menuSelected' }}
                                            @endif">          
                                    <span >
                                        <a href="{{ route('backend.abm-fondos.index') }}" 
                                            style="color:white !important;">
                                                Fondos
                                        </a>        
                                    </span>
                                </div>
                            </li>
                        @endif

                        @if(Auth::user()->tienePermisos(["Administrador","Operaciones"]))           
                        <li class="dropdown" style="margin-top: 10px !important;">
                            <div class="btn btn-primary btn-xs menuButton
                                        @if(strpos(Route::currentRouteName(), 'operaciones')!==false)
                                            {{ 'menuSelected' }} 
                                        @endif">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false" aria-haspopup="true" style="color:white !important;">
                                        Operaciones
                                    </a>

                                    <ul class="dropdown-menu">                                        
                                        @if(Auth::user()->tienePermisos(["Administrador","Operaciones"]))           
                                        <li>
                                            <a href="{{ route('backend.fondos.operaciones-pendientes') }}">Operaciones Pendientes de Liquidacion</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('backend.fondos.operaciones-aprobadas') }}">Operaciones Aprobadas</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.fondos.operaciones-pendientes-comprobante') }}">Pendientes de Comprobante</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('backend.fondos.operaciones-realizadas') }}">Reporte de Operaciones Realizadas</a>
                                        </li>
                                        @endif
                                    </ul>
                            </div>
                                
                        </li>
                        @endif
                        

                        @if(Auth::user()->tienePermisos(["Administrador","Usuarios Internos"]))
                            <li style="margin-top: 10px !important;">
                                <div class="btn btn-primary btn-xs menuButton
                                            @if(strpos(Route::currentRouteName(), 'interno')!==false)
                                                {{ 'menuSelected' }}
                                            @endif">
                                    <span >
                                        <a href="{{ route('backend.interno.index') }}" 
                                            style="color:white !important;">
                                    Usuarios Internos</a>        
                                    </span>
                                </div>
                            </li>
                        @endif

                        @if(Auth::user()->tienePermisos(["Administrador","Prospectos"]))
                            <li style="margin-top: 10px !important;">
                                <div class="btn btn-primary btn-xs menuButton
                                            @if(strpos(Route::currentRouteName(), 'prospecto')!==false)
                                                {{ 'menuSelected' }}
                                            @endif">
                                    <span >
                                        <a href="{{ route('backend.prospectos') }}" 
                                            style="color:white !important;">
                                    Prospectos</a>        
                                    </span>
                                </div>
                            </li>
                        @endif

                        @if(Auth::user()->tienePermisos(["Administrador","Comitentes"]))
                            <li style="margin-top: 10px !important;">
                                <div class="btn btn-primary btn-xs menuButton
                                            @if(strpos(Route::currentRouteName(), 'comitente')!==false)
                                                {{ 'menuSelected' }}
                                            @endif">
                                    <span >
                                        <a href="{{ route('backend.comitentes') }}" 
                                            style="color:white !important;">
                                    Comitentes</a>        
                                    </span>
                                </div>
                            </li>
                        @endif

                        <li class="dropdown" style="margin-top: 10px !important;">
                            <div class="dropdown btn btn-primary btn-xs menuButton">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false" aria-haspopup="true"  style="color:white !important;">
                                        {{ Auth::user()->nombre }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('backend.password.cambiar.form') }}">Cambiar contraseña</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('backend.logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                Cerrar Sesión
                                            </a>

                                            <form id="logout-form" action="{{ route('backend.logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/lib.js') }}"></script>
<script src="{{ asset('js/backend.js') }}"></script>

@yield('javascripts')

{{--<script src="https://cdn.rawgit.com/HubSpot/tether/v1.3.4/dist/js/tether.min.js"></script>--}}
{{-- <script>
    $(document).ready(function () {
        $('body').bootstrapMaterialDesign();
    });
</script>--}}
</body>
</html>
