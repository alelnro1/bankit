<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Plataforma de FCIs') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles-saenz.css') }}" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

@yield('styles')

<!-- Material Design fonts -->
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/FezVrasta/bootstrap-material-design/dist/dist/bootstrap-material-design.min.css">--}}
</head>
<body>
@yield('preloader')
<div id="app">
    <nav class="navbar navbar-default navbar-static-top" style="height: 70px;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <div class="brand-wrapper">

                    </div>
                </a>
                
                @if (!Auth::guest())
                    <div class="navbar-brand" style="padding: 15px; font-size: 18px; color:rgba(2, 54, 105);">
                        <a href="{{ route('comitentes.mostrar-seleccion') }}" class="mostrar-seleccion-comitente">
                            <span id="nombre-comitente" class="font-bold"
                          
                                  title="{{ session('NOMBRE_COMITENTE') }}">
                                    {{ str_limit(session('NOMBRE_COMITENTE'), 10) }}
                            </span>
                        </a>

                        <br/>
                        <span class="font-bold">#</span>

                        <a href="{{ route('comitentes.mostrar-seleccion') }}" class="mostrar-seleccion-comitente">
                            <span id="nro-comitente" class="font-bold">{{ session('NRO_COMITENTE') }}</span>
                        </a>
                        <br>
                        {{--<button title="Seleccionar otro comitente" data-href="{{ route('comitentes.mostrar-seleccion') }}"
                                id="cambiar-comitente"
                                class="btn btn-info ">
                            <i class="fas fa-exchange"></i>
                        </button>--}}
                    </div>

                @endif

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right"  >
                    <!-- Authentication Links -->
                    @guest

                    @else
                        @if(Auth::user()->cambio_password==0)
                            <li style="margin-top : 10px;">
                                <div>
                                    <a class="trigger">
                                        <span  class="btn btn-primary btn-xs">
                                            Operar
                                        </span>
                                    </a>
                                </div>
                            </li>
                        @endif
                        <li style="margin-top : 10px;">
                            <div>
                                <a href="{{ route('frontend.cotizaciones.fondos') }}">
                                    <span  class="btn btn-primary btn-xs">
                                        Cotizaciones
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li style="margin-top : 10px;">
                            <div>
                                <a href="{{ route('tesoreria.cuentas-bancarias.index') }}">
                                    <span class="btn btn-primary btn-xs">
                                        Cuentas Bancarias
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li style="margin-top : 10px;">
                            <div class="dropdown btn btn-primary btn-xs">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false" aria-haspopup="true"  style="color:white !important;">
                                    {{ Auth::user()->email }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('frontend.fondos.operaciones-realizadas') }}">Mis Movimientos</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('frontend.password.cambiar.form') }}">Cambiar contraseña</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('frontend.logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('frontend.logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @if (Auth::user())
                    @if (Auth::user()->cambio_password)
                        <div class="alert alert-warning">
                            Tiene un cambio de contraseña pendiente. Cambiela haciendo
                            <a href="{{ route('frontend.password.cambiar.form') }}">click aquí</a>
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>

@if (Auth::user())
    @include('frontend.form-operar.operar')
@endif

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/lib.js') }}"></script>
@yield('javascripts')
<script src="{{ asset('js/scripts.js') }}"></script>

{{-- <script>
    $(document).ready(function () {
        $('body').bootstrapMaterialDesign();
    });
</script>--}}
</body>
</html>
