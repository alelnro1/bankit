<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Plataforma FCIs') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles-saenz.css') }}" rel="stylesheet">
    <link href="{{ asset('css/apertura-cuenta.css') }}" rel="stylesheet">

@yield('styles')

<!-- Material Design fonts -->
    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/FezVrasta/bootstrap-material-design/dist/dist/bootstrap-material-design.min.css">--}}
</head>
<body>
@yield('preloader')
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url(route('apertura-cuenta.proceso.paso-1')) }}">
                    <div class="brand-wrapper">
                    
                    </div>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest('apertura-cuenta')
                        <li><a class=" btn btn-primary" href="{{ route('frontend.login') }}">Acceda a la plataforma</a></li>
                    @else
                        <li  style="margin-top : 10px;">

                            <div class="dropdown btn btn-primary" >
                                <a style="color:white !important;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::guard('apertura-cuenta')->user()->email }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('apertura-cuenta.password.cambiar.form') }}">Cambiar contraseña</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('apertura-cuenta.logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('apertura-cuenta.logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <div class="navbar navbar-fixed-bottom"></div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/lib.js') }}"></script>
<script src="{{ asset('js/apertura-cuenta.js') }}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

@yield('javascripts')

{{-- <script>
    $(document).ready(function () {
        $('body').bootstrapMaterialDesign();
    });
</script>--}}
</body>
</html>
