@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">Prospectos</div>

    <div class="panel-body">
        @if (count($prospectos) > 0)
            <table class="table table-responsive table-bordered" id="prospectos">
                <thead>
                <tr>
                    <th>Prospecto</th>
                    <th>Titulares</th>
                    <th>Fecha de Creación</th>
                </tr>
                </thead>

                <tbody>
                <div class="alert alert-info">
                    Estos son los prospectos pendientes de revision. Haga click en el id para visualizar y confirmar el alta del mismo
                </div>
                @foreach ($prospectos as $prospecto)
                    <tr>
                        
                            <td>
                                <a href="{{ route('backend.prospecto.ver', ['prospecto' => $prospecto->id]) }}">
                                    {{ $prospecto->id }}        
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('backend.prospecto.ver', ['prospecto' => $prospecto->id]) }}">
                                    <ul>
                                        @foreach ($prospecto->titulares as $titular)
                                            <li>{{ $titular->apellido . ", " 
                                                        . $titular->nombre 
                                                        . "( DNI:" . $titular->numero_documento . ")" }}</li>
                                        @endforeach
                                    </ul>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('backend.prospecto.ver', ['prospecto' => $prospecto->id]) }}">
                                    {{ $prospecto->created_at }}
                                </a>
                            </td>
                        
                    </tr>

                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info">
                No hay prospectos pendientes
            </div>
        @endif
    </div>

@endsection