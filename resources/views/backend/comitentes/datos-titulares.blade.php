


<div id="DatosTitulares"
     data-izimodal-title="DatosTitulares"
     data-izimodal-transitionin="fadeInDown">
    
    <div id="container col-xs-12" style="padding: 10px;">
        <div>
            <table class="table table-bordered table-striped" id="titulares">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>CUIT/CUIL</th>
                    <th>Mail</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($comitente->titulares as $titular)
                    <tr>
                        <td>{{ $titular->nombre }}</td>
                        <td>{{ $titular->apellido }}</td>
                        <td>{{ $titular->cuit_cuil }}</td>
                        <td>{{ $titular->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

