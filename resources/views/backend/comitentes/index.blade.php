@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">Comitentes</div>

    <div class="panel-body">
        @if (count($comitentes) > 0)
            <table class="table table-responsive table-bordered table-striped" id="prospectos">
                <thead>
                <tr>
                    <th>Comitente</th>
                    <th>Denominacion</th>
                    <th>Titulares</th>
                    <th>CBU Asociados</th>
                </tr>
                </thead>

                <tbody>

                @foreach ($comitentes as $comitente)
                    <tr>
                            <td>
                                <a href="{{ route('backend.comitente.ver', ['comitente' => $comitente->id]) }}">
                                    {{ $comitente->numero_comitente }}        
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('backend.comitente.ver', ['comitente' => $comitente->id]) }}"
                                   title="Ver Comitente">
                                    {{ $comitente->descripcion }}        
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('backend.comitente.ver', ['comitente' => $comitente->id]) }}">
                                    <ul>
                                        @foreach ($comitente->titulares as $titular)
                                            <li>{{ $titular->apellido . ", " 
                                                        . $titular->nombre 
                                                        . " (DNI:" . $titular->numero_documento . ")" }}</li>
                                        @endforeach
                                    </ul>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('backend.comitente.ver', ['comitente' => $comitente->id]) }}">
                                    <ul>
                                        @foreach ($comitente->cuentas_bancarias as $cuenta_bancaria)
                                            <li>{{ $cuenta_bancaria->cbu . " (" . $cuenta_bancaria->Moneda->simbolo . ")" }}</li>
                                        @endforeach
                                    </ul>
                                </a>
                            </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info">
                No hay prospectos pendientes
            </div>
        @endif
    </div>

@endsection