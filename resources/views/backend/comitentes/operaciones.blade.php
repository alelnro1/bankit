

<div id="DatosTitulares"
     data-izimodal-title="DatosTitulares"
     data-izimodal-transitionin="fadeInDown">
    
    <div id="container col-xs-12" style="padding: 10px;">
        <div>
            <table class="table table-bordered table-striped" id="titulares">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Fecha Concertacion</th>
                    <th>Fecha Liquidacion</th>
                    <th>Tipo Operacion</th>
                    <th>Importe</th>
                    <th>Cant. Cuotapartes</th>
                    <th>Estado</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($comitente->operaciones as $operacion)
                    <tr>
                        <td>{{ $operacion->id }}</td>
                        <td style="text-align: center;">
                            {{ date("d/m/Y",strtotime($operacion->fecha_concertacion)) }}
                        </td>
                        <td style="text-align: center;">
                            {{ date("d/m/Y",strtotime($operacion->fecha_liquidacion)) }}</td>
                        <td>
                            {{$operacion->TipoOperacion->nombre_display }}</td>
                        </td>
                        <td style="text-align: right;">
                                {{ number_format($operacion->importe,2,",",".") }}
                        </td>
                        <td style="text-align: right;">
                                {{ number_format($operacion->cantidad,6,",",".") }}
                        </td>
                        
                        <td>
                             {{ $operacion->Estado->descripcion }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

