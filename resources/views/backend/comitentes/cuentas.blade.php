<div id="DatosTitulares"
     data-izimodal-title="DatosTitulares"
     data-izimodal-transitionin="fadeInDown">
    
    <div id="container col-xs-12" style="padding: 10px;">
        <div>
            <table class="table table-bordered table-striped" id="titulares">
                <thead>
                <tr>
                    <th>Tipo Cuenta</th>
                    <th>Moneda</th>
                    <th>Banco</th>
                    <th>CBU</th>
                    <th>Alias</th>
                    <th>Fecha Adhesion</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($comitente->cuentas_bancarias as $cuenta_bancaria)
                    <tr>
                        <td>{{ $cuenta_bancaria->Tipo->descripcion }}</td>
                        <td>{{ $cuenta_bancaria->Moneda->descripcion }}</td>
                        <td>{{ $cuenta_bancaria->Banco->descripcion }}</td>
                        <td>{{ $cuenta_bancaria->cbu }}</td>
                        <td>{{ $cuenta_bancaria->alias }}</td>
                        <td>{{ date("d/m/Y",strtotime($cuenta_bancaria->created_at)) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

