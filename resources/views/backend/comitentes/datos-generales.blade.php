<div id="DatosTitulares"
     data-izimodal-title="DatosTitulares"
     data-izimodal-transitionin="fadeInDown">
    
    <div id="container col-xs-12" style="padding: 10px;">
        <div>
            <strong>Denominacion: </strong> {{ $comitente->descripcion }}
            <br>
            <strong>Sucursal: </strong> {{ $comitente->sucursal }}
            <br>
            <strong>Numero de Comitente: </strong> {{ $comitente->numero_comitente }}
            <br>
            <strong>Digito Verificador: </strong> {{ $comitente->digito_verificador }}
            <br>
            <strong>Id: </strong> {{ $comitente->id }}
        </div>
    </div>
</div>

