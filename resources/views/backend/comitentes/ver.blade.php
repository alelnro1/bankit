@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">Comitente

    </div>

    <div class="panel-body">
         <div class="row">
            <div class="col-xs-12">
                <div id="tabs-abm-fci">
                    <ul>
                        <li><a href="#tabs-1">Datos Generales</a></li>
                        <li><a href="#tabs-2">Titulares</a></li>
                        <li><a href="#tabs-3">Operaciones</a></li>
                        <li><a href="#tabs-4">Cuentas Bancarias</a></li>
                    </ul>
                    <div id="tabs-1">
                        @include('backend.comitentes.datos-generales', ['comitente' => $comitente])
                    </div>
                    <div id="tabs-2">
                        @include('backend.comitentes.datos-titulares', ['comitente' => $comitente])
                    </div>
                    <div id="tabs-3">
                        @include('backend.comitentes.operaciones', ['comitente' => $comitente])
                    </div>
                    <div id="tabs-4">
                        @include('backend.comitentes.cuentas', ['comitente' => $comitente])
                    </div>
                </div>
            </div>

        </div>       
    </div>
@endsection
