@extends('layouts.backend-app')

@section('styles')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('css/styles.css') }}">
@endsection

@include('_snippets.preloader')

@section('content')
    <div class="panel-heading">
        Cuentas Bancarias
        pendientes de revisión
    </div>

    <div class="panel-body">
        <div class="alert alert-info">
            Para poder revisar el detalle de la cuenta adicionar por favor clickee en el cbu de la misma.
        </div>
        <br>
        <div id="tabs">
            <ul>
                <li>
                    <a href="#tab-pesos">Pesos <strong>({{ $cant_cuentas_pendientes_pesos }})</strong></a>
                </li>
                <li>
                    <a href="#tab-dolares">Dólares <strong>({{ $cant_cuentas_pendientes_dolares }})</strong></a>
                </li>
            </ul>
            <div id="tab-pesos">
                @if ($cant_cuentas_pendientes_pesos > 0)
                    @include('backend.tesoreria.cuentas-bancarias.pendientes-pesos')
                @else
                    <div class="help-block">
                        No hay solicitudes de cuentas bancarias pendientes.
                    </div>
                @endif
            </div>

            <div id="tab-dolares">
                @if ($cant_cuentas_pendientes_dolares > 0)
                    @include('backend.tesoreria.cuentas-bancarias.pendientes-dolares')
                @else
                    <div class="help-block">
                        No hay solicitudes de cuentas bancarias pendientes.
                    </div>
                @endif
            </div>
        </div>

    </div>

    {{-- En este div se van a ir cargando las vistas de los depositos que se abrieron --}}
    <div id="cuentas"></div>

    @include('modals-estados-notificaciones.alerta')
    @include('modals-estados-notificaciones.aceptado')
    @include('modals-estados-notificaciones.error')
@endsection