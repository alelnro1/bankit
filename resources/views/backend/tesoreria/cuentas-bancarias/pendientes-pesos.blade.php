<table class="table-responsive table table-bordered table-striped">
    <thead>
    <tr>
        <th>Nombre Comitente</th>
        <th>Usuario</th>
        <th>Banco</th>
        <th>CBU</th>
    </tr>
    </thead>

    <tbody>
    @foreach ($cuentas_bancarias_pendientes_pesos as $cuenta_bancaria)
        <tr>
            <td>{{ $cuenta_bancaria->getComitente() }}</td>
            <td>{{ $cuenta_bancaria->getUsuario() }}</td>
            <td>{{ $cuenta_bancaria->getBanco()}}</td>
            <td>
                <a
                        href="{{ route('backend.cuentas-bancarias.ver.solicitud', ['id' => $cuenta_bancaria->id]) }}"
                        class="cargar-solicitud"
                        id="cuenta-{{ $cuenta_bancaria->id }}"
                        data-cuenta="{{ $cuenta_bancaria->id }}"
                        title="Ver detalle">
                        {{ $cuenta_bancaria->getCbu() }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>