<!-- Modal -->
<div id="modal-cuenta-{{ $cuenta->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalle de la Solicitud</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-6">
                    <table class="table table-responsive table-stripped">
                        <thead>
                        <tr>
                            <th colspan="2">INFORMACION</th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <td>Nombre de Comitente</td>
                            <td>{{ $cuenta->getComitente() }}</td>
                        </tr>

                        <tr>
                            <td>Tipo de Cuenta</td>
                            <td>{{ $cuenta->getTipoCuenta() }}</td>
                        </tr>

                        <tr>
                            <td>Moneda</td>
                            <td>{{ $cuenta->getMoneda() }}</td>
                        </tr>
                        <tr>
                            <td>Banco</td>
                            <td>{{ $cuenta->getBanco()}}</td>
                        </tr>


                        <tr>
                            <td>CBU</td>
                            <td>{{ $cuenta->getCbu() }}</td>
                        </tr>

                        <tr>
                            <td>ALIAS</td>
                            <td>{{ $cuenta->getAlias() }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">
                {{-- RECHAZAR --}}
                <div class="text-left col-xs-4">
                    <button
                            data-href="{{ route('backend.cuentas-bancarias.rechazar') }}"
                            data-cuenta="{{ $cuenta->id}}"
                            class="btn btn-default btn-danger rechazar">Rechazar
                    </button>
                </div>

                {{-- ACEPTAR --}}
                <div class="text-right col-xs-8">
                    <div class="col-xs-4" style="float:right">
                        <button
                                data-href="{{ route('backend.cuentas-bancarias.aceptar') }}"
                                data-cuenta="{{ $cuenta->id}}"
                                class="btn btn-default btn-success aceptar">Aceptar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#aba").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>