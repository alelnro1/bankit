<div class="row">
    <form action="{{ route('backend.abm-fondos.procesar-edit', ['fondo' => $fondo->id]) }}" method="POST">
        @csrf
        <div class="col-xs-12">
            <div class="col-xs-6">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td>Descripción</td>
                        <td>
                            <input type="text" value="{{ $fondo->descripcion }}" name="descripcion" class="form-control"
                                   required>
                        </td>
                    </tr>

                    <tr>
                        <td>Nombre Web</td>
                        <td>
                            <input type="text" value="{{ $fondo->nombre_web }}" name="nombre_web" class="form-control"
                                required>
                        </td>
                    </tr>

                    <tr>
                        <td>Opera Web</td>
                        <td>
                            <input type="checkbox" name="opera_web" @if ($fondo->opera_web) checked @endif>
                        </td>
                    </tr>

                    <tr>
                        <td>Límite Mínimo</td>
                        <td>
                            <input type="number" class="form-control" value="{{ $fondo->limite_minimo }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Calificación CNV</td>
                        <td>
                            <input type="text" class="form-control" name="codigo_caja_valores" value="{{ $fondo->codigo_caja_valores }}"
                                required>
                        </td>
                    </tr>

                    <tr>
                        <td>Clase</td>
                        <td>
                            <input type="text" class="form-control" name="clase" value="{{ $fondo->clase }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Abreviatura</td>
                        <td>
                            <input type="text" class="form-control" name="abreviatura" value="{{ $fondo->abreviatura }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Cod Interfaz</td>
                        <td>
                            <input type="text" class="form-control" name="cod_interfaz" value="{{ $fondo->cod_interfaz }}">
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-xs-6">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td>Familia</td>
                        <td>
                            <input type="text" class="form-control" name="familia" value="{{ $fondo->familia }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Categoría</td>
                        <td>
                            <input type="text" class="form-control" name="categoria" value="{{ $fondo->categoria }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Código Bloomberg</td>
                        <td>
                            <input type="text" class="form-control" name="codigo_bloomberg" value="{{ $fondo->codigo_bloomberg }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Sociedad Gerente</td>
                        <td>
                            <input type="text" class="form-control" name="sociedad_gerente" value="{{ $fondo->sociedad_gerente }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Sociedad Custodia</td>
                        <td>
                            <input type="text" class="form-control" name="sociedad_custodia" value="{{ $fondo->sociedad_custodia }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Benchmark</td>
                        <td>
                            <input type="text" class="form-control" name="benchmark" value="{{ $fondo->benchmark }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Código CNV</td>
                        <td>
                            <input type="text" class="form-control" name="codigo_caja_valores" value="{{ $fondo->codigo_caja_valores }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Permanencia recomendada</td>
                        <td>
                            <input type="text" class="form-control" name="permanencia_recomendada" value="{{ $fondo->permanencia_recomendada }}">
                        </td>
                    </tr>

                    <tr>
                        <td>Moneda</td>
                        <td>
                            <select name="moneda_id" class="form-control">
                                @foreach ($monedas as $moneda)
                                    <option value="{{ $moneda->id }}"
                                        @if ($moneda->id == $fondo->moneda_id) selected @endif>
                                        {{ $moneda->descripcion }}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <input type="submit" class="form-control btn btn-primary" value="Confirmar edición">
    </form>
</div>