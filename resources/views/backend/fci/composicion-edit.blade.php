<div id="composicion-edit-item"
     data-izimodal-title="Editar item de composición de FCI"
     data-izimodal-transitionin="fadeInDown">

    <div id="container col-xs-12" style="padding: 10px;">
        <div class="row" style="padding: 10px;">
            <table class="table table-bordered table-responsive table-striped">

                <thead>
                <tr>
                    <th>Descripción</th>
                    <th>Porcentaje</th>
                    <th>Nuevo Porcentaje</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($composiciones as $composicion)
                    <tr>
                        <td>{{ $composicion->descripcion }}</td>
                        <td style="text-align: center;">{{ number_format($composicion->porcentaje, 2, ',', '.') }}</td>
                        <td>
                            <input type="number" data-composicion-id="{{ $composicion->id }}"
                                   class="composicion-nuevo-valor"
                                   value="{{  number_format($composicion->porcentaje, 2, ',', '.') }}" step="0.01">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <hr>

            <div class="row">
                <div class="col-xs-6">
                    <a data-izimodal-close="" data-izimodal-transitionout="bounceOutDown"
                       class="form-control col-xs-6 btn btn-danger"
                       style="color: #FFFFFF !important;">

                        <i class="fa fa-ban"></i> Cancelar
                    </a>
                </div>
                <div class="col-xs-6">
                    <button type="submit" id="confirmar-actualizacion"
                            data-procesar-edit="{{ route('backend.abm-fondos.composicion.procesar-edit') }}"
                            class="col-xs-6 btn btn-success form-control"
                            style="color: #FFFFFF !important;">

                        Actualizar&nbsp;<i class="fa fa-save"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
