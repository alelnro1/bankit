@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">FCI
            <div style="float: right;">
                <a href="{{ route('backend.fondos.exportar-cotizaciones-txt') }}" class="btn btn-sucess btn-xs"
                   id="exportar-op-pend-xls" target="_blank">Exportar a TXT</a>
            </div>
    </div>

    <div class="panel-body">
        @if (Session::has('fci_actualizado'))
            <div class="alert alert-success">
                El FCI ha sido actualizado.
            </div>
        @endif

        @if (count($fondos) > 0)
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Abreviatura</th>
                        <th>Categoria</th>
                        <th>Gerente</th>
                        <th>Custodia</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($fondos as $fondo)
                        <tr>
                            <td> 
                                <a href="{{ route('backend.abm-fondos.show', ['fondo' => $fondo->id]) }}">
                                    {{ $fondo->getNombreWeb() }}
                                </a>
                            </td>
                            <td>{{ $fondo->getAbreviatura() }}</td>
                            <td>{{ $fondo->categoria }}</td>
                            <td>{{ $fondo->sociedad_gerente }}</td>
                            <td>{{ $fondo->sociedad_custodia }}</td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-warning">
                <span>No hay FCI creados en el sistema.</span>
            </div>
        @endif
    </div>
@endsection
