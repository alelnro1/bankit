@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">FCI {{ $fondo->getNombreWeb() }}</div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                <div id="tabs-abm-fci">
                    <ul>
                        <li><a href="#tabs-1">Datos del FCI</a></li>
                        <li><a href="#tabs-2">Composición</a></li>
                    </ul>
                    <div id="tabs-1">
                        @include('backend.fci.edit', ['fondo' => $fondo, 'monedas' => $monedas])
                    </div>
                    <div id="tabs-2">
                        @include('backend.fci.composicion', ['composiciones' => $fondo->Composicion])
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
