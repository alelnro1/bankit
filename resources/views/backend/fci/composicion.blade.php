<div style="text-align: right">
    <button class="btn btn-default"
            id="editar-composicion-fci"
            data-editar-composicion-url="{{ route('backend.abm-fondos.composicion.editar-item', ['fondo' => $fondo]) }}"
            data-instrumento-id="{{ $fondo->id }}">
            <span class="btn btn-warning"><i class="fas fa-edit"></i> Editar Composición de FCI</span>
    </button>
</div>

<br>

<div>
    <table class="table table-bordered table-striped" id="composicion">
        <thead>
        <tr>
            <th>Descripción</th>
            <th>Porcentaje</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($composiciones as $composicion)
            <tr>
                <td>{{ $composicion->descripcion }}</td>
                <td style="text-align: center;"> {{  number_format($composicion->porcentaje, 2, ',', '.') }} % </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

<div id="composicion-a-editar" style="display: none;"></div>