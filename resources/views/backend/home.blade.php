@extends('layouts.backend-app')

@section('content')
    @if (Session::has('CAMBIO_PASSWORD'))
        <div class="alert alert-success">
            La contraseña fue modificada con exito.
        </div>
    @endif

    <div class="panel-heading">Novedades</div>

    <div class="panel-body">
    	
    	@foreach ($novedades as $novedad)
    		<div class="alert alert-info">
    			@if($novedad[0]=="operacionesPendientesLiquidacion")
    				<strong>
    					<a href="{{ route('backend.fondos.operaciones-pendientes') }}">
    						<span>
    						{{ date("d/m/Y",strtotime($novedad[1])) }} 
    						--> Id:{{ $novedad[2]->id }} 
    						</span>
    					</a>
    				</strong>
					<br>
					<p>
						El Comitente # <strong>{{ $novedad[2]->Comitente->numero_comitente }}</strong> ha realizado una operacion de <strong>{{ $novedad[2]->TipoOperacion->nombre }}</strong> al fondo <strong>{{ $novedad[2]->Instrumento->nombre_web }}</strong> por <strong>{{ $novedad[2]->Moneda->simbolo }}
							{{ number_format($novedad[2]->importe,2,",",".") }}</strong>
					
    				
					</p>
    			@endif

    			@if($novedad[0]=="comitentesCreados")
    				<strong>
    					<a href="{{ route('backend.comitentes') }}">
    						<span>
    						{{ date("d/m/Y",strtotime($novedad[1])) }} 
    						--> Id:{{ $novedad[2]->id }} 
    						</span>
    					</a>
    				</strong>
					<br>
					<p>
						Se ha dado de alta el comitente #<strong>{{ $novedad[2]->numero_comitente }}</strong> con denominacion <strong>{{ $novedad[2]->descripcion }} </strong>
					
    				
					</p>
    			@endif

    			@if($novedad[0]=="prospectosFinalizados")
    				<strong>
    					<a href="{{ route('backend.prospectos') }}">
    						<span>
    						{{ date("d/m/Y",strtotime($novedad[1])) }} 
    						--> Id:{{ $novedad[2]->id }} 
    						</span>
    					</a>
    				</strong>
					<br>
					<p>
						El prospecto #<strong>{{ $novedad[2]->id }}</strong> ha finalizado el proceso de alta de clientes y espera revision
					</p>
    			@endif

    			@if($novedad[0]=="usuariosCreados")
    				<strong>
    					<a href="{{ route('backend.interno.index   ') }}">
    						<span>
    						{{ date("d/m/Y",strtotime($novedad[1])) }} 
    						--> Id:{{ $novedad[2]->id }} 
    						</span>
    					</a>
    				</strong>
					<br>
					<p>
						Se ha dado de alta al usuario #<strong>{{ $novedad[2]->id }}</strong>
						.- {{ $novedad[2]->apellido . ", ". $novedad[2]->nombre }}
					</p>
    			@endif

    			@if($novedad[0]=="usuariosModificados")
    				<strong>
    					<a href="{{ route('backend.interno.index') }}">
    						<span>
    						{{ date("d/m/Y",strtotime($novedad[1])) }} 
    						--> Id:{{ $novedad[2]->id }} 
    						</span>
    					</a>
    				</strong>
					<br>
					<p>
						El usuario #<strong>{{ $novedad[2]->id }}</strong>
						.- {{ $novedad[2]->apellido . ", ". $novedad[2]->nombre }}
						ha sido modificado
					</p>
    			@endif
    		</div>
    	@endforeach

    </div>
@endsection
