@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">
        Usuarios Internos
        <div style="float: right;">
            <a href="{{ route('backend.interno.nuevo') }}" class="btn btn-xs btn-success">Nuevo Usuario Interno</a>
        </div>
    </div>

    <div class="panel-body">
        @if(Session::has('interno_creado'))
            <div class="alert alert-success">
                {{ Session::get('interno_creado') }}
            </div>
        @endif

        @if(Session::has('interno_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('interno_actualizado') }}
            </div>
        @endif

        @if(Session::has('interno_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('interno_eliminado') }}
            </div>
        @endif

        <table class="table-responsive table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Permisos</th>
                    <th>Acciones</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($internos as $interno)
                    <tr>
                        <td>{{ $interno->getEmail() }}</td>
                        <td>{{ $interno->Permisos }}</td>
                        <td style="text-align: center;">

                            <a href="{{ route('backend.interno.modificar', ['interno' => $interno->getId()]) }}" class="btn btn-xs btn-warning">
                                Modificar
                            </a>

                            <a href="{{ route('backend.interno.eliminar') }}"
                               data-interno-id="{{ $interno->getId() }}"
                               class="btn btn-xs btn-danger eliminar-interno"
                               data-confirm="Eliminar interno?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
