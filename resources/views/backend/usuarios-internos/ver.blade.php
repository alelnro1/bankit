@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">
        Usuario Interno <b><i>{{ $interno->getNombreCompleto() }}</i></b>

        <div style="float: right;">
            <a href="{{ route('backend.interno.modificar', ['usuario' => $interno->getId()]) }}" class="btn btn-xs btn-default">Modificar</a>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <fieldset>
                    <legend>Datos de Persona</legend>

                    <table class="table table-striped task-table">
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td>{{ $interno->getNombre() }} {{ $interno->getApellido() }}</td>
                        </tr>

                        <tr>
                            <td><strong>Email</strong></td>
                            <td>{{ $interno->getEmail() }}</td>
                        </tr>
                    </table>
                </fieldset>
            </div>

            <div class="col-xs-6">
                <fieldset>
                    <legend>Datos de Usuario</legend>

                    <table class="table table-striped task-table">
                        <tr>
                            <td><strong>Nombre de Usuario</strong></td>
                            <td>{{ $interno->getUsuario() }}</td>
                        </tr>

                        {{--<tr>
                            <td><strong>Fecha de Alta</strong></td>
                            <td>{{ $interno->getFechaDeAlta() }}</td>
                        </tr>--}}
                    </table>
                </fieldset>
            </div>
        </div>


        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop
