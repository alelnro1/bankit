@extends('layouts.backend-app')

@section('content')
    <div class="panel-heading">Modificación de Usuario Interno</div>

    <div class="panel-body">
        <form action="{{ route('backend.interno.modificar.submit', ['usuario' => $interno->getId()]) }}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}
            <fieldset>
                <legend>Datos del Empleado</legend>

                {{-- Nombre --}}
                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="nombre" class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $interno->getNombre() }}" required autofocus>

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Apellido --}}
                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label for="apellido" class="col-md-4 control-label">Apellido</label>

                    <div class="col-md-6">
                        <input id="apellido" type="text" class="form-control" name="apellido" value="{{ $interno->getApellido() }}" required autofocus>

                        @if ($errors->has('apellido'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellido') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Email --}}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $interno->getEmail() }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Permisos</legend>

                <div class="help-block">Seleccione las áreas a las que el interno podrá acceder</div>

                <div class="col-md-4"></div>
                <div class="col-md-6 {{ $errors->has('permisos') ? ' has-error' : '' }}">
                    <select id="permisos" name="permisos[]" multiple="multiple">
                        @foreach ($permisos as $permiso)
                            <option value="{{ $permiso->id }}" @if ($permiso->asignado) selected @endif>{{ $permiso->getNombre() }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('permisos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('permisos') }}</strong>
                        </span>
                    @endif
                </div>

            </fieldset>

            <fieldset>
                <legend>Datos del Usuario</legend>

                {{-- Nombre de Usuario --}}
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="username" class="col-md-4 control-label">Nombre de Usuario</label>

                    <div class="col-md-6">
                        <input id="username" type="text" class="form-control" name="username" value="{{ $interno->getUsuario() }}"required>

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Contraseña --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Contraseña</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Confirmar Contraseña --}}
                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>
            </fieldset>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Modificar Interno
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection