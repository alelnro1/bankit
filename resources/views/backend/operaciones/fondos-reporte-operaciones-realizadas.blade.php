@extends('layouts.backend-app')

@section('styles')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('css/styles.css') }}">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
@endsection

@include('_snippets.preloader')

@section('content')
    <div class="panel-heading">
        Todas las operaciones
    </div>
    <div class="panel-body">
        <input type="hidden" id="operaciones-pendientes-fondos" 
        data-cargar-operacion-url="{{ route('backend.fondos.cargar-operacion-pendiente') }}">
        

        <div class="row">
            <div class="col-xs-8">
                <form action="{{ route('backend.fondos.operaciones-realizadas') }}" method="GET" id="cambiar-fecha">
                    <div class="col-xs-4">
                        <div class='input-group date' id='fecha_desde-operaciones-realizadas'>
                            <input type='text' class="form-control" name="fecha_desde" id="fecha_desde-realizadas"
                                   value="{{ $fecha_desde }}"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class='input-group date' id='fecha_hasta-operaciones-realizadas'>
                            <input type='text' class="form-control" name="fecha_hasta" id="fecha_hasta-realizadas"
                                   value="{{ $fecha_hasta }}"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-xs-4" style="text-align: left;">
                @if (count($operaciones) > 0)
                    <div style="float: left;">
                        <a href="{{ route('operaciones.exportar-realizadas-xls') }}" class="btn btn-sucess btn-xs"
                           id="exportar-op-pend-xls" target="_blank">Exportar a XLS</a>
                    </div>

                @endif
            </div>
        </div>
        <br>


        @if (count($operaciones) > 0)
            <table class="table-responsive table table-bordered table-striped" id="operaciones-aprobadas-fondos">
                <thead>
                <tr>
                    <th style="background: #5faee3;">Operación</th>
                    <th>Instrumento</th>
                    <th>Abreviatura</th>
                    <th>#</th>
                    <th>Moneda</th>
                    <th>Fecha Concertacion</th>
                    <th>Fecha Liquidacion</th>
                    <th>Comitente</th>
                    <th>Cantidad</th>
                    <th>Importe</th>
                    <th>Estado</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($operaciones as $operacion)
                    <tr>
                        <td>{{ $operacion->TipoOperacion->nombre }}</td>
                        <td>
                            <a href="" class="ver-operacion-fondo-sin-botones"
                               data-operacion-id="{{ $operacion->id }}">
                                {{ $operacion->Instrumento->nombre_web }}
                            </a>
                        </td>

                        <td>{{ $operacion->Instrumento->nombre_web }}</td>
                        <td>{{ $operacion->id }}</td>
                        <td>{{ $operacion->Moneda->simbolo }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->fecha_concertacion)) }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->fecha_liquidacion)) }}</td>
                        <td>{{ $operacion->Comitente->descripcion }}</td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->cantidad, 6, ',', '.') }}
                        </td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->importe, 2, ',', '.') }}
                        </td>
                        <td
                        @if($operacion->Estado->id==1 & isset($operacion->Desposito->comprobante))
                            title="Pendiente de Liquidacion"
                        @endif
                        
                        @if($operacion->Estado->id==1 & !isset($operacion->Desposito->comprobante))
                            title="Pendiente de Comprobante"
                        @endif
                        >

                        {{ $operacion->Estado->descripcion }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="info">
                No hay operaciones pendientes
            </div>
        @endif
    </div>

    {{-- En este div se van a ir cargando las vistas de los depositos que se abrieron --}}
    <div id="operaciones"></div>
@endsection