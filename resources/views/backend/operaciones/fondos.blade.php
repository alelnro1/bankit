@extends('layouts.backend-app')

@section('styles')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('css/styles.css') }}">
@endsection

@include('_snippets.preloader')

@section('content')
    <div class="panel-heading">
        Operaciones pendientes de Envio a sociedad Gerente

        @if (count($operaciones) > 0)
            <div style="float: right;">
                <a href="{{ route('operaciones.exportar-pendientes-xls') }}" class="btn btn-sucess btn-xs"
                   id="exportar-op-pend-xls" target="_blank">Exportar a XLS</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        <div class="alert alert-info">
            Para poder revisar el detalle de la operación por favor clickee en la descripcion del instrumento.
        </div>

        @if (count($operaciones) > 0)
            <table class="table-responsive table table-bordered table-striped" id="operaciones-pendientes-fondos"
                   data-cargar-operacion-url="{{ route('backend.fondos.cargar-operacion-pendiente') }}">
                <thead>
                <tr>
                    <th style="background: #5faee3;">Operación</th>
                    <th>Descripción</th>
                    <th>Abreviatura</th>
                    <th>Fecha</th>
                    <th>Comitente</th>
                    <th>Cantidad</th>
                    <th>Importe</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($operaciones as $operacion)
                    <tr>
                        <td>{{ $operacion->getNombreTipoOperacion() }}</td>
                        <td>
                            <a href="" class="ver-operacion-fondo"
                               data-operacion-id="{{ $operacion->id }}">
                                     {{ $operacion->getNombreInstrumento() }}
                            </a>
                        </td>

                        <td>{{ $operacion->getAbreviaturaInstrumento() }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->created_at)) }}</td>
                        <td>{{ $operacion->Comitente->getDescripcion() }}</td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->cantidad, 6, ',', '.') }}
                        </td>

                        <td style="text-align: right;">
                            $ {{ number_format($operacion->importe, 2, ',', '.') }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="info">
                No hay operaciones pendientes
            </div>
        @endif
    </div>

    {{-- En este div se van a ir cargando las vistas de los depositos que se abrieron --}}
    <div id="operaciones"></div>
@endsection