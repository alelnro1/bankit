 
<div id="modal-operacion-pendiente" title="Operación sobre FCI"
    data-procesar-operacion-url="{{ route('backend.fondos.enviar-operacion-intl') }}"
     data-desaprobar-operacion-url="{{ route('backend.fondo-desaprobar-operacion') }}"
     data-estado-operacion="{{ $operacion->Estado->id }}"
    data-tipo-operacion="{{ $operacion->getNombreTipoOperacion() }}">
    <div class="row">
        <div class="col-xs-7">
            <table class="table table-striped table-bordered">
               <tr>
                    <td><strong>Comitente N°:</strong></td>
                    <td>{{ $operacion->Comitente->numero_comitente }}</td>
                </tr>

                <tr>
                    <td><strong>Nombre del Comitente</strong></td>
                    <td>{{ $operacion->getNombreComitente() }}</td>
                </tr>
                <tr>
                    <td><strong>Moneda</strong></td>
                    <td>{{ $operacion->getNombreMoneda() }}</td>
                </tr>

                <tr>
                    <td>
                        <strong>Instrumento</strong>
                    </td>
                    <td>
                        {{ $operacion->getNombreInstrumento() }}
                    </td>
                </tr>

                <tr>
                    <td>
                        <strong>Tipo de Operación</strong>
                    </td>
                    <td>
                        {{ $operacion->getNombreTipoOperacion() }}
                    </td>
                </tr>

                <tr>
                    <td>
                        <strong>Cantidad</strong>
                    </td>
                    <td>
                        {{ number_format($operacion->getCantidad(), 6, ',', '.') }}
                    </td>
                </tr>

                <tr>
                    <td>
                        <strong>Importe</strong>
                    </td>
                    <td>
                        {{ number_format($operacion->getImporte(), 2, ',', '.') }}
                    </td>
                </tr>
            </table>
        </div>

        @if (isset($operacion->Deposito->comprobante))
            <div class="col-xs-5">
                <fieldset>
                    <div style="font-size: 14px; font-family: Raleway, sans-serif; margin-bottom: 20px">COMPROBANTE
                    </div>

                    <div style="max-width: inherit">
                        <div style="width: 100%; height: 100%">

                            @if ($extension == 'doc' || $extension == 'docx' || $extension == 'xls' || $extension == 'xlsx' || $extension == 'msword' )
                                <p class="text-center" style="padding: 50px 0">No se pudo mostrar una visualización
                                    previa del archivo</p>
                                <p class="text-center">
                                    <a href="{{ asset('uploads/comprobantes-depositos/' . $dir_comprobante) }}"
                                       target="_blank">
                                        Descargar
                                    </a>
                                </p>
                            @else
                                <embed id="frPDF" height="300" width="100%"
                                       src="{{ asset('uploads/comprobantes-depositos/' . $dir_comprobante) }}"></embed>
                                <p class="text-center">
                                    <a href="{{ asset('uploads/comprobantes-depositos/' . $dir_comprobante) }}"
                                       target="_blank">
                                        Visualizar Documento
                                    </a>
                                </p>
                            @endif
                        </div>
                    </div>
                </fieldset>
            </div>
        @endif
    </div>

</div>