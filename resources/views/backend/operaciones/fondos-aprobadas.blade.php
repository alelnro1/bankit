@extends('layouts.backend-app')

@section('styles')
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('css/styles.css') }}">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
@endsection

@include('_snippets.preloader')

@section('content')
    <div class="panel-heading">
        Operaciones aprobadas
    </div>
    <div class="panel-body">
        <input type="hidden" id="operaciones-pendientes-fondos" 
        data-cargar-operacion-url="{{ route('backend.fondos.cargar-operacion-pendiente') }}">
        
        <div class="alert alert-info">
            Las operaciones detalladas a continuacion ya fueron aprobadas y estan listas para cargar en SFB
        </div>

        <div class="row">
            <div class="col-xs-4">
                <form action="{{ route('backend.fondos.operaciones-aprobadas') }}" method="GET" id="cambiar-fecha">
                    <div class='input-group date' id='fecha-operaciones-aprobadas'>
                        <input type='text' class="form-control" name="fecha" id="fecha-aprobadas"
                               value="{{ $fecha }}"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-xs-8" style="text-align: left;">
                @if (count($operaciones) > 0)
                    <div style="float: left;">
                        <a href="{{ route('operaciones.exportar-aprobadas-xls') }}" class="btn btn-sucess btn-xs"
                           id="exportar-op-pend-xls" target="_blank">Exportar a XLS</a>
                    </div>

                    <div style="float: left;">
                        <a href="{{ route('operaciones.exportar-aprobadas-txt') }}" class="btn btn-sucess btn-xs"
                           id="exportar-op-pend-xls" target="_blank">Exportar a TXT</a>
                    </div>
                @endif
            </div>
        </div>
        <br>


        @if (count($operaciones) > 0)
            <table class="table-responsive table table-bordered table-striped" id="operaciones-aprobadas-fondos">
                <thead>
                <tr>
                    <th style="background: #5faee3;">Operación</th>
                    <th>Descripción</th>
                    <th>Abreviatura</th>
                    <th>Fecha</th>
                    <th>Comitente</th>
                    <th>Cantidad</th>
                    <th>Importe</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($operaciones as $operacion)
                    <tr>
                        <td>{{ $operacion->getNombreTipoOperacion() }}</td>
                        <td>
                            <a href="" class="ver-operacion-fondo"
                               data-operacion-id="{{ $operacion->id }}">
                                {{ $operacion->getNombreInstrumento() }}
                            </a>
                        </td>

                        <td>{{ $operacion->getAbreviaturaInstrumento() }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->created_at)) }}</td>
                        <td>{{ $operacion->Comitente->getDescripcion() }}</td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->cantidad, 6, ',', '.') }}
                        </td>

                        <td style="text-align: right;">
                            $ {{ number_format($operacion->importe, 2, ',', '.') }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="info">
                No hay operaciones pendientes
            </div>
        @endif
    </div>

    {{-- En este div se van a ir cargando las vistas de los depositos que se abrieron --}}
    <div id="operaciones"></div>
@endsection