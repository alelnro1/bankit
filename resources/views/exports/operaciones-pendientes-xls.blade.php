<table class="table-responsive table table-bordered table-striped" id="operaciones-pendientes-fondos">
    <thead>
    <tr>
        <th style="background: #5faee3;">Operación</th>
        <th>Instrumento</th>
        <th>Abreviatura</th>
        <th>#</th>
        <th>Moneda</th>
        <th>Fecha Concertacion</th>
        <th>Fecha Liquidacion</th>
        <th>Comitente</th>
        <th>Cantidad</th>
        <th>Importe</th>
        <th>Estado</th>
    </tr>
    </thead>

    <tbody>
    @foreach ($operaciones as $operacion)
            <tr>
                        <td>{{ $operacion->TipoOperacion->nombre }}</td>
                        <td>
                            <a href="" class="ver-operacion-fondo-sin-botones"
                               data-operacion-id="{{ $operacion->id }}">
                                {{ $operacion->Instrumento->nombre_web }}
                            </a>
                        </td>

                        <td>{{ $operacion->Instrumento->nombre_web }}</td>
                        <td>{{ $operacion->id }}</td>
                        <td>{{ $operacion->Moneda->simbolo }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->fecha_concertacion)) }}</td>
                        <td>{{ date("d/m/Y H:i", strtotime($operacion->fecha_liquidacion)) }}</td>
                        <td>{{ $operacion->Comitente->descripcion }}</td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->cantidad, 6, ',', '.') }}
                        </td>

                        <td style="text-align: right;">
                            {{ number_format($operacion->importe, 2, ',', '.') }}
                        </td>
                        <td
                        @if($operacion->Estado->id==1 & isset($operacion->Desposito->comprobante))
                            title="Pendiente de Liquidacion"
                        @endif
                        
                        @if($operacion->Estado->id==1 & !isset($operacion->Desposito->comprobante))
                            title="Pendiente de Comprobante"
                        @endif
                        >

                        {{ $operacion->Estado->descripcion }}</td>
            </tr>
    @endforeach
    </tbody>
</table>