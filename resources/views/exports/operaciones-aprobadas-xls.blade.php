<table class="table-responsive table table-bordered table-striped" id="operaciones-aprobadas-fondos">
    <thead>
    <tr>
        <th>Operación</th>
        <th>Descripción</th>
        <th>Abreviatura</th>
        <th>Fecha</th>
        <th>Comitente</th>
        <th>Cantidad</th>
        <th>Importe</th>
    </tr>
    </thead>

    <tbody>
    @foreach ($operaciones as $operacion)
        <tr>
            <td>{{ $operacion->getNombreTipoOperacion() }}</td>
            <td>{{ $operacion->getNombreInstrumento() }}</td>
            <td>{{ $operacion->getAbreviaturaInstrumento() }}</td>
            <td>{{ date("d/m/Y H:i", strtotime($operacion->created_at)) }}</td>
            <td>{{ $operacion->Comitente->getDescripcion() }}</td>

            <td style="text-align: right;">
                {{ number_format($operacion->cantidad, 4, ',', '.') }}
            </td>

            <td style="text-align: right;">
                $ {{ number_format($operacion->importe, 2, ',', '.') }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>