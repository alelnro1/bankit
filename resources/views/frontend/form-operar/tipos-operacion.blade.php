@foreach ($operaciones as $key => $operacion)
    <div
            class="col-xs-6"

            @if ($key % 2 === 0)
            style="float: left; margin-left: -10px; padding-right:10px;"
            @else
            style="float: right"
            @endif
    >
        <input type="radio" name="tipo_operacion" id="tipo-operacion"
               data-nombre="{{ $operacion->getNombre() }}"
               value="{{ $operacion->id }}" required>
        {{ $operacion->getNombreDisplay() }}
    </div>
@endforeach