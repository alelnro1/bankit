<!-- Modal -->
<div class="modal fade" id="modal-confirmar-operacion" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #F2AC25; color: #FFFFFF !important;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirme la Operación</h4>
            </div>

            <div class="modal-body">
                <p>Por favor confirme que la operación a realizar.</p>

                <p>
                    <strong>Instrumento</strong>: <span id="confirmar-operar-especie"></span>
                </p>

                <p>
                    <strong>Plazo de Liquidación</strong>: <span id="confirmar-operar-plazo-liquidacion"></span>
                </p>

                <p>
                    <strong>Tipo de Operación</strong>: <span id="confirmar-operar-tipo-operacion"></span>
                </p>

                <p>
                    <strong>Cantidad de Instrumentos estimados</strong>: <span id="confirmar-operar-cantidad-instrumentos"></span>
                </p>

                <p>
                    <strong>Valor estimado</strong>: $<span id="confirmar-operar-valor-estimado"></span>
                </p>
            </div>
            <div class="col-xs-12">
                <div style="display:none" class="row alert-rescate">
                    <hr>

                    <div class="col-xs-12 alert alert-info">
                        En casos excepcionales, por motivos de fuerza mayor, el pago puede exceder el plazo comercial, siempre dentro del plazo legal de presentada la solicitud, es decir 7 dias.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-xs-1">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="color: #FFFFFF !important;">
                        <i class="fa fa-home"></i> Cancelar
                    </button>
                </div>

                <div class="col-xs-6 pull-right">
                    <button type="button" class="btn btn-success" id="operacion-confirmada" style="color: #FFFFFF !important;">
                        Confirmar&nbsp;<i class="fa fa-save"></i>
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>