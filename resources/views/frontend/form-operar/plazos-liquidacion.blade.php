<option value="">Seleccionar</option>

@foreach ($plazos as $plazo)
    <option value="{{ $plazo->id }}"
            data-dias="{{ $plazo->dias }}">
        {{ $plazo->nombre }}
    </option>
@endforeach