{{-- Formulario de operacion --}}
<div id="modal-operar" class="iziModal"
     data-title="Operar Instrumento"
     data-header-color="#023669"
     data-restore-default-content="true"
     data-width="800"
     data-overlay="true">

    <form
            action="{{ route('operaciones.instrumentos.procesar') }}"
            {{--data-calcular-comision="{{ route('operaciones.instrumentos.calcular.comision') }}"--}}
            id="operar-instrumento">
        {!! csrf_field() !!}

        <div class="alert alert-danger operar-warning" role="alert" style="display: none;">
            <h4 class="alert-heading" id="titulo-mensaje-alert">Error!</h4>
            <p id="operar-mensajes-error"></p>aaa
        </div>

        <div class="container col-xs-12 form-operar" style="padding: 10px;">
            <div class="row">
                <div class="col-xs-6">
                    <label for="tipo_instrumento">Tipo de Instrumento</label>

                    <select name="tipo_instrumento" id="tipo_instrumento"
                            data-cargar-operaciones-url="{{ route('operaciones.disponibles.instrumentos') }}"
                            data-cargar-instrumentos-url="{{ route('operaciones.instrumento.instrumentos-por-operacion') }}"
                            data-cargar-campos-url="{{ route('operaciones.instrumento.campos-para-ingresar') }}"
                            data-cargar-plazos-monedas-url="{{ route('operaciones.instrumento.plazos-monedas-por-instrumento') }}"
                            data-cargar-cuentas-bancarias-url="{{ route('operaciones.instrumento.cuentas-bancarias-por-moneda') }}"
                            class="form-control" required>
                        @foreach (session('TIPOS_INSTRUMENTOS') as $tipo_instrumento)
                            <option value="{{ $tipo_instrumento->id }}">
                                {{ $tipo_instrumento->getDescripcion() }}
                            </option>
                        @endforeach

                    </select>
                </div>

                <div class="col-xs-6">
                    <label for="tipo_operacion">Tipo de Operación</label>

                    <div id="tipos-operaciones"
                            {{--data-cargar-plazos-url="{{ route('operaciones.cargar.plazos.por.tipo') }}"--}}></div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="intrumento">Instrumento</label>

                    <div id="intrumento-operar">
                        <select name="instrumento" id="instrumento"
                                class="form-control"
                                data-live-search="true"
                                data-size="7"
                                required>
                            <option value="">Seleccionar</option>
                        </select>
                    </div>

                    <div id="cargando-intrumento" style="display: none;">
                        <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                    </div>
                </div>

                <div class="col-xs-3">
                    <label for="plazo_liquidacion">Plazos</label>

                    <div id="plazos-liquidacion">
                        <select name="plazo_liquidacion" id="plazo_liquidacion"
                                {{--data-cargar-especies-url="{{ route('operaciones.cargar.especies.liquidacion') }}"--}}
                                class="form-control" required>
                            <option value="">Seleccionar</option>
                        </select>
                    </div>

                    <div id="cargando-plazos-liquidacion" style="display: none;">
                        <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                    </div>
                </div>

                <div class="col-xs-3">
                    <label for="plazo_liquidacion">Moneda</label>

                    <div id="monedas">
                        <select name="moneda" id="moneda"
                                {{--data-cargar-especies-url="{{ route('operaciones.cargar.especies.liquidacion') }}"--}}
                                class="form-control" required>
                            <option value="">Seleccionar</option>
                        </select>
                    </div>

                    <div id="cargando-monedas" style="display: none;">
                        <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                    </div>
                </div>
            </div>

            
            <div id="datos-operacion"></div>

            <div class="col-xs-12">
                <div style="display:none" class="row alert-suscripcion">
                    <hr>

                    <div class="col-xs-12 alert alert-info">
                        La información sobre precios de especies y saldos valorizados es a titulo informativo.
                        <br>    
                        Cosulte la información sobre los precios de especies y saldos.
                    </div>
                </div>


                <div style="display:none" class="row alert-rescate">
                    <hr>

                    <div class="col-xs-12 alert alert-info">
                        El monto de rescate se determina el día en que se acepta la solicitud de rescate, al cierre de las operaciones del fondo, de acuerdo a la normativa vigente. El monto estimado a percibir se determinó con el último valor de la cuotaparte conocido.
                    </div>
                </div>

                <div style="display:none" class="row checkbox-reglamento col-xs-12">
                    <hr>

                    <div class="col-xs-12">
                        <input id="check_reglamento" type="checkbox"> Declaro que acepto el reglamento de gestion. haga
                        click <a id="link_reglamento" href="" target="_blank">aqui</a> para visualizar el mismo
                    </div>
                </div>
            </div>
            <hr>


            <div class="row">
                <div class="col-xs-6">
                    <a data-izimodal-close="" data-izimodal-transitionout="bounceOutDown"
                            class="form-control col-xs-6 btn btn-danger"
                            style="color: #FFFFFF !important;">

                        <i class="fa fa-home"></i> Cancelar
                    </a>
                </div>
                <div class="col-xs-6">
                    <button type="submit" id="confirmar-operacion"
                            class="col-xs-6 btn btn-success form-control"
                            style="color: #FFFFFF !important;">

                        Continuar&nbsp;<i class="fa fa-save"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

@include('frontend.form-operar.confirmar-operacion')