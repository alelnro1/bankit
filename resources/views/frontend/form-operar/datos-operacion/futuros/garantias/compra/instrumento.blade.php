<tr>
    <td>{{ $especie->Abreviatura }}</td>
    <td style="text-align: right;">
        @if (isset($especie->Cotizacion))
            {{ number_format($especie->Cotizacion, 2, ',', '.') }}
        @endif
    </td>
    <td style="text-align: right;">
        {{ number_format($especie->PosicionHoyDisp, 2, ',', '.') }}
    </td>
    <td>
        <input type="number"
               class="cantidad-garantia"
               data-cotizacion="{{ $especie->Cotizacion }}"
               data-abreviatura="{{ str_replace(' ', '', $especie->Abreviatura) }}"

               @if (isset($especie->especie_id))
               data-especie-id="{{ $especie->especie_id }}"
               @endif

               @if (isset($especie->fondo_id))
               data-fondo-id="{{ $especie->fondo_id }}"
               @endif

               @if ($especie->moneda_id != $moneda_pesos_id)
               data-moneda-cotizacion="{{ $cotizacion_dolar }}"
               @else
               data-moneda-cotizacion="1"
               @endif

               max="{{ $especie->PosicionHoyDisp }}"

               @if (isset($especie->Porc_Aforo))
               data-porc-aforo="{{ $especie->Porc_Aforo }}"
               @endif
               min="0"
               style="width: 110px; padding: 0px 5px;"
               pattern="\d+"
               step="1"
               value="0">
    </td>
    <td>
        {{ number_format($especie->Porc_Aforo * 100, 2) }}%
    </td>
    <td id="total-garantia-{{ str_replace(' ', '', $especie->Abreviatura) }}" class="subtotal-garantia"
        data-cantidad=""></td>
</tr>
