<div class="row" id="garantias-instrumentos">
    <div class="col-xs-12">
        <hr>

        <div class="alert alert-info">
            Usted necesita
            $ <strong>
                <span id="garantia-requerida" data-cantidad="{{ $garantia_requerida }}">
                    {{ number_format($garantia_requerida, 2, ',', '.') }}
                </span>
            </strong> de garantía
            y tiene destinados $ <strong><span id="garantia-destinada" data-cantidad=""></span></strong>.
            Espefique los instrumentos que desea asignar como garantía para operar.
        </div>

        <div class="col-xs-12" style="max-height: 220px; overflow-y: scroll;">
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                <tr style="background:  rgb(242, 172, 37); font-weight: bold; color:#FFF">
                    <td>Especie</td>
                    <td>Cotización</td>
                    <td>Disponibles</td>
                    <td>Para Garante</td>
                    <td>% Aforo</td>
                    <td>Total Destinado ARS</td>
                </tr>
                </thead>

                <tbody>
                @foreach ($especies_garantes as $especie)

                    @if (!isset($especie->especie_id) && !(isset($especie->fondo_id)) )
                        {{-- Estamos trabajando con la disponibilidad --}}
                        @include('layouts.modals.datos-operacion.futuros.garantias.compra.disponibilidad', ['disponibilidad' => $especie])
                    @else
                        {{-- Estamos trabajando con un instrumento --}}
                        @include('layouts.modals.datos-operacion.futuros.garantias.compra.instrumento', ['especie' => $especie])
                    @endif

                @endforeach
                </tbody>
            </table>

            <table id="header-fixed"></table>
        </div>
    </div>
</div>