@if ($moneda_especie_solicitada_id == $disponibilidad->moneda_id)
    <tr>
        <td>Disponibilidad: <strong>{{ $disponibilidad->Abreviatura }}</strong></td>
        <td colspan="2" style="text-align: right;">
            {{ number_format($disponibilidad->cantidad, 2, ',', '.') }}
        </td>
        <td>
            <input type="number"
                   class="cantidad-garantia"
                   data-abreviatura="{{ $disponibilidad->Abreviatura }}"
                   data-moneda-id="{{ $disponibilidad->moneda_id }}"

                   @if ($disponibilidad->moneda_id != $moneda_pesos_id)
                   data-moneda-cotizacion="{{ $cotizacion_dolar }}"
                   @else
                   data-moneda-cotizacion="1"
                   @endif

                   data-cotizacion="1"

                   data-garantia-id="{{ $disponibilidad->id }}"

                   min="0"
                   max="{{ $disponibilidad->cantidad }}"
                   style="width: 110px; padding: 0px 5px;"
                   step="0.1"
                   pattern="[0-9]*\.?[0-9]"
                   value="0">
        </td>
        <td id="total-garantia-{{ $disponibilidad->Abreviatura }}" class="subtotal-garantia"
            data-cantidad=""></td>
    </tr>
@endif
