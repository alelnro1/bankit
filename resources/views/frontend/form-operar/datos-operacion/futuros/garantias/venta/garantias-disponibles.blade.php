<div class="row" id="garantias-instrumentos">
    <div class="col-xs-12">
        <hr>

        <div class="alert alert-info">
            Usted necesita $ <strong><span id="garantia-requerida" data-cantidad="{{ $garantia_requerida }}">{{ $garantia_requerida }}</span></strong> de garantía
            y tiene destinados $ <strong><span id="garantia-destinada" data-cantidad=""></span></strong>.
            Espefique los instrumentos que desea asignar como garantía para operar.
        </div>

        <div class="col-xs-12" style="max-height: 220px; overflow-y: scroll;">
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                <tr style="background:  rgb(242, 172, 37); font-weight: bold; color:#FFF">
                    <td>Especie</td>
                    <td>Cotización</td>
                    <td>En garantía</td>
                    <td>Para Liberar</td>
                    <td>Total ARS a liberar</td>
                </tr>
                </thead>

                <tbody>
                @foreach ($especies_garantes as $instrumento)
                    @if (!isset($instrumento->especie_id) && !(isset($instrumento->fondo_id)) )
                        {{-- Estamos trabajando con la disponibilidad --}}
                        @include('layouts.modals.datos-operacion.futuros.garantias.venta.disponibilidad', ['disponibilidad' => $instrumento])
                    @else
                        {{-- Estamos trabajando con un instrumento --}}
                        @include('layouts.modals.datos-operacion.futuros.garantias.venta.instrumento', ['instrumento' => $instrumento])
                    @endif

                @endforeach
                </tbody>
            </table>

            <table id="header-fixed"></table>
        </div>
    </div>
</div>