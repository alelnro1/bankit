<tr>
    <td>{{ $instrumento->Abreviatura }}</td>
    <td style="text-align: right;">
        @if (isset($instrumento->Cotizacion))
            {{ number_format($instrumento->Cotizacion, 2, ',', '.') }}
        @endif
    </td>
    <td style="text-align: right;">
        {{ number_format($instrumento->PosicionHoyDisp, 2, ',', '.') }}
    </td>
    <td>
        <input type="number"
               class="cantidad-garantia"
               data-cotizacion="{{ $instrumento->Cotizacion }}"
               data-abreviatura="{{ str_replace(' ', '', $instrumento->Abreviatura) }}"
               @if (isset($instrumento->especie_id))
               data-especie-id="{{ $instrumento->especie_id }}"
               @endif

               @if (isset($instrumento->fondo_id))
               data-fondo-id="{{ $instrumento->fondo_id }}"
               @endif

               @if ($instrumento->moneda_id != $moneda_pesos_id)
               data-moneda-cotizacion="{{ $cotizacion_dolar }}"
               @else
               data-moneda-cotizacion="1"
               @endif

               max="{{ $instrumento->PosicionHoyDisp }}"

               data-garantia-id="{{ $instrumento->id }}"

               min="0"
               style="width: 110px; padding: 0px 5px;"
               pattern="\d+"
               step="1"
               value="0">
    </td>
    <td id="total-garantia-{{ str_replace(' ', '', $instrumento->Abreviatura) }}" class="subtotal-garantia"
        data-cantidad=""></td>
</tr>
