<!-- COTIZACIONES -->
<div class="row" id="cotizaciones" style="text-align: center; background: #ffff99; padding: 10px; margin:10px 0px;">
    <div class="row">
        <div class="col-xs-3">
            <strong>Mínimo</strong><br>
            $<span id="limite-minimo"></span>
        </div>

        <div class="col-xs-3">
            <strong>Cuotaparte</strong><br>
            $<span id="valor-cuotaparte"></span>
        </div>

        <div class="col-xs-3" id="instr-disponibles-div">
            <strong>Cant Cuotapartes</strong>
            <span id="cant-cuotapartes-disponibles" data-cant-cuotapartes-disponibles=""></span>
        </div>

        {{--<div class="col-xs-3" id="cotizacion-activo-subyacente-div" style="display: none;">
            <strong>Cotiz Acción</strong>
            <span id="cotizacion_accion_subyacente"></span>
        </div>

        <div class="col-xs-3">
            <strong>Saldo</strong><br>
            $<span id="saldo-valorizado"></span>
        </div>--}}
    </div>
</div>


<!-- CAMPOS -->
<div class="row">
    @include('frontend.fondos.rescate.campos-rescate.tipo-rescate')

    <div class="col-xs-6">
        <label for="cantidad">Cantidad</label>
        <input type="text" placeholder="Cantidad" id="cantidad" name="cantidad"
               class="form-control cantidad" autocomplete="off"
               required>
    </div>

    @include('frontend.form-operar.datos-operacion.fondos.campos-especiales.cuentas-bancarias')
</div>