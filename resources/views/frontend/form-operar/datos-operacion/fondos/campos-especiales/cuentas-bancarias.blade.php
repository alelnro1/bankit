<div class="col-xs-10">
    <label for="unidad">Cuenta Bancaria</label>

    <div id="cuentas-bancarias">
        <select name="cuenta_bancaria" id="cuentas_bancarias_select" class="form-control" required>
            <option value="">Seleccionar</option>
        </select>
    </div>

    <div id="cargando-cuentas-bancarias" style="display: none;">
        <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
    </div>
</div>
