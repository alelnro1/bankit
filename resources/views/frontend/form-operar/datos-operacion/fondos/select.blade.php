<option value="">Seleccionar</option>

@foreach ($fondos as $fondo)
    <option value="{{ $fondo->id }}"
            data-nombre="{{ $fondo->getNombreWeb() }}"
            data-reglamento="URL"
            data-limite-minimo="{{ $fondo->getLimiteMinimo() }}">
        {{ $fondo->getNombreWeb() }}
    </option>
@endforeach