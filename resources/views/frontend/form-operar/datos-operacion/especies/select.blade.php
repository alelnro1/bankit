<option value="">Seleccionar</option>

@foreach ($especies as $especie)
    <option value="{{ $especie->Simbolo }}"
            @if (isset($especie->Plata_Disponible))
            data-plata-disponible="{{ $especie->Plata_Disponible }}"
            @else

            data-plata-disponible="0"
            @endif
            @if (isset($especie->Cant_Inst_Dispo))
            data-cant-inst-disponibles="{{ $especie->Cant_Inst_Dispo }}"
            @else
            data-cant-inst-disponibles="0"
            @endif

            data-precio="{{ $especie->Ultimo }}"
            data-cant-lote="{{ $especie->Cant_Lote }}"

            @if (isset($especie->Cotizacion_Activo_Subyacente))
            data-cotizacion-accion="{{ $especie->Cotizacion_Activo_Subyacente }}"
            @else
            data-cotizacion-accion="0"
            @endif

            data-bid="{{ $especie->Precio_Compra  }}"
            data-ask="{{ $especie->Precio_Venta }}">
        {{ $especie->Simbolo }}
    </option>
@endforeach