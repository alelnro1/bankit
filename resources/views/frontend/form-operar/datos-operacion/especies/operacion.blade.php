<!-- COTIZACIONES -->
<div class="row" id="cotizaciones" style="text-align: center; background: #ffff99; padding: 10px; margin:10px 0px;">
    <div class="row">
        <div class="col-xs-3">
            <strong>Precio</strong><br>
            $<span id="precio_especie"></span>
        </div>
        <div class="col-xs-3">
            <strong>Bid</strong><br>
            $<span id="bid_especie"></span>
        </div>
        <div class="col-xs-3">
            <strong>Ask</strong><br>
            $<span id="ask_especie"></span>
        </div>
        <div class="col-xs-3" id="plata-disponible-div" style="display: none;">
            <strong>Dinero Disponible</strong>
            <span id="plata_disponible"></span>
        </div>

        <div class="col-xs-3" id="instr-disponibles-div" style="display: none;">
            <strong>Instr Disponibles</strong>
            <span id="instrumentos_disponibles"></span>
        </div>

        <div class="col-xs-3" id="cotizacion-activo-subyacente-div" style="display: none;">
            <strong>Cotiz Acción</strong>
            <span id="cotizacion_accion_subyacente"></span>
        </div>
    </div>
</div>


<!-- CAMPOS -->
<div class="row">
    <div class="col-xs-6">
        <label for="unidad">Unidad</label>
        <select name="unidad" id="tipo-calculo" class="form-control" required>
            <option value="">Seleccionar</option>
            <option value="dinero">Dinero</option>
            <option value="papeles">Papeles</option>
        </select>
    </div>

    <div class="col-xs-6">
        <label for="cantidad">Cantidad</label>
        <input type="text" placeholder="Cantidad" id="cantidad" name="cantidad"
               class="form-control cantidad" autocomplete="off"
               required>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <label for="tipo_precio">Tipo de Precio</label>

        <select name="tipo_precio" id="tipo_precio" class="form-control" required>
            <option value="">Seleccionar</option>

            @foreach (session('OPERAR_TIPOS_PRECIO') as $tipo_precio)
                <option value="{{ $tipo_precio->id }}">
                    {{ $tipo_precio->nombre }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="col-xs-6" id="div-precio-limite" style="display: none;">
        <label for="precio">Precio</label>

        <div class="input-group">
            <span class="input-group-addon">$</span>
            <input type="text" step="0.01" placeholder="Precio"
                   class="form-control precio-limite"
                   id="precio-limite"
                   name="precio_limite">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <label for="plazo_validez">Plazo de la validez</label>

        <select name="plazo_validez" id="plazo_validez" class="form-control" required>
            <option value="">Seleccionar</option>

            @foreach (session('OPERAR_PLAZOS_VALIDEZ') as $plazo_validez)
                <option value="{{ $plazo_validez->id }}">
                    {{ $plazo_validez->nombre }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="col-xs-6" id="div-fecha-validez" style="display: none;">
        <label for="fecha_validez">Fecha</label>
        <input type="text" class="form-control" id="operar-fecha-validez" name="fecha_validez"
               placeholder="Fecha">
    </div>
</div>