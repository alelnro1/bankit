@extends('layouts.frontend-app')

@section('styles')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <style>
        th {
            text-align: center;
        }

        legend {
            background: #F2AC25;
            color: #FFF;
            padding: 5px;
        }
    </style>
@endsection

@section('content')
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-6 panel-title">
                {{ $fondo->nombre_web }}
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <div style="text-align: center;">
                        <strong>Cotización Cuotaparte</strong> <br>
                            $ {{ number_format($fondo->Cotizaciones[0]->valor_compra, 6, ',', '.') }}                        
                    </div>
                </div>
                <div class="col-xs-3">
                    <div style="text-align: center;">
                        <strong>Variación Diaria</strong> <br>
                        <strong>
                            @if ($fondo->Cotizaciones[0]->var_diaria >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @endif
                        </strong>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div style="text-align: center;">
                        <strong>Rendimiento Mensual</strong>
                        <br>
                        <strong>
                            @if ($fondo->Cotizaciones[0]->var_mensual >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_mensual, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_mensual, 2, ',', '.') }} %
                                </span>
                            @endif
                        </strong>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div style="text-align: center;">
                        <strong>Rendimiento Últimos 12 Meses</strong> <br>
                        <strong>
                            @if ($fondo->Cotizaciones[0]->var_anual >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @endif
                        </strong>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12">
                <div id="tabs-fondo">
                    <ul>
                        <li><a href="#datos-tecnicos">Datos Técnicos</a></li>
                        <li><a href="#rentabilidad">Rentabilidad</a></li>
                        <li><a href="#composicion">Objetivo y Composición</a></li>
                    </ul>

                    @include('frontend.cotizaciones.fondos.single.datos-tecnicos')

                    @include('frontend.cotizaciones.fondos.single.rentabilidad')

                    @include('frontend.cotizaciones.fondos.single.composicion')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="{{ url('js/datetimepicker.js') }}"></script>
@endsection
