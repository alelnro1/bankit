@extends('layouts.frontend-app')

@section('styles')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <style>
        th {
            text-align: center;
        }

        legend {
            background: #F2AC25;
            color: #FFF;
            padding: 5px;
        }
    </style>
@endsection

@section('content')
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-6 panel-title">
                Cotizaciones de FCI
            </div>
        </div>
    </div>
    <br>
    <div class="alert alert-info">
        Haga click en la descripcion de un fondo para poder ver el detalle del Mismo
    </div>
    <div class="panel-body">

        <div id="cotizacion-div">

        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr class="subtitle">
                    <th>Fondo</th>
                    <th>Moneda</th>
                    <th>Patrimonio</th>
                    <th>Valor Cuotaparte</th>
                    <th>Cant Cuotapartes</th>
                    <th>Diaria</th>
                    <th>Anualizada</th>
                    <th>Calificación</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($fondos as $fondo)
                    <tr>
                        <td>
                            <a href="{{ route('frontend.cotizaciones.fondos.single', ['fondo' => $fondo->id]) }}">
                                {{ $fondo->getNombreWeb() }}
                            </a>
                        </td>
                        <td>
                            {{ $fondo->Cotizaciones[0]->Moneda->descripcion }}
                        </td>
                        <td style="text-align: right;">{{ number_format($fondo->Cotizaciones[0]->patrimonio, 0, ',', '.') }}</td>
                        <td style="text-align: right;">{{ number_format($fondo->Cotizaciones[0]->valor_compra, 6, ',', '.') }}</td>
                        <td style="text-align: right;">
                            {{ number_format($fondo->Cotizaciones[0]->cant_cuotapartes, 0, ',', '.') }}
                        </td>
                        <td style="text-align: right;">
                             @if ($fondo->Cotizaciones[0]->var_diaria >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @endif
                        </td>
                        <td style="text-align: right;">
                            @if ($fondo->Cotizaciones[0]->var_anual >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @endif
                        </td>

                        {{--<td style="text-align: right;">{{ number_format($fondo->variacion_anual, 4, ',', '.') }}</td>--}}

                        <td style="text-align: center;">{{ $fondo->calificacion_cnv }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
