<div id="rentabilidad">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <table class="table table-striped">
                    <tr>
                        <td><strong>Patrimonio</strong></td>
                        <td> $ {{ number_format($fondo->Cotizaciones[0]->patrimonio, 2, ',', '.') }} </td>
                    </tr>

                    <tr>
                        <td><strong>Valor Cuotaparte</strong></td>
                        <td> $ {{ number_format($fondo->Cotizaciones[0]->valor_compra, 6, ',', '.') }} </td>
                    </tr>

                    <tr>
                        <td><strong>Valor Cuotaparte</strong></td>
                        <td> $ {{ number_format($fondo->Cotizaciones[0]->cant_cuotapartes, 2, ',', '.') }} </td>
                    </tr>

                    <tr>
                        <td><strong>Inversión Mínima</strong></td>
                        <td> $ {{ number_format($fondo->limite_minimo, 2, ',', '.') }} </td>
                    </tr>
                </table>
            </div>

            <div class="col-xs-6">
                <table class="table table-striped">
                    <tr>
                        <td><strong>Rend. Día</strong></td>
                        <td>
                            @if ($fondo->Cotizaciones[0]->var_diaria >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                                </span>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td><strong>Rend. Último Mes</strong></td>
                        <td>
                            @if ($fondo->Cotizaciones[0]->var_mensual >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_mensual, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_mensual, 2, ',', '.') }} %
                                </span>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td><strong>Rend YTD</strong></td>
                        <td>
                            @if ($fondo->Cotizaciones[0]->var_mensual >= 0)
                                <span style="color:green;">
                                    <i class="fas fa-arrow-up"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @else
                                <span style="color:red;">
                                    <i class="fas fa-arrow-down"></i> {{ number_format($fondo->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                                </span>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td><strong>Fecha Ult. Valoracion</strong></td>
                        <td> {{ date('d/m/Y', strtotime($fondo->Cotizaciones[0]->fecha_cotizacion)) }} </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <h1>Evolución</h1>

        <div class="col-xs-12">
            <div id="historico"></div>
            <?= $grafico_historico->render('AreaChart', 'Historico', 'historico') ?>

        </div>
    </div>
</div>