<div id="datos-tecnicos">
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <table class="table table-striped">
                    <tr>
                        <td><strong>Familia</strong></td>
                        <td>{{ $fondo->familia }}</td>
                    </tr>

                    <tr>
                        <td><strong>Nombre</strong></td>
                        <td>{{ $fondo->nombre_web }}</td>
                    </tr>

                    <tr>
                        <td><strong>Categoría</strong></td>
                        <td>{{ $fondo->categoria }}</td>
                    </tr>

                    <tr>
                        <td><strong>Clase</strong></td>
                        <td>{{ $fondo->clase }}</td>
                    </tr>

                    <tr>
                        <td><strong>Horizonte de inversión</strong></td>
                        <td>Moderado</td>
                    </tr>

                    <tr>
                        <td><strong>Moneda</strong></td>
                        <td>Pesos</td>
                    </tr>

                    <tr>
                        <td><strong>Código Bloomberg</strong></td>
                        <td>{{ $fondo->codigo_bloomberg }}</td>
                    </tr>
                </table>
            </div>

            <div class="col-xs-6">
                <table class="table table-striped">
                    <tr>
                        <td><strong>Sociedad Gerente</strong></td>
                        <td>{{ $fondo->sociedad_gerente }}</td>
                    </tr>

                    <tr>
                        <td><strong>Sociedad Custodia</strong></td>
                        <td>{{ $fondo->sociedad_custodia }}</td>
                    </tr>

                    <tr>
                        <td><strong>Benchmark</strong></td>
                        <td>{{ $fondo->benchmark }}</td>
                    </tr>

                    <tr>
                        <td><strong>Calificación</strong></td>
                        <td>{{ $fondo->calificacion_cnv }}</td>
                    </tr>

                    <tr>
                        <td><strong>Permanencia Recomendada</strong></td>
                        <td>{{ $fondo->permanencia_recomendada }}</td>
                    </tr>

                    <tr>
                        <td><strong>Plazo de Rescate</strong></td>
                        <td> 24 Hrs</td>
                    </tr>

                    <tr>
                        <td><strong>Código Caja</strong></td>
                        <td>{{ $fondo->codigo_caja_valores }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>