<div id="comitentes-modal"
     data-title="Cambiar de comitente"
     style="z-index: 99999999; display: none;">

    <form action="{{ route('comitentes.seleccionar.procesar') }}" method="POST">
        @csrf
        <div class="row" style="padding: 10px;">
            <div class="col-xs-12">
                <table id="listado-comitentes" class="table-responsive table striped table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre comitente</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($comitentes as $comitente)
                        <tr>
                            <td>{{ $comitente->numero_comitente }}</td>
                            <td>{{ $comitente->descripcion }}</td>
                            <td>
                                <input type="radio" name="nro_comitente" class="comitente-seleccion"
                                       value="{{ $comitente->numero_comitente }}">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-default" id="cambiar-comitente-modal"
                    data-cambiar-comitente-url="{{ route('comitentes.seleccionar.procesar') }}">
                Cambiar
            </button>
        </div>
    </form>
</div>