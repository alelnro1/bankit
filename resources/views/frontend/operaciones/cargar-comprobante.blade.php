@extends('layouts.frontend-app')

@section('content')
    <div class="panel-heading">Adjuntar Comprobante</div>

    <div class="panel-body">
        <div class="alert alert-info">
            Para confirmar la operación en cuestión, deberá adjuntar un comprobante del depósito que realizó. Podrá
            hacerlo a continuación.
        </div>

        <div class="row">
            <div class="col-xs-6">
                <fieldset>
                    <legend>Datos sobre la operación</legend>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td><strong>Instrumento</strong></td>
                            <td>{{ $operacion->getNombreInstrumento() }}</td>
                        </tr>

                        <tr>
                            <td><strong>Tipo Operación</strong></td>
                            <td>{{ $operacion->getNombreTipoOperacion() }}</td>
                        </tr>

                        <tr>
                            <td><strong>Moneda</strong></td>
                            <td>{{ $operacion->getNombreMoneda() }}</td>
                        </tr>

                        <tr>
                            <td><strong>Fecha Operacion</strong></td>
                            <td>{{ date("d-m-Y H:i", strtotime($operacion->created_at)) }}</td>
                        </tr>

                        <tr>
                            <td><strong>Cuotapartes estimadas</strong></td>
                            <td>{{ number_format($operacion->cantidad, 6, ',', '.') }}</td>
                        </tr>

                        <tr>
                            <td><strong>Importe estimado</strong></td>
                            <td>{{ number_format($operacion->importe, 2, ',', '.') }}</td>
                        </tr>

                    </table>
                </fieldset>
            </div>

            <div class="col-xs-6">
                <fieldset>
                    <legend>Adjuntar Comprobante</legend>

                    <div class="help-block">Seleccion el comprobante sobre la operación.</div>

                    <form action="{{ route('operaciones.adjuntar-comprobante', ['operacion' => $operacion->id]) }}"
                          enctype="multipart/form-data"
                          method="POST"
                          id="adjuntar-comprobante-operacion">
                        @csrf

                        <div>
                            <label for="comprobante">Comprobante</label>
                            <input type="file" name="comprobante" id="comprobante" class="form-control" required>
                        </div>

                        <br>

                        <div>
                            <input type="submit" class="form-control btn-primary" value="Adjuntar">
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
@endsection
