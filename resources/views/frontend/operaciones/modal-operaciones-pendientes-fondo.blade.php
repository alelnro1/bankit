<div style="padding: 5px;">
    <fieldset>
        <legend>Operaciones Pendientes de {{ $fondo->nombre_web }}</legend>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Instrumento</th>
                        <th>Tipo de Operación</th>
                        <th>Moneda</th>
                        <th>Importe</th>
                        <th>Fecha</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($operaciones as $operacion)
                        <tr>
                            <td>{{ $operacion->getNombreInstrumento() }}</td>
                            <td>{{ $operacion->getNombreTipoOperacion() }}</td>
                            <td style="text-align: center">
                                {{ $operacion->getNombreMoneda() }}
                            </td>
                            <td style="text-align: right">
                                {{ number_format($operacion->importe, 2, ',', '.') }}
                            </td>
                            <td>{{ date("d/m/Y", strtotime($operacion->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>