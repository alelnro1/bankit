<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Instrumento</th>
        <th>Tipo de Operación</th>
        <th>Moneda</th>
        <th>Importe</th>
        <th>Fecha</th>
        @if ($necesita_comprobante)
            <th>Acciones</th>
        @endif
    </tr>
    </thead>

    <tbody>
    @foreach ($operaciones as $operacion)
        <tr>
            <td>{{ $operacion->getNombreInstrumento() }}</td>
            <td>{{ $operacion->getNombreTipoOperacion() }}</td>
            <td style="text-align: right">
                {{ $operacion->getNombreMoneda() }}
            </td>
            <td style="text-align: right">
                {{ number_format($operacion->importe, 2, ',', '.') }}
            </td>
            <td>{{ date("d-m-Y H:i", strtotime($operacion->created_at)) }}</td>

            @if ($necesita_comprobante)
                <td>
                    <a href="{{ route('operaciones.form-adjuntar-comprobante', ['operacion' => $operacion]) }}">
                        <button class="btn btn-xs btn-info"> 
                            <!--<i class="fas fa-paperclip"></i>--> 
                            <span>Adjuntar Comprobante</span>
                        </button>
                    </a>
                    <a href="{{ route('operaciones.cancelar-operacion', ['operacion' => $operacion]) }}"
                       class="cancelar-operacion">
                        <button class="btn btn-xs btn-danger"> 
                            <span>Cancelar Operacion</span>
                        </button>
                    </a>
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>