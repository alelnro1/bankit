<div class="row">
    <div class="col-xs-1"></div>
    <div class="col-xs-10 row-center bg-info" style="padding: 10px">
        <input type="checkbox" id="reglamento-gestion">
        Acepto el
        <u><a href="{{ $fondo->getReglamentoDeGestion() }}" target="_blank">reglamento de gestión</a></u>
        del fondo <i>{{ $fondo->getDescripcion() }}</i>
    </div>
</div>