<table class="table table-bordered table-responsive">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Moneda</th>
        <th>Cant. Cuotapartes</th>
        <th>Valor Moneda Cotización</th>
        <th>Valor Corriente</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    @foreach ($fondos as $fondo)
        <tr>
            <td>
                {{ strtoupper($fondo->Descripcion) }}
            </td>
            <td>
                @if ($fondo->TipoCambio == "1") $ @else U$D @endif
            </td>

            <td style="text-align:right;">
                {{ number_format($fondo->Cantidad + $fondo->CantidadGarantia, 4, ',', '.') }}
            </td>

            <td style="text-align:right">
                {{ number_format($fondo->ValorMonedaCotiz, 2, ',', '.') }}
            </td>

            <td style="text-align:right">
                {{ number_format($fondo->ValorCorriente, 2, ',', '.') }}
            </td>
            
            <td>
                <a
                        href="{{ route('fondos.rescatar.form', ['fondo' => $fondo->CodFondo]) }}"
                        title="Rescatar">
                    <i class="fa fa-share" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
