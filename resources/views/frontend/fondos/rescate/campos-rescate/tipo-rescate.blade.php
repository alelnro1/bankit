<div class="col-xs-6">
    <label for="unidad">Tipo de Rescate</label>
    <select name="tipo_calculo" id="tipo-calculo" class="form-control">
        <option value="cuotaparte">Rescate por cuotapartes</option>
        <option value="monto">Rescate por monto</option>
        <option value="total">Rescate total</option>
    </select>
</div>