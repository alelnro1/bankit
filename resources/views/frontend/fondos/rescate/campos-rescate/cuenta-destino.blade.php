<tr class="{{ $errors->has('destino') ? ' has-error' : '' }}">
    <td>
        <strong>{{ __('fondos.destino_de_fondos') }}</strong>
    </td>
    <td>
        <select name="destino" id="destino" class="form-control">
            <option value="">{{ __('fondos.mi_cuenta_comitente') }}</option>

            {{--@foreach ($cuentas_bancarias as $cuenta_bancaria)
                <option value="{{ $cuenta_bancaria->CodCmtCtaEntLiquidacion }}">
                    {{ $cuenta_bancaria->Banco }} ({{ $cuenta_bancaria->NroCuenta }})
                </option>
            @endforeach--}}
        </select>

        @if ($errors->has('destino'))
            <span class="help-block">
                <strong>{{ $errors->first('destino') }}</strong>
            </span>
        @endif
    </td>
</tr>