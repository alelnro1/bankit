@if(Session::has('errors'))
    <div class="alert alert-danger">
        Verifique los errores del formulario
    </div>
@endif
<div class="row">

    <div class="col-xs-7">
        <form action="{{ route('fondos.suscribir.previsualizar') }}" method="POST" id="form-suscribir">
            {!! csrf_field() !!}
            <table class="table-responsive table table-bordered">
                <tr class="subtitle">
                    <td colspan="2">Datos para Suscripción</td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ __('fondos.nombre_fondo_titulo') }}</strong>
                    </td>
                    <td>
                        <select name="fondo" id="fondo"
                                class="form-control"
                                data-url="{{ route('fondos.suscribir.datos.fondo') }}" required>
                            @foreach ($fondos as $fondo)
                                <option value="{{ $fondo->CodFondo }}">
                                    {{ $fondo->esco_Descripcion }}
                                </option>
                            @endforeach
                        </select>
                    </td>
                </tr>

                @include('fondos.suscripcion.campos-suscripcion.tipo-suscripcion')

                @include('fondos.importe')

                <tr>
                    <td colspan="2">
                        <div class="text-center">
                            <button class="btn btn-info btn-default">{{ __('comunes.siguiente') }}</button>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="col-xs-5" id="informacion-fondo" style="display: none;">
        <table class="table table-responsive table-bordered">
            <tr class="subtitle">
                <td colspan="2">{{ __('comunes.informacion') }}</td>
            </tr>
            <tr>
                <td>
                    <strong>{{ __('comunes.moneda') }}</strong>
                </td>
                <td id="moneda" style="text-align:right;"></td>
            </tr>

            <tr>
                <td>
                    <strong>{{ __('fondos.limite_minimo') }}</strong>
                </td>
                <td id="limite-minimo" style="text-align:right;"></td>
            </tr>

            {{--<tr>
                <td>
                    <strong>{{ __('fondos.limite_maximo') }}</strong>
                </td>
                <td id="limite-maximo"></td>
            </tr>--}}

            <tr>
                <td>
                    <strong>{{ __('fondos.valor_cuotaparte') }}</strong>
                </td>
                <td id="valor-cuotaparte" style="text-align: right;"></td>
            </tr>

            <tr style="font-weight: bold; background: #EEEEEE;">
                <td>
                    <strong>{{ __('fondos.saldo_disponible') }}</strong>
                </td>
                <td id="saldo-disponible" style="text-align:right;"></td>
            </tr>
        </table>
    </div>

    <div style="clear: both;"></div>
</div>