@extends('layouts.app')

@section('styles')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
@endsection

@section('content')
    <div class="panel-body">
        <fieldset>
            <legend>{{ __('fondos.titulo') }}</legend>

            @if(Session::has('errors'))
                <div class="alert alert-danger">
                    Verifique los errores del formulario
                </div>
            @endif

            <div class="row">

                <div class="col-xs-2"></div>
                <div class="row col-xs-9 row-center">
                    <form action="{{ route('fondos.suscribir.procesar', ['fondo' => $cod_fon_encriptado]) }}" method="POST">
                        {!! csrf_field() !!}
                        <table class="table-responsive table table-bordered">
                            <tr>
                                <td>
                                    <strong>{{ __('comunes.fecha') }}</strong>
                                </td>
                                <td>{{ date("d/m/Y", time()) }}</td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>{{ __('fondos.nombre_fondo_titulo') }}</strong>
                                </td>
                                <td>{{ $fondo->getDescripcion() }}</td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>{{ __('comunes.moneda') }}</strong>
                                </td>
                                <td>{{ $fondo->getMonedaNombre() }}</td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>{{ __('fondos.tipo_de_suscripcion') }}</strong>
                                </td>
                                <td>
                                    <strong>{{ $tipo_suscripcion_texto }}</strong>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>{{ __('comunes.importe') }}</strong>
                                </td>
                                <td>
                                    <strong>{{ number_format($monto_estimado, 2, ',', '.') }}</strong>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <strong>Comisión</strong>
                                </td>
                                <td>
                                    <strong>{{ number_format($comision, 2, ',', '.') }}</strong>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>{{ __('fondos.cant_cuotapartes') }}</strong>
                                </td>
                                <td>
                                    <strong>{{ number_format($cuotapartes_estimadas, 4, ',', '.') }}</strong>
                                </td>
                            </tr>

                            <input type="hidden" name="fondo" value="{{ $cod_fon_encriptado }}">

                            <tr>
                                <td colspan="2">
                                    <div class="text-center">
                                        <a href="{{ route('operar.fondos.index') }}" class="btn btn-danger">
                                            <i class="fa fa-home"></i> {{ __('comunes.cancelar') }}
                                        </a>
                                        &nbsp;&nbsp;
                                        <button class="btn btn-success" id="procesar-suscripcion">
                                            {{ __('comunes.confirmar_solicitud') }}
                                            <i class="fa fa-save"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <input type="hidden" name="tipo_calculo" value="{{ $tipo_suscripcion }}">
                            <input type="hidden" name="importe" value="{{ $importe }}">
                        </table>
                    </form>
                </div>
            </div>

            @include('fondos.reglamento-de-gestion')
            <br>

            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-10 row-center bg-info" style="padding: 10px">
                    {!! __('fondos.aclaracion_operacion_fondo') !!} {{ date('d/m/Y H:i', time()) }}
                </div>
            </div>
        </fieldset>
    </div>
@endsection

@section('javascripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="{{ url('js/datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/libs/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/number_format.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/fondos/operaciones.js') }}"></script>
@endsection
