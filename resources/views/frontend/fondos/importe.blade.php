<tr class="{{ $errors->has('importe') ? ' has-error' : '' }}">
    <td style="font-weight: bold;">
        <span id="texto-monto-cantidad">{{ __('fondos.monto_cantidad') }}</span>
    </td>

    <td>
        <input type="text" min="0" step="any" required="required" value="" autocomplete="off"
               class="form-control importe" maxlength="22">
        <input type="hidden" name="importe" id="importe" value="">

        @if ($errors->has('importe'))
            <span class="help-block">
                <strong>{{ $errors->first('importe') }}</strong>
            </span>
        @endif
    </td>
</tr>