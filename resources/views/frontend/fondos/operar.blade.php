@extends('layouts.app')

@section('styles')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
@endsection

@section('content')
    <div class="panel-body">
        <fieldset>
            <legend>{{ __('fondos.titulo') }}</legend>

            @if (Session::has('errors'))
                <div class="alert alert-danger">
                    <strong>Verifique los errores y reintente nuevamente: </strong>
                    @if (isset($errors))
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endif
                </div>
            @elseif (Session::has('solicitud_operacion'))
                <div class="alert alert-success">
                    Solicitud registrada satisfactoriamente.
                </div>
            @elseif (Session::has('operacion_eliminada'))
                <div class="alert alert-success">
                    La operación ha sido cancelada.
                </div>
            @endif

            <div class="row col-xs-12">
                <div class="help-block">
                    {{ __('fondos.seleccion_tipo_operacion') }}

                    <select class="form-control"
                            id="tipo_operacion"
                            data-url="{{ route('fondos.operar.seleccionar.tipo.operacion') }}"
                            data-fondo-default="{{ $cod_fondo_default }}">
                        <option value="susc">{{ __('fondos.tipo_operacion_suscripcion') }}</option>

                        @if ($puede_rescatar)
                            <option value="resc" @if ($operacion_default == "rescate") selected @endif>{{ __('fondos.tipo_operacion_rescate') }}</option>
                        @endif
                    </select>

                    <div id="operacion" style="padding-top: 20px;" data-msg-cargando="{{ __('comunes.cargando') }}"></div>
                </div>
            </div>
        </fieldset>

        {{--@if ($hay_operaciones_sin_confirmar)
            @include('fondos.sin-confirmar')
        @endif--}}
    </div>

    <span style="display: none;" id="monto-mayor-mil">{{ __('fondos.error_monto_mayor_mil') }}</span>
    <span style="display: none;" id="dinero-insuficiente">{{ __('fondos.error_dinero_insuficiente') }}</span>
    <span style="display: none;" id="cuotapartes-insuficientes">{{ __('fondos.error_cuotapartes_insuficientes') }}</span>
@endsection

@section('javascripts')

    <script type="text/javascript" src="{{ url('js/libs/blockUI.js') }}"></script>
    <script type="text/javascript">
        var mensaje = $('#operacion').data('msg-cargando');

        $('div#wrap').block({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: mensaje
        });
    </script>

    <script src="{{ url('js/datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/libs/jquery.mask.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/number_format.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/fondos/operaciones.js') }}"></script>
@endsection
