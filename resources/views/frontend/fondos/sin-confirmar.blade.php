<fieldset>
    <legend>Operaciones sin confirmar</legend>
    <table class="table table-responsive table-bordered table-stripped text-center">
        <thead>
        <tr style="background: rgb(242, 172, 37); color: rgb(255, 255, 255);">
            <th class="text-center">Fondo</th>
            <th class="text-center">Operación</th>
            <th class="text-center">Monto</th>
            <th class="text-center">Fecha</th>
            <th class="text-center">Acciones</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($operaciones_sin_confirmar as $operacion)
            <tr>
                <td>{{ $operacion->Fondo->getAbreviatura() }}</td>
                <td>{{ $operacion->Tipo->getDescripcion() }}</td>
                <td style="text-align: right;">
                    ${{ number_format($operacion->getImporte(), 2, ',', '.') }}
                </td>
                <td>{{ $operacion->getFecha() }}</td>
                <td>
                    <a href="{{ route('fondos.cancelar.operacion', ['operacion' => $operacion->getID()]) }}"
                            class="btn btn-danger cancelar-operacion">
                        Cancelar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</fieldset>
