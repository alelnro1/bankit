@extends('layouts.seleccionar-comitente')

@section('content')
    <div class="panel-heading">
        Seleccione el comitente con quien desea operar
    </div>
    <div class="panel-body">
        <form action="{{ route('comitentes.seleccionar.procesar') }}" method="POST">
            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                @foreach ($comitentes as $comitente)
                    <input type="radio" id="{{ $comitente->numero_comitente }}" name="nro_comitente" value="{{ $comitente->numero_comitente }}">

                    <label for="{{ $comitente->numero_comitente }}">{{ $comitente->numero_comitente }} ({{ $comitente->descripcion }})</label>

                    <br>
                @endforeach
            </div>

            <hr>
            <div class="form-group">
                <div class="col-xs-4 pull-left">
                    <a href="{{ route('frontend.logout') }}" class="btn btn-danger"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Cancelar
                    </a>
                </div>

                <div class="col-xs-4 pull-right">
                    <button type="submit" class="btn btn-primary">
                        Acceder
                    </button>
                </div>
            </div>
            <div style="clear:both;"></div>
        </form>
    </div>
@endsection
