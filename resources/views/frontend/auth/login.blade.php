@extends('layouts.frontend-app')

@section('content')
    <div class="panel-heading">Login</div>

    <div class="panel-body">
        {{-- MENSAJE POR EL CAMBIO DE CONTRASEÑA SATISFACTORIO --}}
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if (Session::has('proceso_finalizado'))
            <div class="alert alert-success">
                Proceso de Apertura de Cuenta finalizado. En breve nos estaremos contactando con usted con novedades.
            </div>
        @endif

        @if (Session::has('recuperar-password'))
            <div class="alert alert-success">
                Se envió un mail a su casilla de correo con una nueva contraseña temporal para ingresar al sistema.
            </div>
        @endif


        <form class="form-horizontal" method="POST" action="{{ route('frontend.login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ session('email_error') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                           autofocus>

                    @if (session('email_error'))
                        <span class="help-block">
                            <strong>{{ session('email_error') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Contraseña</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
        <!--
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Mantener Sesion iniciada
                        </label>
                    </div>
                </div>
            </div>
        -->
            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Ingresa
                    </button>

                    <a class="btn btn-info" href="{{ route('frontend.password.forgot-form') }}">
                        ¿Olvidaste tu contraseña?
                    </a>

                    <a class="btn btn-warning" href="{{ route('apertura-cuenta.register') }}">Registrarse</a>
                    <!--
                    <a href="{{ route('frontend.autogestion') }}" class="btn btn-primary">Asociar Comitente</a>
                    -->
                </div>
            </div>
        </form>
    </div>
@endsection
