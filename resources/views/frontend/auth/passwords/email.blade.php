@extends('layouts.frontend-app')

@section('content')
    <div class="panel-heading">Recuperar Contraseña</div>

    <div class="panel-body">
        @if (session('success'))
            <div class="alert alert-success">
                Se ha enviado un email con su nueva contraseña.
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="{{ route('frontend.password.reset') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Recuperar Contraseña
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
