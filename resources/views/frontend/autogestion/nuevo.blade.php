@extends('layouts.frontend-app')

@section('content')
    <div class="panel-body">
        <div class="panel-heading" style="width:100%; height: 50px; ">
            Asociar una nuevo comitente
        </div>

        <div class="alert alert-info">   
            Ingrese los datos solicitados a continuacion para generar el usuario para operar en la plataforma.
        </div>

        @if (Session::has('errors'))
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        <i class="fas fa-exclamation-triangle"></i> <strong>Hay errores!</strong>
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if (Session::has('datos_invalidos'))
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        <i class="fas fa-exclamation-triangle"></i> <strong>Hay errores!</strong><br>
                        Los datos ingresados son inválidos.
                    </div>
                </div>
            </div>
        @endif

        @if (Session::has('comitente_asociado'))
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-success">
                        <i class="fas fa-check-circle"></i> El comitente ha sido asociado correctamente.
                        Ingrese haciendo <a href="{{ route('frontend.login') }}">click aquí y utilice la clave temporal que se le ha enviado a su correo electronico</a>
                    </div>
                </div>
            </div>
        @endif


        <fieldset>

            <div class="row">
                <div class="col-xs-12">
                <form action="{{ route('frontend.autogestion.agregar-comitente') }}" method="POST">
                    @csrf
                    
                    <div class="col-xs-6">
                        <label for="numero_cuenta">Num. Documento</label>

                        <div class="input-group">
                            <input type="number" name="dni" class="form-control" value="{{ old('dni') }}" 
                            required>
                        </div>
                    </div>


                    <div class="col-xs-9">
                        <label for="numero_cuenta">Num. Comitente</label>

                        <div class="input-group">
                            <input type="number" name="comitente" class="form-control" value="{{ old('comitente') }}" required>
                        </div>
                    </div>
                    
                    <div class="col-xs-9">
                        <label for="numero_cuenta">Email</label>

                        <div class="input-group">
                            <input type="text" name="email" class="form-control" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <div class="col-xs-10"></div>
                    <div class="col-xs-2">
                        <input type="submit" class="form-control btn-primary" value="Asociar">
                    </div>
                </form>
                </div>
            </div>

        </fieldset>
    </div>
@endsection