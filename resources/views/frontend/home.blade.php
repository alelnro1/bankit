@extends('layouts.frontend-app')

@section('content')
    <div class="panel-heading">Estado de Cuenta</div>

    <div class="panel-body">
        @if (Session::has('comprobante_adjunto'))
            <div class="alert alert-success">
                <span>Comprobante adjuntado correctamente.</span>
            </div>
        @endif

        <fieldset>
                <legend>Posicion valorizada de FCIs


                </legend>
                <div class="row totalPosicionWrapper">
                    <div class="col-xs-12 totalPosicion">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3 detallePosicion">
                            <span class="col-xs-12" style="text-align: center;">
                                Total Pesos
                            </span>
                            <span class="col-xs-12" style="text-align: center;">
                                $ {{ number_format($totales["pesos"], 2, ',', '.') }}
                            </span>                  
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-3 detallePosicion">
                            <span class="col-xs-12" style="text-align: center;">
                                Total Dolares
                            </span>
                            <span class="col-xs-12" style="text-align: center;">
                                $ {{ number_format($totales["dolares"], 2, ',', '.') }}
                            </span>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
        </fieldset>    

        <div class="row">
            <div class="col-xs-12">
                {!! $vista_posiciones !!}
            </div>
        </div>

        
        <br><br><br>
        <div class="alert alert-warning">
            <strong>
                Las suscripciones realizadas pasadas las 16 horas serán perfeccionadas al día siguiente
            </strong>
        </div>
        @if (count($operaciones_con_comprobante) > 0)

            <fieldset>
                <legend>Operaciones pendientes de liquidacion</legend>
                    <div class='alert alert-info'>
                        Las operaciones pendientes se encuentran sujetas a revision y no son consideras en el calculo de su posicion actual
                    </div>

                    @include('frontend.operaciones.operaciones-pendientes', [
                    'operaciones' => $operaciones_con_comprobante,
                    'necesita_comprobante' => false
                    ])

            </fieldset>
        @endif

        <br><br><br>
        
        @if (count($operaciones_sin_comprobante) > 0)
            <fieldset>
                <legend>Operaciones con comprobante faltante</legend>
                <div class='alert alert-info'>
                    Debe adjuntar el comprobante de deposito para proceder con la suscripcion del fondo. Hasta entonces no se realizara la suscripcion.
                </div>

                    @include('frontend.operaciones.operaciones-pendientes', [
                    'operaciones' => $operaciones_sin_comprobante,
                    'necesita_comprobante' => true
                    ])

            </fieldset>
        @endif


    </div>

    <div id="modal-operaciones-fondo-pendientes" style="display: none; height: 80%; max-width: 80%;"></div>
@endsection
