<table class="table table-bordered table-striped table-condensed">
    <thead>
    <tr>
        <th>Banco</th>
        <th>Cuenta</th>
        <th>CBU</th>
        <th>Fecha Asociacion</th>
        <th>Accion</th>
    </tr>
    </thead>

    <tbody>
    @foreach ($cuentas as $cuenta)
    
        <tr>
            <td>{{ $cuenta->Banco->getDescripcion() }}</td>
            <td class="text-right">{{ $cuenta->getNumeroCuenta() }}</td>
            <td class="text-right">{{ $cuenta->getCbu() }}</td>
            <td class="text-right">{{-- date("d/m/Y" , strtotime($cuenta->created_at())) --}}</td>
            <td class="text-center">
                    <a href="{{ route('tesoreria.cuentas-bancarias-desvincular', ['cuenta' => $cuenta->id]) }}"
                       class="desvincular-cuenta-bancaria">
                        <button class="btn btn-xs btn-danger"> 
                            <span>Desvincular</span>
                        </button>
                    </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>