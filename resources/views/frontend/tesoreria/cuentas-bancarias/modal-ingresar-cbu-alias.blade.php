<div id="modal-cbu-alias" class="col-xs-12" title="Cuenta Bancarias por CBU" style="display: none;">
    <div class="help-block">
        Ingrese el cbu ó el alias de la cuenta bancaria que desea agregar.
    </div>

    <div class="row col-xs-10">
        <form action="{{ route('tesoreria.cuentas-bancarias.precargar-datos') }}"
              id="precargar-datos-cuenta-bancaria-form" method="POST">
            @csrf
            <div class="col-xs-10">
                <label for="cbu">CBU</label>
                <input type="text" name="cbu" id="cbu" class="form-control" style="width: 230px;">
            </div>

            <div class="col-xs-10">
                <label for="cbu">Alias</label>
                <input type="text" name="alias" id="alias" class="form-control" style="width: 230px;">
            </div>

             <div class="col-xs-10 right">
                 <br>
                 <input type="submit" class="btn btn-primary" value="Agregar cuenta">
             </div>
        </form>
    </div>
</div>