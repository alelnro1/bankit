@extends('layouts.frontend-app')

@include('_snippets.preloader')

@section('content')
    <div class="panel-body">
        <fieldset>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6 panel-title">
                        Asociar una nueva cuenta bancaria
                    </div>
                </div>
            </div>
            <br>
            <form action="{{ route('tesoreria.cuentas-bancarias.procesar') }}" method="POST" id="alta-cuenta-bancaria">
                {!! csrf_field() !!}

                <div class="row">
                    
                    <div class='col-sm-4'>
                        <div class="form-group {{ $errors->has('cuenta_bancaria_tipo_id') ? ' has-error' : '' }}">
                            <label for="cuenta_bancaria_tipo_id" class="campoObligatorio">Tipo de Cuenta</label>

                            <div>
                                <select name="cuenta_bancaria_tipo_id" id="cuenta_bancaria_tipo_id" class="form-control"
                                        required>
                                    <option value="">Seleccionar...</option>
                                    @foreach ($tipos_cuentas as $tipo_cuenta)
                                        <option value="{{ $tipo_cuenta->id }}"
                                            @if ($tipo_cuenta->cod_bcra == $datos_por_cbu->TipoCTA) selected @endif>
                                            {{ $tipo_cuenta->getNombre() }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has('cuenta_bancaria_tipo_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cuenta_bancaria_tipo_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class="form-group {{ $errors->has('banco_id') ? ' has-error' : '' }}">
                            <label for="banco_id" class="campoObligatorio">Banco</label>

                            <div>
                                <select name="banco_id" id="banco_id" class="form-control"
                                        required>
                                    <option value="">Seleccione una opcion...</option>
                                    @foreach ($bancos as $banco)
                                        <option value="{{ $banco->id }}"
                                            @if ($banco->cod_banco == $datos_por_cbu->NroBanco) selected @endif>
                                            {{ $banco->getDescripcion() }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has('tipo_cuenta_bancaria_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tipo_cuenta_bancaria_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class="form-group {{ $errors->has('moneda_id') ? ' has-error' : '' }}">
                            <label for="moneda_id"  class="campoObligatorio">Moneda</label>

                            <div>
                                <select name="moneda_id" id="moneda_id" class="form-control" required>
                                    <option value="">Seleccione una opcion...</option>
                                    @foreach ($monedas as $moneda)
                                        <option value="{{ $moneda->id }}">{{ $moneda->getDescripcion() }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($errors->has('moneda_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('moneda_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class='col-sm-6'>
                        <div class="form-group {{ $errors->has('cbu') ? ' has-error' : '' }}">
                            <label for="cbu"  class="campoObligatorio">CBU</label>

                            <input type="text" id="cbu" name="cbu" class="form-control" data-old="{{ old('cbu')}}"
                                   value="{{ $datos_por_cbu->CBU }}"
                                   required/>

                            @if ($errors->has('cbu'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cbu') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class='col-sm-6'>
                        <div class="form-group {{ $errors->has('alias') ? ' has-error' : '' }}">
                            <label for="alias" class="campoObligatorio">Alias</label>

                            <input type="text" id="alias" name="alias" class="form-control"
                                   value="{{ old('numero') ?: $datos_por_cbu->AliasCBU }}" required>

                            @if ($errors->has('alias'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('alias') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class='col-sm-12'>
                        <br>
                        <div style="text-align: right;">
                            <input type="submit" value="Confirmar" class="btn btn-primary">
                            <a href="{{ url('/') }}">
                                <input type="button" value="Cancelar" class="btn btn-danger">
                            </a>

                        </div>
                    </div>
                </div>
            </form>
        </fieldset>
    </div>
@endsection