@extends('layouts.frontend-app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection

@include('_snippets.preloader')

@section('content')
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-6 panel-title">
                Cuentas Bancarias
            </div>
            <div class="col-xs-6 text-right">
                <button class="btn btn-white" id="abrir-modal-agregar-por-cbu-alias">
                    Asociar una nueva Cuenta Bancaria
                </button>
            </div>
        </div>
    </div>

    <div class="panel-body">
        @if (Session::has('success'))
            <div class="alert alert-success">
                Cuenta creada correctamente
            </div>
        @endif

        <fieldset>
            <legend>Cuentas Bancarias en Pesos</legend>

            @if (count($cuentas_peso) > 0)
                @include('frontend.tesoreria.cuentas-bancarias.listado', ['cuentas' => $cuentas_peso])
            @else
                <div class="help-block">No hay cuentas bancarias en pesos</div>
            @endif
        </fieldset>

        <fieldset>
            <legend>Cuentas Bancarias en Dólares</legend>

            @if (count($cuentas_dolar) > 0)
                @include('frontend.tesoreria.cuentas-bancarias.listado', ['cuentas' => $cuentas_dolar])
            @else
                <div class="help-block">No hay cuentas bancarias en dólares</div>
            @endif
        </fieldset>
    </div>

    @include('frontend.tesoreria.cuentas-bancarias.modal-ingresar-cbu-alias')
@endsection

@section('javascripts')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection