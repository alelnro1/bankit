<fieldset>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Cant Cuotapartes</th>
            <th>Saldo Inicial</th>
            <th>Valor Cuotaparte</th>
            <th>Variacion Diaria</th>
            <th>Variacion Anual</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($posicion_instrumentos as $posicion)
            @if(isset($posicion->disponible) && $posicion->disponible!=0)            
                <tr>
                    <td>{{ $posicion->nombre_web }}</td>
                    <td style="text-align: right;">
                        {{ number_format($posicion->disponible, 4, ',', '.') }}
                    </td>
                    <td style="text-align: right;">
                        
                            $ {{ number_format($posicion->disponible 
                                                * $posicion->Cotizaciones[0]->valor_compra, 2, ',', '.') }}
                        
                    </td>
                    <td style="text-align: right;">
                        $ {{ number_format($posicion->Cotizaciones[0]->valor_compra, 6, ',', '.') }} 
                    </td>

                    <td style="text-align: right;">
                        @if ($posicion->Cotizaciones[0]->var_diaria >= 0)
                            <span style="color:green;">
                                <i class="fas fa-arrow-up"></i> {{ number_format($posicion->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                            </span>
                        @else
                            <span style="color:red;">
                                <i class="fas fa-arrow-down"></i> {{ number_format($posicion->Cotizaciones[0]->var_diaria, 2, ',', '.') }} %
                            </span>
                        @endif
                    </td>

                    <td style="text-align: right;">
                        @if ($posicion->Cotizaciones[0]->var_anual >= 0)
                            <span style="color:green;">
                                <i class="fas fa-arrow-up"></i> {{ number_format($posicion->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                            </span>
                        @else
                            <span style="color:red;">
                                <i class="fas fa-arrow-down"></i> {{ number_format($posicion->Cotizaciones[0]->var_anual, 2, ',', '.') }} %
                            </span>
                        @endif
                    </td>                    
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>


{{-- 

<div class="col-md-7">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Cant Cuotapartes</th>
            <th>Saldo Inicial</th>

        </tr>
        </thead>

        <tbody>
        @foreach ($posicion_instrumentos as $posicion)
            @if(isset($posicion->pendiente_liquidar) && $posicion->pendiente_liquidar!=0)            
                <tr>
                    <td>{{ $posicion->nombre_web }}</td>
                    <td style="text-align: right;">
                        {{ number_format($posicion->pendiente_liquidar, 6, ',', '.') }}
                    </td>
                    <td style="text-align: right;">
                        
                            $ {{ number_format($posicion->pendiente_liquidar 
                                                * $posicion->Cotizaciones[0]->valor_compra, 2, ',', '.') }}
                        
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>

<div class="col-md-7">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Cant Cuotapartes</th>
            <th>Saldo Inicial</th>

        </tr>
        </thead>

        <tbody>
        @foreach ($posicion_instrumentos as $posicion)
            @if(isset($posicion->pendiente_comprobante) && $posicion->pendiente_comprobante!=0)            
                <tr>
                    <td>{{ $posicion->nombre_web }}  </td>
                    <td style="text-align: right;">
                        {{ number_format($posicion->pendiente_comprobante, 6, ',', '.') }}
                    </td>
                    <td style="text-align: right;">
                        
                            $ {{ number_format($posicion->pendiente_comprobante 
                                                * $posicion->Cotizaciones[0]->valor_compra, 2, ',', '.') }}
                        
                    </td>        
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>
--}}
</fieldset>