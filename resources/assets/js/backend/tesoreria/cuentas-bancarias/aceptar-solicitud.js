$('body')
    .on('click', '.aceptar', function (e) {
        e.preventDefault();

        var cuenta_id = $(this).data('cuenta');

        $.ajax({
            url: $(this).data('href'),
            type: 'POST',
            dataType: 'json',
            data: {
                'cuenta': cuenta_id,
                'aba': $('#aba').val(),
                'swift': $('#swift').val(),
                '_token': $('input[name=_token]').val()
            },
            success: function (data) {
                if (data.aceptado !== true) {
                    $('#mensaje-error').empty();

                    $.each(data.errores, function (key, error) {
                        $('#mensaje-error').append(
                            '<div>' + error + '</div>'
                        );
                    });

                    $('#modal-error').modal('show');
                } else {
                    $('#titulo-mensaje-aceptado').empty().html("Aceptada!");
                    $('#mensaje-aceptado').empty().html("La cuenta bancaria fue aceptada");

                    $('#modal-aceptado').modal('show');
                    setTimeout(function () {
                        location.reload();
                    }, 2500);
                }
            }
        });
    });