$('body')
    .on('click', '.cargar-solicitud', function (e) {
        e.preventDefault();

        // ID del deposito a cargar
        var id = $(this).data('cuenta');

        if ($('#modal-cuenta-' + id).length <= 0) {
            $.ajax({
                url: $(this).attr('href'),
                success: function (data) {
                    $('#cuentas').append(data);
                    $('#modal-cuenta-' + id).modal('show');
                }
            });
        } else {
            $('#modal-cuenta-' + id).modal('show');
        }

        return true;
    });