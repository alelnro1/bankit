$('body')
    .on('click', '.rechazar', function (e) {
        e.preventDefault();

        var cuenta_id = $(this).data('cuenta');

        $.ajax({
            url: $(this).data('href'),
            type: 'POST',
            data: {
                'cuenta': cuenta_id,
                '_token': $('input[name=_token]').val()
            },
            success: function (data) {
                $('#titulo-mensaje-aceptado').empty().html("Rechazada!");
                $('#mensaje-aceptado').empty().html("La cuenta bancaria fue rechazada");

                $('#modal-aceptado').modal('show');
                setTimeout(function () {
                    location.reload();
                }, 2500);
            }
        });
    });