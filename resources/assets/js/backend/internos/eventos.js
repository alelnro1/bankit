$('body')
    .on('click', '.eliminar-interno', function (e) {
        e.preventDefault();

        var elem = $(this),
            url = elem.attr('href'),
            interno_id = elem.data('interno-id');

        $.confirm({
            title: 'Confirmar!',
            content: '¿Está seguro que desea eliminar el interno?',
            buttons: {
                confirmar: {
                    action: function () {
                        EliminarInterno(url, interno_id);
                    },
                    btnClass: 'btn-green'
                },
                cancel: {
                    action: function () {
                        e.preventDefault();
                    },
                    btnClass: 'btn-red'
                }
            }
        });
    });

$('#permisos').multiselect({
    nonSelectedText: 'Seleccione...'
});