var EliminarInterno = function (url, interno_id) {
    $.ajax({
        url: url,
        type: 'POST',
        data: {
            'interno_id': interno_id
        },
        success: function (data) {
            $.confirm({
                title: 'Usuario eliminado!',
                content: 'El usuario ha sido eliminado',
                buttons: {
                    Ok: function () {
                        location.reload()
                    }
                }
            });
        }
    });
};