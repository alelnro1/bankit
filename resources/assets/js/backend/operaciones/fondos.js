$('body')
    .on('click', '.ver-operacion-fondo', function (e) {
        e.preventDefault();

        var operacion_id = $(this).data('operacion-id');

        $.ajax({
            url: $('#operaciones-pendientes-fondos').data('cargar-operacion-url'),
            data: {
                'operacion_id': operacion_id
            },
            type: 'POST',
            beforeSend: function () {
                $("#loading").fadeIn(500);
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            dataType: 'json',
            success: function (data) {
                $('#operaciones').empty().html(data.modal);

                var modal_operacion_pendiente = $('#modal-operacion-pendiente'),
                    procesar_operacion_intl_url = modal_operacion_pendiente.data('procesar-operacion-url'),
                    desaprobar_operacion_url = modal_operacion_pendiente.data('desaprobar-operacion-url'),
                    tipo_operacion = modal_operacion_pendiente.data('tipo-operacion');
                    estado_operacion = modal_operacion_pendiente.data('estado-operacion');

                if(estado_operacion==1) {
                    $('#modal-operacion-pendiente').dialog({
                        modal: true,
                        width: 620,
                        buttons: [
                            {
                                text: 'Desaprobar',
                                click: function () {
                                    Acciones.Desaprobar(desaprobar_operacion_url, operacion_id);
                                }
                            },
                            {
                                text: 'Procesar',
                                click: function () {
                                    Acciones.Procesar(procesar_operacion_intl_url, operacion_id, tipo_operacion);
                                }
                            }
                        ],
                        open: function(event) {
                            $('.ui-dialog-buttonset').find('button:contains("Desaprobar")').removeClass('ui-button').addClass('btn btn-danger');
                            $('.ui-dialog-buttonset').find('button:contains("Procesar")').removeClass('ui-button').addClass('btn btn-success');
                        }
                    });
                } else {
                    $('#modal-operacion-pendiente').dialog({
                        modal: true,
                        width: 620
                    });                    
                }
            }
        })
    });

$('body')
    .on('click', '.ver-operacion-fondo-sin-botones', function (e) {
        e.preventDefault();

        var operacion_id = $(this).data('operacion-id');

        $.ajax({
            url: $('#operaciones-pendientes-fondos').data('cargar-operacion-url'),
            data: {
                'operacion_id': operacion_id
            },
            type: 'POST',
            beforeSend: function () {
                $("#loading").fadeIn(500);
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            dataType: 'json',
            success: function (data) {
                $('#operaciones').empty().html(data.modal);

                var modal_operacion_pendiente = $('#modal-operacion-pendiente'),
                    procesar_operacion_intl_url = modal_operacion_pendiente.data('procesar-operacion-url'),
                    desaprobar_operacion_url = modal_operacion_pendiente.data('desaprobar-operacion-url'),
                    tipo_operacion = modal_operacion_pendiente.data('tipo-operacion');
                    estado_operacion = modal_operacion_pendiente.data('estado-operacion');

                    console.log(estado_operacion);


                    $('#modal-operacion-pendiente').dialog({
                        modal: true,
                        width: 620
                    });                    
            }
        })
    });