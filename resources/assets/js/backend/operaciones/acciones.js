Acciones = {
    Desaprobar: function (desaprobar_operacion_intl_url, operacion_id, tipo_operacion) {
        $.confirm({
            title: 'Confirmar acción',
            content: '¿Está seguro que desea desaprobar la operacion?',
            buttons: {
                cancelar: {
                    btnClass: 'btn-red'
                },
                confirmar: {
                    btnClass: 'btn-green',
                    action: function(){
                        $.ajax({
                            url: desaprobar_operacion_intl_url,
                            beforeSend: function () {
                                $("#loading").fadeIn(500);
                            },
                            complete: function () {
                                $("#loading").fadeOut(500);
                            },
                            data: {
                                operacion_id: operacion_id
                            },
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
                                $.alert({
                                    title: 'Operación Desaprobada',
                                    icon: 'fa fa-success',
                                    type: 'green',
                                    content: 'La operación se desaprobó correctamente',
                                    buttons: {
                                        Ok: function () {
                                            location.reload();
                                        }
                                    }
                                });
                            }
                        })
                    }
                }
            }
        });
    },

    Procesar: function (procesar_operacion_intl_url, operacion_id, tipo_operacion) {
        $.confirm({
            title: 'Confirmar acción',
            content: '¿Está seguro que desea procesar la operacion?',
            buttons: {
                cancelar: {
                    btnClass: 'btn-red'
                },
                confirmar: {
                    btnClass: 'btn-green',
                    action: function(){
                        $.ajax({
                            url: procesar_operacion_intl_url,
                            beforeSend: function () {
                                $("#loading").fadeIn(500);
                            },
                            complete: function () {
                                $("#loading").fadeOut(500);
                            },
                            data: {
                                'operacion_id': operacion_id,
                                'tipo_operacion': tipo_operacion
                            },
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
                                if (data.procesado == true) {
                                    $.alert({
                                        title: 'Operación Procesada',
                                        icon: 'fa fa-success',
                                        type: 'green',
                                        content: 'La operación se procesó correctamente',
                                        buttons: {
                                            Ok: function () {
                                                location.reload();
                                            }
                                        }
                                    });
                                } else {
                                    $.alert({
                                        title: 'ERROR! Operación NO Procesada',
                                        icon: 'fa fa-error',
                                        type: 'red',
                                        content: 'La operación no se pudo procesar. Reintente más tarde'
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    }
};