$('#fecha-operaciones-aprobadas').datetimepicker({
    format: 'DD/MM/YYYY'
}).on('dp.change', function () {
    $('form#cambiar-fecha').submit();
});

$('#fecha_desde-operaciones-realizadas').datetimepicker({
    format: 'DD/MM/YYYY'
}).on('dp.change', function () {
    $('form#cambiar-fecha').submit();
});

$('#fecha_hasta-operaciones-realizadas').datetimepicker({
    format: 'DD/MM/YYYY'
}).on('dp.change', function () {
    $('form#cambiar-fecha').submit();
});
