ProcesarEditComposicionFCI = function (procesar_edit_url) {
    var composiciones = $('.composicion-nuevo-valor'),
        array_nuevas_composiciones = [],
        total_valores = 0;

    $('.composicion-nuevo-valor').each(function (key) {
        var composicion = $(this),
            nuevo_valor = parseFloat(composicion.val()),
            composicion_id = composicion.data('composicion-id');

        array_nuevas_composiciones.push({
            'valor': nuevo_valor,
            'composicion_id' : composicion_id
        });

        total_valores += nuevo_valor;
    });

    if (total_valores.toFixed(2) != 100) {
        $.notify('ERROR! Los valores no suman 100%.', 'warn');
    } else {
        $.ajax({
            url: procesar_edit_url,
            data: {
                composiciones: array_nuevas_composiciones,
                fondo_id: $('#editar-composicion-fci').data('instrumento-id')
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.procesado) {
                    $.notify('Porcentajes actualizados! Espere...', 'success');

                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        });
    }
};