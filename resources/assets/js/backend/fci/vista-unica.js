$("#tabs-abm-fci").tabs();

$('body')
    .on('click', '#editar-composicion-fci', function () {
        var elem = $(this),
            url = elem.data('editar-composicion-url'),
            instrumento_id = elem.data('instrumento-id');

        $.ajax({
            url: url,
            data: {
                instrumento_id: instrumento_id
            },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#composicion-a-editar').empty().append(data);

                $('#composicion-edit-item').iziModal().iziModal('open');
            }
        });
    })
    .on('click', '#confirmar-actualizacion', function () {
        var elem = $(this),
            procesar_edit_url = elem.data('procesar-edit');

        $.confirm({
            title: 'Confirmar',
            content: '¿Está seguro que desea confirmar el cambio de composición de FCI?',
            buttons: {
                confirmar: {
                    text: 'Confirmar',
                    btnClass: 'btn-green',
                    action: function () {
                        ProcesarEditComposicionFCI(procesar_edit_url);
                    }
                },
                cancel:
                    {
                        text: 'Cancelar',
                        btnClass: 'btn-red',
                        action: function () {
                            $.alert('Something else?');
                        }
                    }
            }
        });
    })
    .on("click", '#opener', function () {
        $("#dialog").dialog("open");
    });