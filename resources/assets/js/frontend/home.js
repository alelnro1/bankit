if ($("#modal-operaciones-fondo-pendientes").length > 0) {
    $("#modal-operaciones-fondo-pendientes").iziModal({
        closeOnEscape: false,
        transitionIn: 'bounceInDown',
        transitionOut: 'bounceOutDown'
    });
}

$('body')
    .on('click', '.cancelar-operacion',  function (e) {
        e.preventDefault();

        var elem = $(this),
            url = elem.attr('href');

        $.confirm({
            title: 'Confirmar',
            content: '¿Está seguro que desea cancelar la operación?',
            buttons: {
                cancelar: {
                    btnClass: 'btn-red',
                    action: function(){

                    }
                },
                confirmar: {
                    btnClass: 'btn-green',
                    action: function(){
                        $.ajax({
                            url: url,
                            type: 'GET',
                            dataType: 'json',
                            beforeSend: function () {
                                $("#loading").fadeIn(500);
                            },
                            complete: function () {
                                $("#loading").fadeOut(500);
                            },
                            success: function (data) {
                                if (data.procesado == true) {
                                    $.alert({
                                        title: 'Operación Cancelada',
                                        icon: 'fa fa-success',
                                        type: 'green',
                                        content: 'La operación se canceló correctamente',
                                        buttons: {
                                            Ok: function () {
                                                location.reload();
                                            }
                                        }
                                    });
                                } else {
                                    $.alert({
                                        title: 'ERROR',
                                        icon: 'fa fa-warning',
                                        type: 'green',
                                        content: 'La operación no se pudo cancelar. Reintente más tarde.',
                                        buttons: {
                                            Ok: function () {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            }
        });
    })

    .on('click', '.desvincular-cuenta-bancaria',  function (e) {
        e.preventDefault();

        var elem = $(this),
            url = elem.attr('href');

        $.confirm({
            title: 'Confirmar',
            content: '¿Está seguro que desea desvincular la cuenta bancaria?',
            buttons: {
                cancelar: {
                    btnClass: 'btn-red',
                    action: function(){

                    }
                },
                confirmar: {
                    btnClass: 'btn-green',
                    action: function(){
                        $.ajax({
                            url: url,
                            type: 'GET',
                            dataType: 'json',
                            beforeSend: function () {
                                $("#loading").fadeIn(500);
                            },
                            complete: function () {
                                $("#loading").fadeOut(500);
                            },
                            success: function (data) {
                                if (data.procesado == true) {
                                    $.alert({
                                        title: 'Cuenta Bancaria Desvinculada',
                                        icon: 'fa fa-success',
                                        type: 'green',
                                        content: 'La cuenta se desvinculó correctamente',
                                        buttons: {
                                            Ok: function () {
                                                location.reload();
                                            }
                                        }
                                    });
                                } else {
                                    $.alert({
                                        title: 'ERROR',
                                        icon: 'fa fa-danger',
                                        type: 'red',
                                        content: data.mensaje,
                                        buttons: {
                                            Ok: function () {

                                            }
                                        }
                                    });
                                }
                            }
                        })
                    }
                }
            }
        });
    })
    
    .on('click', '.operaciones-de-fondo-pendientes', function (e) {
        e.preventDefault();

        var elem = $(this),
            cargar_operaciones_pendientes_url = elem.data('cargar-operaciones-pendientes-url'),
            fondo_id = elem.data('fondo');

        console.log(elem);

        $.ajax({
            url: cargar_operaciones_pendientes_url,
            data: {
                fondo: fondo_id
            },
            dataType: 'html',
            type: 'POST',
            success: function (data) {
                $('#modal-operaciones-fondo-pendientes').empty().html(data);

                $('#modal-operaciones-fondo-pendientes').iziModal('open');
            }
        });
    });


$('#fecha_desde-operaciones-realizadas').datetimepicker({
    format: 'DD/MM/YYYY'
}).on('dp.change', function () {
    $('form#cambiar-fecha').submit();
});

$('#fecha_hasta-operaciones-realizadas').datetimepicker({
    format: 'DD/MM/YYYY'
}).on('dp.change', function () {
    $('form#cambiar-fecha').submit();
});