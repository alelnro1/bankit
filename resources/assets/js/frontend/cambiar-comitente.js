$('body')
    .on('click', '.mostrar-seleccion-comitente', function (e) {
        e.preventDefault();

        var elem = $(this),
            url = elem.attr('href');

        $.ajax({
            url: url,
            type: 'POST',
            beforeSend: function () {
                $("#loading").fadeIn(500);
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            dataType: 'html',
            success: function (data) {
                $('body').append(data);

                $('#comitentes-modal').iziModal({
                    transitionIn: 'bounceInDown'
                }).iziModal('open');
            }
        })
    })
    /*.on('click', '#cambiar-comitente-modal', function (e) {
        var elem = $(this),
            url = elem.data('cambiar-comitente-url');

        $.ajax({
            url: url,
            type: 'POST',
            data: {
                nro_comitente: $('.comitente-seleccion:checked').data('comitente-numero')
            },
            success: function (data) {
                if (data.success == "true") {
                    location.reload('/');
                } else {
                    $.notify('Hubo un error.', 'error');
                }
            }
        })
    })*/;