$('#cbu').mask('0000000000000000000000');

$('body')
    .on('click', '#abrir-modal-agregar-por-cbu-alias', function () {
        dialog = $('#modal-cbu-alias').dialog({
            modal: true
        });
    })
    .on('input', 'input#alias', function () {
        var elem = $(this),
            valor = elem.val();

        elem.val(valor.toUpperCase());
    })
    .on('submit', '#precargar-datos-cuenta-bancaria-form', function (e) {
        e.preventDefault();

        var form = $(this),
            cbu = $('#cbu').val(),
            alias = $('#alias').val();

        if (cbu == "" && alias == "") {
            $.alert({
                title: 'Faltan datos!',
                content: 'Escriba el CBU o el ALIAS',
                type: 'red'
            });
        } else {
            form[0].submit();
        }
    })
    .on('submit', '#alta-cuenta-bancaria', function (e) {
        e.preventDefault();

        var form = $(this),
            url = form.attr('action'),
            data = form.serializeArray();

        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                $("#loading").fadeIn(500);
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            success: function (data) {
                window.location = data.redirect_url;
            },
            error: function (request, status, error) {
                var err = JSON.parse(request.responseText);

                 $.each(err.errors, function (key, error) {
                    $.notify(error, 'error');
                });
            }
        });
    });