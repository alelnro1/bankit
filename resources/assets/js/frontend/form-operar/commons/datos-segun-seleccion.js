/**
 * Contenedor de funciones que cargan dinamicamente los datos segun la seleccion
 * realizada en alguno de los campos
 * @type {{tiposOperacionesPorTipoInstrumento: CargarDatosSegunSeleccion.tiposOperacionesPorTipoInstrumento, especiesPorInstrumento: CargarDatosSegunSeleccion.especiesPorInstrumento, plazosLiquidacionPorTipoOperacion: CargarDatosSegunSeleccion.plazosLiquidacionPorTipoOperacion}}
 */
var CargarDatosSegunSeleccion = {

    /**
     * Se cargan los tipos de operaciones segun el instrumento seleccionado
     * En caso de ser fondos se carga Suscripcion/Rescate
     * En caso de ser otros instrumentos se carga Compra/Venta
     */
    tiposOperacionesPorTipoInstrumento: function () {
        // Limpiamos las especies
        //$('select#especie').empty().selectpicker('refresh');

        $.ajax({
            url: $('select#tipo_instrumento').data('cargar-operaciones-url'),
            type: 'POST',
            data: {
                'id': $('select#tipo_instrumento option:selected').val()
            },
            beforeSend: function () {
                $('div#tipos-operaciones').empty().html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>');
            },
            dataType: 'json',
            success: function (data) {
                var tipos_operaciones = data.tipos_operaciones,
                    div_tipos_operaciones = $('div#tipos-operaciones');

                div_tipos_operaciones.empty().html(tipos_operaciones);
            }
        })
    },

    camposParaIngresarInfoPorInstrumento: function (tipo_instrumento) {
        if (tipo_instrumento == undefined) {
            tipo_instrumento = $.trim($('select#tipo_instrumento option:selected').text());
        }

        $.ajax({
            url: $('select#tipo_instrumento').data('cargar-campos-url'),
            type: 'POST',
            data: {
                'tipo_instrumento': tipo_instrumento,
                'tipo_operacion': $('input#tipo-operacion:checked').data('nombre')
            },
            beforeSend: function () {
                $('div#datos-operacion').empty().html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>');
            },
            dataType: 'json',
            success: function (data) {
                var div_datos_operacion = $('div#datos-operacion');

                div_datos_operacion.empty().html(data.campos).fadeIn();

                FormOperacion.cargarValoresEspecie();

                FuncionesComunes.habilitarMask();

                FuncionesComunes.habilitarFecha();
            }
        })
    },

    instrumentosPorOperacion: function () {
        $.ajax({
            url: $('select#tipo_instrumento').data('cargar-instrumentos-url'),
            type: 'POST',
            data: {
                'tipo_instrumento': $.trim($('select#tipo_instrumento option:selected').html()),
                'tipo_operacion': $('#tipo-operacion:checked').data('nombre')
            },
            dataType: 'json',
            beforeSend: function () {
                $('#instrumento-operar').hide();
                $('#cargando-instrumento').show();
            },
            success: function (data) {
                var select_instrumentos = $('select#instrumento');

                select_instrumentos.empty().append(data.instrumentos);

                $('#cargando-instrumento').hide();
                $('div#instrumento-operar').show();

                //especies.selectpicker('refresh');
            }
        });
    },

    plazosLiquidacionPorInstrumento: function () {
        $.ajax({
            url: $('select#tipo_instrumento').data('cargar-plazos-monedas-url'),
            type: 'POST',
            data: {
                'instrumento': $('select#instrumento option:selected').data('nombre'),
                'tipo_operacion': $('input#tipo-operacion:checked').data('nombre')
            },
            dataType: 'json',
            beforeSend: function () {
                $('#plazos-liquidacion, #monedas').hide();
                $('#cargando-plazos-liquidacion, #cargando-monedas').show();
            },
            success: function (data) {
                var plazo_liquidacion_select = $('#plazo_liquidacion'),
                    moneda_select = $('#moneda');

                plazo_liquidacion_select.empty().append(data.plazos);
                moneda_select.empty().append(data.monedas);
                $('span#limite-minimo').empty().html(
                    number_format(data.datos_instrumento.limite_minimo, 2, ',', '.')
                );

                $('span#valor-cuotaparte').empty().html(
                    number_format(data.datos_instrumento.cotizacion, 6, ',', '.')
                );

                $('span#cant-cuotapartes-disponibles')
                    .empty()
                    .html(
                        number_format(data.datos_instrumento.cant_cuotapartes, 7, ',', '.')
                    )
                    .data('cant-cuotapartes-disponibles', number_format(data.datos_instrumento.cant_cuotapartes, 7, ',', '.'));

                $('#cargando-plazos-liquidacion, #cargando-monedas').hide();
                $('#plazos-liquidacion, #monedas').show();
            }
        });
    },

    cargarReglamentoDeGestion: function () {
        var tipo_operacion = $('input#tipo-operacion:checked').data('nombre'),
            reglamento = $("select#especie").find(':selected').data('reglamento');

        $("#link_reglamento").attr("href", reglamento);

        if (tipo_operacion == "Suscripcion") {
            $(".alert-suscripcion").show();
            $(".alert-rescate").hide();
            $(".checkbox-reglamento").show();
            $("#confirmar-operacion").prop('disabled', true);
        } else {
            $(".alert-suscripcion").hide();
            $(".alert-rescate").show();
            $(".checkbox-reglamento").hide();
            $("#confirmar-operacion").prop('disabled', false);
        }
    },

    cargarCuentasBancariasDeMoneda: function () {
        $.ajax({
            url: $('select#tipo_instrumento').data('cargar-cuentas-bancarias-url'),
            type: 'POST',
            data: {
                'moneda': $('select#moneda option:selected').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $('#cuentas-bancarias').hide();
                $('#cargando-cuentas-bancarias').show();
            },
            success: function (data) {
                var cuentas_bancarias_select = $('#cuentas_bancarias_select');

                cuentas_bancarias_select.empty().append(data.cuentas);

                $('#cargando-cuentas-bancarias').hide();
                $('#cuentas-bancarias').show();
            }
        });
    },

    /**
     * Se cargan los plazos de liquidacion segun el tipo seleccionado
     * Para compra se carga Instantanea, 24hs, 48hs y para venta 48hs
     */
    /*plazosLiquidacionPorTipoOperacion: function (plazo_seleccionado) {
        // Cargamos los plazos de liquidacion para el tipo de operacion seleccionado
        $.ajax({
            url: $('div#tipos-operaciones').data('cargar-plazos-url'),
            type: 'POST',
            data: {
                'tipo_instrumento': $('select#tipo_instrumento option:selected').html(),
                'id': $('input#tipo-operacion:checked').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $('#plazos-liquidacion').hide();
                $('#cargando-plazos-liquidacion').show();
            },
            success: function (data) {
                var plazo_liquidacion_select = $('#plazo_liquidacion');

                plazo_liquidacion_select.empty();

                $.each(data.plazos, function (key, plazo) {
                    plazo_liquidacion_select.append(
                        "<option value='" + plazo.id + "'>" + plazo.nombre + "</option>"
                    );
                });

                // Si nos mandan un plazo para seleccionar, lo selecciono
                if (plazo_seleccionado)
                    plazo_liquidacion_select.find('option[value="' + plazo_seleccionado + '"]').attr("selected", true);

                CargarDatosSegunSeleccion.especiesPorInstrumentoYPlazoLiq();

                $('#cargando-plazos-liquidacion').hide();
                $('#plazos-liquidacion').show();
            }
        });
    },*/

    plataOInstrumentosDisponibles: function () {
        var tipo_instrumento = $.trim($('select#tipo_instrumento option:selected').html()),
            tipo_operacion = $('#tipo-operacion:checked').val(),
            instrumento_seleccionada = $('#instrumento option:selected'),
            cotizacion_accion = FuncionesComunes.numberFormat(instrumento_seleccionada.data('cotizacion-accion'), 2),
            plata_disponible = FuncionesComunes.numberFormat(instrumento_seleccionada.data('plata-disponible'), 2, ',', '.'),
            cant_inst_disponibles = FuncionesComunes.numberFormat(instrumento_seleccionada.data('cant-inst-disponibles'), 0, ',', '.');

        console.log(cant_inst_disponibles);

        if (tipo_instrumento === "Acciones" || tipo_instrumento == "Futuros") {
            // Es una compra
            if (tipo_operacion === "1") {
                $('#instr-disponibles-div').hide();
                $('#cotizacion-activo-subyacente-div').hide();
                $('#plata-disponible-div').show();

                if (plata_disponible != "NaN") {
                    $('span#plata_disponible').html(plata_disponible);
                }
            } else {
                $('#plata-disponible-div').hide();
                $('#instr-disponibles-div').show();

                if (cant_inst_disponibles != "NaN") {
                    $('span#instrumentos_disponibles').html(cant_inst_disponibles);
                }
            }
        } else if (tipo_instrumento === "Opciones") {

            $('#cotizacion-activo-subyacente-div').show();

            if (cotizacion_accion != "NaN") {
                $('#cotizacion_accion_subyacente').html(cotizacion_accion);
            }

            if (tipo_operacion == "9") {
                $('#plata-disponible-div').hide();
                $('#instr-disponibles-div').show();
                $('span#instrumentos_disponibles').html(cant_inst_disponibles * 100);
            }
        }
    },

    /**
     * Se cargan las especies por el tipo de instrumento y el plazo de liquidacion seleccionado
     */
    /*especiesPorInstrumentoYPlazoLiq: function () {
        $.ajax({
            url: $('select#plazo_liquidacion').data('cargar-especies-url'),
            type: 'POST',
            data: {
                'tipo_instrumento': $('select#tipo_instrumento option:selected').val(),
                'plazo_liquidacion': $('select#plazo_liquidacion option:selected').val(),
                'tipo_operacion': $('#tipo-operacion:checked').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $('div#especie-operar').hide();
                $('#cargando-especie').show();
            },
            success: function (data) {
                var especies_select = $('#especie');

                especies_select
                    .empty()
                    .append(data);

                $('#cargando-especie').hide();
                $('div#especie-operar').show();

                $('#especie').selectpicker('refresh');
            }
        });
    },*/

    cargarPreciosDeEspecie: function () {
        var especie = $('#instrumento option:selected'),
            precio = especie.data('precio'),
            bid = especie.data('bid'),
            ask = especie.data('ask'),
            limite_minimo = especie.data('limite-minimo'),
            saldo_valorizado = especie.data('saldo-valorizado'),
            plata_disponible = especie.data('plata-disponible'),
            cant_cuotapartes_disponibles = especie.data('cant-cuotapartes-disponibles'),
            valor_cuotaparte = especie.data('valor-cuotaparte');

        // Lleno los campos de precios
        if (precio != undefined) {
            $('#precio_especie').empty().html(precio);
        }

        $('#bid_especie').empty().html(bid);
        $('#ask_especie').empty().html(ask);

        // Fondos
        $('#valor-cuotaparte').empty().html(valor_cuotaparte);
        $('#limite-minimo').empty().html(limite_minimo);
        $('#saldo-valorizado').empty().html(saldo_valorizado);
        $('#plata-disponible').empty().html(plata_disponible);
        $('#cant-cuotapartes-disponibles').empty().html(cant_cuotapartes_disponibles)
            .data('cant-cuotapartes-disponibles', cant_cuotapartes_disponibles);
    }
};