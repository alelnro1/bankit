var FuncionesComunes = {
    limpiarPrecios: function () {
        $('#precio_especie, #bid_especie, #ask_especie, #plata_disponible, #cotizacion_accion_subyacente, #limite-minimo, #limite-minimo, #valor-cuotaparte').empty();

        $('#valor-cuotaparte').html('10');
    },

    habilitarFecha: function () {
        $('#operar-fecha-validez')
            .datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                minDate: moment().add(1, "days"),
                useCurrent: false
            });
    },

    numberFormat: function (number, decimals, dec_point, thousands_sep) {
        number = number * 1;//makes sure `number` is numeric value
        var str = number.toFixed(decimals ? decimals : 0).toString().split('.');
        var parts = [];
        for (var i = str[0].length; i > 0; i -= 3) {
            parts.unshift(str[0].substring(Math.max(0, i - 3), i));
        }
        str[0] = parts.join(thousands_sep ? thousands_sep : ',');
        return str.join(dec_point ? dec_point : '.');
    },

    /**
     * Obtengo el precio a utilizar, ya sea de bid o de ask
     * segun si estoy haciendo una compra o venta respectivamente
     *
     * @returns {number}
     */
    precioBidOAsk: function () {
        var precio_bid_o_ask = 0,
            bid_especie = $('span#bid_especie').html(),
            ask_especie = $('span#ask_especie').html(),
            precio_limite = $('#precio-limite').val().replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.'),
            tipo_operacion = $('#tipo-operacion:checked').data('nombre');

        if ($.trim($('select#tipo_precio option:selected').html()) == "Precio Limite") {
            precio_bid_o_ask = precio_limite;
        } else {
            if (tipo_operacion == "Compra") {
                precio_bid_o_ask = bid_especie;
            } else {
                precio_bid_o_ask = ask_especie;
            }
        }

        return precio_bid_o_ask;
    },

    habilitarMask: function () {
        var tipo_instrumento = $.trim($('#tipo_instrumento option:selected').html()),
            tipo_precio = $('#tipo-calculo option:selected').val(),
            mask_precio = '000.000.000.000,00';

        if (tipo_instrumento == "Fondos") {
            if (tipo_precio == "monto") {
                mask_cantidad = '000.000.000.000.000,00';
            } else {
                mask_cantidad = '000.000.000.000.000,0000';
            }
        } else if (tipo_instrumento == "Opciones") {
            mask_cantidad = '000.000.000.000.000';

        } else {
            if (tipo_precio == "dinero") {
                mask_cantidad = '000.000.000.000.000,00';
            } else {
                mask_cantidad = '000.000.000.000.000';
            }
        }

        $('.cantidad')
            .mask(mask_cantidad, {
                reverse: true
            });

        $('.precio-limite')
            .mask(mask_precio, {
                reverse: true
            });
    },

    blockUI: function () {
        $('div#wrap').block({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                'z-index': 9999999999999999
            },
            message: 'Espere...'
        });
    },

    sumarGarantias: function () {
        var total = 0;

        $.each($('td.subtotal-garantia'), function () {
            var cantidad = $(this).data('cantidad');
            if (cantidad > 0) {
                total = total + $(this).data('cantidad');
            }
        });

        $('span#garantia-destinada').data('cantidad', total);
        $('span#garantia-destinada').empty().html(FuncionesComunes.numberFormat(total, 2, ',', '.'));
    },

    mostrarError: function (mensaje) {
        $.alert({
            title: 'Error',
            icon: 'fa fa-warning',
            type: 'red',
            content: mensaje
        });
    }
};