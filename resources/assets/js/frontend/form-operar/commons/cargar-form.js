var FormOperacion = {
    abrirModal: function (tipo_instrumento) {
        $("#modal-operar").iziModal({
            closeOnEscape: false
        });

        $('#modal-operar').iziModal('open');

        $('#modal-operar').tinyDraggable({handle: '.iziModal-header'});

        /*if (tipo_instrumento !== null) {
            CargarDatosSegunSeleccion.camposParaIngresarInfoPorInstrumento(tipo_instrumento);

            var option = $("#tipo_instrumento").find("option:contains(" + tipo_instrumento + ")");

            option.attr('selected', 'selected');
        }*/
    },

    seleccionarOperacion: function (operacion, plazo_seleccionado) {
        var int = setInterval(function () {
            var tipo_operacion = $('#tipo-operacion[data-nombre="' + operacion + '"]');

            if (tipo_operacion.length > 0) {
                tipo_operacion.attr('checked', true);
                clearInterval(int);

                CargarDatosSegunSeleccion.plazosLiquidacionPorTipoOperacion(plazo_seleccionado);
            }
        }, 100);
    },

    seleccionarEspecie: function (especie) {
        var int = setInterval(function () {
            if ($('select#especie option').length > 1) {

                //$('select#especie').on('loaded.bs.select', function (e) {
                if (especie != null) {
                    $('select#especie').selectpicker('val', especie);

                    FormOperacion.cargarValoresEspecie();
                }

                clearInterval(int);
                //});
            }
        }, 100);
    },

    // Se carga el precio, bid, ask, dinero disponible etc de la especie. El sector amarillo del modal
    cargarValoresEspecie: function () {
        CargarDatosSegunSeleccion.cargarPreciosDeEspecie();

        CargarDatosSegunSeleccion.plataOInstrumentosDisponibles();
    }
};