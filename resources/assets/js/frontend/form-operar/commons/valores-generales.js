var ValoresGenerales = {
    formatearCantidadAVisualizar: function (cantidad) {
        return parseFloat(
            $('#cantidad')
                .val()
                .replace('.', '')
                .replace('.', '')
                .replace('.', '')
                .replace('.', '')
                .replace(',', '.')
        )
    },

    formatearValorCuotaparte: function (valor) {
        return parseFloat(
            $('#valor-cuotaparte')
                .html()
                .replace(',', '.')
        );
    },

    TipoCalculo: {

        calcularCantidadInstrumentos: function (tipo_calculo, cantidad_previsualizar, valor_cuotaparte) {
            var cantidad = null;
            console.log(cantidad_previsualizar);

            if (tipo_calculo === "monto") {
                cantidad = parseFloat(cantidad_previsualizar / valor_cuotaparte).toFixed(7);
            }  else {
                if (tipo_calculo === "total") { 
                    cantidad = parseFloat(cantidad_previsualizar).toFixed(7);
                } else {
                    cantidad = parseFloat(cantidad_previsualizar).toFixed(2);
                }
            }

            console.log("&&&&");
            console.log(cantidad_previsualizar / valor_cuotaparte);
            console.log("&&&&");

            return cantidad;
        },

        calcularValorEstimado: function (tipo_calculo, cantidad_previsualizar, valor_cuotaparte) {
            var valor_estimado = null;


            if (tipo_calculo == "monto") {
                valor_estimado = parseFloat(cantidad_previsualizar * valor_cuotaparte).toFixed(2);
            } else if (tipo_calculo) {
                valor_estimado = parseFloat(cantidad_previsualizar * valor_cuotaparte).toFixed(2);
            }


            return valor_estimado;
        }
    }
};