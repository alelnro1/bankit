var Operar = {
    fondo: function () {
        var form_operacion_instrumento = $('#operar-instrumento'),
            tipo_calculo = $('#tipo-calculo').val(),
            cantidad = $('#cantidad').val(),
            cantidad_previsualizar = parseFloat($('#cantidad').val().replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.')).toFixed(4),
            valor_cuotaparte = parseFloat($('#valor-cuotaparte').html().replace(',', '.')).toFixed(4),
            limite_minimo = $('#limite-minimo').html(),
            cant_lote = 1;

        // TODO: Extender esto
        if (tipo_calculo == "monto") {
            cantidad_instrumentos = parseFloat(cantidad_previsualizar / valor_cuotaparte).toFixed(4);
            valor_estimado = parseFloat(cantidad_instrumentos * valor_cuotaparte).toFixed(2);
        } else {
            cantidad_instrumentos = parseFloat(cantidad_previsualizar).toFixed(4);
            valor_estimado = parseFloat(cantidad_previsualizar * valor_cuotaparte).toFixed(2);
        }

        var form_ajax = form_operacion_instrumento.serializeArray();
        form_ajax.push({name: 'cant_instrumentos', value: cantidad_instrumentos});
        form_ajax.push({name: 'valor_estimado', value: valor_estimado});
        form_ajax.push({name: 'cant_lote', value: cant_lote});
        form_ajax.push({name: 'valor_cuotaparte', value: valor_cuotaparte});

        if (tipo_calculo == "total") {
            form_ajax.push({name: 'cantidad', value: cantidad});
        }

        return form_ajax;
    },

    especie: function () {
        var form_operacion_instrumento = $('#operar-instrumento'),
            precio_bid_o_ask = FuncionesComunes.precioBidOAsk(),
            cantidad = $('#cantidad').val().replace('.', '').replace('.', '').replace('.', '').replace('.', ''),
            cantidad_previsualizar = parseFloat($('#cantidad').val().replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.')).toFixed(0),
            cant_lote = $('#especie option:selected').data('cant-lote'); // Una opcion representa 100 acciones

        // TODO: EXTENDER ESTO
        if ($('select#tipo-calculo option:selected').val() == "dinero") {
            cantidad_instrumentos = parseFloat(Math.floor(cantidad_previsualizar / precio_bid_o_ask)).toFixed(0);
        } else {
            cantidad_instrumentos = cantidad_previsualizar;
        }

        var form_ajax = form_operacion_instrumento.serializeArray();
        form_ajax.push({name: 'cant_instrumentos', value: cantidad_instrumentos});
        form_ajax.push({name: 'cant_lote', value: cant_lote});

        return form_ajax;
    },

    futuros: function () {
        var form_ajax, garantias = new Array();

        // Traemos los mismos datos que con especies
        form_ajax = Operar.especie();

        // Buscamos las garantias
        $.each($('input.cantidad-garantia'), function () {
            var elem = $(this),
                especie_id = elem.data('especie-id'),
                fondo_id = elem.data('fondo-id'),
                moneda_id = elem.data('moneda-id'),
                garantia_id = elem.data('garantia-id'),
                cantidad_garantia = elem.val();

            if (cantidad_garantia > 0) {
                var garantia = {
                    especie_id: especie_id,
                    fondo_id: fondo_id,
                    moneda_id: moneda_id,
                    garantia_id: garantia_id,
                    cantidad_garantia: cantidad_garantia
                };

                garantias.push(garantia);
            }
        });

        // Agregamos el listado de garantias al form para procesar
        form_ajax.push({name: 'garantias', value: JSON.stringify(garantias)});

        return form_ajax;
    }
};