var AbrirModalPrecargado = {
    cargarConTipoInstrumento: function (elem) {
        var tipo_instrumento = elem.data('tipo');

        FormOperacion.abrirModal(tipo_instrumento);

        CargarDatosSegunSeleccion.tiposOperacionesPorTipoInstrumento();

        return true;
    },

    cargarEspecieConPrecio: function (elem) {
        AbrirModalPrecargado.cargarConTipoInstrumento(elem);

        // Buscamos el precio de la especie a operar
        var precio = elem.attr('data-precio'),
            tipo_operacion = elem.data('tipo-operacion'),
            especie = elem.data('especie');

        // Esperamos a que se carguen los campos para el instrumento (unidad, tipo de precio, plazo de validez)
        var int = setInterval(function () {
            var tipo_calculo = $('#tipo-calculo'),
                tipo_precio = $('#tipo_precio'),
                precio_limite = $('#precio-limite'),
                tipo_operacion_select = $('input[name=tipo_operacion][type="radio"]'),
                plazo_seleccionado = 3; // 48hs

            if (tipo_calculo.is(':visible') != false && tipo_operacion_select.is(':visible') != false) {

                // Seleccionamos la operacion a realizar
                FormOperacion.seleccionarOperacion(tipo_operacion, plazo_seleccionado);

                // Una vez seleccionado el tipo de operacion, cargamos los plazos
                CargarDatosSegunSeleccion.plazosLiquidacionPorTipoOperacion();

                // Una vez cargados los plazos, y cargadas las especies, seleccionamos la especie a operar
                FormOperacion.seleccionarEspecie(especie);

                // Seleccionamos el tipo de precio limite
                $('#tipo_precio option[value=2]').prop('selected', true)

                precio_limite.val(FuncionesComunes.numberFormat(precio, 2, ',')).parent().parent().show();

                clearInterval(int);
            }
        }, 100);
    }
};