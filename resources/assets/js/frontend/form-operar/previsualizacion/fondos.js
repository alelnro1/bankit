var PrevisualizarOperacionFondo = function () {
    var modal_confirmar_operacion = $('#modal-confirmar-operacion'),
        fondo = $('#instrumento').find('option:selected').html(),
        plazo_liquidacion = $('#plazo_liquidacion').find('option:selected').html(),
        tipo_calculo = $('#tipo-calculo').val(),
        cantidad = ValoresGenerales.formatearCantidadAVisualizar($('#cantidad').val()),
        tipo_operacion = $('#tipo-operacion:checked').data('nombre'),
        limite_minimo = $('#instrumento').find('option:selected').data('limite-minimo'),
        cant_lote = 1;

    if (tipo_operacion == "Suscripcion") {
        if (cantidad < limite_minimo) {
            FuncionesComunes.mostrarError('El mínimo a operar es $1.000');

            return false;
        }
    }

    modal_confirmar_operacion.find('span#confirmar-operar-especie').html(fondo);
    modal_confirmar_operacion.find('span#confirmar-operar-plazo-liquidacion').html(plazo_liquidacion);
    modal_confirmar_operacion.find('span#confirmar-operar-tipo-operacion').html(tipo_operacion);

    valor_cuotaparte = ValoresGenerales.formatearValorCuotaparte($('#valor-cuotaparte').html());
    cantidad_instrumentos = ValoresGenerales.TipoCalculo.calcularCantidadInstrumentos(tipo_calculo, cantidad, valor_cuotaparte);
    valor_estimado = ValoresGenerales.TipoCalculo.calcularValorEstimado(tipo_calculo, cantidad_instrumentos, valor_cuotaparte);

    modal_confirmar_operacion.find('span#confirmar-operar-cantidad-instrumentos').html(
        number_format(cantidad_instrumentos, 6, ',', '.')
    );

    modal_confirmar_operacion.find('span#confirmar-operar-valor-estimado').html(
        number_format(valor_estimado, 2, ',', '.')
    );

    modal_confirmar_operacion.modal();
};