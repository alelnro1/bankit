ProcesarFormOperar = function () {
    var instrumento = $.trim($('select#tipo_instrumento option:selected').html()),
        form_operacion_instrumento = $('#operar-instrumento');

    if (instrumento == "Fondos") {
        form_ajax = OperarFondo();
    }

    // Envio la solicitud de procesar la operacion
    $.ajax({
        url: form_operacion_instrumento.attr('action'),
        type: 'POST',
        data: form_ajax,
        dataType: 'json',
        beforeSend: function () {
            $('#operar-warning').hide();
        },
        success: function (data) {
            console.log(data);
            if (data.procesado === true) {
                $.alert({
                    title: 'Operación Procesada',
                    icon: 'fa fa-success',
                    type: 'green',
                    content: 'La operación se procesó correctamente',
                    buttons: {
                        "Ok" : function () {
                            window.location = '/';
                        }
                    }
                });
            } else {
                $('#modal-confirmar-operacion').modal('hide');

                $.alert({
                    title: 'ERROR!',
                    icon: 'fa fa-warning',
                    type: 'red',
                    content: data.error,
                        "Ok" : function () {
                        }
                });
            }
        }
    });
};