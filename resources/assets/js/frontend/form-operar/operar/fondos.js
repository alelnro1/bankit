var OperarFondo = function () {
    var form_operacion_instrumento = $('#operar-instrumento'),
        tipo_calculo = $('#tipo-calculo').val(),
        cantidad = $('#cantidad').val(),
        //cantidad_previsualizar = ValoresGenerales.formatearCantidadAVisualizar($('#cantidad').val()),//parseFloat($('#cantidad').val().replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.')).toFixed(4),
        //valor_cuotaparte = ValoresGenerales.formatearValorCuotaparte($('#valor-cuotaparte').html()),//parseFloat($('#valor-cuotaparte').html().replace(',', '.')).toFixed(4),
        limite_minimo = $('#limite-minimo').html(),
        cant_lote = 1;

    // TODO: Extender esto
    /*if (tipo_calculo == "monto") {
        cantidad_instrumentos = parseFloat(cantidad_previsualizar / valor_cuotaparte).toFixed(4);
        valor_estimado = parseFloat(cantidad_instrumentos * valor_cuotaparte).toFixed(2);
    } else {
        cantidad_instrumentos = parseFloat(cantidad_previsualizar).toFixed(4);
        valor_estimado = parseFloat(cantidad_previsualizar * valor_cuotaparte).toFixed(2);
    }*/

    cantidad_previsualizar = ValoresGenerales.formatearCantidadAVisualizar(cantidad);
    valor_cuotaparte = ValoresGenerales.formatearValorCuotaparte($('#valor-cuotaparte').html());
    cantidad_instrumentos = ValoresGenerales.TipoCalculo.calcularCantidadInstrumentos(tipo_calculo, cantidad_previsualizar, valor_cuotaparte)
    valor_estimado = ValoresGenerales.TipoCalculo.calcularValorEstimado(tipo_calculo, cantidad_instrumentos, valor_cuotaparte);

    var form_ajax = form_operacion_instrumento.serializeArray();
    form_ajax.push({name: 'cant_instrumentos', value: cantidad_instrumentos});
    form_ajax.push({name: 'valor_estimado', value: valor_estimado});
    form_ajax.push({name: 'cant_lote', value: cant_lote});
    form_ajax.push({name: 'valor_cuotaparte', value: valor_cuotaparte});

    if (tipo_calculo == "total") {
        form_ajax.push({name: 'cantidad', value: cantidad});
    }

    return form_ajax;
};