if ($("#modal-operar").length > 0) {
    $("#modal-operar").iziModal({
        closeOnEscape: false,
        transitionIn: 'bounceInDown',
        transitionOut: 'bounceOutDown'
    });
}

$(document)
    .on('click', '.trigger', function (event) {
        event.preventDefault();

        var tipo_instrumento_seleccionado = $.trim($("#tipo_instrumento option:selected").text());

        FormOperacion.abrirModal(tipo_instrumento_seleccionado);

        CargarDatosSegunSeleccion.tiposOperacionesPorTipoInstrumento();
    })
    // Prompt dialog de confirmar operacion
    .on('submit', '#operar-instrumento', function (e) {
        e.preventDefault();

        var instrumento_actual = $.trim($('#tipo_instrumento option:selected').html());

        if (instrumento_actual == "Fondos") {
            var tipo_operacion = $('input#tipo-operacion:checked').data('nombre'),
                reglamento_check = $('#check_reglamento:checkbox:checked').length;

            if ((tipo_operacion == "Suscripcion" && reglamento_check > 0) || tipo_operacion == "Rescate") {
                PrevisualizarOperacionFondo();
            } else {
                FuncionesComunes.mostrarError('Debe confirmar que ha leído el reglamento de gestión');
            }
        }
    })
    .on('change', 'input#tipo-operacion', function () {
        var instrumento_actual = $.trim($('#tipo_instrumento option:selected').html());

        CargarDatosSegunSeleccion.instrumentosPorOperacion();

        CargarDatosSegunSeleccion.camposParaIngresarInfoPorInstrumento();
    })
    .on('change', 'select#plazo_liquidacion', function () {
        //FuncionesComunes.limpiarPrecios();
    })
    .on('change', 'select#moneda', function () {
        CargarDatosSegunSeleccion.cargarCuentasBancariasDeMoneda();
    })
    .on('change', 'select#instrumento', function () {
        var instrumento_actual = $.trim($('#tipo_instrumento option:selected').html());
        var instrumento_actual = $.trim($('#tipo_instrumento option:selected').html());

        CargarDatosSegunSeleccion.plazosLiquidacionPorInstrumento();

        CargarDatosSegunSeleccion.cargarReglamentoDeGestion();
    })
    .on('click', '#check_reglamento', function () {
        var is_check = $('#check_reglamento').is(":checked");

        if (is_check) {
            $("#confirmar-operacion").prop("disabled", false);
        } else {
            $("#confirmar-operacion").prop("disabled", true);
        }
    })
    .on('change', 'select#plazo_validez', function () {
        if ($.trim($('select#plazo_validez option:selected').html()) == "Válida hasta el") {
            $('#div-fecha-validez').show();
        } else {
            $('#div-fecha-validez').hide();
        }
    })
    .on('change', 'select#tipo_precio', function () {
        if ($.trim($('select#tipo_precio option:selected').html()) == "Precio Limite") {
            $('#div-precio-limite').show();
        } else {
            $('#div-precio-limite').hide();
        }
    })
    .on('change', '#tipo_instrumento', function () {
        $('#datos-operacion').empty();

        CargarDatosSegunSeleccion.tiposOperacionesPorTipoInstrumento();
    })
    .on('click', '#operacion-confirmada', function (e) {
        e.preventDefault();

        ProcesarFormOperar();
    })
    .on('closing', '#modal-operar', function () {
        $('#modal-operar').iziModal({
            restoreDefaultContent: true
        });
    })

    // Manejamos rescate total
    .on('change', '#tipo-calculo', function () {
        var tipo_calculo = $('#tipo-calculo option:selected').val(),
            cant_cuotapartes_disponibles =
                $('#cant-cuotapartes-disponibles')
                    .data('cant-cuotapartes-disponibles');

        /*if (cant_cuotapartes_disponibles != undefined) {
            cant_cuotapartes_disponibles =
                cant_cuotapartes_disponibles
                    .replace('.', '')
                    .replace('.', '')
                    .replace('.', '');
        }*/

        if (tipo_calculo == "total") {
            $('#cantidad').val(cant_cuotapartes_disponibles).prop('disabled', true);
        } else {
            $('#cantidad').val('').prop('disabled', false);
        }

        FuncionesComunes.habilitarMask();
    })
    .on('click', '.abrir-modal-operar', function (e) {
        e.preventDefault();

        AbrirModalPrecargado.cargarConTipoInstrumento($(this));
    })
    .on('click', '.operar-instrumento-cotizacion', function (e) {
        e.preventDefault();

        AbrirModalPrecargado.cargarEspecieConPrecio($(this));
    })
    .on('submit', '#adjuntar-comprobante-operacion', function (e) {
        e.preventDefault();

        $.confirm({
            title: 'Confirmar!',
            content: '¿Está seguro que el comprobante que desea adjuntar se corresponde con la operación?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-green',
                    action: function () {
                        e.currentTarget.submit();
                    }
                },
                cancelar: {
                    btnClass: 'btn-red',
                    action: function () {

                    }
                }
            }
        });
    });