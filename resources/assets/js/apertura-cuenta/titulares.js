var Titulares = {
    Guardar: function (form, titular_id) {
        var url_guardar_titular = $('button.guardar-titular').data('guardar-titular-url'),
            titular_id_input = form.find('#titular_id');
            datos_form = form.serializeArray();

        $.ajax({
            url: url_guardar_titular,
            type: 'POST',
            dataType: 'json',
            data: datos_form,
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar(); 
            },
            complete: function () {
                $("#loading").fadeOut(500);

                MensajePorCantidadTitulares();
            },
            success: function (data) {
                var titular_id = data.titular_id;

                titular_id_input.val(titular_id);

                //console.log($(form) + ' > .eliminar-titular'));
                $(form).find('.eliminar-titular').attr('data-titular-id', titular_id);

                $.alert({
                    title: 'Exito',
                    icon: 'fa fa-success',
                    type: 'green',
                    content: 'Titular Guardado',
                    buttons: {
                        "Ok" : function () {
                            
                        }
                    }
                });
            },
            error: function (data) {
                GuardarAJAXComunes.ProcesarErrorGuardado(form, data);
            }
        });
    },

    Eliminar: function (titular_id, prospecto_id, tit_accordion) {
        var url_eliminar_titular = $('button.eliminar-titular').data('eliminar-titular-url');

        if (titular_id) {
            $.ajax({
                url: url_eliminar_titular,
                type: 'POST',
                dataType: 'json',
                data: {
                    titular_id: titular_id,
                    prospecto_id: prospecto_id
                },
                complete: function () {
                    MensajePorCantidadTitulares();
                },
                success: function (data) {
                    location.reload();
                    // Si hay mas de 1 titular => eliminamos solo al titular. Sino eliminamos todo
                    var cantidad_de_titulares = $('#titulares > h3').length;

                    if (cantidad_de_titulares > 1) {
                        //tit_accordion.parent().prev("h3").remove();
                        //tit_accordion.parent().remove();

                        tit_accordion.parent().parent().prev("h3").remove();
                        tit_accordion.parent().parent().remove();
                    } else {
                        location.reload();
                        tit_accordion.parent().parent().prev("h3").remove();
                        tit_accordion.parent().parent().remove();
                    }

                    MensajePorCantidadTitulares();


                    $.alert({
                        title: 'Exito',
                        icon: 'fa fa-success',
                        type: 'green',
                        content: 'Titular Eliminado con exito',
                        buttons: {
                            "Ok" : function () {
                                window.location = '/';
                            }
                        }
                    });
                },
                error: function (data) {

                    $.alert({
                        title: 'Error',
                        icon: 'fa fa-danger',
                        type: 'red',
                        content: 'Error, intente nuevamente mas tarde',
                        buttons: {
                            "Ok" : function () {
                                window.location = '/';
                            }
                        }
                    });
                }
            });
        } else {
            location.reload();
            tit_accordion.parent().parent().prev("h3").remove();
            tit_accordion.parent().parent().remove();
        }



        MensajePorCantidadTitulares();
    },

    NuevoTitular: function (collapsible, titular_id, div_titular, numero_documento) {
        var div_titular_nuevo = $('#titular-nuevo').html(),
            div_titulares = $('#titulares'),
            prospecto_id = $('input[name="prospecto_id"]').val();

        // El titular existe => lo hago non collapsible
        if (titular_id) {
            div_titulares.append(div_titular).accordion("refresh", {header: "> div > .h3"});
            Titulares.RelacionarTitularExistente(prospecto_id, titular_id);
        } else {
            if (div_titulares.accordion("instance") != undefined) { // Ya hay algun titular
                div_titulares.append(div_titular_nuevo).accordion("refresh", {header: "> div > .h3"});

            } else {
                div_titulares.append(div_titular_nuevo).accordion({collapsible: collapsible, header: "> div > .h3"});
            }

            // Buscamos el ultimo titular del accordion para cargarle el cuit
            $('form#form_datos_personales').last().find('#cuit_cuil').val(numero_documento);
        }
    },

    RelacionarTitularExistente: function (prospecto_id, titular_id) {
        $.ajax({
            url: $('#modal-titular-documento').data('relacionar-titular-existente-url'),
            data: {
                'titular_id': titular_id,
                'prospecto_id': prospecto_id
            },
            type: 'POST'
        });
    },

    ConsultarPorDocumento: function (tipo_documento, numero_documento, consultar_documento_url) {
        $.ajax({
            url: consultar_documento_url,
            data: {
                'tipo_documento': tipo_documento,
                'numero_documento': numero_documento
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar();
            },
            complete: function () {
                $('button#consultar-documento').prop('disabled', false);
                $("#loading").fadeOut(500);
            },
            success: function (data) {
                var titular_id = data.titular_id,
                    div_titular = data.div_titular;

                if (titular_id == null) {
                    Titulares.NuevoTitular(true, titular_id, div_titular, numero_documento);
                } else {
                    Titulares.NuevoTitular(false, titular_id, div_titular, numero_documento);
                }

                $('.fecha_nacimiento').datetimepicker({
                    format: 'DD/MM/YYYY'
                });

                $("#modal-titular-documento").dialog('close');

                MensajePorCantidadTitulares();
            }
        });
    }
};