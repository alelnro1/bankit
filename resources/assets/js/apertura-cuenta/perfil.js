$('body')
    .on('click', '.item-perfil', function () {
        var riesgo = $('#riesgo:checked').val(),
            experiencia = $('#experiencia:checked').val(),
            informacion = $('#informacion:checked').val(),
            horizonte_inversion = $('#horizonte-inversion:checked').val(),
            tiempo = $('#tiempo:checked').val(),
            div_perfil_riesgo = $('#perfil-riesgo'),
            valor_perfil_riesgo = $('#valor-perfil-riesgo');

        if (Perfil.TieneDatosCompletos()) {
            riesgo = parseInt(riesgo.substr(riesgo.length - 5)[0]);
            experiencia = parseInt(experiencia.substr(experiencia.length - 5)[0]);
            informacion = parseInt(informacion.substr(informacion.length - 5)[0]);
            horizonte_inversion = parseInt(horizonte_inversion.substr(horizonte_inversion.length - 5)[0]);
            tiempo = parseInt(tiempo.substr(tiempo.length - 5)[0]);

            perfil = Perfil.Calcular(riesgo, experiencia, informacion, horizonte_inversion, tiempo);

            valor_perfil_riesgo.empty().html(perfil);
            div_perfil_riesgo.show();

            Perfil.GuardarPerfilRiesgo(riesgo, experiencia, informacion, horizonte_inversion, tiempo, perfil);
        } else {
            div_perfil_riesgo.hide();
        }
    });

Perfil = {
    Calcular: function (riesgo, experiencia, informacion, horizonte_inversion, tiempo) {
        var sumatoria = riesgo + experiencia + informacion + horizonte_inversion + tiempo,
            perfil = "";
        if (sumatoria >= 0 && sumatoria <= 4) {
            perfil = "Conservador";
        } else if (sumatoria >= 5 && sumatoria <= 10) {
            perfil = "Moderado";
        } else if (sumatoria > 10) {
            perfil = "Agresivo";
        }

        return perfil;
    },

    TieneDatosCompletos: function () {
        var riesgo = $('#riesgo:checked').val(),
            experiencia = $('#experiencia:checked').val(),
            informacion = $('#informacion:checked').val(),
            horizonte_inversion = $('#horizonte-inversion:checked').val(),
            tiempo = $('#tiempo:checked').val();

        return (riesgo != undefined && experiencia != undefined && informacion != undefined && horizonte_inversion != undefined && tiempo != undefined);
    },

    GuardarPerfilRiesgo: function (riesgo, experiencia, informacion, horizonte_inversion, tiempo, perfil) {
        var riesgo = $('#riesgo:checked').data('id'),
            experiencia = $('#experiencia:checked').data('id'),
            informacion = $('#informacion:checked').data('id'),
            horizonte_inversion = $('#horizonte-inversion:checked').data('id'),
            tiempo = $('#tiempo:checked').data('id'),
            prospecto_id = $('#prospecto_id').val();

            /*perfil_calculado = Perfil.Calcular(
                $('#riesgo:checked').val(), $('#experiencia:checked').val(),
                $('#informacion:checked').val(), $('#horizonte-inversion:checked').val(), $('#tiempo:checked').val())*/;

        $.ajax({
            url: $('#perfil-form').data('guardar-perfil-url'),
            data: {
                riesgo: riesgo,
                experiencia: experiencia,
                informacion: informacion,
                horizonte_inversion: horizonte_inversion,
                tiempo: tiempo,
                perfil_calculado: perfil,
                prospecto_id: prospecto_id
            },
            type: 'POST'
        })
    }
};