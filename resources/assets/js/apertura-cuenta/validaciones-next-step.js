var ValidacionNextStep = {
    error: null,

    puedeSeguir: function () {
        var paso_actual = $('.rms-current-section').attr('id'),
            puede_seguir = true;

        switch (paso_actual) {
            case "step-titulares":
                puede_seguir = ValidacionNextStep.TieneTitulares();
                break;

            case "step-cuentas-bancarias":
                puede_seguir = ValidacionNextStep.TieneCuentasBancarias();
                break;

            case "step-perfil":
                puede_seguir = ValidacionNextStep.TienePerfilDeRiesgo();

                break;

            case "step-listo":
                puede_seguir = ValidacionNextStep.AceptoTerminos();
                break;
        }

        // Si no puede seguir mostramos la alerta
        if (!puede_seguir) {
            $.alert({
                title: 'Error',
                icon: 'fa fa-warning',
                type: 'red',
                content: ValidacionNextStep.error
            });
        } else {
            // Si estamos en el anteultimo paso, vamos a generar el PDF del cliente
            if (paso_actual == "step-perfil") {
                var pdf_armado = false;
                $.ajax({
                    url: $('#perfil-form').data('generar-pdf-url'),
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        $("#loading").fadeIn(500);
                    },
                    complete: function () {
                        $("#loading").fadeOut(500);
                    },
                    success: function (data) {
                        $('#pdf-tyc').attr('data', data.pdf);

                        pdf_armado = true;
                    }
                })
            }
        }

        // Verificamos si se esta armando el PDF => ultimo paso, hay que
        // esperar que vuelva la ruta al PDF para poder mostrarlo

        var int = setInterval(function () {
            // Estamos en el ultimo paso => vamos a mandar a hacer el PDF y esperamos a que vuelva la ruta
            if (pdf_armado !== undefined) {
                if (pdf_armado === true) {

                }
            } else {
                // No es el ultimo paso => seguimos como si nada
                clearInterval(int);
            }
        }, 200);

        return puede_seguir;
    },

    TieneTitulares: function () {
        if ($('#titulares > h3').length <= 0) {
            ValidacionNextStep.error = "Complete los titulares";

            return false;
        }

        return true;
    },

    TieneCuentasBancarias: function () {
        var form_pesos_saved = $('#form_cuentas_bancarias_pesos').data('saved'),
            form_dolares_saved = $('#form_cuentas_bancarias_dolares').data('saved');

        if (!form_pesos_saved && !form_dolares_saved) {
            ValidacionNextStep.error = "Complete una cuenta bancaria en pesos y/o en dólares."

            return false;
        }

        return true;
    },

    TienePerfilDeRiesgo: function () {
        if (Perfil.TieneDatosCompletos()) {
            return true;
        }

        ValidacionNextStep.error = "Tiene que definir el perfil para continuar";

        return false;
    },

    AceptoTerminos: function () {
        var terminos = $('#confirmar-tyc');

        // Verifico que exista el campo, si no existe estoy con el back
        if (terminos.length > 0) {
            if (terminos.is(':checked')) {
                return true;
            } else {
                ValidacionNextStep.error = "Tiene que aceptar los términos y condiciiones para finalizar el proceso";
            }
        } else {
            return true;
        }
    }
};