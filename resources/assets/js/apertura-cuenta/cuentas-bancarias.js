var CuentasBancarias = {
    BuscarPorCBU :  function () {
        CuentasBancarias.BuscarCuenta(dato, 'cbu');
    },
    BuscarPorAlias :  function (dato) {
        CuentasBancarias.BuscarCuenta(dato, 'alias');
    },
    BuscarCuenta : function (dato, tipo) {
        var form = $('form:visible');

        $.ajax({
            url: $('#tabs').data('buscar-cuenta-bancaria-url'),
            data: {
                'dato' : dato,
                'tipo' : tipo
            },
            type: 'POST',
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar();
            },
            complete: function () {
                $("#loading").fadeOut(500);

                MensajePorCantidadTitulares();
            },
            dataType: 'json',
            success: function (data) {
                if (data.success === false) {
                    $.alert({
                        title: 'Error',
                        icon: 'fa fa-danger',
                        type: 'red',
                        content: data.error,
                        buttons: {
                            "Ok" : function () {
                            }
                        }
                    });
                } else {
                    var cuenta_bancaria_select = form.find('select[id="cuenta_bancaria_tipo_id"]'),
                        banco_select = form.find('select[id="banco_id"]'),
                        cbu_input = form.find('#cbu'),
                        alias_input = form.find('#alias'),

                        tipo_cuenta = data.TipoCTA,
                        cod_banco = data.NroBanco,
                        cbu = data.CBU,
                        alias = data.AliasCBU;

                    console.log(cuenta_bancaria_select);
                    console.log(cuenta_bancaria_select.find('option[data-cod-bcra="' + tipo_cuenta + '"]'));

                    cuenta_bancaria_select.find('[data-cod-bcra="' + tipo_cuenta + '"]').prop('selected', true);

                    banco_select.find('[data-cod-banco="' + cod_banco + '"]').prop('selected', true);

                    cbu_input.val(cbu);

                    alias_input.val(alias);
                }
            }
        })
    },
    Guardar : function (form, titular_id) {
        var url_guardar_titular = $('button.guardar-cuenta-bancaria').data('guardar-titular-url'),
            datos_form = form.serializeArray();

        $.ajax({
            url: url_guardar_titular,
            type: 'POST',
            dataType: 'json',
            data: datos_form,
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar();
            },
            complete: function () {
                $("#loading").fadeOut(500);

                MensajePorCantidadTitulares();
            },
            success: function (data) {
                var cuenta_bancaria = data.cuenta_bancaria,
                    form_cta_bancaria = $('button.guardar-cuenta-bancaria').parent(); // Form en pesos o en dólares

                $('#cuenta_bancaria_id').val(cuenta_bancaria);

                form_cta_bancaria.data('saved', true);

                $.alert({
                    title: 'Cuenta Bancaria adherida',
                    icon: 'fa fa-success',
                    type: 'green',
                    content: 'Se agrego la cuenta bancaria con exito',
                    buttons: {
                        "Ok" : function () {
                            
                        }
                    }
                });
            },
            error: function (data) {
                GuardarAJAXComunes.ProcesarErrorGuardado(form, data);
            }
        });
    }
};
