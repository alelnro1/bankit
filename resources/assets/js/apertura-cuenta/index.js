var GuardarAJAXComunes = {
    AntesDeEnviar: function () {
        // Eliminamos todos los errores
        $('.error-help-block').empty();
        $('.inpt-form-group').removeClass('has-error');

        $("#loading").fadeIn(500);
    },

    ProcesarErrorGuardado: function (form, data) {
        var errors_array = $.parseJSON(data.responseText),
            errores = errors_array.errors;

        // Agregamos
        $.each(errores, function (campo, error) {

            if (campo.indexOf('.') >= 0) {
                campo = campo.split('.')[1];
            }

            console.log(campo);

            $('#' + form.attr('id') + ' #' + campo).closest('.inpt-form-group').addClass('has-error');
            $('#' + form.attr('id') + ' #' + campo).parent().parent().find('.error-help-block').empty().html('<strong>' + error[0] + '</strong>');
        });

        $.alert({
            title: 'Error!',
            icon: 'fa fa-error',
            type: 'red',
            content: 'Revise los datos ingresados',
            buttons: {
                "Ok" : function () {
                    
                }
            }
        });
    },

    RegistrarUltimoPaso: function () {
        $.ajax({
            url: $('#btn-siguiente').data('registrar-ultimo-paso-url'),
            type: 'POST',
            data: {
                step: $('.rms-current-step').attr('id')
            }
        })
    },

    FinalizarProceso: function () {
        $.ajax({
            url: $('#finalizar-apertura-cuenta').data('finalizar-apertura-cuenta-url'),
            type: 'POST',
            data: {
                'cbu_pesos': $('#form_cuentas_bancarias_pesos #cbu').val(),
                'cbu_dolar': $('#form_cuentas_bancarias_dolares #cbu').val()
            },
            beforeSend: function () {
                $("#loading").fadeIn(500);
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            dataType: 'json',
            success: function (data) {
                if (data.cbus_validos === true) {
                    window.location = $('#finalizar-apertura-cuenta').data('finalizar-apertura-cuenta-logout');
                } else {

                    if (data.cbu_pesos !== true && data.cbu_dolar !== true) {
                        cuenta_erronea = "en pesos y dólares";
                    } else if (data.cbu_pesos !== true) {
                        cuenta_erronea = "en pesos";
                    } else if (data.cbu_dolar !== true) {
                        cuenta_erronea = "en dólares";
                    }

                    //$.notify('ERROR! Los CBU de los titulares no se corresponden con los CBU de la cuenta bancaria ' + cuenta_erronea);

                     $.alert({
                        title: 'Error!',
                        icon: 'fa fa-danger',
                        type: 'red',
                        content: 'Los CBU de los titulares no se corresponden con los CBU de la cuenta bancaria ' + cuenta_erronea,
                        buttons: {
                            "Ok" : function () {
                                
                            }
                        }
                    });
                }
            }
        });
    },

    GenerarComitente: function (datos_form) {
        var elem = $('#confirmar-apertura-cuenta');

        $.ajax({
            url: elem.data('generar-comitente-url'),
            type: 'POST',
            data: datos_form,
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar();
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            success: function (data) {
                if (data.success == false) {
                    $.alert({
                        title: 'Error.',
                        icon: 'fa fa-danger',
                        type: 'red',
                        content: data.error,
                        buttons: {
                            "Ok" : function () {

                            }
                        }
                    });

                } else {
                    $.alert({
                        title: 'Comitente Generado.',
                        icon: 'fa fa-success',
                        type: 'green',
                        content: 'El comitente se genero con exito. Aguarde...',
                        buttons: {
                            "Ok" : function () {

                            }
                        }
                    });

                    setTimeout(function () {

                        window.location = '/admin/prospectos';
                    }, 1500);
                }
            }
        });
    }
};