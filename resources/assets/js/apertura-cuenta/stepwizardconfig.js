$('#rms-wizard').stepWizard({
    stepTheme: 'defaultTheme', /*defaultTheme,steptheme1,steptheme2*/
    allstepClickable: false,
    compeletedStepClickable: true,
    stepCounter: true,
    StepImage: true,
    animation: true,
    animationClass: "fadeIn",
    stepValidation: true,
    validation: true,
    field: {
        nombre: {
            required: true,
            minlength: 5,
            Regex: /^[a-zA-Z0-9]+$/
        },
        username: {
            required: true,
            minlength: 7,
            Regex: /^[a-zA-Z0-9]+$/
        },
        password: {
            required: true,
            minlength: 5,
            maxlength: 20,
            Regex: /^(?=.*[0-9_\W]).+$/
        }/*,
        /*email: {
            required: true,
            Regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        }*/
    },
    message: {
        username: "El campo es obligatorio.",
        usernameLength: "El campo es obligatorio.",
        usernameRegex: "El campo es obligatorio.",
        password: "Please provide a password",
        passwordMinLength: "Your Password must consist of at least 5 characters",
        passwordRegex: "Your password must contain at least one Special character",
        cpassword: " Please provide a ConfirmPassword",
        cpassword1: " Enter ConfirmPassword Same as Password",
        email: " Please Enter a Email Address",
        emailRegex: " Please Enter valid a Email Address",
        salutation: " Please Select Salutation",
        gender: " Please Select Gender",
        firstname: "Please Enter a First Name",
        lastname: "Please Enter a last Name",
        phone: "Please Enter a Phone Number",
        phoneRegex: "phone number is Not Valid",
        zipcode: "Please Enter a ZipCode",
        zipcodeRegex: "zip code is Not Valid",
        state: "Please select State",
        address: "Please Enter a FUll Address",
        country: "Please select Country Name",
        cardtype: "Please Select Card Type",
        cardnum: "Card Number Require",
        cardnumRegex: "Card Number is not valid",
        cvc: "CVC Required",
        cvcRegex: "CVC not valid",
        holdername: "Please Enter a Card Holder Name",
        exmonth: "Please Select Expiry Month",
        exyear: "Please Select Expiry Year",
    }
});