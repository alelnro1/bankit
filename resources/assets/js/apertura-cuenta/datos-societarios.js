var DatosSocietarios = {
    Guardar: function (form, titular_id) {
        var url_guardar_datos_societarios = $('button#guardar-datos-societarios').data('guardar-url'),
            datos_form = form.serializeArray();

        $.ajax({
            url: url_guardar_datos_societarios,
            type: 'POST',
            dataType: 'json',
            data: datos_form,
            beforeSend: function () {
                GuardarAJAXComunes.AntesDeEnviar();
            },
            complete: function () {
                $("#loading").fadeOut(500);
            },
            success: function (data) {
                var titular_id = data.titular_id;

                $('#titular_id').val(titular_id);

                $.alert({
                    title: 'Exito',
                    icon: 'fa fa-success',
                    type: 'green',
                    content: 'Datos de sociedad guardados.',
                    buttons: {
                        "Ok" : function () {
                        }
                    }
                });
            },
            error: function (data) {
                GuardarAJAXComunes.ProcesarErrorGuardado(form, data);
            }
        });
    },

    Eliminar: function (titular_id, prospecto_id, tit_accordion) {
        var url_eliminar_titular = $('button.eliminar-titular').data('eliminar-titular-url'),
            accordion_head = tit_accordion.parent().parent().prev("h3"),
            accordion_body = tit_accordion.parent().parent();

        if (titular_id) {
            $.ajax({
                url: url_eliminar_titular,
                type: 'POST',
                dataType: 'json',
                data: {
                    titular_id: titular_id,
                    prospecto_id: prospecto_id
                },
                complete: function () {
                    MensajePorCantidadTitulares();
                },
                success: function (data) {
                    accordion_head.remove();
                    accordion_body.remove();

                    $.notify('Titular eliminado.', 'success');
                },
                error: function (data) {
                    $.notify('Error. Intente más tarde.', 'error');
                }
            });
        } else {
            accordion_head.remove();
            accordion_body.remove();
        }

        MensajePorCantidadTitulares();
    }
};