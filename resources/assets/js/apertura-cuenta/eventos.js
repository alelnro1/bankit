$(document)
    .ready(function () {
        var int = setInterval(function () {

            if ($('.step-label:visible').length > 0) {

                // Verificamos si estamos en los titulares
                if ($.trim($('.step-label:visible').html()) == "Paso 1") {

                    // Verifico si tiene algun titular agregado. Si no tiene => Abro el modal de una
                    if ($('.ui-accordion-header').length <= 0) {
                        $("#modal-titular-documento").dialog({
                            modal: true
                        });
                    }
                }

                clearInterval(int);
            }
        });

        $('.fecha_nacimiento').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        if ($.trim($('#estado_civil_id option:selected').html()) == "Casado") {
            $('#datos-conyuge').show();
        }

        if ($.trim($('#pep').is(":checked"))) {
            $('#div-funcion_pep').show();
        }

        mostrarOcultarPEP();
    });

$('body')
    .on('click', '.nuevo-titular', function (e) {
        $('#numero-documento-a-consultar').val("");

        // Quito el mensaje que no hay titulares
        $('#sin-titulares').hide();

        $("#modal-titular-documento").dialog({
            modal: true
        });
    })

    .on('click', '.funciones-pep', function (e) {
        $("#modal-titular-funciones-pep").dialog({
            modal: true,
            width:'1000px'
        });
    })

    .on('click', '.guardar-titular', function (e) {
        e.preventDefault();

        var elem = $(this),
            form_titular = elem.closest('form');

        Titulares.Guardar(form_titular);
    })

    .on('change select', '#estado_civil_id', function (e) {
        var estado_civil = $.trim($('#estado_civil_id option:selected').html());

        if (estado_civil == "Casado") {
            $('#datos-conyuge').show();
        } else {
            $('#datos-conyuge').hide();
        }
    })

    .on('click', '#pep', function (e) {
        mostrarOcultarPEP();
    })

    .on('click', '.eliminar-titular', function (e) {
        e.preventDefault();

        var tit_accordion = $(this),
            titular_id = tit_accordion.data('titular-id'),
            prospecto_id = $('.prospecto_id').val();

        $.confirm({
            title: 'Eliminar Titular',
            content: '¿Está seguro que desea eliminar el titular?',
            buttons: {
                eliminar: {
                    btnClass: 'btn-green',
                    action: function () {
                        Titulares.Eliminar(titular_id, prospecto_id, tit_accordion);
                    }
                },
                cancelar: {
                    btnClass: 'btn-red'
                }
            }
        });
    })

    .on('click', '.guardar-cuenta-bancaria', function (e) {
        e.preventDefault();

        var elem = $(this),
            form_titular = elem.closest('form');

        CuentasBancarias.Guardar(form_titular);

    }).on('click', '#btn-siguiente, #btn-anterior', function (e) {
    var step = $('.rms-current-step').attr('id');

    if (step != "datos-titulares") {
        $('span.agregar_persona').hide();
    } else {
        $('span.agregar_persona').show();
    }

}).on('click', '#finalizar-apertura-cuenta', function (e) {
    // Verificamos que el paso "listo" esté completo para mostrar la confirmacion
    // Que esté listo significa que tienen que estar checkeados los terminos de apertura de cuenta
    if ($('#listo').hasClass('completed-step')) {
        $.confirm({
            title: 'Finalizar Proceso',
            content: '¿Está seguro que desea finalizar el proceso de apertura de cuenta?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-green',
                    action: function () {
                        GuardarAJAXComunes.FinalizarProceso();
                    }
                },
                cancelar: {
                    btnClass: 'btn-red'
                }
            }
        });
    }

    // Esto viene del back
}).on('click', '#confirmar-apertura-cuenta', function () {
    var form_generacion_comitente = $('form#generacion-comitente'),
        datos_form = form_generacion_comitente.serializeArray();

    $.confirm({
        title: 'Confirmar Apertura de Cuenta',
        content: '¿Está seguro que desea confirmar el apertura de cuenta del comitente <strong> ' + datos_form[1].value + '</strong> con número de comitente <strong>' + datos_form[2].value + '-' + datos_form[3].value + '-' +  datos_form[4].value + '</strong>?',
        buttons: {
            confirmar: {
                btnClass: 'btn-green',
                action: function () {
                    GuardarAJAXComunes.GenerarComitente(datos_form);
                }
            },
            cancelar: {
                btnClass: 'btn-red'
            }
        }
    });
    /*if (confirm('¿Está seguro que desea confirmar la apertura de cuenta?')) {
        GuardarAJAXComunes.ConfirmarApertura();

    }*/
}).on('click', '#guardar-datos-societarios', function () {
    var form = $('#datos-societarios-form'),
        titular_id = $('#titular_id').val();

    DatosSocietarios.Guardar(form, titular_id);
})
    .on('click', '#buscar-cuenta-alias', function (e) {
        e.preventDefault();

        var dato = $(this).parent().parent().find('input').val();

        CuentasBancarias.BuscarCuenta(dato, 'alias');
    })
    .on('click', '#buscar-cuenta-cbu', function (e) {
        e.preventDefault();

        var dato = $(this).parent().parent().find('input').val();

        CuentasBancarias.BuscarCuenta(dato, 'cbu');
    })
    .on('click', '#consultar-documento', function () {

        // Deshabilitamos el boton para que no haga dos consultas a la vez
        $(this).prop('disabled', true);

        var tipo_documento = $('#tipo-documento-a-consultar').val(),
            numero_documento = $('#numero-documento-a-consultar').val(),
            consultar_documento_url = $(this).data('consultar-documento-url');

        Titulares.ConsultarPorDocumento(tipo_documento, numero_documento, consultar_documento_url);
    });
/*.on('click', '#generar-comitente', function (e) {
    e.preventDefault();

    var form_generacion_comitente = $('form#generacion-comitente'),
        datos_form = form_generacion_comitente.serializeArray();

    $.confirm({
        title: 'Confirme generar el comitente',
        content: '¿Está seguro que desea generar al comitente <strong> ' + datos_form[0].value + '</strong> con número de comitente <strong>' + datos_form[1].value + '</strong>?',
        buttons: {
            confirmar: {
                btnClass: 'btn-green',
                action: function () {
                    GuardarAJAXComunes.GenerarComitente(datos_form);
                }
            },
            cancelar: {
                btnClass: 'btn-red'
            }
        }
    });
});*/


var MensajePorCantidadTitulares = function () {
    if ($('#titulares > h3').length <= 0) {
        $('#sin-titulares').show();
    }
};

// Inicializamos el accordion por si hay titulares
$('#titulares').accordion({
    collapsible: true,
    autoHeight: false,
    heightStyle: "content"
});

$("#tabs").tabs();

$('#fecha_constitucion').datetimepicker({
    format: 'YYYY-MM-DD'
});

function mostrarOcultarPEP() {
    if ($('#pep').is(':checked')) {
        $('#div-funcion_pep').show();
    } else {
        $('#div-funcion_pep').hide();
    }
}

MensajePorCantidadTitulares();